import { IMemory } from "types"

declare const Memory: IMemory;

export class Names {

    public static getAuglName(): string {
        if (!Memory.names) Memory.names = {n: 0, s: 0, p: 0, q: 0};
        const n = Memory.names.n + 1;
        Memory.names.n = n;
        return n.toString();
    }

    public static getPlatoonName(): string {
        if (!Memory.names) Memory.names = {n: 0, s: 0, p: 0, q: 0};
        const p = Memory.names.p + 1;
        Memory.names.p = p;
        return "Platoon " + p.toString();
    }

    public static getSquadName(): string {
        if (!Memory.names) Memory.names = {n: 0, s: 0, p: 0, q: 0};
        const q = Memory.names.q + 1;
        Memory.names.q = q;
        return "Squad " + q.toString();
    }

    public static getNextSpawnName(): string {
        if (!Memory.names) Memory.names = {n: 0, s: 0, p: 0, q: 0};
        const names = ["Kingslanding", "Dragonstone", "Dorne", "Braavos", "Volantis", "Qarth", "Mereen", "Astapor", "Asshai", "Lannisport", "Winterfell",
            "Ellesmera", "Tronjheim", "Ilirea", "Carvahall", "Buragh", "Dalgon", "Galfni", "Orthiad", "Tarnag", "Aberon", "Aroughs", "Belatona", "Bullridge",
            "Ceunon", "Cithri", "Dauth", "Feinster", "Gil'ead", "Kirtan", "Kuasta", "Lithgow", "Luthvira"];
        const index = Memory.names.s || 0;
        Memory.names.s = index + 1;
        const name = names[index];
        if (!name) {
            console.log("Error: Out of spawn names!");
            return index+"";
        }
        return name;
    }

    public static getSignatureText(): string {
        return "🖤";
    }
}