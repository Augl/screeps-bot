import { IScreepsGame } from "screeps";

declare const Game: IScreepsGame;

/**
 * @deprecated Cache keeping often used values or function results in memory to reduce CPU cost.
 */
export class Cache {

    private static cache: {[key: string] : CacheEntry | undefined} = {};

    /**
     * Returns and caches the given game object.
     * @param id ID of the game object.
     * @param maxAge Maximum number of ticks since last fetch from game.
     * @returns The game object.
     */
    public static getGameObject<T>(id: string, maxAge = 5): T {
        let result = this.cache[id];
        if (!result || result.timestamp + maxAge < Game.time) {
            // no valid cache entry
            result = {
                value: Game.getObjectById(id),
                timestamp: Game.time
            };
            this.cache[id] = result;
        }
        return result.value as T;
    }

    /**
     * Returns the cached value with the given key or invokes the provided callback function and then stores the result in the cache.
     * @param key The key of the value.
     * @param maxAge The maximum amount of ticks since the value was put into the cache.
     * @param callback The function to call if there is no cached value.
     * @param thisObj The this object to provide to the callback function.
     * @param args The arguments.
     * @returns The desired value.
     */
    public static storeFunction<R>(key: string, maxAge: number, callback: () => R, thisObj: unknown): R {
        maxAge = maxAge || 10;
        let result = this.cache[key];        
        if (!result || result.timestamp + maxAge < Game.time) {
            // non-existing or expired cache result, invoke callback function and store result
            result = {
                value: callback.apply(thisObj),
                timestamp: Game.time
            };
            this.cache[key] = result;
        }
        return result.value as R;
    }

}

class CacheEntry {
    constructor(
        public timestamp: number,
        public value: unknown
    ) { }
}