export const Levels = [0, 300, 550, 800, 1300, 1800, 2300, 5600, 12900] as const;

export const Allies = ["laserbahia"];

export enum Job {
    HARVESTER = 1,
    UPGRADER = 2,
    BUILDER = 3,
    CARRIER = 4,
    DISTRIBUTER = 5,
    REPAIRMAN = 6,        
    BUILDER_DEFENSE = 7,
    EXPLORER = 8,
    RESERVER = 9,
    INTERCARRIER = 10,
    CLAIMER = 11,
    WARRIOR = 12,
    HEALER = 13,
    CHEMIST = 14,
}

export enum AuglAITask {
    HARVEST = 1,
    DEPOSIT_SOURCE = 2,
    DEPOSIT_CENTRE = 3,
    DEPOSIT_SPAWN = 4,
    BUILD_GENERAL = 5,
    BUILD_DEFENSE = 6,
    UPGRADE = 7,
    WITHDRAW_SOURCE = 8,
    WITHDRAW_CENTRE = 9,
    WITHDRAW_LINK_CENTRE = 10,
    WITHDRAW_ANY = 11,
    DISMANTLE = 12,
    REPAIR = 13,
    EXPLORE = 14,
    CLAIM = 15,
    RESERVE = 16,
    DEPOSIT_REMOTE = 17,
    WAIT = 18,
    DEPOSIT_ANY = 19,
    DEPOSIT_TERMINAL = 20,
    WITHDRAW_TERMINAL = 21,
    DEPOSIT_LAB = 22,
    WITHDRAW_LAB = 23,
    DEPOSIT_TOWER = 24,

    ASSEMBLE = 101,
    HEAL_SQUAD = 102,
    ATTACK_MELEE = 103,
    MARCH = 104,
    TEARDOWN = 105,
    FOLLOW = 106,
}

export enum BuildingAITask {
    LINK_TRANSFER = 1,
    TOWER_REPAIR = 2,
    TOWER_DEFEND = 3,
    TOWER_REPAIR_DEFENSE = 4,
    LAB_IDLE = 5,
    LAB_FILL = 6,
    LAB_REACT = 7,
    LAB_EMPTY = 8,
}

export enum WorldAITask {
    SPAWN_MILITARY = 1,
    SPAWN_CIVILIAN = 2,
    POPULATION = 3,
    BUILD = 4,
    CLASSIFY = 5,
    EXPLORE = 6,
    DEFENSE = 7,
    TRANSFER = 8,
    CHEMISTRY = 9,
}

export enum SquadAITask {
    POPULATION = 1
}

export enum PlatoonAITask {
    COORDINATE = 1
}

export enum MilitaryAction {
    SPAWN = 1,
    GATHER = 2,
    FIGHT = 3,
    ELIMINATED = 4,
}

export enum WorldType {
    SECTOR = 1,
    COLONY = 2,
    ALLIED = 3,
    ENEMY = 4,
    WARZONE = 5,
    EMPTY = 6,
    UNKNOWN = 9
}

export enum PlatoonType {
    TINY_DEFENSE = 1,
    SMALL_DEFENSE = 2
}

export enum SquadType {
    TINY_DEFENSE = 1,
    SMALL_DEFENSE = 2
}

export enum Result {
    OK = 0,
    ERR_NOT_OWNER = -1,
    ERR_NO_PATH = -2,
    ERR_NAME_EXISTS = -3,
    ERR_BUSY = -4,
    ERR_NOT_FOUND = -5,
    ERR_NOT_ENOUGH_ENERGY = -6,
    ERR_NOT_ENOUGH_RESOURCES = -6,
    ERR_INVALID_TARGET = -7,
    ERR_FULL = -8,
    ERR_NOT_IN_RANGE = -9,
    ERR_INVALID_ARGS = -10,
    ERR_TIRED = -11,
    ERR_NO_BODYPART = -12,
    ERR_NOT_ENOUGH_EXTENSIONS = -6,
    ERR_RCL_NOT_ENOUGH = -14,
    ERR_GCL_NOT_ENOUGH = -15,
    ERR_UNKNOWN = -99
}

export enum ResourceType {
    ENERGY = "energy",
    HYDROGEN = "H",
    OXYGEN = "O",    
    UTRIUM = "U",
    LEMERGIUM = "L",
    KEANIUM = "K",
    ZYNTHIUM = "Z",
    CATALYST = "X",
    HYDROXIDE = "OH",
    ZYNTHIUM_KEANITE = "ZK",
    UTRIUM_LEMERGITE = "UL",
    GHODIUM = "G",
    UTRIUM_HYDRIDE = "UH",
    UTRIUM_OXIDE = "UO",
    KEANIUM_HYDRIDE = "KH",
    KEANIUM_OXIDE = "KO",
    LEMERGIUM_HYDRIDE = "LH",
    LEMERGIUM_OXIDE = "LO",
    ZYNTHIUM_HYDRIDE = "ZH",
    ZYNTHIUM_OXIDE = "ZO",
    GHODIUM_HYDRIDE = "GH",
    GHODIUM_OXIDE = "GO",
    UTRIUM_ACID = "UH2O",
    UTRIUM_ALKALIDE = "UHO2",
    KEANIUM_ACID = "KH2O",
    KEANIUM_ALKALIDE = "KHO2",
    LEMERGIUM_ACID = "LH2O",
    LEMERGIUM_ALKALIDE = "LHO2",
    ZYNTHIUM_ACID = "ZH2O",
    ZYNTHIUM_ALKALIDE = "ZHO2",
    GHODIUM_ACID = "GH2O",
    GHODIUM_ALKALIDE = "GHO2",
    CATALYZED_UTRIUM_ACID = "XUH2O",
    CATALYZED_UTRIUM_ALKALIDE = "XUHO2",
    CATALYZED_KEANIUM_ACID = "XKH2O",
    CATALYZED_KEANIUM_ALKALIDE = "XKHO2",
    CATALYZED_LEMERGIUM_ACID = "XLH2O",
    CATALYZED_LEMERGIUM_ALKALIDE = "XLHO2",
    CATALYZED_ZYNTHIUM_ACID = "XZH2O",
    CATALYZED_ZYNTHIUM_ALKALIDE = "XZHO2",
    CATALYZED_GHODIUM_ACID = "XGH2O",
    CATALYZED_GHODIUM_ALKALIDE = "XGHO2",
}

export enum FindType {
    FIND_SOURCES = 105,
    FIND_STRUCTURES = 107,
    FIND_HOSTILE_CREEPS = 103,
    FIND_HOSTILE_STRUCTURES = 109,
    FIND_CREEPS = 101,
    FIND_MINERALS = 116,
}

export enum LookType {
    LOOK_TERRAIN = "terrain",
    LOOK_SOURCES = "source",
    LOOK_STRUCTURES = "structure",
}

export enum TerrainType {
    SWAMP = "swamp",
    PLAIN = "plain",
    WALL = "wall"
}

export enum StructureType {
    SPAWN = "spawn",
    CONTAINER = "container",
    WALL = "constructedWall",
    RAMPART = "rampart",
    ROAD = "road",
    EXTENSION = "extension",
    TOWER = "tower",
    STORAGE = "storage",
    LINK = "link",
    INVADER_CORE = "invaderCore",
    LAB = "lab",
    TERMINAL = "terminal",    
    EXTRACTOR = "extractor",
    NUKER = "nuker",
}

export enum BodyPart {
    MOVE = "move",
    WORK = "work",
    CARRY = "carry",
    ATTACK = "attack",
    RANGED_ATTACK = "ranged_attack",
    TOUGH = "tough",
    HEAL = "heal",
    CLAIM = "claim"
}

export enum Direction {
    TOP = 1,
    TOP_RIGHT = 2,
    RIGHT = 3,
    BOTTOM_RIGHT = 4,
    BOTTOM = 5,
    BOTTOM_LEFT = 6,
    LEFT = 7,
    TOP_LEFT = 8,
}

export enum OrderType {
    BUY = "buy",
    SELL = "sell",
}