import { IScreepsGame } from "screeps";
import { IMemory } from "types";

declare const Memory: IMemory;
declare const Game: IScreepsGame;

export class Performance {

    public static measure<T>(code: () => T, name: string): T {
        const start = Game.cpu.getUsed();
        const result = code();
        const used = Game.cpu.getUsed() - start;
        if (!Memory.performance) Memory.performance = {};
        if (!Memory.performance[name]) Memory.performance[name] = {n: 0, startTime: Game.time, sum: 0};
        Memory.performance[name].n++;
        Memory.performance[name].sum += used;
        return result;
    }

    public static print(): void {
        if (!Memory.performance) return;
        for (const name in Memory.performance) {            
            const timeDelta = Game.time - Memory.performance[name].startTime;
            console.log("Performance of ", name, ": ", Memory.performance[name].sum/Memory.performance[name].n, " per run, ", Memory.performance[name].sum/timeDelta, " per tick");
        }
    }
}