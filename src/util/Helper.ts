import { IPosition } from "types";
import { BodyPart, TerrainType } from "./Constants";

export class Helper {

    /**
     * Calculates the distance between two points.
     * @param a One point.
     * @param b The second point.
     * @returns The distance.
     */
    public static distance(a: IPosition, b: IPosition): number {
        if (a.roomName != b.roomName) return 50;
        return Math.max(Math.abs(a.x - b.x), Math.abs(a.y - b.y));
    }

    /**
     * Returns the element in the array for which predicate is true and which has the lowest weight.
     * @param array The array of options.
     * @param predicate The predicate that has to be true.
     * @param weightFunc The function returning the weight.
     * @returns The found element or undefined.
     */
    public static getMinWith<T>(array: T[], predicate: (arg: T) => boolean, weightFunc: (arg: T) => number): T | undefined {
        let currentMin;
        let currentMinWeight = Number.POSITIVE_INFINITY;
        for (let i = 0; i < array.length; i++) {
            if (predicate(array[i])) {
                const weight = weightFunc(array[i]);
                if (weight < currentMinWeight) {
                    currentMinWeight = weight
                    currentMin = array[i];
                }
            }
        }
        return currentMin;
    }

    /**
     * Finds the center of the largest free area in the room.
     * @param terrainCallback The callback function to get the terrain at a position.
     * @returns The center of the largest area.
     */
    public static findLargestArea(terrainCallback: (x: number, y: number) => TerrainType): { x: number, y: number } {
        // find the largest area using a dynamic programming approach.
        let maxX = 0;
        let maxY = 0;
        let maxSize = 0;
        const dp: number[][] = [];
        for (let y = 3; y < 47; y++) {
            for (let x = 3; x < 47; x++) {
                if (!dp[x - 1]) dp[x - 1] = [];
                if (!dp[x]) dp[x] = [];
                let result = Math.min((dp[x][y - 1] || 0), Math.min((dp[x - 1][y] || 0), (dp[x - 1][y - 1] || 0)));
                if (terrainCallback(x, y) != TerrainType.WALL) {
                    result++;
                }
                else {
                    result = 0;
                }
                dp[x][y] = result;
                if (result > maxSize) {
                    maxX = x;
                    maxY = y;
                    maxSize = result;
                }
            }
        }
        const finalX = maxX - (Math.floor(maxSize / 2));
        const finalY = maxY - (Math.floor(maxSize / 2));
        return { x: finalX, y: finalY };
    }

    /**
     * Returns the requested body.
     * @param body Map of how many body parts of each type to add.
     * @returns Array of body parts.
     */
    public static getBody(body: Partial<Record<BodyPart, number>>): BodyPart[] {
        const result: BodyPart[] = [];
        const numberOfTough = body.tough ?? 0;
        body.tough = 0;
        // find type with most parts
        let maxType: BodyPart | undefined = undefined;
        let maxNumber = 0;
        for (const type in body) {
            const toAdd = body[type as BodyPart] ?? 0;
            if (toAdd > maxNumber) {
                maxNumber = toAdd;
                maxType = type as BodyPart;
            }
        }
        if (!maxType) return [];
        // create starting array with those parts
        for (let i = 0; i < maxNumber; i++) {
            result.push(maxType);
        }
        body[maxType] = 0;
        // iterate over other body parts
        for (const type in body) {
            const toAdd = body[type as BodyPart];
            if (!toAdd) continue;
            // calculate ratio to later infer indeces to add this type
            const ratio = result.length / toAdd;
            let sum = 0;
            for (let i = 0; i < toAdd; i++) {
                sum += ratio;
                result.splice(Math.floor(sum), 0, type as BodyPart);
                sum += 1;
            }
        }
        // add tough parts to the beginning
        const toughs = [];
        for (let i = 0; i < numberOfTough; i++) {
            toughs.push(BodyPart.TOUGH);
        }
        return toughs.concat(result);
    }

    /**
     * Returns the spiral position at given index around given position.
     * @param x The centre x coordinate.
     * @param y The centre y coordinate.
     * @param i The index of the desired position.
     * @returns The desired position.
     */
    public static getSpiralPosition(x: number, y: number, i: number): { x: number, y: number } {
        // find current ring
        let side = 1;
        let square = 1;
        let prevSquare = 0;
        while (i >= square) {
            side += 2;
            prevSquare = square;
            square = side * side;
        }
        // calculate current side
        let currentIndex = i - prevSquare;
        const halfSide = Math.floor(side / 2);
        if (currentIndex < side - 1) {
            // top
            y -= halfSide;
            x += currentIndex - halfSide;
        }
        else if (currentIndex < side * 2 - 2) {
            // right
            currentIndex -= side - 1;
            x += halfSide;
            y += currentIndex - halfSide;
        }
        else if (currentIndex < side * 3 - 3) {
            // bottom
            currentIndex -= side * 2 - 2;        
            y += halfSide;
            x -= currentIndex - halfSide;
        }
        else {
            // left
            currentIndex -= side * 3 - 3;
            x -= halfSide;
            y -= currentIndex - halfSide;
        }
        return { x: x, y: y };
    }
}