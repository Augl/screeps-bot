import { BuildingBase } from "class/buildings/Base";
import { ConstructionSite } from "class/buildings/ConstructionSite";
import { Empire } from "class/Empire";
import { PlatoonType, StructureType } from "./Constants";

/**
 * The debugger offers a set of functions that can be called by the console to check the current state of the system.
 */
export class Debugger {

    private empire: Empire;

    constructor(empire: Empire) {
        this.empire = empire;
    }

    /**
     * Returns deconstruction targets.
     * @param roomName The name of the room to look at.
     * @returns Array of buildings.
     */
    public getDeconstructionTargets(roomName: string): BuildingBase[] {
        return this.empire.getWorld(roomName)?.getBuildings()?.getDeconstructionTargets() ?? [];
    }

    /**
     * Returns construction sites.
     * @param roomName The name of the room to look at.
     * @param civilian If civilian buildings should be considered.
     * @param military If defense buildings should be considered.
     * @returns Array of construction sites.
     */
    public getConstructionSites(roomName: string, civilian: boolean, military: boolean): ConstructionSite[] {
        let result = this.empire.getWorld(roomName)?.getBuildings()?.getConstructionSites() ?? [];
        if (!civilian) {
            result = result.filter(cs => cs.isDefenseBuilding());
        }
        if (!military) {
            result = result.filter(cs => !cs.isDefenseBuilding());
        }
        return result;
    }

    /**
     * Adds a deconstruction target.
     * @param roomName The name of the room to look at.
     * @param ID The ID of the building to deconstruct.
     */
    public addDeconstructionTarget(roomName: string, ID: string): string {
        const buildings = this.empire.getWorld(roomName)?.getBuildings()
        if (!buildings) {
            return "Room has no building registry";
        }
        let target: BuildingBase | undefined;
        target = buildings.getPeripheryContainers().find(b => b.getID() == ID);
        if (!target) target = buildings.getPeripheryLinks().find(b => b.getID() == ID);
        if (!target) target = [buildings.getCentreLink()].find(b => b && b.getID() == ID);
        if (!target) target = [buildings.getControllerLink()].find(b => b && b.getID() == ID);
        if (!target) target = buildings.getCentreStores().find(b => b.getID() == ID);
        if (!target) target = buildings.getSpawns().find(b => b.getID() == ID);
        if (!target) target = buildings.getExtensions().find(b => b.getID() == ID);
        if (!target) target = buildings.getTowers().find(b => b.getID() == ID);
        if (!target) target = buildings.getWalls().find(b => b.getID() == ID);
        if (!target) target = buildings.getRamparts().find(b => b.getID() == ID);
        if (!target) target = buildings.getRoads().find(b => b.getID() == ID);
        if (!target) target = [buildings.getStorage()].find(b => b && b.getID() == ID);
        if (!target) target = [buildings.getTerminal()].find(b => b && b.getID() == ID);
        if (!target) target = buildings.getLabs().find(b => b.getID() == ID);
        if (!target) target = [buildings.getExtractor()].find(b => b && b.getID() == ID);
        if (!target) target = [buildings.getNuker()].find(b => b && b.getID() == ID);
        if (target) {
            buildings.addDeconstructionTarget(target);
            return "Successfully added deconstruction target";
        }
        return "No target with given ID found";
    }

    /**
     * Adds a construction site.
     * @param roomName The name of the room to look at.
     * @param x The x coordinate of the desired building.
     * @param y The y coordinate of the desired building.
     * @param type The type of the desired building.
     * @param name The name of the desired building.
     */
    public addConstructionSite(roomName: string, x: number, y: number, type: StructureType, name?: string): string {
        const world = this.empire.getWorld(roomName);
        if (!world) {
            return "Room has no building registry";
        }
        world?.createConstructionSite(x, y, type, name);
        return "Successfully added construction site";
    }

    /**
     * Relocates the platoon with the given name to the given room.
     * @param platoonName The platoon to relocate.
     * @param targetRoomName The new target room.
     */
    public relocatePlatoon(platoonName: string, targetRoomName: string): string {
        const platoon = this.empire.getPlatoon(platoonName);
        if (!platoon) {
            return "No platoon found with the name " + platoonName;
        }
        const target = this.empire.getWorld(targetRoomName);
        if (!target) {
            return "Target room does not exist";
        }
        this.empire.relocatePlatoon(platoon, target);
        return "Platoon successfully relocated";
    }

    /**
     * Requests a new platoon.
     * @param targetName The target room.
     * @param type The type of platoon.
     */
    public requestPlatoon(targetName: string, type: PlatoonType): string {
        const target = this.empire.getWorld(targetName);
        if (!target) {
            return "Target room does not exist";
        }
        this.empire.requestPlatoon(target, type);
        return "Platoon successfully requested";
    }
}