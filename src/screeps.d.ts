/* eslint-disable @typescript-eslint/no-misused-new */
/* eslint-disable @typescript-eslint/no-empty-interface */
import { IAuglMemory, IPosition } from "types";
import { BodyPart, Direction, FindType, LookType, OrderType, ResourceType, Result, StructureType } from "util/Constants";

interface IScreepsGame {
    time: number;
    rooms: {[roomName: string]: IScreepsRoom | undefined};
    creeps: {[name: string]: IScreepsCreep | undefined};
    structures: {[ID: string]: IScreepsStructure | undefined};
    constructionSites: {[ID: string]: IScreepsConstructionSite | undefined};
    cpu: {
        bucket: number, getUsed(): number,
        limit: number,
        generatePixel(): void, 
        getHeapStatistics(): {used_heap_size: number, heap_size_limit: number}
    };
    map: {
        describeExits(roomName: string): {"1": string | undefined, "3": string | undefined, "5": string | undefined, "7": string | undefined}
    };
    market: IScreepsMarket;
    getObjectById<T>(id: string): T | undefined;
    notify(message: string): void;
}

interface IScreepsOrder {
    id: string;
    created: number;
    type: OrderType;
    resourceType: ResourceType;
    roomName: string;
    amount: number;
    price: number;
}

interface IScreepsMarket {
    getAllOrders(filter: {type?: OrderType, resourceType?: ResourceType}): IScreepsOrder[];
    deal(orderId: string, amount: number, targetRoom?: string): Result;
}

interface IScreepsRoomVisualStyle {

}

interface IScreepsRoomVisual {
    poly(points: {x: number, y: number}[], style?: IScreepsRoomVisualStyle): IScreepsRoomVisual;
    text(text: string, x: number, y: number): IScreepsRoomVisual;
}

interface IScreepsPathStep {
    x: number; 
    y: number;
    direction: Direction;
}

interface IScreepsRoom {
    energyAvailable: number;
    energyCapacityAvailable: number;
    visual: IScreepsRoomVisual;
    controller: IScreepsController;
    name: string;
    find<T>(type: FindType): T[];
    findPath(from: IPosition, to: IPosition, opts: {
        ignoreCreeps?: boolean;
        ignoreDestructibleStructures?: boolean;
        ignoreRoads?: boolean;
        costCallback?: (roomName: string, matrix: IScreepsCostMatrix) => IScreepsCostMatrix;
        maxOps?: number;
        range?: number;
        plainCost?: number;
        swampCost?: number;
        serialize?: boolean;
        maxRooms?: number;
    }): IScreepsPathStep[];
    lookForAt<T>(type: LookType, x: number, y: number): T[];
    lookForAtArea<T>(type: LookType, top: number, left: number, bottom: number, right: number, asArray: boolean): T[];
    createConstructionSite(x: number, y: number, structureType: StructureType, name?: string): Result;
}

interface IScreepsRoomPosition {
    new (x: number, y: number, roomName: string): IPosition;
}

interface IScreepsRoomObject {
    pos: IPosition;
    room?: IScreepsRoom;
}

interface IScreepsSource extends IScreepsRoomObject {
    energy: number;
    energyCapacity: number;
    id: string;
}

interface IScreepsMineral extends IScreepsRoomObject {
    id: string;
    mineralType: ResourceType;
    ticksToRegeneration: number;
}

interface IScreepsStructure extends IScreepsRoomObject {
    structureType: StructureType;
    id: string;
    hits: number;
    hitsMax: number;
}


interface IScreepsStore extends IScreepsStructure {
    store: {
        getCapacity(resource?: ResourceType): number;
        getFreeCapacity(resource?: ResourceType): number;
        getUsedCapacity(resource?: ResourceType): number;
    } & { [P in ResourceType]: number }
}

interface IScreepsSpawn extends IScreepsStore {
    spawning: {remainingTime: number};
    hits: number;
    hitsMax: number;
    spawnCreep(body: BodyPart[], name: string, opts?: {memory: IAuglMemory}): Result;
}

interface IScreepsController extends IScreepsStructure {
    id: string;
    level: number;
    owner: {username: string};
    my: boolean;
    sign?: {username: string};
    reservation: {username: string, ticksToEnd: number};
}

interface IScreepsContainer extends IScreepsStore {
    ticksToDecay: number;
}

interface IScreepsExtension extends IScreepsStore {
}

interface IScreepsRoad extends IScreepsStructure {
    ticksToDecay: number;
}

interface IScreepsTower extends IScreepsStore {
    attack(target: IScreepsCreep): Result;
    repair(target: IScreepsStructure): Result;
}

interface IScreepsLink extends IScreepsStore {
    transferEnergy(target: IScreepsLink): Result;
}

interface IScreepsWall extends IScreepsStructure {

}

interface IScreepsRampart extends IScreepsStructure {

}

interface IScreepsStorage extends IScreepsStore {
    
}

interface IScreepsInvaderCore extends IScreepsStructure {
    
}

interface IScreepsTerminal extends IScreepsStore {
    send(resourceType: ResourceType, amount: number, destination: string, description?: string): Result;
}

interface IScreepsLab extends IScreepsStore {
    cooldown: number;
    runReaction(lab1: IScreepsLab, lab2: IScreepsLab): Result;
}

interface IScreepsExtractor extends IScreepsStructure {
    
}

interface IScreepsNuker extends IScreepsStore {
    
}

interface IScreepsConstructionSite extends IScreepsRoomObject {
    structureType: StructureType;
}

interface IScreepsCostMatrix {
    new(): IScreepsCostMatrix;
    set(x: number, y: number, cost: number): void;
    clone(): IScreepsCostMatrix;
}

interface IScreepsPathFinderOpts {
    roomCallback?: (roomName: string) => IScreepsCostMatrix | false | undefined;
    plainCost?: number;
    swampCost?: number;
    flee?: boolean;
    maxOps?: number;
    maxRooms?: number;
    maxCost?: number;
}

interface IScreepsPathFinderResult {
    path: IPosition[];
    ops: number;
    cost: number;
    incomplete: boolean;
}

interface IScreepsPathFinder {
    CostMatrix: IScreepsCostMatrix;
    search(origin: IPosition, goal: {pos: IPosition, range: number}, opts?: IScreepsPathFinderOpts): IScreepsPathFinderResult;
}

interface IScreepsMoveToOps {
    ignoreCreeps?: boolean;
    reusePath?: number;
    visualizePathStyle?: {opacity?: number};
    range?: number;
    maxOps?: number;
    costCallback?: (roomName: string, matrix: IScreepsCostMatrix) => void;
}

interface IScreepsCreep extends IScreepsRoomObject, IScreepsStore {
    spawning: boolean;
    memory: IAuglMemory;
    ticksToLive: number;
    body: {type: BodyPart}[];
    fatigue: number;
    owner: {username: string};
    attack(target: IScreepsCreep | IScreepsStructure): Result;
    attackController(target: IScreepsController): Result;
    build(target: IScreepsConstructionSite): Result;
    claimController(target: IScreepsController): Result;
    dismantle(target: IScreepsStructure): Result;
    getActiveBodyparts(type: BodyPart): number;
    harvest(target: IScreepsRoomObject): Result;
    heal(target: IScreepsCreep): Result;
    move(direction: Direction): Result;
    moveTo(target: IPosition, opts: IScreepsMoveToOps): Result;
    repair(target: IScreepsStructure): Result;
    reserveController(target: IScreepsController): Result;
    signController(target: IScreepsController, text: string): Result;
    transfer(target: IScreepsStructure, resourceType: ResourceType, amount?: number): Result;
    upgradeController(target: IScreepsController): Result;
    withdraw(target: IScreepsStructure, resourceType: ResourceType, amount?: number): Result;
}