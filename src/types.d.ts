import { Direction, Job, PlatoonType, SquadType, WorldType } from "util/Constants";

interface IMemory {
    rooms?: {[roomName: string]: ISectorMemory | undefined};
    creeps?: {[name: string]: IAuglMemory | undefined};
    buildings?: {[ID: string]: IBuildingMemory | undefined};
    platoons?: {[name: string]: IPlatoonMemory | undefined};
    squads?: {[name: string]: ISquadMemory | undefined};
    names?: {n: number, s: number, p: number, q: number};
    debugVisuals?: boolean;
    performance?: {
        [key: string]: {
            startTime: number;
            n: number;
            sum: number;
        }
    };
}

interface ISpawnQueue {
    memory: IAuglMemory;
    level: number;
    home: string;
}

interface ISectorMemory {
    sq?: {[key: string]: ISpawnQueue | undefined};
    spawnCooldown?: number;
    bq?: {l: number};
    neighbours?: string[];
    type?: WorldType;
    inactive?: boolean;
    rq?: {[key: string]: {x: number, y: number, type: string} | undefined}
}

interface IAuglMemory {
    sourceId?: string;
    home?: string;
    job?: Job;
    isMoving?: boolean;
    mc?: {x: number, y: number, c: number};
    targets?: string[];
    task?: number;
}

interface IBuildingMemory {
    deconstructing?: boolean;
}

interface IPlatoonMemory {
    type?: PlatoonType;
    targetRoom?: string;
}

interface ISquadMemory {
    type?: SquadType;
    platoon?: string;
}

interface IPosition {
    x: number;
    y: number;
    roomName: string;
    
    getDirectionTo(x: number, y: number): Direction;
}

interface IBuildConfig {
    l: number; // Current build level
    s?: number; // Current step within the current level
    ss?: number; // Current substep within the current step
    isBuilding?: boolean; // If a wall was built in the previous tick, only used when building walls
    cs?: ICentreState; // Data to know where to continue building in the centre
    pos?: {x: number, y: number}; // Position of some specific building to create
}

interface ICentreState {
    i: number; // current index in the spiral around the centre
    x: number; // X coordinate of centre
    y: number; // Y coordinate of centre
}