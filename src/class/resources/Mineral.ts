import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsMineral } from "screeps";
import { IPosition } from "types";
import { ResourceType } from "util/Constants";

declare const Game: IScreepsGame;

export class MineralSource {

    public ID: string;

    private underlying: WorldObject<IScreepsMineral | undefined>;

    constructor(ID: string) {
        this.ID = ID;
        this.underlying = new WorldObject(() => Game.getObjectById(this.ID));
    }

    /**
     * @returns Returns the objects position.
     */
    public getPosition(): IPosition | undefined {
        return this.underlying.value(true)?.pos;
    }

    /**
     * @returns The underlying screeps source object.
     */
    public getUnderlying(): IScreepsMineral | undefined {
        return this.underlying.value(true);
    }

    /**
     * @returns The resource contained in the mineral deposit.
     */
    public getResourceType(): ResourceType | undefined {
        return this.underlying.value(true)?.mineralType;
    }

    /**
     * @returns The number of ticks until the mineral is refilled.
     */
    public getTicksToRegeneration(): number | undefined {
        return this.underlying.value(false)?.ticksToRegeneration;
    }
}