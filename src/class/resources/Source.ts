import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsSource } from "screeps";
import { IPosition } from "types";

declare const Game: IScreepsGame;

export class EnergySource {

    public workspaces: IPosition[];

    public sourceID: string;

    private underlying: WorldObject<IScreepsSource | undefined>;

    constructor(workspaces: IPosition[], sourceID: string) {
        this.workspaces = workspaces;
        this.sourceID = sourceID;
        this.underlying = new WorldObject(() => Game.getObjectById(this.sourceID));
    }

    /**
     * @returns Returns the objects position.
     */
    public getPosition(): IPosition | undefined {
        return this.underlying.value(true)?.pos;
    }

    /**
     * @returns The underlying screeps source object.
     */
    public getUnderlying(): IScreepsSource | undefined {
        return this.underlying.value(true);
    }
}