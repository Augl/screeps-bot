import { WorldObject } from "class/WorldObject";
import { IScreepsController, IScreepsGame } from "screeps";
import { BuildingBase } from "./Base";

declare const Game: IScreepsGame;

export class Controller extends BuildingBase {

    constructor(ID: string) {        
        const underlying = new WorldObject<IScreepsController | undefined>(() => Game.getObjectById(ID));
        super(underlying, ID);
    }

    public getLevel(): number {
        const controller = this.underlying.value(true);
        if (!controller) return 0;
        return (controller as IScreepsController).level;
    }

    public getOwner(): string {
        const controller = this.underlying.value(true);
        if (!controller) return "";
        return (controller as IScreepsController).owner?.username ?? "";
    }

    public isMine(): boolean {
        const controller = this.underlying.value(true);
        if (!controller) return false;
        return (controller as IScreepsController).my;
    }

    public getSign(): {username: string} | undefined {
        const controller = this.underlying.value(true);
        if (!controller) return;
        return (controller as IScreepsController).sign;        
    }

    public getReservation(): {username: string, ticksToEnd: number} | undefined {
        const controller = this.underlying.value(true);
        if (!controller) return;
        return (controller as IScreepsController).reservation;
    }
}