import { WorldBase } from "class/world/Base";
import { Sector } from "class/world/Sector";
import { IScreepsGame, IScreepsInvaderCore, IScreepsPathFinder, IScreepsStructure } from "screeps";
import { IMemory } from "types";
import { FindType, LookType, StructureType } from "util/Constants";
import { Helper } from "util/Helper";
import { BuildingBase } from "./Base";
import { ConstructionSite } from "./ConstructionSite";
import { Container } from "./Container";
import { Extension } from "./Extension";
import { Extractor } from "./Extractor";
import { InvaderCore } from "./InvaderCore";
import { Lab } from "./Lab";
import { LabComplex } from "./LabComplex";
import { Link } from "./Link";
import { Nuker } from "./Nuker";
import { Rampart } from "./Rampart";
import { Road } from "./Road";
import { Spawner } from "./Spawn";
import { Storage } from "./Storage";
import { Terminal } from "./Terminal";
import { Tower } from "./Tower";
import { Wall } from "./Wall";

declare const Game: IScreepsGame;
declare const Memory: IMemory;
declare const PathFinder: IScreepsPathFinder;

/**
 * Keeps track of all buildings in a room.
 */
export class BuildingRegistry {

    private peripheryContainers?: Container[];

    private centreContainers?: Container[];

    private peripheryLinks?: Link[];

    private centreLink: Link | undefined;

    private controllerLink: Link | undefined;

    private didInitStorage = false;

    private storage: Storage | undefined;

    private spawns?: Spawner[];

    private extensions?: Extension[];

    private towers?: Tower[];

    private roads?: Road[];

    private ramparts?: Rampart[];

    private walls?: Wall[];

    private damaged: BuildingBase[] = [];

    private damagedUpdateTime = 0;

    private damagedFullCheckTime = 0;

    private toDeconstruct: BuildingBase[] = [];

    private toDeconstructUpdateTime = 0;

    private constructionSiteAdded: number | undefined = Game.time;

    private completedUpdateTime = 0;

    private constructionSites?: ConstructionSite[];

    private invaderCoreUpdateTime = 0;

    private invaderCore?: InvaderCore;

    private terminal?: Terminal;

    private didInitTerminal = false;

    private labs?: Lab[];

    private labComplexes?: LabComplex[];

    private extractor?: Extractor;

    private didInitExtractor = false;

    private nuker?: Nuker;

    private didInitNuker = false;

    private sector: WorldBase;

    public costMatrix = new PathFinder.CostMatrix();

    constructor(sector: WorldBase) {
        this.sector = sector;
    }

    /**
     * Initializes the given raw screeps structure.
     * @param structure The structure to initialize.
     */
     private initializeBuilding(structure: IScreepsStructure) {
        let building: BuildingBase | undefined;
        if (!structure || !structure.structureType) {
            return;
        }
        // put in cost matrix
        if (structure.structureType == StructureType.ROAD) {
            this.costMatrix.set(structure.pos.x, structure.pos.y, 1);
        }
        else if (structure.structureType != StructureType.RAMPART && structure.structureType != StructureType.CONTAINER) {
            this.costMatrix.set(structure.pos.x, structure.pos.y, 255);
        }
        // actual initialization
        if (structure.structureType == StructureType.CONTAINER) {
            const containerPos = structure.pos;
            // check if container is located close to a source
            const sources = this.sector.getSources();
            const mineralPos = this.sector.getMineral()?.getPosition();
            let closeToSource = false;
            for (const source of sources) {
                const sourcePos = source.getPosition();
                if (sourcePos && Helper.distance(sourcePos, containerPos) < 3) {
                    closeToSource = true;
                }
            }
            if (closeToSource || (mineralPos && Helper.distance(mineralPos, containerPos) < 3)) {
                if (!this.peripheryContainers) this.peripheryContainers = [];
                building = new Container(structure.id)
                this.peripheryContainers.push(building as Container);
            }
            else {
                if (!this.centreContainers) this.centreContainers = [];
                building = new Container(structure.id);
                this.centreContainers.push(building as Container);
            }
        }
        else if (structure.structureType == StructureType.ROAD) {
            if (!this.roads) this.roads = [];
            building = new Road(structure.id)
            this.roads.push(building as Road);
        }
        else if (structure.structureType == StructureType.SPAWN) {
            if (!this.spawns) this.spawns = [];
            building = new Spawner(structure.id);
            this.spawns.push(building as Spawner);
        }
        else if (structure.structureType == StructureType.EXTENSION) {
            if (!this.extensions) this.extensions = [];
            building = new Extension(structure.id);
            this.extensions.push(building as Extension);
        }
        else if (structure.structureType == StructureType.TOWER) {
            if (!this.towers) this.towers = [];
            building = new Tower(structure.id, this.sector);
            this.towers.push(building as Tower);
        }
        else if (structure.structureType == StructureType.LINK) {
            // check if container is located close to a source
            const sources = this.sector.getSources();
            let closeToSource = false;
            for (const source of sources) {
                const sourcePos = source.getPosition();
                const containerPos = structure.pos;
                if (sourcePos && Helper.distance(sourcePos, containerPos) < 3) {
                    closeToSource = true;
                }
            }
            const cPos = this.sector.getController()?.getPosition();
            if (closeToSource) {
                if (!this.peripheryLinks) this.peripheryLinks = [];
                building = new Link(structure.id, this.sector, true);
                this.peripheryLinks.push(building as Link);
            }
            else if (cPos && Helper.distance(cPos, structure.pos) < 3) {
                building = new Link(structure.id, this.sector, false);
                this.controllerLink = (building as Link);
            }
            else {
                building = new Link(structure.id, this.sector, false);
                this.centreLink = (building as Link);
            }
        }
        else if (structure.structureType == StructureType.WALL) {
            if (!this.walls) this.walls = [];
            if (structure.hits) {
                building = new Wall(structure.id, this.sector);
                this.walls.push(building as Wall);
            }
        }
        else if (structure.structureType == StructureType.RAMPART) {
            if (!this.ramparts) this.ramparts = [];
            building = new Rampart(structure.id, this.sector);
            this.ramparts.push(building as Rampart);
        }
        else if (structure.structureType == StructureType.STORAGE) {
            this.storage = new Storage(structure.id);
        }
        else if (structure.structureType == StructureType.TERMINAL) {
            this.terminal = new Terminal(structure.id);
        }
        else if (structure.structureType == StructureType.LAB) {
            if (!this.labs) this.labs = [];
            building = new Lab(structure.id);
            this.labs.push(building as Lab);
        }
        else if (structure.structureType == StructureType.EXTRACTOR) {
            this.extractor = new Extractor(structure.id);
        }
        else if (structure.structureType == StructureType.NUKER) {
            this.nuker = new Nuker(structure.id);
        }
        if (building?.shouldBeDeconstructed()) {
            this.toDeconstruct.push(building);
        }
    }

    /**
     * Initializes all buildings in the sector.
     */
    private initiallizeAll(): void {
        if (!this.centreContainers) {
            this.initializeContainers();
        }
        if (!this.didInitStorage) {            
            this.initializeStorage();
        }
        if (!this.peripheryLinks) {
            this.initializeLinks();
        }
        if (!this.roads) {
            this.initializeRoads();
        }
        if (!this.spawns) {
            this.initializeSpawns();
        }
        if (!this.towers) {
            this.initializeTowers();
        }
        if (!this.walls) {
            this.initializeWalls();
        }
        if (!this.ramparts) {
            this.initializeRamparts();
        }
        if (!this.extensions) {
            this.initializeExtensions();
        }
        if (!this.didInitTerminal) {
            this.initializeTerminal();
        }
        if (!this.labs) {
            this.initializeLabs();
        }
        if (!this.didInitExtractor) {
            this.initializeExtractor();
        }
    }
    
    /**
     * Initializes all containers in the room.
     */
    private initializeContainers(): void {
        this.centreContainers = [];
        this.peripheryContainers = [];
        const containers = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.CONTAINER);
        for (const c of containers) {
            this.initializeBuilding(c);
        }
    }

    /**
     * Initializes all links in the room.
     */
    private initializeLinks(): void {
        this.peripheryLinks = [];
        this.centreLink = undefined;
        this.controllerLink = undefined;
        const links = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.LINK);
        for (const l of links) {
            this.initializeBuilding(l);
        }
    }

    /**
     * Initializes all roads in the room.
     */
    private initializeRoads(): void {
        this.roads = [];
        const roads = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.ROAD);
        for (const r of roads) {            
            this.initializeBuilding(r);
        }
    }

    /**
     * Initializes all spawns in the room.
     */
    private initializeSpawns(): void {
        this.spawns = [];
        const spawns = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.SPAWN);
        for (const s of spawns) {            
            this.initializeBuilding(s);
        }
    }

    /**
     * Initializes all extensions in the room.
     */
    private initializeExtensions(): void {
        this.extensions = [];
        const extensions = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.EXTENSION);
        for (const e of extensions) {            
            this.initializeBuilding(e);
        }
    }

    /**
     * Initializes all towers in the room.
     */
    private initializeTowers(): void {
        this.towers = [];
        const towers = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.TOWER);
        for (const t of towers) {            
            this.initializeBuilding(t);
        }
    }

    /**
     * Initializes all walls in the room.
     */
    private initializeWalls(): void {
        this.walls = [];
        const walls = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.WALL);
        for (const w of walls) {            
            this.initializeBuilding(w);
        }
    }

    /**
     * Initializes all ramparts in the room.
     */
    private initializeRamparts(): void {
        this.ramparts = [];
        const ramparts = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.RAMPART);
        for (const r of ramparts) {            
            this.initializeBuilding(r);
        }
    }

    /**
     * Initializes the storage in the room.
     */
    private initializeStorage(): void {
        this.storage = undefined;
        const storage = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.STORAGE)[0];
        this.didInitStorage = true;
        this.initializeBuilding(storage);
    }

    /**
     * Initializes the terminal in the room.
     */
    private initializeTerminal(): void {
        this.terminal = undefined;
        const terminal = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.TERMINAL)[0];
        this.didInitTerminal = true;
        this.initializeBuilding(terminal);
    }

    /**
     * Initializes the extractor in the room.
     */
    private initializeExtractor(): void {
        this.extractor = undefined;
        const extractor = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.EXTRACTOR)[0];
        this.didInitExtractor = true;
        this.initializeBuilding(extractor);
    }

    /**
     * Initializes all labs in the room.
     */
    private initializeLabs(): void {
        this.labs = [];
        const labs = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.LAB);
        for (const l of labs) {            
            this.initializeBuilding(l);
        }
    }

    /**
     * Initializes the nuker in the room.
     */
    private initializeNuker(): void {
        this.nuker = undefined;
        const nuker = this.sector.find<IScreepsStructure>(FindType.FIND_STRUCTURES).filter(s => s.structureType == StructureType.NUKER)[0];
        this.didInitNuker = true;
        this.initializeBuilding(nuker);
    }

    /**
     * Initializes all construction sites in the room.
     */
    private initializeConstrucionSites(): void {
        this.constructionSites = [];
        for (const ID in Game.constructionSites) {
            const cs = Game.constructionSites[ID];
            if (cs?.room?.name == this.sector.roomName) {
                this.constructionSites.push(new ConstructionSite(ID));
            }
        }
    }

    /**
     * Checks if any construction site was completed and if so initializes the newly created structure.
     */
    private checkNewBuildings(): void {
        if (this.completedUpdateTime >= Game.time || !this.constructionSites) {
            return
        }
        this.completedUpdateTime = Game.time;
        const stillUnderConstruction: ConstructionSite[] = [];
        for (const sc of this.constructionSites) {
            const pos = sc.getPosition();
            const type = sc.getStructureType();
            if (sc.isCompleted() && pos && type) {
                const found = this.sector.lookForAt(LookType.LOOK_STRUCTURES, pos.x, pos.y);
                for (const result of found as IScreepsStructure[]) {
                    if (result.structureType == type) {
                        this.initializeBuilding(result);
                    }
                    else if (result.structureType == StructureType.ROAD && type != StructureType.RAMPART) {
                        // deconstruct road underneath new building
                        const road = this.roads?.find(r => r.getID() == result.id);
                        if (road) {
                            road.setDeconstructing();
                            this.addDeconstructionTarget(road);
                        }
                    }
                }
                if (found.length == 0) {
                    this.addRebuildTarget(sc.getID(), pos.x, pos.y, type);
                }           
            }
            else {
                stillUnderConstruction.push(sc);
            }
        }
        this.constructionSites = stillUnderConstruction;
    }

    /**
     * Finds all buildings that need to be repaired or rebuilt.
     */
    private checkForDamagedBuildings(): void {
        const damagedMap = new Map<string, boolean>();
        const damaged = [];
        // check previously damaged buildings
        for (const building of this.damaged) {
            if (!building.isDestroyed() && !building.isRepaired()) {
                // not fully repaired yet
                damaged.push(building);
                damagedMap.set(building.getID(), true);
            }
        }
        if (this.damagedFullCheckTime > Game.time - 30) return;
        this.damagedFullCheckTime = Game.time;
        // helper function that checks all buildings in a given array
        const checkArray = (array?: BuildingBase[]): BuildingBase[] => {
            if (!array) return [];
            for (const b of array) {
                if (b.isDestroyed() && !b.shouldBeDeconstructed()) {
                    const pos = b.getPosition();
                    const type = b.getType();
                    if (pos && type) {
                        this.addRebuildTarget(b.getID(), pos.x, pos.y, type);
                    }
                }
                if (b.isDestroyed()) {
                    delete Memory.buildings?.[b.getID()];
                }
                else if (!damagedMap.has(b.getID()) && b.needsRepair()) {
                    damaged.push(b);
                }

            }
            return array.filter(b => !b.isDestroyed());
        };
        // check all other buildings
        this.peripheryContainers = checkArray(this.peripheryContainers) as Container[];
        this.centreContainers = checkArray(this.centreContainers) as Container[];
        this.spawns = checkArray(this.spawns) as Spawner[];
        this.extensions = checkArray(this.extensions) as Extension[];
        this.towers = checkArray(this.towers) as Tower[];
        this.roads = checkArray(this.roads) as Road[];
        this.ramparts = checkArray(this.ramparts) as Rampart[];
        this.walls = checkArray(this.walls) as Wall[];
        this.peripheryLinks = checkArray(this.peripheryLinks) as Link[];
        if (this.centreLink) this.centreLink = (checkArray([this.centreLink]) as Link[])[0];
        if (this.controllerLink) this.controllerLink = (checkArray([this.controllerLink]) as Link[])[0];
        if (this.storage) this.storage = (checkArray([this.storage]) as Storage[])[0];
        if (this.terminal) this.terminal = (checkArray([this.terminal]) as Terminal[])[0];
        if (this.extractor) this.extractor = (checkArray([this.extractor]) as Extractor[])[0];
        if (this.nuker) this.nuker = (checkArray([this.nuker]) as Nuker[])[0];
        this.labs = checkArray(this.labs) as Lab[];
        this.damaged = damaged;
    }

    /**
     * Adds the given data to the rebuild queue.
     * @param id ID of the structure / construction site that was destroyed
     * @param x X coordinate
     * @param y Y coordinate
     * @param type The type of building.
     */
    private addRebuildTarget(id: string, x: number, y: number, type: StructureType) {
        const memory = this.sector.getMemory();
        const repairQueue = memory.rq ?? {};
        repairQueue[id] = {x: x, y: y, type: type};
        memory.rq = repairQueue;
        this.sector.setMemory(memory);
    }

    /**
     * Runs all the buildings that have AI.
     */
    public run(): void {        
        for (const t of this.towers ?? []) {
            t.runAI();
        }
        for (const l of this.peripheryLinks ?? []) {
            l.runAI();
        }
        for (const cl of this.labComplexes ?? []) {
            cl.run();
        }
    }

    /**
     * @returns Array of damaged buildings.
     */
    public getDamagedBuildings(): BuildingBase[] {
        if (this.damagedUpdateTime < Game.time) {
            this.damagedUpdateTime = Game.time;            
            this.initiallizeAll();
            this.checkForDamagedBuildings();
        }
        this.checkNewBuildings();
        return this.damaged;
    }

    /**
     * @returns Array of containers located at sources.
     */
    public getPeripheryContainers(): Container[] {
        if (!this.peripheryContainers) {
            this.initializeContainers();
        }
        this.checkNewBuildings();
        return this.peripheryContainers ?? [];
    }

    /**
     * @returns Array of links in the periphery.
     */
    public getPeripheryLinks(): Link[] {
        if (!this.peripheryLinks) {
            this.initializeLinks();
        }
        this.checkNewBuildings();
        return this.peripheryLinks ?? [];
    }

    /**
     * @returns The link in the centre.
     */
    public getCentreLink(): Link | undefined {
        if (!this.peripheryLinks) {
            this.initializeLinks();
        }
        this.checkNewBuildings();
        return this.centreLink;
    }

    /**
     * @returns The link at the controller.
     */
    public getControllerLink(): Link | undefined {
        if (!this.peripheryLinks) {
            this.initializeLinks();
        }
        this.checkNewBuildings();
        return this.controllerLink;
    }

    /**
     * @returns Array of containers in the centre.
     */
    public getCentreStores(): Container[] {
        if (!this.centreContainers) {
            this.initializeContainers();
        }
        this.checkNewBuildings();
        return this.centreContainers ?? [];
    }

    /**
     * @returns Array of spawns in the room.
     */
    public getSpawns(): Spawner[] {
        if (!this.spawns) {
            this.initializeSpawns();
        }
        this.checkNewBuildings();
        return this.spawns ?? [];
    }

    /**
     * @returns Array of extensions in the room.
     */
    public getExtensions(): Extension[] {
        if (!this.extensions) {
            this.initializeExtensions();
        }
        this.checkNewBuildings();
        return this.extensions ?? [];
    }

    /**
     * @returns Array of towers in the room.
     */
    public getTowers(): Tower[] {
        if (!this.towers) {
            this.initializeTowers();
        }
        this.checkNewBuildings();
        return this.towers ?? [];
    }

    /**
     * @returns Array of walls in the room.
     */
    public getWalls(): Wall[] {
        if (!this.walls) {
            this.initializeWalls();
        }
        this.checkNewBuildings();
        return this.walls ?? [];
    }

    /**
     * @returns Array of rampards in the room.
     */
    public getRamparts(): Rampart[] {
        if (!this.ramparts) {
            this.initializeRamparts();
        }
        this.checkNewBuildings();
        return this.ramparts ?? [];
    }

    /**
     * @returns Array of roads in the room.
     */
    public getRoads(): Road[] {
        if (!this.roads){
            this.initializeRoads();
        }
        this.checkNewBuildings();
        return this.roads ?? [];
    }

    /**
     * @returns Storage in the room.
     */
    public getStorage(): Storage | undefined {
        if (!this.didInitStorage) {
            this.initializeStorage();
        }
        this.checkNewBuildings();
        return this.storage;
    }

    /**
     * @returns Terminal in the room.
     */
    public getTerminal(): Terminal | undefined {
        if (!this.didInitTerminal) {
            this.initializeTerminal();
        }
        this.checkNewBuildings();
        return this.terminal;
    }

    /**
     * @returns Array of labs in the room.
     */
    public getLabs(): Lab[] {
        if (!this.labs) {
            this.initializeLabs();
        }
        this.checkNewBuildings();
        return this.labs ?? [];
    }

    /**
     * @returns Array of lab complexes in the room.
     */
    public getLabComplexes(): LabComplex[] {
        if (!this.labs) {
            this.initializeLabs();
        }
        if (this.labs && this.labs.length == 10 && !this.labComplexes) {
            this.labComplexes = LabComplex.getLabComplexes(this.labs, this.sector as Sector);
        }
        this.checkNewBuildings();
        return this.labComplexes ?? [];
    }

    /**
     * @returns Extractor in the room.
     */
    public getExtractor(): Extractor | undefined {
        if (!this.didInitExtractor) {
            this.initializeExtractor();
        }
        this.checkNewBuildings();
        return this.extractor;
    }

    /**
     * @returns Nuker in the room.
     */
    public getNuker(): Nuker | undefined {
        if (!this.didInitNuker) {
            this.initializeNuker();
        }
        this.checkNewBuildings();
        return this.nuker;
    }

    /**
     * Notifies the registry of a new construction site in the room.
     */
    public addConstructionSite(): void {
        this.constructionSiteAdded = Game.time;
    }

    /**
     * @returns Array of construction sites in the room.
     */
    public getConstructionSites(): ConstructionSite[] {
        if (!this.constructionSites) {
            this.initializeConstrucionSites();
        }
        else if (this.constructionSiteAdded && this.constructionSiteAdded < Game.time) {
            this.initializeConstrucionSites();
            this.constructionSiteAdded = undefined;
        }
        this.checkNewBuildings();
        return this.constructionSites ?? [];
    }

    /**
     * Adds the given building to the array of deconstruction targets.
     * @param building The building to deconstruct.
     */
    public addDeconstructionTarget(building: BuildingBase): void {
        this.toDeconstruct.push(building);
        building.setDeconstructing();
        // remove from damaged building array
        const damagedBuildings = [];
        for (const damaged of this.damaged) {
            if (damaged.getID() != building.getID()) {
                damagedBuildings.push(damaged);
            }
        }
        this.damaged = damagedBuildings;
    }

    /**
     * @returns Array of buildings to dismantle.
     */
    public getDeconstructionTargets(): BuildingBase[] {
        if (this.toDeconstructUpdateTime < Game.time) {
            this.toDeconstructUpdateTime = Game.time;
            this.toDeconstruct = this.toDeconstruct.filter(b => !b.isDestroyed());
        }
        return this.toDeconstruct;
    }

    /**
     * @returns The invader core in the room, if any.
     */
    public getInvaderCore(): InvaderCore | undefined {
        if (!this.invaderCore && this.invaderCoreUpdateTime+50 < Game.time) {
            this.invaderCoreUpdateTime = Game.time;
            const cores = this.sector.find<IScreepsInvaderCore>(FindType.FIND_HOSTILE_STRUCTURES).filter(s => s.structureType == StructureType.INVADER_CORE);
            if (cores[0]) {
                this.invaderCore = new InvaderCore(cores[0].id);
            }
        }
        if (this.invaderCore && this.invaderCore.isDestroyed()) {
            this.invaderCore = undefined;
        }
        return this.invaderCore;
    }
}