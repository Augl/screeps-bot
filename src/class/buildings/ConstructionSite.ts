import { WorldObject } from "class/WorldObject";
import { IScreepsConstructionSite, IScreepsGame } from "screeps";
import { IPosition } from "types";
import { StructureType } from "util/Constants";

declare const Game: IScreepsGame;

export class ConstructionSite {

    private underlying: WorldObject<IScreepsConstructionSite | undefined>;

    private pos?: IPosition;

    private type?: StructureType;

    private ID: string;

    constructor(ID: string) {
        this.underlying = new WorldObject(() => {
            const site = Game.getObjectById<IScreepsConstructionSite | undefined>(ID);
            if (site) {
                this.pos = site.pos;
                this.type = site.structureType;
            }
            return site;
        });
        this.ID = ID;
    }

    public getID(): string {
        return this.ID;
    }

    public getStructureType(): StructureType | undefined {
        return this.type;
    }

    public getUnderlying(): IScreepsConstructionSite | undefined {
        return this.underlying.value(false);
    }

    public isCompleted(): boolean {
        return this.underlying.value(true) == undefined;
    }

    public getPosition(): IPosition | undefined {
        return this.pos;
    }

    public isDefenseBuilding(): boolean {
        return this.type == StructureType.WALL || this.type == StructureType.RAMPART;
    }
}