import { AIRunner } from "ai/AIRunner";
import { AIBase } from "ai/Base";
import { AILabReact } from "ai/building/LabReact";
import { AINoop } from "ai/Noop";
import { ChemicalReaction } from "ai/world/sector/Chemistry";
import { Sector } from "class/world/Sector";
import { IMemory } from "types";
import { BuildingAITask, ResourceType, Result } from "util/Constants";
import { Helper } from "util/Helper";
import { Lab } from "./Lab";

declare const Memory: IMemory;

export class LabComplex extends AIRunner<BuildingAITask, AIBase> {

    private reagents: Lab[];

    private products: Lab[];

    private reaction?: ChemicalReaction;

    private sector: Sector;

    private didError = false;

    constructor(labs: Lab[], sector: Sector) {
        super();
        const minY = labs.reduce((min, l) => {
            const y = l.getPosition()?.y ?? 50;
            if (y < min) return y;
            return min;
        }, 50);
        this.reagents = labs.filter(l => l.getPosition()?.y == minY);
        if (this.reagents.length != 2) {
            throw Error("There should be exactly two reagent labs!");
        }
        this.products = labs.filter(l => l.getPosition()?.y != minY);
        this.sector = sector;
    }

    /**
     * Sets the reaction performed by the complex.
     * @param reaction The reaction to perform.
     */
    public setReaction(reaction: ChemicalReaction): void {
        this.reaction = reaction;
    }

    /**
     * @returns The checmical reaction performed by this complex.
     */
    public getReaction(): ChemicalReaction | undefined {
        return this.reaction;
    }

    /**
     * Runs the current reaction in all production labs.
     */
    public runReaction(): void {
        const reagents = this.getReagentLabs();
        for (const productLab of this.getProductLabs()) {
            const result = productLab.runReaction(reagents[0].lab, reagents[1].lab);
            if (result == Result.ERR_INVALID_ARGS) {
                this.didError = true;
            }
        }
    }

    /**
     * @returns Array of labs and the mineral type they should be filled with.
     */
    public getReagentLabs(): {lab: Lab, mineral: ResourceType | undefined}[] {
        return [
            {lab: this.reagents[0], mineral: this.reaction?.reagent1},
            {lab: this.reagents[1], mineral: this.reaction?.reagent2},
        ];
    }

    /**
     * @returns Labs containing the product.
     */
    public getProductLabs(): Lab[] {
        return this.products;
    }

    /**
     * @returns All labs in the complex.
     */
    public getAllLabs(): Lab[] {
        return this.products.concat(this.reagents);
    }

    /**
     * @returns If the lab complex requires a chemist.
     */
    public needsChemist(): boolean {
        return this.currentID == BuildingAITask.LAB_FILL || this.currentID == BuildingAITask.LAB_EMPTY;
    }

    /**
     * @returns If the reagent labs should be filled.
     */
    public shouldFill(): boolean {
        return this.currentID == BuildingAITask.LAB_FILL;
    }

    /**
     * @returns If the labs should be emptied.
     */
    public shouldEmpty(): boolean {
        return this.currentID == BuildingAITask.LAB_EMPTY;
    }

    /**
     * @returns If a new reaction can be processed.
     */
    public isReady(): boolean {
        return this.currentID == BuildingAITask.LAB_IDLE;
    }

    public run(): void {
        super.runAI();
        if (Memory.debugVisuals) {
            let text = "Idle";
            if (this.currentID == BuildingAITask.LAB_FILL) {
                text = "Fill";
            }
            else if (this.currentID == BuildingAITask.LAB_REACT) {
                text = "Process";
            }
            else if (this.currentID == BuildingAITask.LAB_EMPTY) {
                text = "Empty";
            }
            if (!this.reagents) return;
            const x = (this.reagents[0].getPosition()?.x ?? 0) + (this.reagents[1].getPosition()?.x ?? 0);
            this.sector.getUnderlying()?.visual.text(text, x/2, (this.reagents[0].getPosition()?.y ?? 0)+1);
        }
    }

    /**
     * @returns Array of AI to run.
     */
    public getAIs(): { AIs: Map<BuildingAITask, AIBase>; startTask: BuildingAITask; } | undefined {
        return {
            AIs: new Map([
                [BuildingAITask.LAB_IDLE, new AINoop()],
                [BuildingAITask.LAB_FILL, new AINoop()],
                [BuildingAITask.LAB_REACT, new AILabReact(this)],
                [BuildingAITask.LAB_EMPTY, new AINoop()],
            ]),
            startTask: BuildingAITask.LAB_REACT
        };
    }

    /**
     * Decides what AI to run next.
     * @param currentTask The previously run AI.
     * @returns The AI to run next.
     */
    public doTransition(currentTask: BuildingAITask): BuildingAITask[] {
        if (this.didError) {
            // if an error has occured labs should be emptied to try again.
            this.didError = false;
            this.reaction = undefined;
            return [BuildingAITask.LAB_EMPTY];
        }
        if (currentTask == BuildingAITask.LAB_IDLE) {
            if (this.reaction) {
                return [BuildingAITask.LAB_FILL];
            }
            return [BuildingAITask.LAB_IDLE];
        }
        else if (currentTask == BuildingAITask.LAB_FILL) {
            if (this.reagents.find(r => r.getContainedResources().length != 2 || r.getFreeCapacity(r.getContainedResources()[1]) > 0) == undefined) {
                // all reagents are full, start processing
                return [BuildingAITask.LAB_REACT];
            }
            return [BuildingAITask.LAB_FILL];
        }
        else if (currentTask == BuildingAITask.LAB_REACT) {
            if (this.reagents.find(r => r.getContainedResources().length == 1) != undefined) {
                // one reagents is empty, processing done
                return [BuildingAITask.LAB_EMPTY];
            }
            return [BuildingAITask.LAB_REACT];
        }
        else if (currentTask == BuildingAITask.LAB_EMPTY) {
            if (this.getAllLabs().find(p => p.getContainedResources().length >= 2) == undefined) {
                // all labs are empty
                this.reaction = undefined;
                return [BuildingAITask.LAB_IDLE];
            }
            return [BuildingAITask.LAB_EMPTY];
        }
        throw new Error("Unexpected state for lab complex: " + currentTask);
    }

    /**
     * Finds the lab complexes in the array of labs.
     * @param labs The lab buildings in the room.
     * @returns The lab complexes in the room.
     */
    public static getLabComplexes(labs: Lab[], sector: Sector): LabComplex[] {
        const complexes: LabComplex[] = [];
        for (const current of labs) {
            const cPos = current.getPosition();
            const neighbours = labs.filter(l => {
                const lPos = l.getPosition();
                if (lPos && cPos) return Helper.distance(cPos, lPos) <= 1;
                return false;
            });
            if (neighbours.length == 5) {
                complexes.push(new LabComplex(neighbours, sector));
            }
        }
        return complexes;
    }
}