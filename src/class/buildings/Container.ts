import { WorldObject } from "class/WorldObject";
import { IScreepsContainer, IScreepsGame } from "screeps";
import { StoreBase } from "./StoreBase";

declare const Game: IScreepsGame;

export class Container extends StoreBase{

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsContainer | undefined>(() => Game.getObjectById<IScreepsContainer>(ID), 200);
        super(underlying, ID);
    }

    public needsRepair(): boolean {
        if (this.shouldBeDeconstructed()) return false;
        // overrides parent method because containers have frequent decay
        const hp = this.getHitPoints();
        const max = this.getMaxHitPoints();
        return (hp != undefined && max != undefined && hp / max < 0.5);
    }
}