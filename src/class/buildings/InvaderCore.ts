import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsInvaderCore } from "screeps";
import { BuildingBase } from "./Base";

declare const Game: IScreepsGame;

export class InvaderCore extends BuildingBase {

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsInvaderCore | undefined>(() => Game.getObjectById(ID));
        super(underlying, ID);
    }
}