import { AIBase } from "ai/Base";
import { AITowerDefend } from "ai/building/TowerDefend";
import { AITowerRepair } from "ai/building/TowerRepair";
import { AITowerRepairDefense } from "ai/building/TowerRepairDefense";
import { WorldBase } from "class/world/Base";
import { WorldObject } from "class/WorldObject";
import { IScreepsCreep, IScreepsGame, IScreepsTower } from "screeps";
import { BuildingAITask, ResourceType, Result } from "util/Constants";
import { BuildingBase } from "./Base";
import { StoreBase } from "./StoreBase";

declare const Game: IScreepsGame;

export class Tower extends StoreBase {

    private sector: WorldBase;

    constructor(ID: string, sector: WorldBase) {
        const underlying = new WorldObject<IScreepsTower | undefined>(() => Game.getObjectById(ID));
        super(underlying, ID);
        this.sector = sector;
    }

    public isFull(resource?: ResourceType): boolean {
        const container = this.underlying.value(true) as IScreepsTower | undefined;
        if (!container) return false;
        return container.store.getFreeCapacity(resource) < 50;
    }

    public attack(creep: IScreepsCreep): Result {
        const tower = this.underlying.value(false) as IScreepsTower | undefined;
        if (!tower) return Result.ERR_UNKNOWN;
        return tower.attack(creep);
    }

    public repair(target: BuildingBase): Result {
        const struct = target.getUnderlying();
        const tower = this.underlying.value(false) as IScreepsTower | undefined;
        if (!struct || !tower) return Result.ERR_UNKNOWN;
        return tower.repair(struct);
    }

    public getAIs(): { AIs: Map<BuildingAITask, AIBase>; startTask: BuildingAITask; } | undefined {
        return {
            AIs: new Map<BuildingAITask, AIBase>([
                [BuildingAITask.TOWER_DEFEND, new AITowerDefend(this, this.sector)],
                [BuildingAITask.TOWER_REPAIR, new AITowerRepair(this, this.sector)],
                [BuildingAITask.TOWER_REPAIR_DEFENSE, new AITowerRepairDefense(this, this.sector)]
            ]),
            startTask: BuildingAITask.TOWER_DEFEND
        };
    }

    public doTransition(currentTask: BuildingAITask): BuildingAITask[] {
        if (currentTask == BuildingAITask.TOWER_DEFEND) {
            return [BuildingAITask.TOWER_DEFEND, BuildingAITask.TOWER_REPAIR, BuildingAITask.TOWER_REPAIR_DEFENSE];
        }
        else if (currentTask == BuildingAITask.TOWER_REPAIR) {
            return [BuildingAITask.TOWER_DEFEND, BuildingAITask.TOWER_REPAIR, BuildingAITask.TOWER_REPAIR_DEFENSE];
        }
        else if (currentTask == BuildingAITask.TOWER_REPAIR_DEFENSE) {
            return [BuildingAITask.TOWER_DEFEND, BuildingAITask.TOWER_REPAIR, BuildingAITask.TOWER_REPAIR_DEFENSE];
        }
        throw new Error("Unexpected state for tower: " + currentTask);
    }
}