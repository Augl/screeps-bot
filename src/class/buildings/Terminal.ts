import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsStore, IScreepsTerminal } from "screeps";
import { ResourceType, Result } from "util/Constants";
import { StoreBase } from "./StoreBase";

declare const Game: IScreepsGame;

export class Terminal extends StoreBase {

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsTerminal | undefined>(() => Game.getObjectById(ID), 200);
        super(underlying, ID);
    }    

    public isFull(resource?: ResourceType): boolean {
        const container = this.underlying.value(true) as IScreepsStore | undefined;
        if (!container) return false;
        if (resource == ResourceType.ENERGY) return container.store.getUsedCapacity(ResourceType.ENERGY) >= 20000
        return container.store.getFreeCapacity(resource) == 0;
    }

    public sendResources(resourceType: ResourceType, amount: number, destination: string, description?: string): Result {
        const underlying = this.getUnderlying();
        if (!underlying) return Result.ERR_UNKNOWN;
        return (underlying as IScreepsTerminal).send(resourceType, amount, destination, description)
    }
}