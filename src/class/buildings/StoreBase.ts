import { WorldObject } from "class/WorldObject";
import { IScreepsStore } from "screeps";
import { ResourceType } from "util/Constants";
import { BuildingBase } from "./Base";

export class StoreBase extends BuildingBase {

    constructor(underlying: WorldObject<IScreepsStore | undefined>, ID: string) {
        super(underlying, ID);
    }

    public isEmpty(resource?: ResourceType): boolean {
        const container = this.underlying.value(true) as IScreepsStore | undefined;
        if (!container) return false;
        return container.store.getUsedCapacity(resource) == 0;
    }

    public hasResource(amount: number, resource: ResourceType): boolean {
        const container = this.underlying.value(true) as IScreepsStore | undefined;
        if (!container) return false;
        return container.store.getUsedCapacity(resource) > amount;
    }

    public isFull(resource?: ResourceType): boolean {
        const container = this.underlying.value(true) as IScreepsStore | undefined;
        if (!container) return false;
        return container.store.getFreeCapacity(resource) == 0;
    }

    public getCapacity(resource?: ResourceType): number {
        const container = this.underlying.value(true) as IScreepsStore | undefined;
        if (!container) return 0;
        return container.store.getCapacity(resource);
    }

    public getFreeCapacity(resource?: ResourceType): number {
        const container = this.underlying.value(true) as IScreepsStore | undefined;
        if (!container) return 0;
        return container.store.getFreeCapacity(resource);
    }

    public getUsedCapacity(resource?: ResourceType): number {
        const container = this.underlying.value(true) as IScreepsStore | undefined;
        if (!container) return 0;
        return container.store.getUsedCapacity(resource);
    }

    public getContainedResources(): ResourceType[] { 
        const container = this.underlying.value(true) as IScreepsStore | undefined;
        if (!container) return [];
        const result = [];
        for (const resource in container.store) {
            result.push(resource as ResourceType);
        }
        return result;
    }
}