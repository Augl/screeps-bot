import { WorldBase } from "class/world/Base";
import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsRampart } from "screeps";
import { BuildingBase } from "./Base";

declare const Game: IScreepsGame;

export class Rampart extends BuildingBase {

    private room: WorldBase;

    constructor(ID: string, room: WorldBase) {
        const underlying = new WorldObject<IScreepsRampart | undefined>(() => Game.getObjectById(ID), 100);
        super(underlying, ID);
        this.room = room;
    }
    
    public needsRepair(): boolean {
        if (this.shouldBeDeconstructed()) return false;
        let relativeMax = this.room.getLevel() * 500000;
        if (this.room.getLevel() == 8) relativeMax = 30000000;
        const hp = this.getHitPoints();
        return (hp != undefined && hp < relativeMax);
    }

    public isRepaired(): boolean {
        if (this.shouldBeDeconstructed()) return true;
        const hp = this.getHitPoints();
        let relativeMax = this.room.getLevel() * 500000;
        if (this.room.getLevel() == 8) relativeMax = 30000000;
        return (hp != undefined && relativeMax != undefined && hp == relativeMax);
    }
}