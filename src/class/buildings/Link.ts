import { AIBase } from "ai/Base";
import { AILinkTransfer } from "ai/building/LinkTransfer";
import { WorldBase } from "class/world/Base";
import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsLink } from "screeps";
import { BuildingAITask, Result } from "util/Constants";
import { StoreBase } from "./StoreBase";

declare const Game: IScreepsGame;

export class Link extends StoreBase {

    private isSender: boolean;

    private sector: WorldBase;
    
    constructor(ID: string, sector: WorldBase, sender: boolean) {
        const underlying = new WorldObject<IScreepsLink | undefined>(() => Game.getObjectById(ID));
        super(underlying, ID);
        this.isSender = sender;
        this.sector = sector;
    }

    public transferEnergy(target: Link): Result {
        const link = this.underlying.value(false) as IScreepsLink;
        const t = target.underlying.value(false) as IScreepsLink;
        if (!link || !t) return Result.ERR_UNKNOWN;
        return link.transferEnergy(t);
    }

    public getAIs(): { AIs: Map<BuildingAITask, AIBase>; startTask: BuildingAITask; } | undefined {
        return {
            AIs: new Map([
                [BuildingAITask.LINK_TRANSFER, new AILinkTransfer(this, this.sector, this.isSender)]
            ]),
            startTask: BuildingAITask.LINK_TRANSFER
        };
    }

    public doTransition(currentTask: BuildingAITask): BuildingAITask[] {
        return [currentTask];
    }
}