import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsStorage } from "screeps";
import { StoreBase } from "./StoreBase";

declare const Game: IScreepsGame;

export class Storage extends StoreBase {

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsStorage | undefined>(() => Game.getObjectById(ID), 200);
        super(underlying, ID);
    }
}