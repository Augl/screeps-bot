import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsExtractor } from "screeps";
import { BuildingBase } from "./Base";

declare const Game: IScreepsGame;

export class Extractor extends BuildingBase {

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsExtractor | undefined>(() => Game.getObjectById(ID), 200);
        super(underlying, ID);
    }
}