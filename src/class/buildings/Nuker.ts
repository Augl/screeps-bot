import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsNuker } from "screeps";
import { StoreBase } from "./StoreBase";

declare const Game: IScreepsGame;

export class Nuker extends StoreBase {

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsNuker | undefined>(() => Game.getObjectById(ID), 1000);
        super(underlying, ID);
    }
}