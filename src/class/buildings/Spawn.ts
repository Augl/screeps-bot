import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsSpawn } from "screeps";
import { IAuglMemory } from "types";
import { BodyPart, ResourceType, Result } from "util/Constants";
import { StoreBase } from "./StoreBase";

declare const Game: IScreepsGame;

export class Spawner extends StoreBase {

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsSpawn | undefined>(() => Game.getObjectById(ID));
        super(underlying, ID);
    }

    public isSpawning(): boolean {
        const spawn = this.underlying.value(false) as IScreepsSpawn | undefined;
        if (!spawn) return false;
        return spawn.spawning != undefined;
    }

    public spawnCreep(body: BodyPart[], name: string, memory: IAuglMemory): Result {
        const spawn = this.underlying.value(false) as IScreepsSpawn | undefined;
        if (!spawn) return Result.ERR_INVALID_TARGET;
        return spawn.spawnCreep(body, name, {memory: memory});
    }

    public isFull(): boolean {
        const container = this.underlying.value(false) as IScreepsSpawn | undefined;
        if (!container) return false;
        return container.store.getFreeCapacity(ResourceType.ENERGY) == 0;
    }
}