import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsRoad } from "screeps";
import { BuildingBase } from "./Base";

declare const Game: IScreepsGame;

export class Road extends BuildingBase {

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsRoad | undefined>(() => Game.getObjectById(ID), 1000);
        super(underlying, ID);
    }

    public needsRepair(): boolean {
        // overrides parent method because roads have frequent decay
        const hp = this.getHitPoints();
        const max = this.getMaxHitPoints();
        return (hp != undefined && max != undefined && hp / max < 0.5);
    }   
}