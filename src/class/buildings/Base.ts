import { AIRunner } from "ai/AIRunner";
import { AIBase } from "ai/Base";
import { WorldObject } from "class/WorldObject";
import { IScreepsStructure } from "screeps";
import { IMemory, IPosition } from "types";
import { BuildingAITask, StructureType } from "util/Constants";

declare const Memory: IMemory;

export abstract class BuildingBase extends AIRunner<BuildingAITask, AIBase> {

    protected underlying: WorldObject<IScreepsStructure | undefined>;

    protected ID: string;

    private type: StructureType | undefined;

    private pos: IPosition | undefined;

    constructor(underlying: WorldObject<IScreepsStructure | undefined>, ID: string) {
        super();
        this.underlying = underlying;
        this.ID = ID;
        const building = this.underlying.value(true);
        this.type = building?.structureType;
        this.pos = building?.pos
    }

    public getID(): string {
        return this.ID;
    }

    public getPosition(): IPosition | undefined {
        return this.pos;
    }

    public getType(): StructureType | undefined {
        return this.type;
    }

    public getHitPoints(): number {
        return this.underlying.value(true)?.hits ?? 0;
    }
    
    public getMaxHitPoints(): number {
        return this.underlying.value(true)?.hitsMax ?? 0;
    }

    public needsRepair(): boolean {
        if (this.shouldBeDeconstructed()) return false;
        const hp = this.getHitPoints();
        const max = this.getMaxHitPoints();
        return (hp != undefined && max != undefined && hp < max);
    }

    public isRepaired(): boolean {
        if (this.shouldBeDeconstructed()) return true;
        const hp = this.getHitPoints();
        const max = this.getMaxHitPoints();
        return (hp != undefined && max != undefined && hp == max);
    }

    public getUnderlying(): IScreepsStructure | undefined {
        return this.underlying.value(false);
    }

    public isDefenseBuilding(): boolean {
        const type = this.underlying.value(true)?.structureType;
        return type == StructureType.WALL || type == StructureType.RAMPART;
    }

    public setDeconstructing(): void {
        if (!Memory.buildings) Memory.buildings = {};
        Memory.buildings[this.ID] = {deconstructing: true};
    }

    public shouldBeDeconstructed(): boolean {
        return Memory.buildings?.[this.ID]?.deconstructing ?? false;
    }

    public isDestroyed(): boolean {        
        return this.underlying.value(true) == undefined;
    }
    
    public getAIs(): { AIs: Map<BuildingAITask, AIBase>; startTask: BuildingAITask; } | undefined {
        return undefined;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public doTransition(currentTask: BuildingAITask): BuildingAITask[] {
        throw new Error("BuildingBase is not supposed to do AI transitions!");
    }
}