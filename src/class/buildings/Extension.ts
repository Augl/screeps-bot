import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsExtension } from "screeps";
import { ResourceType } from "util/Constants";
import { StoreBase } from "./StoreBase";

declare const Game: IScreepsGame;

export class Extension extends StoreBase {

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsExtension | undefined>(() => Game.getObjectById(ID) as IScreepsExtension | undefined);
        super(underlying, ID);
    }

    public isFull(resource?: ResourceType): boolean {
        const container = this.underlying.value(false) as IScreepsExtension | undefined;
        if (!container) return false;
        return container.store.getFreeCapacity(resource) == 0;
    }
}