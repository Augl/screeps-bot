import { WorldObject } from "class/WorldObject";
import { IScreepsGame, IScreepsLab } from "screeps";
import { Result } from "util/Constants";
import { StoreBase } from "./StoreBase";

declare const Game: IScreepsGame;

export class Lab extends StoreBase {

    constructor(ID: string) {
        const underlying = new WorldObject<IScreepsLab | undefined>(() => Game.getObjectById(ID), 200);
        super(underlying, ID);
    }

    public getCooldown(): number {
        return (this.underlying.value(false) as IScreepsLab).cooldown ?? -1;
    }

    public runReaction(reagent1: Lab, reagent2: Lab): Result {
        const underlying = this.underlying.value(false) as IScreepsLab;
        if (!underlying) return Result.ERR_UNKNOWN;
        const r1 = reagent1.getUnderlying() as IScreepsLab;
        const r2 = reagent2.getUnderlying() as IScreepsLab;
        if (!r1 || !r2) return Result.ERR_UNKNOWN;
        return underlying.runReaction(r1, r2);
    }
}