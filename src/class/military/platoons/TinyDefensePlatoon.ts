import { AIBase } from "ai/Base";
import { AIPlatoonControl } from "ai/military/platoon/Control";
import { Empire } from "class/Empire";
import { PlatoonAITask, SquadType } from "util/Constants";
import { SquadBase } from "../squads/SquadBase";
import { TinyDefenseSquad } from "../squads/TinyDefenseSquad";
import { PlatoonBase } from "./PlatoonBase";

export class TinyDefensePlatoon extends PlatoonBase {

    private squad?: TinyDefenseSquad;

    constructor(name: string, empire: Empire) {
        super(name, empire);
        if (!this.squad) {
            super.createNewSquad(SquadType.TINY_DEFENSE);
        }
    }

    /**
     * @returns Map from TaskIDs to AI modules.
     */
    public getAIs(): { AIs: Map<PlatoonAITask, AIBase>; startTask: PlatoonAITask; } | undefined {
        return {
            AIs: new Map<PlatoonAITask, AIBase>([
                [PlatoonAITask.COORDINATE, new AIPlatoonControl(this)]
            ]),
            startTask: PlatoonAITask.COORDINATE
        };
    }

    /**
     * Decides what AI to run next.
     * @param currentTask The previous task.
     * @returns The next task.
     */
    public doTransition(currentTask: PlatoonAITask): PlatoonAITask[] {
        return [currentTask];
    }

    /**
     * Runs the squad.
     */
    public run(): void {
        super.runAI();
        if (!this.squad) return;
        this.squad.run();
    }
    
    /**
     * Adds the given squad to the platoon.
     * @param squad The squad to add.
     */
    public addSquad(squad: SquadBase): void {
        if (squad.getType() == SquadType.TINY_DEFENSE) {
            this.squad = squad as TinyDefenseSquad;
        }
        else {
            console.log("Trying to add unexpected squad " + squad.getName() + " to platoon " + this.name);
        }
    }

    /**
     * @returns All squads of the platoon.
     */
    public getSquads(): SquadBase[] {
        const squads = [];
        if (this.squad) squads.push(this.squad);
        return squads;
    }
}