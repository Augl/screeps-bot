import { AIRunner } from "ai/AIRunner";
import { AIBase } from "ai/Base";
import { BuildingBase } from "class/buildings/Base";
import { Empire } from "class/Empire";
import { IScreepsCreep } from "screeps";
import { IMemory, IPlatoonMemory } from "types";
import { MilitaryAction, PlatoonAITask, PlatoonType, SquadType } from "util/Constants";
import { Names } from "util/Names";
import { SmallDefenseSquad } from "../squads/SmallDefenseSquad";
import { SquadBase } from "../squads/SquadBase";
import { TinyDefenseSquad } from "../squads/TinyDefenseSquad";

declare const Memory: IMemory;

export abstract class PlatoonBase extends AIRunner<PlatoonAITask, AIBase> {

    protected memory: IPlatoonMemory;

    protected name: string;

    protected empire: Empire;

    protected action: MilitaryAction;

    constructor(name: string, empire: Empire) {
        super();
        console.log("Initializing platoon", name);
        this.name = name;
        this.memory = Memory.platoons?.[name] ?? {};
        this.empire = empire;
        this.action = MilitaryAction.SPAWN;
        this.initializeSquads();
    }

    /**
     * Initializes all squads of the platoon.
     */
    private initializeSquads(): void {
        for (const squadName in Memory.squads) {
            if (Memory.squads[squadName]?.platoon == this.name) {
                this.initializeSquad(squadName);
            }
        }
    }

    /**
     * Initializes one squad.
     * @param squadName The name of the squad to initialize.
     */
    private initializeSquad(squadName: string): void {
        const type = Memory.squads?.[squadName]?.type;
        let squad = undefined;
        if (type == SquadType.TINY_DEFENSE) {
            console.log("Initializing TinyDefenseSquad...");
            squad = new TinyDefenseSquad(squadName, this);
        }
        else if (type == SquadType.SMALL_DEFENSE) {
            console.log("Initializing SmallDefenseSquad...");
            squad = new SmallDefenseSquad(squadName, this);
        }
        if (squad) {
            this.addSquad(squad);
            this.empire.addSquad(squad);
        }
    }

    /**
     * Creates a new squad and calls the addSquad method.
     * @param type The type of squad to create.
     */
    protected createNewSquad(type: SquadType): void {
        const squadName = Names.getSquadName();
        if (!Memory.squads) Memory.squads = {};
        Memory.squads[squadName] = {platoon: this.name, type: type};
        this.initializeSquad(squadName);
    }

    /**
     * @returns Returns the platoon's name
     */
    public getName(): string {
        return this.name;
    }

    /**
     * Used during initialization to add squads to the platoon.
     * @param squad The squad to add.
     */
    public abstract addSquad(squad: SquadBase): void;

    /**
     * @returns Array of squads controlled by the platoon.
     */
    public abstract getSquads(): SquadBase[];

    /**
     * @returns True if the platoon is ready for a new assignment.
     */
    public isReady(): boolean {
        // check if current room is still under attack
        const currentRoom = this.empire.getWorld(this.getTargetRoom() ?? "");
        const enemies = currentRoom?.getEnemies();
        if ((enemies && enemies.length > 0) || currentRoom?.getBuildings()?.getInvaderCore()) {
            // not done defending
            return false;
        }
        return this.action == MilitaryAction.FIGHT;
    }

    /**
     * Runs the platoon AIs and squads.
     */
    public abstract run(): void;

    /**
     * Assigns the given room to the platoon.
     * @param room The room to act in.
     */
    public setTargetRoom(room: string): void {
        this.memory.targetRoom = room;
        if (!Memory.platoons) return;
        Memory.platoons[this.name] = this.memory;
    }

    /**
     * @returns The target room.
     */
    public getTargetRoom(): string | undefined {
        return this.memory.targetRoom
    }
    
    /**
     * Returns true if the platoon is weaker than the given type. If the types can not be compared undefined will be returnes.
     * @param platoonType The type of platoon to compare to.
     * @returns True if the platoon is weaker, false if it is equal or stronger, undefined if not comparable.
     */
    public isWeakerThan(platoonType: PlatoonType): boolean | undefined {
        const currentType = this.getType();
        if (platoonType == currentType) {
            return false;
        }
        return undefined;
    }

    /**
     * @returns The type of the platoon.
     */
    public getType(): PlatoonType {
        if (!this.memory.type) {
            throw new Error("Platoon" + this.name + "has no type!");
        }
        return this.memory.type;
    }

    /**
     * @returns The empire.
     */
    public getEmpire(): Empire {
        return this.empire;
    }

    /**
     * @returns All enemy creeps in the target room.
     */
    public getEnemies(): IScreepsCreep[] {
        if (!this.memory.targetRoom) return [];
        const targetRoom = this.empire.getWorld(this.memory.targetRoom);
        return targetRoom?.getEnemies() ?? [];
    }

    /**
     * @returns Hostile buildings to attack.
     */
    public getHostileBuildings(): BuildingBase[] {
        if (!this.memory.targetRoom) return [];
        const targetRoom = this.empire.getWorld(this.memory.targetRoom);
        const core = targetRoom?.getBuildings()?.getInvaderCore();
        if (core) return [core];
        return [];
    }

    /**
     * @returns The current action.
     */
    public getAction(): MilitaryAction {
        return this.action;
    }

    /**
     * Sets the platoon action.
     * @param action The action to execute.
     */
    public setAction(action: MilitaryAction): void {
        this.action = action;
        for (const squad of this.getSquads()) {
            squad.setAction(action);
        }
    }

    /**
     * @returns True if the platoon has been eliminated.
     */
    public wasEliminated(): boolean {
        return this.action == MilitaryAction.ELIMINATED;
    }

    /**
     * Deletes all squads from memory and deregisters them from the empire.
     */
    public deleteSquads(): void {
        if (!Memory.squads) return;
        for (const s of this.getSquads()) {
            s.deleteAugls();
            delete Memory.squads[s.getName()];
            this.empire.removeSquad(s);
        }
    }
}