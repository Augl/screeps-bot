import { AIBase } from "ai/Base";
import { AISquadPopulation } from "ai/military/squad/Population";
import { AuglBase } from "class/augl/Base";
import { Healer } from "class/augl/military/Healer";
import { Warrior } from "class/augl/military/Warrior";
import { Job, MilitaryAction, SquadAITask } from "util/Constants";
import { PlatoonBase } from "../platoons/PlatoonBase";
import { SquadBase } from "./SquadBase";

export class TinyDefenseSquad extends SquadBase {

    private warrior?: Warrior;

    private healer?: Healer;

    constructor(name: string, platoon: PlatoonBase) {
        super(name, platoon);
    }

    /**
     * @returns Map from TaskIDs to AI modules.
     */
    public getAIs(): { AIs: Map<SquadAITask, AIBase>; startTask: SquadAITask; } | undefined {
        return {
            AIs: new Map<SquadAITask, AIBase>([
                [SquadAITask.POPULATION, new AISquadPopulation(this)]
            ]),
            startTask: SquadAITask.POPULATION
        };
    }

    /**
     * Decides what AI to run next.
     * @param currentTask The previous task.
     * @returns The next task.
     */
    public doTransition(currentTask: SquadAITask): SquadAITask[] {
        return [currentTask];
    }

    /**
     * Runs the squad AIs and augls.
     */
    public run(): void {
        super.runAI();
        this.warrior?.run();
        this.healer?.run();
    }

    /**
     * Sets the current action to execute.
     * @param action The action to run.
     */
    public setAction(action: MilitaryAction): void {
        this.warrior?.setAction(action);
        this.healer?.setAction(action);
    }

    /**
     * Adds the given augl to the squad.
     * @param augl The augl to add.
     */
    public addAugl(augl: AuglBase): void {
        if (augl.memory.job == Job.WARRIOR) {
            this.warrior = augl as Warrior;
        }
        else if (augl.memory.job == Job.HEALER) {
            this.healer = augl as Healer;
        }
        else {
            console.log("Trying to add augl with unexpected job " + augl.memory.job + " to TinyDefenseSquad.");
        }
    }

    /**
     * @returns Array of augls to spawn.
     */
    public getDesiredPopulation(): { job: Job; level: number; }[] {
        const population = [];
        if (!this.warrior) {
            population.push({job: Job.WARRIOR, level: 3});
        }
        if (!this.healer) {
            population.push({job: Job.HEALER, level: 3});
        }
        return population;
    }

    /**
     * @returns The squad's leader.
     */
    public getLeader(): AuglBase | undefined {
        return this.warrior
    }

    /**
     * @returns An array of all augls.
     */
    public getAugls(): AuglBase[] {
        const augls = [];
        if (this.warrior) augls.push(this.warrior);
        if (this.healer) augls.push(this.healer);
        return augls;
    }

    /**
     * @returns True if all augls have been spawned.
     */
    public hasSpawned(): boolean {
        return this.warrior != undefined && this.healer != undefined;
    }
}