import { AIRunner } from "ai/AIRunner";
import { AIBase } from "ai/Base";
import { AuglBase } from "class/augl/Base";
import { Healer } from "class/augl/military/Healer";
import { Warrior } from "class/augl/military/Warrior";
import { IScreepsGame } from "screeps";
import { IMemory, ISquadMemory } from "types";
import { Job, MilitaryAction, SquadAITask, SquadType } from "util/Constants";
import { Helper } from "util/Helper";
import { PlatoonBase } from "../platoons/PlatoonBase";

declare const Memory: IMemory;
declare const Game: IScreepsGame;

export abstract class SquadBase extends AIRunner<SquadAITask, AIBase> {

    protected name: string;

    protected memory: ISquadMemory;

    protected platoon: PlatoonBase;

    constructor(name: string, platoon: PlatoonBase) {
        super();
        console.log("Initializing squad", name);
        this.name = name;
        this.memory = Memory.squads?.[name] ?? {};
        this.platoon = platoon;
        this.initializeAugls();
    }

    /**
     * Initializes all augls for the squad.
     */
    private initializeAugls(): void {
        for (const name in Game.creeps) {
            if (Game.creeps[name]?.memory.home != this.name) continue;
            const job = Game.creeps[name]?.memory.job;
            if (!job) continue;
            if (job == Job.WARRIOR) {
                this.addAugl(new Warrior(name, this));
            }
            else if (job == Job.HEALER) {
                this.addAugl(new Healer(name, this));
            }
            else {
                throw new Error("Trying to initialize augl with unexpected job " + job + " to squad " + this.name);
            }
        }
    }
    
    /**
     * Adds the given augl to the squad.
     * @param augl The augl to add.
     */
    public abstract addAugl(augl: AuglBase): void;

    /**
     * @returns Array of augl descriptions the squad needs.
     */
    public abstract getDesiredPopulation(): {job: Job, level: number}[];

    /**
     * Runs the squad AIs and augls.
     */
    public abstract run(): void;

    /**
     * Sets the current action of the squad.
     * @param action The action to fullfill.
     */
    public abstract setAction(action: MilitaryAction): void;

    /**
     * @returns The augl that can be considered to be the leader.
     */
    public abstract getLeader(): AuglBase | undefined;

    /**
     * @returns An array of all augls.
     */
    public abstract getAugls(): AuglBase[];

    /**
     * @returns The type of squad.
     */
    public getType(): SquadType {
        if (!this.memory.type) {
            throw new Error("Squad" + this.name + "has no type!");
        }
        return this.memory.type;
    }

    /**
     * @returns The name of the squad.
     */
    public getName(): string {
        return this.name;
    }

    /**
     * @returns The platoon in command.
     */
    public getPlatoon(): PlatoonBase {
        return this.platoon;
    }

    /**
     * @returns True if all augls of the squad have gathered.
     */
    public hasGathered(): boolean {
        const augls = this.getAugls();
        const leaderPos = this.getLeader()?.getPosition();
        if (!leaderPos) return false;
        let maxDistance = 0;
        for (const augl of augls) {
            const auglPos = augl.getPosition();
            if (!auglPos) return false;
            const distance = Helper.distance(leaderPos, auglPos);
            if (distance > maxDistance) {
                maxDistance = distance;
            }
        }
        return maxDistance < 3;
    }

    /**
     * @returns True if all augls of the squad have died.
     */
    public wasEliminated(): boolean {
        const alive = this.getAugls().find(a => a.getTicksToLive() > 0);
        return alive == undefined;
    }

    /**
     * @returns True if all augls have been spawned.
     */
    public abstract hasSpawned(): boolean;

    /**
     * Deletes the augls' memories.
     */
    public deleteAugls(): void {
        if (!Memory.creeps) return;
        const augls = this.getAugls();
        for (const augl of augls) {
            delete Memory.creeps[augl.name];
        }
    }
}