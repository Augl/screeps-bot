import { IScreepsGame } from "screeps";
import { IMemory } from "types";
import { Levels, PlatoonType, ResourceType, WorldType } from "util/Constants";
import { Names } from "util/Names";
import { AuglBase } from "./augl/Base";
import { PlatoonBase } from "./military/platoons/PlatoonBase";
import { SmallDefensePlatoon } from "./military/platoons/SmallDefensePlatoon";
import { TinyDefensePlatoon } from "./military/platoons/TinyDefensePlatoon";
import { SquadBase } from "./military/squads/SquadBase";
import { WorldBase } from "./world/Base";
import { Colony } from "./world/Colony";
import { Confederate } from "./world/Confederate";
import { Empty } from "./world/Empty";
import { Sector } from "./world/Sector";
import { UnknownRoom } from "./world/Unknown";
import { Warzone } from "./world/Warzone";

declare const Game: IScreepsGame;
declare const Memory: IMemory;

/**
 * The empire is the top most object in the hierarchy and controlls the rooms.
 */
export class Empire {

    private sectors: Sector[] = [];

    private colonies: Colony[] = [];

    private confederates: Confederate[] = [];

    private world: {[name: string]: WorldBase | undefined} = {};

    private squads: {[name: string]: SquadBase | undefined} = {};

    private platoons: {[roomName: string]: PlatoonBase[] | undefined} = {};

    private platoonMap: {[name: string]: PlatoonBase | undefined} = {};

    private roomIndex = 0;

    private lastExecutedRoomIndex = 0;

    constructor() {
        console.log("Initializing Empire");
        this.initializeRooms();
        this.initializePlatoons();
    }

    /**
     * Initializes the assigned world objects for all roooms.
     */
    private initializeRooms(): void {
        // init currently visible rooms.
        for (const roomName in Game.rooms) {
            this.initializeRoom(roomName, this.roomIndex);
            this.roomIndex++;
        }
        // initialize rooms that have memory but are not currently visible
        for (const roomName in Memory.rooms) {
            if (this.world[roomName] == undefined) {
                this.initializeRoom(roomName, this.roomIndex);
                this.roomIndex++;
            }
        }
    }

    /**
     * Initializes one room.
     * @param roomName The name of the room.
     */
    private initializeRoom(roomName: string, index: number): void {
        const type = Memory.rooms?.[roomName]?.type;
        if (type == WorldType.SECTOR) {
            console.log("Initializing Sector...");
            const sector = new Sector(roomName, this, index);
            this.sectors.push(sector);
            this.world[roomName] = sector;
        }
        else if (type == WorldType.ALLIED) {
            console.log("Initializing Confederate...");     
            const confederate = new Confederate(roomName, this, index);
            this.confederates.push(confederate);
            this.world[roomName] = confederate;
        }
        else if (type == WorldType.COLONY) {
            console.log("Initializing Colony...");     
            const colony = new Colony(roomName, this, index);
            this.colonies.push(colony);
            this.world[roomName] = colony;
        }
        else if (type == WorldType.WARZONE) {
            console.log("Initializing Warzone...");     
            const warzone = new Warzone(roomName, this, index);
            this.world[roomName] = warzone;
        }
        else if (type == WorldType.EMPTY) {
            console.log("Initializing Empty...");     
            const empty = new Empty(roomName, this, index);
            this.world[roomName] = empty;
        }
        else {
            console.log("Initializing UnknownRoom...", roomName);
            const unknown = new UnknownRoom(roomName, this, index);
            this.world[roomName] = unknown;
        }
    }

    /**
     * Initializes all platoons.
     */
    private initializePlatoons(): void {
        for (const platoonName in Memory.platoons) {
            this.initializePlatoon(platoonName);
        }
    }

    /**
     * Initializes one platoon.
     * @param platoonName The name of the platoon.
     */
    private initializePlatoon(platoonName: string): void {
        const type = Memory.platoons?.[platoonName]?.type;
        const room = Memory.platoons?.[platoonName]?.targetRoom;
        if (!room) {
            console.log("No room to initialize platoon!");
            return;
        }
        const platoons = this.platoons[room] ?? [];
        let newPlatoon;
        if (type == PlatoonType.TINY_DEFENSE) {
            console.log("Initializing TinyDefensePlatoon...");
            newPlatoon = new TinyDefensePlatoon(platoonName, this);
        }
        else if (type == PlatoonType.SMALL_DEFENSE) {
            console.log("Initializing SmallDefensePlatoon...");
            newPlatoon = new SmallDefensePlatoon(platoonName, this);
        }
        if (!newPlatoon) return;
        platoons.push(newPlatoon);
        this.platoons[room] = platoons;
        this.platoonMap[newPlatoon.getName()] = newPlatoon;
    }

    /**
     * @param room The room to check for.
     * @returns The number of platoons assigned to the given room.
     */
    public numberOfPlatoons(room: string): number {
        return this.platoons[room]?.length ?? 0;
    }

    /**
     * Runs all sector's AIs.
     */
    public run(): void {
        let limit = Game.cpu.limit - (Game.cpu.limit/10);
        if (Game.cpu.bucket > 5000) {
            limit = 500;
        }
        let cpuSum = 0;
        let executedRooms = 0;
        const rooms = Object.values(this.world);
        for (let i = 0; i < rooms.length; i++) {
            const sector = rooms[this.lastExecutedRoomIndex];
            if (sector) {
                const startCPU = Game.cpu.getUsed();
                sector.run();
                cpuSum += Game.cpu.getUsed() - startCPU;
                executedRooms++;
            }
            this.lastExecutedRoomIndex = (this.lastExecutedRoomIndex+1)%rooms.length;
            if (cpuSum/executedRooms > limit - Game.cpu.getUsed()) {
                break;
            }
        }
        for (const rooms in this.platoons) {
            const platoons = this.platoons[rooms] ?? [];
            for (const p of platoons) {
                if (p.wasEliminated()) {
                    const targetRoom = p.getTargetRoom();
                    if (targetRoom) {
                        this.platoons[targetRoom] = this.platoons[targetRoom]?.filter(p => !p.wasEliminated());
                    }
                    if (Memory.platoons) Memory.platoons[p.getName()] = undefined;
                    p.deleteSquads();
                }
                else p.run();
            }
        }
    }

    /**
     * Adds the given squad to the empire.
     * @param squad The squad to add.
     */
    public addSquad(squad: SquadBase): void {
        this.squads[squad.getName()] = squad;
    }

    /**
     * Removes the given squad.
     * @param squad The squad to deregister.
     */
    public removeSquad(squad: SquadBase): void {
        console.log("Remove squad", squad.getName());
        delete this.squads[squad.getName()];
    }

    /**
     * Adds the given augl to it's home sector or corresponding squad.
     * @param augl The augl to add.
     */
    public addAugl(augl: AuglBase): void {
        const sector = this.world[augl.memory.home ?? ""];
        if (sector) {
            sector?.addAugl(augl);
        }
        else {
            const squad = this.squads[augl.memory.home ?? ""];
            if (squad) {
                squad.addAugl(augl);
            }
        }
    }

    /**
     * Processes a room that had it's type changed.
     * @param room The room that was reclassified.
     */
    public reclassifyRoom(room: WorldBase): void {
        console.log("Reclassifying room " + room.roomName);
        // remove rooms in the wrong array
        this.sectors = this.sectors.filter(s => s.getType() == WorldType.SECTOR);
        this.confederates = this.confederates.filter(c => c.getType() == WorldType.ALLIED);
        // recreate as new object
        this.initializeRoom(room.roomName, room.roomIndex);
    }

    /**
     * Returns the world object for the given name.
     * @param roomName The name of the room.
     * @returns The world object.
     */
    public getWorld(roomName: string): WorldBase | undefined {
        let room = this.world[roomName];
        if (!room && Game.rooms[roomName]) {
            this.initializeRoom(roomName, this.roomIndex);
            this.roomIndex++;
            room = this.world[roomName];
        }
        return room;
    }

    /**
     * @returns All sectors in the empire.
     */
    public getAllSectors(): Sector[] {
        return this.sectors;
    }

    /**
     * Requests a platoon from the empire.
     * @param requestingRoom The room where the platoon should be active in.
     * @param platoonType The type of platoon to send.
     */
    public requestPlatoon(requestingRoom: WorldBase, platoonType: PlatoonType): void {
        // find all rooms nearby that contain a platoon
        const rooms = this.findClosestWith(requestingRoom.roomName, r => {
            return this.platoons[r.roomName] != undefined;
        }, true, 2);
        // tell all of them to defend the calling room and decide if there needs to be a new one
        let allAreWeaker = true;
        for (const room of rooms.found) {
            const platoon = this.platoons[room.roomName]?.[0];
            if (platoon && platoon.isReady()) {
                allAreWeaker = allAreWeaker && (platoon.isWeakerThan(platoonType) ?? true);
                this.relocatePlatoon(platoon, requestingRoom);
            }
        }
        // spawn new platoon if needed
        if (allAreWeaker) {
            const platoonName = Names.getPlatoonName();
            if (!Memory.platoons) Memory.platoons = {};
            Memory.platoons[platoonName] = {
                targetRoom: requestingRoom.roomName,
                type: platoonType
            };
            console.log("Spawn new platoon");
            this.initializePlatoon(platoonName);
        }
    }

    /**
     * Updates the target room of the given platoon.
     * @param platoon The platoon to relocate.
     * @param targetRoom The new target room.
     */
    public relocatePlatoon(platoon: PlatoonBase, targetRoom: WorldBase): void {
        const room = this.getWorld(platoon.getTargetRoom() ?? "");
        if (!room) {
            console.log("Cannot relocate platoon: No current target room");
            return;
        }
        platoon.setTargetRoom(targetRoom.roomName);
        // remove from current platoon list
        this.platoons[room.roomName] = this.platoons[room.roomName]?.filter(p => p.getName() != platoon.getName());
        // add to new one
        const platoonList = this.platoons[targetRoom.roomName] ?? [];
        platoonList.push(platoon);
        this.platoons[targetRoom.roomName] = platoonList;
        console.log("Relocating platoon", platoon.getName(), "from", room.roomName);
    }

    /**
     * @param name The name of the platoon.
     * @returns The platoon with the given name or undefined.
     */
    public getPlatoon(name: string): PlatoonBase | undefined {
        return this.platoonMap[name];
    }

    /**
     * Returns the squad for the given name.
     * @param name The name of the squad.
     * @returns The squad object.
     */
    public getSquad(name: string): SquadBase | undefined {
        return this.squads[name];
    }

    /**
     * Finds the room of given type that is closest to the provided room.
     * @param room The room to be close to.
     * @param type The type of the desired room.
     * @returns The closest room of the given type or undefined.
     */
    public getClosestRoomByType(room: WorldBase, type: WorldType): WorldBase | undefined {
        const result = this.findClosestWith(room.roomName, w => w.getType() == type, false, 100);
        return result.found[0];
    }

    /**
     * Returns the closest room that is able to spawn augls.
     * @param room The room to start searching from.
     * @param level The minimal level of augl that should be spawnable.
     * @returns A room that can spawn augls, or undefined.
     */
    public getClosestSpawnRoom(room: string, level: number): WorldBase | undefined {
        // try to find room with high enough level and plenty of energy.
        const result = this.findClosestWith(room, w => 
            w.getLevel() >= level && w.getEnergyCapacity() >= Levels[level] && w.getStoredEnergy() > 100, 
            false, 5
        );
        if (result.found[0]) {
            return result.found[0];
        }
        // return any room with high enough level
        return this.findClosestWith(room, w => w.getLevel() >= level && w.getEnergyCapacity() >= Levels[level], false, 5).found[0];
    }

    /**
     * Returns the closest room that is able to store more energy.
     * @param room The room to start searching from.
     * @returns A room that can store energy, or undefined.
     */
    public getClosestStorageRoom(room: WorldBase): WorldBase | undefined {
        const result = this.findClosestWith(room.roomName, w => {
            return w.getType() == WorldType.SECTOR && (
                (w.getLevel() < 8 && w.getBuildings()?.getStorage()?.isFull() == false) ||
                (w.getLevel() == 8 && w.getBuildings()?.getStorage()?.hasResource(850000, ResourceType.ENERGY) == false)
            )
        }, false, 3);
        return result.found[0];
    }

    /**
     * Finds the path from the start room to the target room.
     * @param start The room to start from.
     * @param target The room to go to.
     * @returns The path of rooms to cross.
     */
    public getPath(start: string, target: string): string[] {
        const result = this.findClosestWith(start, w => w.roomName == target, false, 100, false);
        const preMap = result.preMap;
        const path = [target];
        let current = preMap.get(target);
        while (current != undefined && current.roomName != start) {
            path.push(current.roomName);
            current = preMap.get(current.roomName);
        }
        path.reverse();
        return path;
    }

    /**
     * Performs breadth-first search on the known rooms to find all that satisfy a given predicate.
     * @param start The name of the room to start from.
     * @param pred The predicate the deisred rooms should satisfy.
     * @param findAll If not only the closest but all rooms should be found.
     * @param maxDistance The greates allowed distance.
     * @param allowHostile If rooms controlled by hostile entities are allowed on the path.
     * 
     * @returns The found rooms and the predecessor map to calculate path, distance etc.
     */
    private findClosestWith(start: string, pred: (room: WorldBase) => boolean, findAll: boolean, maxDistance: number, allowHostile = true): {preMap: Map<string, WorldBase>, found: WorldBase[]} {
        const preMap = new Map<string, WorldBase>();
        const distanceMap = new Map<string, number>();
        const visited: {[name: string]: boolean} = {};
        const stack = [start];
        distanceMap.set(start, 0);
        const results: WorldBase[] = [];
        while (stack.length > 0) {
            const currentName = stack.shift();
            if (!currentName) continue;
            const currentDistance = distanceMap.get(currentName) ?? 0;
            if (currentDistance > maxDistance) continue;
            visited[currentName] = true;
            const current = this.world[currentName];
            if (!current) continue;
            if (!allowHostile && (current.getType() == WorldType.WARZONE || current.getType() == WorldType.ENEMY)) {
                continue;
            }
            if (pred(current)) {
                results.push(current);
                if (!findAll) break;
            }
            for (const roomName of current.getNeighbours()) {
                if (!visited[roomName]) {
                    preMap.set(roomName, current);
                    stack.push(roomName);
                    distanceMap.set(roomName, currentDistance + 1);
                }
            }
        }
        return {preMap: preMap, found: results};
    }
}