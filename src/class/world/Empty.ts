import { AIBase } from "ai/Base";
import { WorldAITask } from "util/Constants";
import { WorldBase } from "./Base";

export class Empty extends WorldBase {

    public getAIs(): { AIs: Map<WorldAITask, AIBase>; startTask: WorldAITask; } | undefined {
        return undefined;
    }
    
    public doTransition(currentTask: WorldAITask): WorldAITask[] {
        return [currentTask];
    }    
}