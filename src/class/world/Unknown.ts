import { AIBase } from "ai/Base";
import { AIUnknownClassify } from "ai/world/unknown/Classify";
import { WorldAITask } from "util/Constants";
import { WorldBase } from "./Base";

export class UnknownRoom extends WorldBase {

    public getAIs(): { AIs: Map<WorldAITask, AIBase>; startTask: WorldAITask; } | undefined {
        return {
            AIs: new Map([
                [WorldAITask.CLASSIFY, new AIUnknownClassify(this)]
            ]),
            startTask: WorldAITask.CLASSIFY
        };
    }

    public doTransition(currentTask: WorldAITask): WorldAITask[] {
        return [currentTask];
    }
    
}