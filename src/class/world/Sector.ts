import { AIBase } from "ai/Base";
import { AISectorExplore } from "ai/world/Explore";
import { AISectorBuild } from "ai/world/sector/Build";
import { AISectorChemistry } from "ai/world/sector/Chemistry";
import { AISectorDefend } from "ai/world/sector/Defend";
import { AISectorPopulation } from "ai/world/sector/Population";
import { AISectorSpawnCivilian } from "ai/world/sector/SpawnCivilian";
import { AISectorSpawnMilitary } from "ai/world/sector/SpawnMilitary";
import { AISectorTransfer } from "ai/world/sector/Transfer";
import { Empire } from "class/Empire";
import { IAuglMemory, ISpawnQueue } from "types";
import { Job, WorldAITask } from "util/Constants";
import { WorldBase } from "./Base";

export class Sector extends WorldBase {

    constructor(roomName: string, empire: Empire, roomIndex: number) {
        super(roomName, empire, roomIndex);
    }

    /**
     * Adds the given augl to the spawn queue.
     * @param job The job of the augl.
     * @param memory The initial memory.
     * @param level The level of augl that should be created.
     * @param home The home sector name.
     * @returns If the augl has been added to the queue.
     */
    public addToSpawnQueue(job: Job, memory: IAuglMemory, level: number, home: string): boolean {
        const sectorMem = this.getMemory();
        if (!sectorMem.sq) sectorMem.sq = {};
        if (sectorMem.sq[job]) {
            return false;
        }
        sectorMem.sq[job] = {home: home, memory: memory, level: level};
        this.setMemory(sectorMem);
        return true;
    }

    /**
     * Removes the augl from the spawn queue for the given job.
     * @param job The job of the augl.
     */
    public removeFromSpawnQueue(job: Job): void {
        const memory = this.getMemory();
        if (memory.sq) {
            delete memory.sq[job];
        }
        this.setMemory(memory);
    }

    /**
     * Returns the spawn queue.
     * @returns Object with augl to spawn per job.
     */
    public getSpawnQueue(): {[key: string]: ISpawnQueue | undefined} {
        return this.getMemory().sq || {};
    }

    /**
     * Returns the AIs to the parent AIRunner.
     * @returns A map of task IDs to AI modules.
     */
    public getAIs(): { AIs: Map<WorldAITask, AIBase>; startTask: WorldAITask; } | undefined {
        return {
            AIs: new Map<WorldAITask, AIBase>([
                [WorldAITask.BUILD, new AISectorBuild(this)],
                [WorldAITask.POPULATION, new AISectorPopulation(this)],
                [WorldAITask.SPAWN_CIVILIAN, new AISectorSpawnCivilian(this)],
                [WorldAITask.SPAWN_MILITARY, new AISectorSpawnMilitary(this)],
                [WorldAITask.EXPLORE, new AISectorExplore(this)],
                [WorldAITask.TRANSFER, new AISectorTransfer(this)],
                [WorldAITask.CHEMISTRY, new AISectorChemistry(this)],
                [WorldAITask.DEFENSE, new AISectorDefend(this)],
            ]),
            startTask: WorldAITask.POPULATION
        };
    }

    /**
     * Decides which AI to run next.
     * @param currentTask The previously executed task.
     * @returns An array of task IDs that can be run next.
     */
    public doTransition(currentTask: WorldAITask): WorldAITask[] {
        // return tasks ordered by priority and cooldown
        return [currentTask, WorldAITask.DEFENSE, WorldAITask.SPAWN_MILITARY, WorldAITask.SPAWN_CIVILIAN, WorldAITask.POPULATION, WorldAITask.BUILD, 
            WorldAITask.EXPLORE, WorldAITask.TRANSFER, WorldAITask.CHEMISTRY];
    }
}