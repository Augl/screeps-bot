import { AIBase } from "ai/Base";
import { AIConfederatePopulation } from "ai/world/confederate/Population";
import { AISectorExplore } from "ai/world/Explore";
import { WorldAITask } from "util/Constants";
import { WorldBase } from "./Base";

export class Confederate extends WorldBase {

    public getAIs(): { AIs: Map<WorldAITask, AIBase>; startTask: WorldAITask; } | undefined {
        return {
            AIs: new Map<WorldAITask, AIBase>([
                [WorldAITask.EXPLORE, new AISectorExplore(this)],
                [WorldAITask.POPULATION, new AIConfederatePopulation(this)]
            ]),
            startTask: WorldAITask.EXPLORE
        };
    }
    
    public doTransition(currentTask: WorldAITask): WorldAITask[] {
        if (currentTask == WorldAITask.EXPLORE) {
            return [WorldAITask.POPULATION, WorldAITask.EXPLORE];
        }
        return [WorldAITask.EXPLORE, WorldAITask.POPULATION];
    }    
}