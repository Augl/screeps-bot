import { AIBase } from "ai/Base";
import { AIColonyBuild } from "ai/world/colony/Build";
import { AIColonyDefend } from "ai/world/colony/Defend";
import { AIColonyPopulation } from "ai/world/colony/Population";
import { AISectorExplore } from "ai/world/Explore";
import { IMemory } from "types";
import { ResourceType, WorldAITask } from "util/Constants";
import { WorldBase } from "./Base";

declare const Memory: IMemory;

export class Colony extends WorldBase {

    private storageRoom?: WorldBase;

    public getAIs(): { AIs: Map<WorldAITask, AIBase>; startTask: WorldAITask; } | undefined {
        return {
            AIs: new Map<WorldAITask, AIBase>([
                [WorldAITask.EXPLORE, new AISectorExplore(this)],
                [WorldAITask.BUILD, new AIColonyBuild(this)],
                [WorldAITask.POPULATION, new AIColonyPopulation(this)],
                [WorldAITask.DEFENSE, new AIColonyDefend(this)]
            ]),
            startTask: WorldAITask.EXPLORE
        };
    }
    
    public doTransition(currentTask: WorldAITask): WorldAITask[] {
        // return tasks ordered by priority
        return [currentTask, WorldAITask.DEFENSE, WorldAITask.BUILD, WorldAITask.POPULATION, WorldAITask.EXPLORE];
    }
    
    public getStorageRoom(): WorldBase | undefined {
        if (!this.storageRoom || this.storageRoom.getBuildings()?.getStorage()?.isFull() == true ||
            (this.storageRoom.getLevel() == 8 && this.storageRoom.getBuildings()?.getStorage()?.hasResource(950000, ResourceType.ENERGY))        
        ) {
            this.storageRoom = this.empire.getClosestStorageRoom(this);
        }
        return this.storageRoom;
    }

    public run(): void {
        super.run();
        if (Memory.debugVisuals) {
            this.room.value(true)?.visual.text("Storage room: " + (this.storageRoom?.roomName ?? "unknown"), 5, 1);
        }
    }
}