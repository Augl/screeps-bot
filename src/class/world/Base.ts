import { AIRunner } from "ai/AIRunner";
import { AIBase } from "ai/Base";
import { AuglBase } from "class/augl/Base";
import { Builder } from "class/augl/civilian/Builder";
import { Carrier } from "class/augl/civilian/Carrier";
import { Chemist } from "class/augl/civilian/Chemist";
import { Claimer } from "class/augl/civilian/Claimer";
import { DefenseBuilder } from "class/augl/civilian/DefenseBuilder";
import { Distributer } from "class/augl/civilian/Distributer";
import { Explorer } from "class/augl/civilian/Explorer";
import { Harvester } from "class/augl/civilian/Harvester";
import { InterCarrier } from "class/augl/civilian/InterCarrier";
import { Repairman } from "class/augl/civilian/Repairman";
import { Reserver } from "class/augl/civilian/Reserver";
import { Upgrader } from "class/augl/civilian/Upgrader";
import { BuildingRegistry } from "class/buildings/BuildingRegistry";
import { Controller } from "class/buildings/Controller";
import { Empire } from "class/Empire";
import { MineralSource } from "class/resources/Mineral";
import { EnergySource } from "class/resources/source";
import { WorldObject } from "class/WorldObject";
import { IScreepsCreep, IScreepsGame, IScreepsMineral, IScreepsPathFinder, IScreepsPathStep, IScreepsRoom, IScreepsRoomPosition, IScreepsSource } from "screeps";
import { IMemory, IPosition, ISectorMemory } from "types";
import { Allies, FindType, Job, LookType, Result, StructureType, TerrainType, WorldAITask, WorldType } from "util/Constants";

declare const Memory: IMemory;
declare const Game: IScreepsGame;
declare const RoomPosition: IScreepsRoomPosition;
declare const PathFinder: IScreepsPathFinder;

export abstract class WorldBase extends AIRunner<WorldAITask, AIBase> {

    public roomName: string;

    public readonly roomIndex: number;

    protected room: WorldObject<IScreepsRoom | undefined>;

    private sources?: {[ID: string]: EnergySource};

    private mineral?: MineralSource;

    public augls: {[job: string]: AuglBase[] | undefined} = {};

    public empire: Empire;

    private controller?: Controller;

    private buildings: BuildingRegistry;

    private isUnderAttack = false;

    private enemyCreeps: IScreepsCreep[] = [];

    constructor(roomName: string, empire: Empire, roomIndex: number) {
        super();
        this.roomIndex = roomIndex;
        this.roomName = roomName;
        this.room = new WorldObject(() => Game.rooms[roomName]);
        this.empire = empire;
        this.buildings = new BuildingRegistry(this);
        this.initializeAugls();
    }

    public run(): void {
        // check if room is inactive
        if (!this.getMemory().inactive){
            // check if the room is under attack
            this.checkForEnemies();
            // run AIs
            super.runAI();
            // run all augls
            this.runAugls();
            // run all buildings
            this.buildings.run();
        }
    }

    /**
     * Returns the sector's memory, use setMemory to make the changes persistant.
     * @returns The sector's memory.
     */
    public getMemory(): ISectorMemory {
        return Memory.rooms?.[this.roomName] ?? {};
    }

    /**
     * Sets the sector's memory.
     */
     public setMemory(memory: ISectorMemory): void {
        if (!Memory.rooms) Memory.rooms = {};
        Memory.rooms[this.roomName] = memory;
    }

    /**
     * Looks for enemies in the sector and decides if the sector is under attack.
     */
    private checkForEnemies(): void {
        if (this.isUnderAttack) {
            const creeps = this.find<IScreepsCreep>(FindType.FIND_HOSTILE_CREEPS);
            this.enemyCreeps = creeps.filter(c => !Allies.includes(c.owner.username));
            if (this.enemyCreeps.length == 0) {
                console.log("Sector is no longer under attack.");
                this.isUnderAttack = false;
            }
        }
        else if (Game.time % 15 == 3) {
            const creeps = this.find<IScreepsCreep>(FindType.FIND_HOSTILE_CREEPS);
            this.enemyCreeps = creeps.filter(c => !Allies.includes(c.owner.username));
            if (this.enemyCreeps.length > 0) {
                console.log("Sector under attack!");
                this.isUnderAttack = true;
            }
        }
    }

    /**
     * Runs the AI of all augls.
     */
    private runAugls(): void {
        for (const job in this.augls) {
            const augls = this.augls[job];
            if (!augls) continue;
            const livingAugls = [];
            for (const augl of augls) {
                if (augl.getTicksToLive() > 0) {
                    livingAugls.push(augl);
                    augl.run();
                }
                else {
                    delete Memory.creeps?.[augl.name];
                }
            }
            this.augls[job] = livingAugls;
        }
    }

    /**
     * Initializes all augls of the room.
     */
    private initializeAugls(): void {
        for (const name in Game.creeps) {
            if (Game.creeps[name]?.memory.home != this.roomName) continue;
            const job = Game.creeps[name]?.memory.job;
            if (!job) continue;
            if (!this.augls[job]) this.augls[job] = [];
            if (job == Job.HARVESTER) {
                this.augls[job]?.push(new Harvester(name, this));
            }
            else if (job == Job.UPGRADER) {
                this.augls[job]?.push(new Upgrader(name, this));
            }
            else if (job == Job.BUILDER) {
                this.augls[job]?.push(new Builder(name, this));
            }
            else if (job == Job.REPAIRMAN) {
                this.augls[job]?.push(new Repairman(name, this));
            }
            else if (job == Job.CARRIER) {
                this.augls[job]?.push(new Carrier(name, this));
            }
            else if (job == Job.DISTRIBUTER) {
                this.augls[job]?.push(new Distributer(name, this));
            }
            else if (job == Job.BUILDER_DEFENSE) {
                this.augls[job]?.push(new DefenseBuilder(name, this));
            }
            else if (job == Job.EXPLORER) {
                this.augls[job]?.push(new Explorer(name, this));
            }
            else if (job == Job.CLAIMER) {
                this.augls[job]?.push(new Claimer(name, this));
            }
            else if (job == Job.RESERVER) {
                this.augls[job]?.push(new Reserver(name, this));
            }
            else if (job == Job.INTERCARRIER) {
                this.augls[job]?.push(new InterCarrier(name, this));
            }
            else if (job == Job.CHEMIST) {
                this.augls[job]?.push(new Chemist(name, this));
            }
        }
    }

    /**
     * @returns Returns the underlying room object.
     */
    public getUnderlying(): IScreepsRoom | undefined {
        return this.room.value(false);
    }

    /**
     * Adds the given augl to the sector.
     * @param augl The augl to add.
     */
    public addAugl(augl: AuglBase): void {
        if (!augl.memory.job) return;
        if (!this.augls[augl.memory.job]) this.augls[augl.memory.job] = [];
        this.augls[augl.memory.job]?.push(augl);
    }

    /**
     * @returns All augls in this room.
     */
    public getAllAugls(): {[job: string]: AuglBase[] | undefined} {
        return this.augls;
    }

    /**
     * Returns the augls with the given job.
     * @param job The job to filter by.
     * @returns Array of Augls with the given job.
     */
    public getAuglsWithJob(job: Job): AuglBase[] {
        return this.augls[job] || [];
    }

    /**
     * Returns the available energy to spawn.
     * @returns The amount of available energy.
     */
    public getAvailableEnergy(): number {
        return this.room.value(false)?.energyAvailable || 0;
    }

    /**
     * Returns the available energy capacity in the room.
     * @returns The energy capacity.
     */
    public getEnergyCapacity(): number {
        return this.room.value(false)?.energyCapacityAvailable || 0;
    }

    /**
     * Returns the amount of energy available in centre stores.
     * @returns Amount of stored energy.
     */
    public getStoredEnergy(): number {
        const storage = this.buildings.getStorage();
        const centreStores = this.buildings.getCentreStores();
        return centreStores.reduce((acc, store) => acc + store.getUsedCapacity(), storage?.getUsedCapacity() ?? 0);
    }

    /**
     * @returns The building registry of the room.
     */
    public getBuildings(): BuildingRegistry | undefined {
        if (this.room.value(false) == undefined) return undefined;
        return this.buildings;
    }

    /**
     * Returns all sources in the room.
     * @returns Energy sources in the room.
     */
    public getSources(): EnergySource[] {
        if (!this.sources && this.room) {
            const sources = this.find<IScreepsSource>(FindType.FIND_SOURCES);
            const result: {[ID: string]: EnergySource} = {};
            for (const s of sources) {
                // look at area around the source
                const map = this.lookForAtArea(LookType.LOOK_TERRAIN, s.pos.y-1, s.pos.x-1, s.pos.y+1, s.pos.x+1);
                const workspaces = [];
                for (const m of map as {x: number, y: number, terrain: TerrainType}[]) {
                    if (m.terrain != TerrainType.WALL) {
                        workspaces.push(new RoomPosition(m.x, m.y, this.roomName));
                    }
                }
                result[s.id] = new EnergySource(workspaces, s.id);
            }
            this.sources = result;
        }
        const result = [];
        for (const id in this.sources) {
            result.push(this.sources[id]);
        }
        return result;
    }

    /**
     * Returns the source with the given id.
     * @param id The id of the source.
     * @returns The source.
     */
    public getSource(id: string): EnergySource | undefined {
        if (!this.sources) this.getSources();
        return this.sources?.[id];
    }

    /**
     * @returns The mineral deposit in the room.
     */
    public getMineral(): MineralSource | undefined {
        if (!this.mineral) {
            const mineral = this.find<IScreepsMineral>(FindType.FIND_MINERALS)[0];
            if (!mineral) return;
            this.mineral = new MineralSource(mineral.id);
        }
        return this.mineral;
    }

    /**
     * Creates a new construction site.
     * @param pos The desired position of the construction site.
     * @param type The desired structure to build.
     * @param name The name of the structure, only for Spawns.
     * @returns The action result.
     */
    public createConstructionSite(x: number, y: number, type: StructureType, name?: string): Result {
        const room = this.room.value(true);
        if (!room) return Result.ERR_UNKNOWN;
        this.buildings.addConstructionSite();
        return room.createConstructionSite(x, y, type, name);
    }

    /**
     * @returns The controller in the room.
     */
    public getController(): Controller | undefined {
        const room = this.room.value(false);
        if (!this.controller && room && room.controller) {
            this.controller = new Controller(room.controller.id);
        }
        if (!room) {
            return undefined;
        }
        return this.controller;
    }

    /**
     * Returns the terrain at the given point.
     * @param x X coordinate of the point.
     * @param y Y coordinate of the poin.
     * @returns The terrain type.
     */
    public getTerrainAt(x: number, y: number): TerrainType {
        const terrains = this.lookForAt(LookType.LOOK_TERRAIN, x, y) as TerrainType[];
        return terrains[0];
    }

    /**
     * Finds all objects of the given type in the room. Results are not cached.
     * @param type Type of object to look for.
     * @returns Array of found objects.
     */
    public find<T>(type: FindType): T[] {
        return this.room.value(true)?.find(type) || [];
    }

    /**
     * Gets an object with the given type at the specified room position.
     * @param type One of the LookType constants.
     * @param x X position in the room.
     * @param y Y position in the room.
     * @returns An array of found objects.
     */
    public lookForAt(type: LookType, x: number, y: number): unknown[] {
        return this.room.value(true)?.lookForAt(type, x, y) || [];
    }

    /**
     * Gets the list of objects with the given type at the specified room area.
     * @param type One of the LookType constants.
     * @param top The top Y boundary of the area.
     * @param left The left X boundary of the area.
     * @param bottom The bottom Y boundary of the area.
     * @param right The right X boundary of the area.
     * @returns Array of found objects.
     */
    public lookForAtArea(type: LookType, top: number, left: number, bottom: number, right: number): unknown[] {
        return this.room.value(true)?.lookForAtArea(type, top, left, bottom, right, true) || [];
    }

    /**
     * @returns The current level of the controller in the room.
     */
    public getLevel(): number {
        const controller = this.getController();
        if (!controller) return -1;
        return controller.getLevel();
    }

    /**
     * @returns Map of directions to neighbouring rooms.
     */
    public describeExits(): {"1": string | undefined, "3": string | undefined, "5": string | undefined, "7": string | undefined} {
        // only used when building walls, no need for caching
        return Game.map.describeExits(this.roomName);
    }

    /**
     * @returns Array of room names adjacent to this room.
     */
    public getNeighbours(): string[] {
        const memory = this.getMemory();
        if (!memory.neighbours) {
            const map = Game.map.describeExits(this.roomName);
            const neighbours = [map[1], map[3], map[5], map[7]];
            memory.neighbours = neighbours.filter(n => n != undefined) as string[];
        }
        this.setMemory(memory);
        return memory.neighbours;
    }

    /**
     * @returns The type of room.
     */
    public getType(): WorldType {
        return Memory.rooms?.[this.roomName]?.type ?? WorldType.UNKNOWN;
    }

    /**
     * Reclassifies the room with the empire.
     * @param type The new type of room.
     */
    public changeClassification(type: WorldType): void {
        this.getMemory().type = type;
        this.empire.reclassifyRoom(this);
    }

    /**
     * Finds the path between the given positions.
     * @param from The start position.
     * @param to The end position.
     * @param range The range to the end position that is good enough.
     * @param ignoreCreeps If other creeps should be ignored during search.
     * @returns A path.
     */
    public findPath(from: IPosition, to: IPosition, range: number, ignoreCreeps: boolean): IScreepsPathStep[] {
        const room = this.room.value(true);
        if (!room) return [];
        const start = new RoomPosition(from.x, from.y, this.roomName);
        const end = new RoomPosition(to.x, to.y, this.roomName);
        return room.findPath(start, end, {
            ignoreCreeps: ignoreCreeps,
            range: range,
            maxOps: 5000,
            maxRooms: 1,
        });
    }

    /**
     * Finds the path to the given neighbouring room.
     * @param from The start position.
     * @param to The name of the neighbouring room to find a path to.
     * @param ignoreCreeps If other creeps should be ignored during search.
     * @returns A path.
     */
    public findPathToNeighbour(from: IPosition, to: string, ignoreCreeps: boolean): IPosition[] {
        const room = this.room.value(true);
        if (!room) {
            console.log("FindPathToNeighbour: room is undefined", this.roomName, this.room.value(false))
            return [];
        }
        const start = new RoomPosition(from.x, from.y, this.roomName);
        const end = new RoomPosition(25, 25, to);
        const path = PathFinder.search(start, { pos: end, range: 20 }, {
            plainCost: 2,
            swampCost: 10,
            maxRooms: 2,
            maxOps: 5000,
            roomCallback: (roomName: string) => {
                const room = this.empire.getWorld(roomName);
                if (!room) {
                    console.log("findPathToNeighbour: Room not found: ", roomName);
                    return;
                }
                if (roomName != this.roomName && roomName != to) {	
                    console.log("findPathToNeighbour: Not considering room " + roomName + " when searching from " + this.roomName + " to " + to);	
                    return false;	
                }
                if (!ignoreCreeps){
                    const matrix = room.getBuildings()?.costMatrix.clone();
                    if (!matrix) return;
                    const creeps = room.find(FindType.FIND_CREEPS) as IScreepsCreep[];
                    for (const creep of creeps) {
                        matrix.set(creep.pos.x, creep.pos.y, 255);
                    }
                    return matrix;
                }
                return room.getBuildings()?.costMatrix;
            }
        });
        return path.path;
    }

    /**
     * @returns An array of enemy creeps.
     */
    public getEnemies(): IScreepsCreep[] {
        return this.enemyCreeps;
    }

    /**
     * @returns True if the room is under my control.
     */
    public isMine(): boolean {
        if (this.getMemory().inactive) return false;
        return this.getType() == WorldType.SECTOR || this.getType() == WorldType.COLONY;
    }
}