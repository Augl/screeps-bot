import { AIAuglBuildGeneral } from "ai/augl/civilian/BuildGeneral";
import { AIAuglDepositSource } from "ai/augl/civilian/DepositSource";
import { AIAuglDepositSpawn } from "ai/augl/civilian/DepositSpawn";
import { AIAuglHarvest } from "ai/augl/civilian/Harvest";
import { AIAuglUpgrade } from "ai/augl/civilian/Upgrade";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Harvester extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map([
                [AuglAITask.HARVEST, new AIAuglHarvest(this, this.home)],
                [AuglAITask.DEPOSIT_SOURCE, new AIAuglDepositSource(this, this.home)],
                [AuglAITask.DEPOSIT_SPAWN, new AIAuglDepositSpawn(this, this.home)],
                [AuglAITask.BUILD_GENERAL, new AIAuglBuildGeneral(this, this.home)],
                [AuglAITask.UPGRADE, new AIAuglUpgrade(this, this.home)]
            ]),
            startTask: AuglAITask.HARVEST
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        if (currentTask == AuglAITask.HARVEST) {
            if (super.isFull()) {
                return [AuglAITask.DEPOSIT_SOURCE, AuglAITask.DEPOSIT_SPAWN, AuglAITask.BUILD_GENERAL, AuglAITask.UPGRADE];
            }
            return [AuglAITask.HARVEST];
        }
        else {
            if (super.isEmpty()) {
                return [AuglAITask.HARVEST];
            }
            return [currentTask, AuglAITask.DEPOSIT_SOURCE, AuglAITask.BUILD_GENERAL, AuglAITask.DEPOSIT_SPAWN, AuglAITask.UPGRADE];
        }
    }
}