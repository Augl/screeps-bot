import { AIAuglBuildDefense } from "ai/augl/civilian/BuildDefense";
import { AIAuglBuildGeneral } from "ai/augl/civilian/BuildGeneral";
import { AIAuglDepositAny } from "ai/augl/civilian/DepositAny";
import { AIAuglRepair } from "ai/augl/civilian/Repair";
import { AIAuglUpgrade } from "ai/augl/civilian/Upgrade";
import { AIAuglWithdrawAny } from "ai/augl/civilian/WithdrawAny";
import { AIAuglWait } from "ai/augl/Wait";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Repairman extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.WITHDRAW_ANY, new AIAuglWithdrawAny(this, this.home)],
                [AuglAITask.REPAIR, new AIAuglRepair(this, this.home)],
                [AuglAITask.BUILD_GENERAL, new AIAuglBuildGeneral(this, this.home)],
                [AuglAITask.BUILD_DEFENSE, new AIAuglBuildDefense(this, this.home)],
                [AuglAITask.UPGRADE, new AIAuglUpgrade(this, this.home)],
                [AuglAITask.WAIT, new AIAuglWait(this)],
                [AuglAITask.DEPOSIT_ANY, new AIAuglDepositAny(this, this.home)]
            ]),
            startTask: AuglAITask.WITHDRAW_ANY
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        if (this.getTicksToLive() < 50) {
            return [AuglAITask.DEPOSIT_ANY, AuglAITask.WAIT];
        }
        if (currentTask == AuglAITask.REPAIR || currentTask == AuglAITask.BUILD_DEFENSE || currentTask == AuglAITask.BUILD_GENERAL || 
            currentTask == AuglAITask.UPGRADE || currentTask == AuglAITask.WAIT) {
            if (!super.isEmpty()) {
                // spend the energy
                return [currentTask, AuglAITask.REPAIR, AuglAITask.BUILD_GENERAL, AuglAITask.BUILD_DEFENSE, AuglAITask.UPGRADE, AuglAITask.WAIT];
            }
            // gather energy
            return [AuglAITask.WITHDRAW_ANY, AuglAITask.WAIT];
        }
        else if (currentTask == AuglAITask.WITHDRAW_ANY) {
            if (!super.isFull()) {
                // not done withdrawing yet
                return [AuglAITask.WITHDRAW_ANY, AuglAITask.WAIT];
            }
            // spend the energy
            return [AuglAITask.REPAIR, AuglAITask.BUILD_GENERAL, AuglAITask.BUILD_DEFENSE, AuglAITask.UPGRADE, AuglAITask.WAIT];
        }
        throw new Error("Unexpected state for Builder: " + currentTask);
    }
}