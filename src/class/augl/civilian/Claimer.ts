import { AIAuglClaim } from "ai/augl/civilian/Claim";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Claimer extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.CLAIM, new AIAuglClaim(this, this.home)]
            ]),
            startTask: AuglAITask.CLAIM
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        return [currentTask];
    }
}