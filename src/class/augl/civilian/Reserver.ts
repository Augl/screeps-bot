import { AIAuglReserve } from "ai/augl/civilian/Reserve";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Reserver extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.RESERVE, new AIAuglReserve(this, this.home)]
            ]),
            startTask: AuglAITask.RESERVE
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        return [currentTask];
    }
}