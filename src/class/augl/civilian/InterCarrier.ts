import { AIAuglDepositRemote } from "ai/augl/civilian/DepositRemote";
import { AIAuglWithdrawSource } from "ai/augl/civilian/WithdrawSource";
import { AIAuglWait } from "ai/augl/Wait";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class InterCarrier extends AuglCivilianBase {

    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } | undefined {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.WITHDRAW_SOURCE, new AIAuglWithdrawSource(this, this.home)],
                [AuglAITask.DEPOSIT_REMOTE, new AIAuglDepositRemote(this, this.home)],
                [AuglAITask.WAIT, new AIAuglWait(this)]
            ]),
            startTask: AuglAITask.WITHDRAW_SOURCE
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        if (super.getTicksToLive() < 100) {
            return [AuglAITask.DEPOSIT_REMOTE, AuglAITask.WAIT];
        }
        if (currentTask == AuglAITask.WITHDRAW_SOURCE || currentTask == AuglAITask.WAIT) {
            if (!super.isFull()) {
                // did not withdraw yet
                return [AuglAITask.WITHDRAW_SOURCE, AuglAITask.WAIT];
            }
            // did withdraw
            return [AuglAITask.DEPOSIT_REMOTE, AuglAITask.WAIT];
        }
        else if (currentTask == AuglAITask.DEPOSIT_REMOTE) {
            if (super.isEmpty()) {
                // deposition done
                return [AuglAITask.WITHDRAW_SOURCE, AuglAITask.WAIT];
            }
            // did not deposit yet
            return [AuglAITask.DEPOSIT_REMOTE, AuglAITask.WAIT];
        }
        throw new Error("Unexpected state for InterCarrier " + currentTask);
    }
    
}