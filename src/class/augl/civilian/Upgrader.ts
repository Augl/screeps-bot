import { AIAuglHarvest } from "ai/augl/civilian/Harvest";
import { AIAuglUpgrade } from "ai/augl/civilian/Upgrade";
import { AIAuglWithdrawAny } from "ai/augl/civilian/WithdrawAny";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Upgrader extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map([
                [AuglAITask.HARVEST, new AIAuglHarvest(this, this.home)],
                [AuglAITask.WITHDRAW_ANY, new AIAuglWithdrawAny(this, this.home)],
                [AuglAITask.UPGRADE, new AIAuglUpgrade(this, this.home)]
            ]),
            startTask: AuglAITask.HARVEST
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        if (currentTask == AuglAITask.HARVEST) {
            if (super.isFull()) {
                return [AuglAITask.UPGRADE];
            }
            return [AuglAITask.WITHDRAW_ANY, AuglAITask.HARVEST];
        }
        else if (currentTask == AuglAITask.WITHDRAW_ANY) {
            if (!super.isFull()) {
                // not done withdrawing
                return [AuglAITask.WITHDRAW_ANY];
            }
            return [AuglAITask.UPGRADE];
        }
        else if (currentTask == AuglAITask.UPGRADE) {
            if (!super.isEmpty()) {
                return [AuglAITask.UPGRADE];
            }
            return [AuglAITask.WITHDRAW_ANY, AuglAITask.HARVEST];
        }
        throw new Error("Unexpected state for Upgrader " + currentTask);
    }
}