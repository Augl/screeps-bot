import { AIAuglDepositCentre } from "ai/augl/civilian/DepositCentre";
import { AIAuglDepositSpawn } from "ai/augl/civilian/DepositSpawn";
import { AIAuglDepositTerminal } from "ai/augl/civilian/DepositTerminal";
import { AIAuglDepositTower } from "ai/augl/civilian/DepositTower";
import { AIAuglWithdrawCentre } from "ai/augl/civilian/WIthdrawCentre";
import { AIAuglWithdrawCentreLink } from "ai/augl/civilian/WithdrawCentreLink";
import { AIAuglWithdrawSource } from "ai/augl/civilian/WithdrawSource";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Distributer extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.WITHDRAW_CENTRE, new AIAuglWithdrawCentre(this, this.home)],
                [AuglAITask.WITHDRAW_SOURCE, new AIAuglWithdrawSource(this, this.home)],
                [AuglAITask.DEPOSIT_SPAWN, new AIAuglDepositSpawn(this, this.home)],
                [AuglAITask.DEPOSIT_CENTRE, new AIAuglDepositCentre(this, this.home)],
                [AuglAITask.DEPOSIT_TERMINAL, new AIAuglDepositTerminal(this, this.home)],
                [AuglAITask.DEPOSIT_TOWER, new AIAuglDepositTower(this, this.home)],
                [AuglAITask.WITHDRAW_LINK_CENTRE, new AIAuglWithdrawCentreLink(this, this.home)],
            ]),
            startTask: AuglAITask.WITHDRAW_CENTRE
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        switch (currentTask) {

            case AuglAITask.WITHDRAW_CENTRE:
            case AuglAITask.WITHDRAW_SOURCE:
            case AuglAITask.WITHDRAW_LINK_CENTRE:
                if (super.isEmpty()) {
                    // augl did not withdraw energy yet
                    return [AuglAITask.WITHDRAW_LINK_CENTRE, currentTask, AuglAITask.WITHDRAW_CENTRE, AuglAITask.WITHDRAW_SOURCE];
                }
                // augl did withdraw, time to deposit
                return [AuglAITask.DEPOSIT_SPAWN, AuglAITask.DEPOSIT_TOWER, AuglAITask.DEPOSIT_TERMINAL, AuglAITask.DEPOSIT_CENTRE];

            case AuglAITask.DEPOSIT_SPAWN:
            case AuglAITask.DEPOSIT_CENTRE:
            case AuglAITask.DEPOSIT_TERMINAL:
            case AuglAITask.DEPOSIT_TOWER:
                if (!super.isEmpty()) {
                    // augl still carries energy, continue deposit
                    return [currentTask, AuglAITask.DEPOSIT_SPAWN, AuglAITask.DEPOSIT_TOWER, AuglAITask.DEPOSIT_TERMINAL, AuglAITask.DEPOSIT_CENTRE];
                }
                // augl is empty, get energy from container if there is still something to fill
                if (super.getAI(AuglAITask.DEPOSIT_SPAWN).shouldExecute() || super.getAI(AuglAITask.DEPOSIT_TOWER).shouldExecute() || 
                        super.getAI(AuglAITask.DEPOSIT_TERMINAL).shouldExecute()) {
                    return [AuglAITask.WITHDRAW_LINK_CENTRE, AuglAITask.WITHDRAW_CENTRE, AuglAITask.WITHDRAW_SOURCE];
                }
                return [AuglAITask.WITHDRAW_LINK_CENTRE, currentTask];

            default:
                throw new Error("Unexpected state " + currentTask + " for distributer!");
        }
    }
}