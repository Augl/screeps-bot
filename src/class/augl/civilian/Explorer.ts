import { AIAuglExplore } from "ai/augl/civilian/Explore";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Explorer extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.EXPLORE, new AIAuglExplore(this, this.home)]

            ]),
            startTask: AuglAITask.EXPLORE
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        return [currentTask];
    }
}