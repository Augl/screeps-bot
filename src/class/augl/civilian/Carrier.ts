import { AIAuglDepositCentre } from "ai/augl/civilian/DepositCentre";
import { AIAuglDepositSpawn } from "ai/augl/civilian/DepositSpawn";
import { AIAuglDepositTerminal } from "ai/augl/civilian/DepositTerminal";
import { AIAuglWithdrawCentre } from "ai/augl/civilian/WIthdrawCentre";
import { AIAuglWithdrawSource } from "ai/augl/civilian/WithdrawSource";
import { AIAuglWait } from "ai/augl/Wait";
import { AIBase } from "ai/Base";
import { AuglAITask, ResourceType } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Carrier extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.WITHDRAW_SOURCE, new AIAuglWithdrawSource(this, this.home)],
                [AuglAITask.DEPOSIT_CENTRE, new AIAuglDepositCentre(this, this.home)],
                [AuglAITask.WITHDRAW_CENTRE, new AIAuglWithdrawCentre(this, this.home)],
                [AuglAITask.DEPOSIT_SPAWN, new AIAuglDepositSpawn(this, this.home)],
                [AuglAITask.DEPOSIT_TERMINAL, new AIAuglDepositTerminal(this, this.home)],
                [AuglAITask.WAIT, new AIAuglWait(this)]
            ]),
            startTask: AuglAITask.WITHDRAW_SOURCE
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        switch (currentTask) {

            case AuglAITask.WITHDRAW_SOURCE:
            case AuglAITask.WITHDRAW_CENTRE:
                if (!super.isEmpty()) {
                    // augl did withdraw at least some energy from the container, time to bring it to the centre
                    if (super.getCarriedResource() != ResourceType.ENERGY) {
                        return [AuglAITask.DEPOSIT_TERMINAL];
                    }
                    return [AuglAITask.DEPOSIT_SPAWN, AuglAITask.DEPOSIT_CENTRE];
                }
                // augl is still empty, fill up at source
                return [currentTask, AuglAITask.WITHDRAW_SOURCE, AuglAITask.WITHDRAW_CENTRE];

            case AuglAITask.DEPOSIT_TERMINAL:
            case AuglAITask.WAIT:
            case AuglAITask.DEPOSIT_CENTRE:
            case AuglAITask.DEPOSIT_SPAWN:
                if (super.isEmpty()) {
                    // augl is empty, deposition complete
                    if (super.getAI(AuglAITask.WITHDRAW_SOURCE).shouldExecute()) {
                        // can withdraw energy from source
                        return [AuglAITask.WITHDRAW_SOURCE];
                    }
                    if (super.getAI(AuglAITask.DEPOSIT_SPAWN).shouldExecute()) {
                        // spawn or extensions could use energy, but there is none in source container, so get from centre
                        return [AuglAITask.WITHDRAW_CENTRE];
                    }
                    return [AuglAITask.WAIT];
                }
                // augl still has energy, still have to deposit
                return [currentTask, AuglAITask.DEPOSIT_CENTRE];
        
            default:
                throw new Error("Unexpected state " + currentTask + " for carrier!");
        }
    }
}