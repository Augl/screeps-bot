import { AIAuglBuildDefense } from "ai/augl/civilian/BuildDefense";
import { AIAuglBuildGeneral } from "ai/augl/civilian/BuildGeneral";
import { AIAuglDepositAny } from "ai/augl/civilian/DepositAny";
import { AIAuglDepositCentre } from "ai/augl/civilian/DepositCentre";
import { AIAuglDismantle } from "ai/augl/civilian/Dismantle";
import { AIAuglHarvest } from "ai/augl/civilian/Harvest";
import { AIAuglUpgrade } from "ai/augl/civilian/Upgrade";
import { AIAuglWithdrawAny } from "ai/augl/civilian/WithdrawAny";
import { AIAuglWait } from "ai/augl/Wait";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Builder extends AuglCivilianBase {

    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.HARVEST, new AIAuglHarvest(this, this.home)],
                [AuglAITask.WITHDRAW_ANY, new AIAuglWithdrawAny(this, this.home)],
                [AuglAITask.BUILD_GENERAL, new AIAuglBuildGeneral(this, this.home)],
                [AuglAITask.BUILD_DEFENSE, new AIAuglBuildDefense(this, this.home)],
                [AuglAITask.DISMANTLE, new AIAuglDismantle(this, this.home)],
                [AuglAITask.DEPOSIT_CENTRE, new AIAuglDepositCentre(this, this.home)],
                [AuglAITask.UPGRADE, new AIAuglUpgrade(this, this.home)],
                [AuglAITask.WAIT, new AIAuglWait(this)],
                [AuglAITask.DEPOSIT_ANY, new AIAuglDepositAny(this, this.home)]

            ]),
            startTask: AuglAITask.WITHDRAW_ANY
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        if (this.getTicksToLive() < 50) {
            return [AuglAITask.DEPOSIT_ANY, AuglAITask.WAIT];
        }
        if (currentTask == AuglAITask.BUILD_GENERAL || currentTask == AuglAITask.BUILD_DEFENSE ||
            currentTask == AuglAITask.DEPOSIT_CENTRE || currentTask == AuglAITask.UPGRADE || currentTask == AuglAITask.WAIT) {
            if (!super.isEmpty()) {
                // go on spending energy
                return [currentTask, AuglAITask.BUILD_GENERAL, AuglAITask.BUILD_DEFENSE, AuglAITask.UPGRADE, AuglAITask.DEPOSIT_CENTRE, AuglAITask.WAIT];
            }
            // harvest energy
            return [AuglAITask.DISMANTLE, AuglAITask.WITHDRAW_ANY, AuglAITask.HARVEST, AuglAITask.WAIT];
        }
        else if (currentTask == AuglAITask.HARVEST || currentTask == AuglAITask.WITHDRAW_ANY || currentTask == AuglAITask.DISMANTLE) {
            if (!super.isFull()) {
                // go on gathering energy
                return [AuglAITask.DISMANTLE, AuglAITask.WITHDRAW_ANY, AuglAITask.HARVEST, AuglAITask.WAIT];
            }
            // spend energy
            return [AuglAITask.BUILD_GENERAL, AuglAITask.BUILD_DEFENSE, AuglAITask.UPGRADE, AuglAITask.DEPOSIT_CENTRE, AuglAITask.WAIT];
        }
        throw new Error("Unexpected state for Builder: " + currentTask);
    }
}