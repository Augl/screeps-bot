import { WorldBase } from "class/world/Base";
import { AuglBase } from "../Base";

export abstract class AuglCivilianBase extends AuglBase {

    protected home: WorldBase;

    constructor(name: string, home: WorldBase) {
        super(name, home.empire);
        this.home = home;
    }
}