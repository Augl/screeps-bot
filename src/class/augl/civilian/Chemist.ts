import { AIAuglDepositLab } from "ai/augl/civilian/DepositLab";
import { AIAuglDepositTerminal } from "ai/augl/civilian/DepositTerminal";
import { AIAuglWithdrawLab } from "ai/augl/civilian/WithdrawLab";
import { AIAuglWithdrawMineral } from "ai/augl/civilian/WithdrawMineral";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class Chemist extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.WITHDRAW_TERMINAL, new AIAuglWithdrawMineral(this, this.home)],
                [AuglAITask.DEPOSIT_LAB, new AIAuglDepositLab(this, this.home)],
                [AuglAITask.DEPOSIT_TERMINAL, new AIAuglDepositTerminal(this, this.home)],
                [AuglAITask.WITHDRAW_LAB, new AIAuglWithdrawLab(this, this.home)],
            ]),
            startTask: AuglAITask.WITHDRAW_TERMINAL
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        if (currentTask == AuglAITask.WITHDRAW_TERMINAL || AuglAITask.DEPOSIT_TERMINAL) {
            if (super.isEmpty() && super.getTicksToLive() > 50) {
                return [AuglAITask.WITHDRAW_TERMINAL, AuglAITask.WITHDRAW_LAB];
            }
            return [AuglAITask.DEPOSIT_LAB, AuglAITask.DEPOSIT_TERMINAL];
        }
        else if (currentTask == AuglAITask.DEPOSIT_LAB) {
            if (!super.isEmpty()) {
                return [AuglAITask.DEPOSIT_LAB, AuglAITask.DEPOSIT_TERMINAL];
            }
            return [AuglAITask.WITHDRAW_TERMINAL, AuglAITask.WITHDRAW_LAB];
        }
        else if (currentTask == AuglAITask.WITHDRAW_LAB) {
            if (super.isEmpty() && super.getTicksToLive() > 50) {
                return [AuglAITask.WITHDRAW_LAB];
            }
            return [AuglAITask.DEPOSIT_TERMINAL];
        }
        throw new Error("Unexpected transition state for chemist " + this.name);
    }
}