import { AIAuglBuildDefense } from "ai/augl/civilian/BuildDefense";
import { AIAuglBuildGeneral } from "ai/augl/civilian/BuildGeneral";
import { AIAuglWithdrawAny } from "ai/augl/civilian/WithdrawAny";
import { AIBase } from "ai/Base";
import { AuglAITask } from "util/Constants";
import { AuglCivilianBase } from "./Base";

export class DefenseBuilder extends AuglCivilianBase {
    
    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.WITHDRAW_ANY, new AIAuglWithdrawAny(this, this.home)],
                [AuglAITask.BUILD_GENERAL, new AIAuglBuildGeneral(this, this.home)],
                [AuglAITask.BUILD_DEFENSE, new AIAuglBuildDefense(this, this.home)]

            ]),
            startTask: AuglAITask.WITHDRAW_ANY
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        if (currentTask == AuglAITask.WITHDRAW_ANY) {
            if (!super.isFull()) {
                // not done withdrawing yet
                return [AuglAITask.WITHDRAW_ANY];
            }
            // build non-defense or defense construction site
            return [AuglAITask.BUILD_DEFENSE, AuglAITask.BUILD_GENERAL];
        }
        else if (currentTask == AuglAITask.BUILD_GENERAL) {
            if (!super.isEmpty()) {
                // not done building yet
                return [AuglAITask.BUILD_DEFENSE, AuglAITask.BUILD_GENERAL];
            }
            // dismantle building or get energy from a container
            return [AuglAITask.WITHDRAW_ANY];
        }
        else if (currentTask == AuglAITask.BUILD_DEFENSE) {
            if (!super.isEmpty()) {
                // not done building yet
                return [AuglAITask.BUILD_DEFENSE, AuglAITask.BUILD_GENERAL];
            }
            // dismantle building or get energy from a container
            return [AuglAITask.WITHDRAW_ANY];
        }
        throw new Error("Unexpected state for DefenseBuilder: " + currentTask);
    }
}