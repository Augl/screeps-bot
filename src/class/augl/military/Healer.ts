import { AIAuglAssemble } from "ai/augl/military/Assemble";
import { AIAuglFollow } from "ai/augl/military/FollowLeader";
import { AIAuglHealSquad } from "ai/augl/military/HealSquad";
import { AIAuglMarch } from "ai/augl/military/March";
import { AIAuglWait } from "ai/augl/Wait";
import { AIBase } from "ai/Base";
import { AuglAITask, MilitaryAction } from "util/Constants";
import { AuglMilitaryBase } from "./Base";

/**
 * A Healer has heal body parts to support other soldiers or civilian augls.
 */
 export class Healer extends AuglMilitaryBase {

    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } | undefined {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.ASSEMBLE, new AIAuglAssemble(this, this.squad)],
                [AuglAITask.HEAL_SQUAD, new AIAuglHealSquad(this, this.squad)],
                [AuglAITask.MARCH, new AIAuglMarch(this, this.squad)],
                [AuglAITask.FOLLOW, new AIAuglFollow(this, this.squad)],
                [AuglAITask.WAIT, new AIAuglWait(this)]
            ]),
            startTask: AuglAITask.ASSEMBLE
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        if (this.action == MilitaryAction.GATHER) {
            return [AuglAITask.ASSEMBLE];
        }
        else if (this.action == MilitaryAction.FIGHT) {            
            return [AuglAITask.HEAL_SQUAD, AuglAITask.FOLLOW, AuglAITask.MARCH, AuglAITask.WAIT];
        }
        return [currentTask];
    } 
}