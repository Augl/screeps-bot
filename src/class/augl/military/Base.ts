import { SquadBase } from "class/military/squads/SquadBase";
import { MilitaryAction } from "util/Constants";
import { AuglBase } from "../Base";

export abstract class AuglMilitaryBase extends AuglBase {

    protected squad: SquadBase;

    protected action = MilitaryAction.GATHER;

    constructor(name: string, squad: SquadBase) {
        super(name, squad.getPlatoon().getEmpire());
        this.squad = squad;
    }

    /**
     * Sets the current action to execute.
     * @param action The action to run.
     */
    public setAction(action: MilitaryAction): void {
        this.action = action;
    }
}