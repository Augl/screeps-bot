import { AIAuglAssemble } from "ai/augl/military/Assemble";
import { AIAuglMarch } from "ai/augl/military/March";
import { AIAuglAttackMelee } from "ai/augl/military/Melee";
import { AIAuglTeardown } from "ai/augl/military/Teardown";
import { AIAuglWait } from "ai/augl/Wait";
import { AIBase } from "ai/Base";
import { AuglAITask, MilitaryAction } from "util/Constants";
import { AuglMilitaryBase } from "./Base";

/**
 * A Warrior has attack body parts for melee battles.
 */
export class Warrior extends AuglMilitaryBase {

    public getAIs(): { AIs: Map<AuglAITask, AIBase>; startTask: AuglAITask; } | undefined {
        return {
            AIs: new Map<AuglAITask, AIBase>([
                [AuglAITask.ASSEMBLE, new AIAuglAssemble(this, this.squad)],
                [AuglAITask.ATTACK_MELEE, new AIAuglAttackMelee(this, this.squad)],
                [AuglAITask.MARCH, new AIAuglMarch(this, this.squad)],
                [AuglAITask.TEARDOWN, new AIAuglTeardown(this, this.squad)],
                [AuglAITask.WAIT, new AIAuglWait(this)]
            ]),
            startTask: AuglAITask.ASSEMBLE
        };
    }

    public doTransition(currentTask: AuglAITask): AuglAITask[] {
        if (this.action == MilitaryAction.GATHER) {
            return [AuglAITask.ASSEMBLE];
        }
        else if (this.action == MilitaryAction.FIGHT) {
            return [AuglAITask.ATTACK_MELEE, AuglAITask.TEARDOWN, AuglAITask.MARCH, AuglAITask.WAIT]
        }
        throw new Error("Unexpected state for Warrior: " + currentTask);
    }
}