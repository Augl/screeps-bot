import { AIRunner } from "ai/AIRunner";
import { AIBase } from "ai/Base";
import { BuildingBase } from "class/buildings/Base";
import { ConstructionSite } from "class/buildings/ConstructionSite";
import { Controller } from "class/buildings/Controller";
import { StoreBase } from "class/buildings/StoreBase";
import { Empire } from "class/Empire";
import { MineralSource } from "class/resources/Mineral";
import { EnergySource } from "class/resources/source";
import { WorldBase } from "class/world/Base";
import { WorldObject } from "class/WorldObject";
import { IScreepsController, IScreepsCreep, IScreepsGame, IScreepsPathStep } from "screeps";
import { IAuglMemory, IMemory, IPosition } from "types";
import { AuglAITask, BodyPart, Direction, Job, ResourceType, Result } from "util/Constants";
import { Helper } from "util/Helper";

declare const Game: IScreepsGame;
declare const Memory: IMemory;

export abstract class AuglBase extends AIRunner<AuglAITask, AIBase> {

    public name: string;

    public memory: IAuglMemory;

    public underlying: WorldObject<IScreepsCreep | undefined>;

    protected empire: Empire;

    protected moveConfig: {target?: {x: number, y: number}, targetRoom?: string, path: IScreepsPathStep[] | IPosition[], blocked: number};

    constructor(name: string, empire: Empire) {
        super();
        this.name = name;
        this.memory = Memory.creeps?.[name] || {};
        this.empire = empire;
        this.underlying = new WorldObject(() => Game.creeps[name]);
        this.moveConfig = {target: {x: -1, y: -1}, path: [], blocked: 0};
    }

    public run(): void {
        // update memory object
        this.memory = Memory.creeps?.[this.name] || {};
        // check if creep is still being spawned
        const creep = this.underlying.value(false);
        if (!creep || creep.spawning) return;
        // run AI.
        this.memory.isMoving = false;
        super.runAI();
        // draw debug visuals
        if (Memory.debugVisuals) {
            this.drawDebugVisuals(creep);
        }
        // move from room exit to avoid switching rooms back and forth
        if (!this.memory.isMoving) {
            if (creep.pos.y < 2){
                creep.move(Direction.BOTTOM);
            }
            else if (creep.pos.x < 2){
                creep.move(Direction.RIGHT);
            }
            else if (creep.pos.y > 47){
                creep.move(Direction.TOP);
            }
            else if (creep.pos.x > 47){
                creep.move(Direction.LEFT);
            }
        }
        this.memory.task = this.currentID;
        // set memory object
        if (!Memory.creeps) Memory.creeps = {};
        Memory.creeps[this.name] = this.memory;
    }

    private drawDebugVisuals(creep: IScreepsCreep): void {
        if (!creep.room) return;
        const job = this.memory.job;
        let text = "";
        if (job == Job.HARVESTER){
            text = "🪓";
        }
        else if (job == Job.UPGRADER){
            text = "🔼";
        }
        else if (job == Job.BUILDER){
            text = "🔨";
        }
        else if (job == Job.CARRIER){
            text = "🚚";
        }
        else if (job == Job.REPAIRMAN){
            text = "🔧";
        }
        else if (job == Job.BUILDER_DEFENSE) {
            text = "🧰";
        }
        else if (job == Job.DISTRIBUTER) {
            text = "🚛";
        }
        else if (job == Job.EXPLORER) {
            text = "🔭";
        }
        else if (job == Job.RESERVER) {
            text = "📘";
        }
        else if (job == Job.INTERCARRIER) {
            text = "✈️";
        }
        else if (job == Job.CLAIMER) {
            text = "📕";
        }
        else if (job == Job.WARRIOR) {
            text = "⚔️";
        }
        else if (job == Job.HEALER) {
            text = "🚑";
        }
        else if (job == Job.CHEMIST) {
            text = "⚗️";
        }
        creep.room.visual.text(text, creep.pos.x+1, creep.pos.y);
    }

    /**
     * @returns The augl's position.
     */
    public getPosition(): IPosition | undefined {
        return this.underlying.value(false)?.pos;
    }

    /**
     * @returns The underlying creep.
     */
    public getUnderlying(): IScreepsCreep | undefined {
        return this.underlying.value(false);
    }

    /**
     * Attacks the given creep.
     * @param target The creep to attack.
     * @returns The action result.
     */
    public attack(target: IScreepsCreep): Result {
        const pos = this.getPosition();
        const creep = this.underlying.value(true);
        if (!pos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, target.pos) <= 1) {
            return creep.attack(target);
        }
        else {
            return this.moveToTarget(target.pos, 1);
        }
    }

    /**
     * Attacks the given building.
     * @param target The building to attack.
     * @returns The action result.
     */
    public teardown(target: BuildingBase): Result {
        const pos = this.getPosition();
        const targetPos = target.getPosition();
        const creep = this.underlying.value(true);
        if (!pos || !targetPos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, targetPos) <= 1) {
            const targetBuilding = target.getUnderlying();
            if (!targetBuilding) return Result.ERR_UNKNOWN;
            return creep.attack(targetBuilding);
        }
        else {
            return this.moveToTarget(targetPos, 1);
        }
    }

    /**
     * Heals the given creep.
     * @param target The creep to heal.
     * @returns The action result.
     */
    public heal(target: AuglBase): Result {
        const pos = this.getPosition();
        const targetPos = target.getPosition();
        const creep = this.underlying.value(true);
        if (!pos || !targetPos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, targetPos) <= 1) {
            const targetCreep = target.getUnderlying();
            if (!targetCreep) return Result.ERR_UNKNOWN;
            return creep.heal(targetCreep);
        }
        else {
            return this.moveToTarget(targetPos, 1);
        }
    }

    /**
     * Moves the augl to the given source and harvests energy.
     * @param source Source to gather energy from.
     * @returns The action result.
     */
    public harvest(source: EnergySource | MineralSource): Result {
        const pos = this.getPosition();
        const sourcePos = source.getPosition();
        const creep = this.underlying.value(true);
        if (!pos || !sourcePos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, sourcePos) <= 1) {
            const sourceUnderlying = source.getUnderlying();
            if (!sourceUnderlying) return Result.ERR_UNKNOWN;
            return creep.harvest(sourceUnderlying);
        }
        else {
            return this.moveToTarget(sourcePos, 1);
        }
    }

    /**
     * Transfers the given resource to the given target.
     * @param target The target to transfer to.
     * @param resourceType The resource to move.
     * @returns The action result.
     */
    public transfer(target: BuildingBase, resourceType: ResourceType): Result {
        const pos = this.getPosition();
        const targetPos = target.getPosition();
        const creep = this.underlying.value(true);
        if (!pos || !targetPos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, targetPos) <= 1) {
            const structure = target.getUnderlying();
            if (!structure) return Result.ERR_UNKNOWN;
            return creep.transfer(structure, resourceType);
        }
        else {
            return this.moveToTarget(targetPos, 1);
        }
    }

    /**
     * Withdraws the given resource from the given container.
     * @param target The container to withdraw from.
     * @param resourceType The resource to withdraw.
     * @returns The action result.
     */
    public withdraw(target: StoreBase, resourceType: ResourceType): Result {
        const pos = this.getPosition();
        const targetPos = target.getPosition();
        const creep = this.underlying.value(true);
        if (!pos || !targetPos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, targetPos) <= 1) {
            const structure = target.getUnderlying();
            if (!structure) return Result.ERR_UNKNOWN;
            return creep.withdraw(structure, resourceType);
        }
        else {
            return this.moveToTarget(targetPos, 1);
        }
    }

    /**
     * Builds the given building.
     * @param target The construction site to work on.
     * @returns The action result.
     */
    public build(target: ConstructionSite): Result {
        const pos = this.getPosition();
        const targetPos = target.getPosition()
        const creep = this.underlying.value(true);
        if (!pos || !creep || !targetPos) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, targetPos) <= 3) {
            const constructionSite = target.getUnderlying();
            if (!constructionSite) return Result.ERR_UNKNOWN;
            return creep.build(constructionSite);
        }
        else {
            return this.moveToTarget(targetPos, 3);
        }
    }

    /**
     * Repairs the given structure.
     * @param target The structure to repair.
     * @returns The action result.
     */
    public repair(target: BuildingBase): Result {
        const pos = this.getPosition();
        const targetPos = target.getPosition()
        const creep = this.underlying.value(true);
        if (!pos || !creep || !targetPos) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, targetPos) <= 3) {
            const structure = target.getUnderlying();
            if (!structure) return Result.ERR_UNKNOWN;
            return creep.repair(structure);
        }
        else {
            return this.moveToTarget(targetPos, 3);
        }
    }

    /**
     * Deconstructs the given structure.
     * @param target The structure to dismantle.
     * @returns The action result.
     */
    public dismantle(target: BuildingBase): Result {
        const pos = this.getPosition();
        const targetPos = target.getPosition()
        const creep = this.underlying.value(true);
        if (!pos || !creep || !targetPos) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, targetPos) <= 1) {
            const structure = target.getUnderlying();
            if (!structure) return Result.ERR_UNKNOWN;
            return creep.dismantle(structure);
        }
        else {
            return this.moveToTarget(targetPos, 1);
        }
    }

    /**
     * Moves the augl to the controller to upgrade it.
     * @param target The controller to upgrade.
     * @returns The result.
     */
    public upgradeController(target: Controller): Result {
        const controller = target.getUnderlying() as IScreepsController | undefined;
        const pos = this.getPosition();
        const creep = this.underlying.value(true);
        if (!controller || !pos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, controller.pos) <= 3) {
            return creep.upgradeController(controller);
        }
        else {
            return this.moveToTarget(controller.pos, 3);
        }
    }

    /**
     * Moves the augl to the controller to claim it.
     * @param target The controller to claim.
     * @returns The result.
     */
    public claimController(target: Controller): Result {
        const controller = target.getUnderlying() as IScreepsController | undefined;
        const pos = this.getPosition();
        const creep = this.underlying.value(true);
        if (!controller || !pos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, controller.pos) <= 1) {
            return creep.claimController(controller);
        }
        else {
            return this.moveToTarget(controller.pos, 1);
        }
    }

    /**
     * Moves the augl to the controller to reserves it.
     * @param target The controller to reserve.
     * @returns The result.
     */
    public reserveController(target: Controller): Result {
        const controller = target.getUnderlying() as IScreepsController | undefined;
        const pos = this.getPosition();
        const creep = this.underlying.value(true);
        if (!controller || !pos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, controller.pos) <= 1) {
            return creep.reserveController(controller);
        }
        else {
            return this.moveToTarget(controller.pos, 1);
        }
    }

    /**
     * Moves the augl to the controller to attacks it.
     * @param target The controller to attack.
     * @returns The result.
     */
    public attackController(target: Controller): Result {
        const controller = target.getUnderlying() as IScreepsController | undefined;
        const pos = this.getPosition();
        const creep = this.underlying.value(true);
        if (!controller || !pos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, controller.pos) <= 1) {
            return creep.attackController(controller);
        }
        else {
            return this.moveToTarget(controller.pos, 1);
        }
    }

    /**
     * Moves the augl to the controller to sign it.
     * @param target The controller to sign.
     * @param text The text to use.
     * @returns The result.
     */
    public signController(target: Controller, text: string): Result {
        const controller = target.getUnderlying() as IScreepsController | undefined;
        const pos = this.getPosition();
        const creep = this.underlying.value(true);
        if (!controller || !pos || !creep) return Result.ERR_UNKNOWN;
        if (Helper.distance(pos, controller.pos) <= 1) {
            return creep.signController(controller, text);
        }
        else {
            return this.moveToTarget(controller.pos, 1);
        }
    }

    /**
     * Moves the augl to the given position.
     * @param target The target to move to.
     * @param range The desired distance to the target.
     * @returns The screeps result code.
     */
    public moveToTarget(target: IPosition, range: number): Result {
        if (this.getCurrentRoom().roomName != target.roomName){
            return this.move(undefined, undefined, target.roomName);
        }
        else {
            return this.move(target, range, undefined);
        }
    }

    /**
     * Moves the augl to the given neighbouring room.
     * @param target The room to move to.
     * @returns The screeps result code.
     */
    public moveToRoom(target: string): Result {
        return this.move(undefined, undefined, target);
    }

    /**
     * Moves the augl along the path to either an room-internal target or a neighbouring room.
     * @param target The room-internal target.
     * @param range The desired distance to the room-internal target.
     * @param targetRoom The neighbouring rooom name.
     * @returns The screeps result code.
     */
    private move(target?: IPosition, range?: number, targetRoom?: string): Result {
        this.memory.isMoving = true;
        if (Memory.debugVisuals && this.moveConfig.path.length > 0) {
            this.underlying.value(true)?.room?.visual.poly(this.moveConfig.path);
        }
        // get creep object
        const creep = this.underlying.value(false);
        if (!creep) return Result.ERR_UNKNOWN;
        if (creep.fatigue > 0) {
            return Result.ERR_TIRED;
        }
        // get current room
        const currentRoom = this.getCurrentRoom();
        // get position
        const pos = this.getPosition();
        if (!pos) return Result.ERR_UNKNOWN;
        // check if the path should be recalculated
        const shouldForce = this.moveConfig.blocked >= 5;
        if (target && range != undefined) {
            if (this.moveConfig.path.length == 0 || target.x != this.moveConfig.target?.x || target.y != this.moveConfig.target?.y || shouldForce) {
                const newPath = currentRoom.findPath(pos, target, range, !shouldForce);
                this.moveConfig = {target: target, path: newPath, blocked: -1};
            }
        }
        else if (targetRoom) {
            if (this.moveConfig.path.length == 0 || targetRoom != this.moveConfig.targetRoom || shouldForce) {
                const empirePath = this.getCurrentRoom().empire.getPath(this.getCurrentRoom().roomName, targetRoom);
                const newPath = currentRoom.findPathToNeighbour(pos, empirePath[0], !shouldForce);
                this.moveConfig = {targetRoom: targetRoom, path: newPath, blocked: -1};
            }
        }
        // find next step to take
        let step = this.moveConfig.path[0];
        if (!step) {
            return Result.ERR_NOT_FOUND;
        }
        if (step.x != pos.x || step.y != pos.y) {
            // no progress since last tick
            this.moveConfig.blocked += 1;
        }
        else {
            this.moveConfig.path.shift();
            step = this.moveConfig.path[0];
            this.moveConfig.blocked = 0;
        }
        if (!step) {
            return Result.ERR_NOT_FOUND;
        }
        // calculate direction if not already existing
        let direction: Direction;
        if ("direction" in step) {
            direction = step.direction;
        }
        else {
            const currentPos = this.getPosition();
            if (!currentPos) return Result.ERR_UNKNOWN;
            direction = currentPos.getDirectionTo(step.x, step.y);
        }
        // take the next step
        if (direction) {
            if (!targetRoom && (step.x == 0 || step.x == 49 || step.y == 0 || step.y == 49)) {
                // movement should be inside the one room, avoid exiting the current room
                this.moveConfig = {target: {x: -1, y: -1}, blocked: -1, path: []};
                // use best possible direction depending on where the augl is stuck and where it wants to go
                if ((step.x == 0 && direction == Direction.BOTTOM_LEFT) || (step.x == 49 && direction == Direction.BOTTOM_RIGHT)) {
                    direction = Direction.BOTTOM;
                }
                else if (step.x == 0 && direction == Direction.TOP_LEFT || (step.x == 49 && direction == Direction.TOP_RIGHT)) {
                    direction = Direction.TOP;
                }
                else if (step.y == 0 && direction == Direction.TOP_LEFT || (step.y == 49 && direction == Direction.BOTTOM_LEFT)) {
                    direction = Direction.LEFT;
                }
                else if (step.y == 0 && direction == Direction.TOP_RIGHT || (step.y == 49 && direction == Direction.BOTTOM_RIGHT)) {
                    direction = Direction.RIGHT;
                }
            }
            return creep.move(direction);
        }
        return Result.ERR_UNKNOWN;
    }

    /**
     * @returns Returns if the augl is moving or not.
     */
    public isMoving(): boolean {
        return this.memory.isMoving || false;
    }

    /**
     * @returns If the augl is full with resources.
     */
    public isFull(): boolean {
        const workParts = (this.underlying.value(false)?.getActiveBodyparts(BodyPart.WORK) ?? 0);
        if (workParts == 0) {
            return (this.underlying.value(false)?.store.getFreeCapacity() ?? 0) == 0;
        }
        return (this.underlying.value(false)?.store.getFreeCapacity() ?? 0) < workParts*2;
    }

    /**
     * @returns If the augl is emtpy.
     */
    public isEmpty(): boolean {
        return this.underlying.value(false)?.store.getUsedCapacity() == 0;
    }

    /**
     * @returns The capacity of the augl.
     */
    public getCarryCapacity(): number {
        return this.underlying.value(true)?.store.getCapacity() || 0;
    }

    /**
     * @returns The type of resource carried by the augl.
     */
    public getCarriedResource(): ResourceType | undefined {
        const store = this.underlying.value(true)?.store;
        if (!store) return;
        for (const resource in store) {
            return resource as ResourceType;
        }
        return;
    }

    /**
     * @returns The number of ticks for the augl to live.
     */
    public getTicksToLive(): number {
        const creep = this.underlying.value(false);
        if (!creep) {
            // augl died
            return 0;
        }
        else {
            // ticksToLive is undefined when creep is being spawned.
            return creep.ticksToLive || 100;
        }
    }

    /**
     * @returns The hit points of the augl.
     */
    public getHits(): number {
        const creep = this.underlying.value(false);
        if (!creep) return 0;
        return creep.hits;
    }

    /**
     * @returns True if the augl is injured.
     */
    public isInjured(): boolean {
        const creep = this.underlying.value(false);
        if (!creep) return false;
        return creep.hits < creep.hitsMax;
    }

    /**
     * @returns The room the augl is currently located in.
     */
    public getCurrentRoom(): WorldBase {
        const creep = this.underlying.value(false);
        if (!creep || !creep.room) throw new Error("WorldBase.getCurrentRoom: creep or room undefined.");
        const room = this.empire.getWorld(creep.room.name);
        if (!room) throw new Error("WorldBase.getCurrentRoom: No room object in empire for requested name.");
        return room;
    }
}