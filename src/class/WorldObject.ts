import { IScreepsGame } from "screeps";

declare const Game: IScreepsGame;

export class WorldObject<T> {

    private underlying: T;

    private updateFunction: () => T;

    private updateTime: number;

    private needUpdate: boolean;

    private maxAge: number;

    private nextRequiredUpdateTime: number;

    constructor(updateFunction: () => T, maxAge = 100) {
        this.updateFunction = updateFunction;
        this.updateTime = Game.time;
        this.needUpdate = false;
        this.underlying = updateFunction();
        this.maxAge = maxAge;
        this.nextRequiredUpdateTime = Game.time + maxAge
    }

    /**
     * Gets the world object that has been updated this tick.
     * @param returnCached If a cached version of the object is good enough.
     * @returns The underlying world object.
     */
    public value(returnCached: boolean): T {
        if (this.updateTime != Game.time && (!returnCached || this.needUpdate || this.nextRequiredUpdateTime <= Game.time)) {
            // explicit update requested either right now or during previous tick
            this.underlying = this.updateFunction();
            this.updateTime = Game.time;
            this.nextRequiredUpdateTime = Game.time + this.maxAge;
            this.needUpdate = false;
        }
        if (!returnCached) {
            // request update on next call
            this.needUpdate = true;
        }
        return this.underlying;
    }
}