import { Empire } from "class/Empire"
import { IScreepsGame } from "screeps";
import { Debugger } from "util/Debugger";
import { Performance } from "util/Performance";

declare const global: {debugger: Debugger};
declare const Game: IScreepsGame;
let empire: Empire | undefined;

export const loop = function(): void {
    if (!empire) {
        empire = new Empire();
        global.debugger = new Debugger(empire);
        Game.notify("Empire reset.");
    }
    empire.run();
    if (Game.cpu.bucket == 10000) {
        Game.cpu.generatePixel();
    }
    if (Game.time % 50 == 0) {
        const statistics = Game.cpu.getHeapStatistics();
        console.log("Heap statistics: Used ", statistics.used_heap_size, " of available ", statistics.heap_size_limit, ", which is ", 
            statistics.used_heap_size*100/statistics.heap_size_limit, "%");
        Performance.print();
    }
}