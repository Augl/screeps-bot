import { AIBase } from "./Base";

/**
 * AIRunner is the superclass of all entities that have AI modules that need to be executed.
 */
export abstract class AIRunner<K extends string | number, AI extends AIBase> {

    private AIs?: Map<K, AI>;

    protected currentID?: K;

    private didInit = false;

    /**
     * Sets the AIs and initial task to use.
     */
    public abstract getAIs(): { AIs: Map<K, AI>, startTask: K } | undefined;

    /**
     * Runs the AI.
     */
    public runAI(): void {
        if (!this.didInit) {
            this.didInit = true;
            const init = this.getAIs();
            this.AIs = init?.AIs;
            this.currentID = init?.startTask;
        }
        if (!this.AIs || !this.currentID) {
            return;
        }
        // make transition first
        const options = this.doTransition(this.currentID);
        // find next AI module to run
        for (const ID of options) {
            const current = this.AIs.get(ID);
            if (!current) {
                throw new Error("AIRunner: Current AI module is undefined!");
            }
            if (ID != this.currentID) {
                current.reset();
            }
            if (current.shouldExecute()) {
                // run first AI that can be executed
                this.currentID = ID;
                current.run();
                break;
            }
        }
    }

    /**
     * Decides what AI module to run next.
     * @param currentTask The last task that was executed.
     * @returns Array of AI task IDs in order of priority.
     */
    public abstract doTransition(currentTask: K): K[];

    /**
     * Returns the AI module for the given ID.
     * @param task The desired AI module to get.
     * @returns The AI module for the given ID.
     */
    public getAI(task: K): AI {
        const ai = this.AIs?.get(task);
        if (ai == undefined) {
            throw new Error("AIRunner: No AI module for the desired task!");
        }
        return ai;
    }

}