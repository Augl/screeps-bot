import { IScreepsGame } from "screeps";
import { AIBase } from "./Base";

declare const Game: IScreepsGame;

export class AICooldownBase extends AIBase {

    protected nextExecutionTime: number;

    protected cooldown: number;

    constructor(cooldown: number) {
        super()
        this.nextExecutionTime = 0;
        this.cooldown = cooldown;
    }

    public shouldExecute(): boolean {
        return Game.time >= this.nextExecutionTime;
    }
    
    public run(): void {
        this.nextExecutionTime = Game.time + this.cooldown;
    }
    
}