import { AICooldownBase } from "ai/CooldownBase";
import { WorldBase } from "class/world/Base";
import { Sector } from "class/world/Sector";
import { Allies, Job, WorldType } from "util/Constants";

/**
 * AI module that handles classification of previously unclassified rooms.
 */
export class AIUnknownClassify extends AICooldownBase {

    private sector: WorldBase;

    constructor(sector: WorldBase) {
        super(200);
        this.sector = sector;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True every 200 ticks.
     */
    public shouldExecute(): boolean {
        return super.shouldExecute();
    }

    /**
     * Decides what type of room the current room should be.
     */
    public run(): void {
        super.run();
        console.log("Running classification AI");
        if (this.sector.getUnderlying() == undefined) {
            console.log("Room not visible for classification, send explorer.");
            const spawnSector = this.sector.empire.getClosestSpawnRoom(this.sector.roomName, 1);
            if (!spawnSector) {
                console.log("AISectorExplore: No sector to spawn explorer in!");
                return;
            }
            (spawnSector as Sector).addToSpawnQueue(Job.EXPLORER, {targets: [this.sector.roomName]}, 1, this.sector.roomName);
            return;
        }
        const controller = this.sector.getController();
        if (controller) {
            // check if room belongs to ally
            const isAllied = Allies.includes(controller.getOwner() ?? "");
            if (isAllied) {
                this.sector.changeClassification(WorldType.ALLIED);
            }
            // check if room is mine
            else if (controller.getOwner() == "Augl") {
                this.sector.changeClassification(WorldType.SECTOR);
            }
            // check if room belongs to enemy
            else if (controller.getOwner().length > 0) {
                this.sector.changeClassification(WorldType.ENEMY);
            }
            // check number of sources
            else if (this.sector.getSources().length == 1) {
                this.sector.changeClassification(WorldType.COLONY);
            }
            else if (this.sector.getSources().length == 2) {
                this.sector.changeClassification(WorldType.SECTOR);
            }
        }
        else {
            if (this.sector.getSources().length == 3) {
                this.sector.changeClassification(WorldType.WARZONE);
            }
            else {
                this.sector.changeClassification(WorldType.EMPTY);
            }
        }
        const memory = this.sector.getMemory();
        memory.inactive = true;
        this.sector.setMemory(memory);
    }
}