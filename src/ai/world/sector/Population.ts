import { AICooldownBase } from "ai/CooldownBase";
import { Sector } from "class/world/Sector";
import { Job, Levels } from "util/Constants";

/**
 * AI module that handles scheduling of augl spawning.
 */
export class AISectorPopulation extends AICooldownBase {

    private sector: Sector;

    constructor(sector: Sector) {
        super(100);
        this.sector = sector;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True with a cooldown of 100.
     */
    public shouldExecute(): boolean {
        return super.shouldExecute();
    }

    /**
     * Checks which augls need to be spawned.
     */
    public run(): void {
        super.run();
        // find sector to spawn the augls
        const spawnSector = this.sector.empire.getClosestSpawnRoom(this.sector.roomName, 1) as Sector;
        if (!spawnSector) throw new Error("AISectorPopulation: No spawn sector found!");
        let level = 0;
        if (spawnSector.roomName == this.sector.roomName) {
            // use maximal possible level
            const capacity = spawnSector.getEnergyCapacity();
            for (let i = 0; i < Levels.length; i++) {
                if (Levels[i] <= capacity) {
                    level = i;
                }
            }
        }
        else {
            level = 3;
        }
        if (level == 0) throw new Error("AISectorPopulation: Energy level is 0!");
        if (this.sector.getLevel() > 0) {
            // controller was claimed
            this.checkHarvesterPopulation(level, spawnSector);
            this.checkUpgraderPopulation(level, spawnSector);
            this.checkBuilderPopulation(level, spawnSector);
            this.checkRepairmanPopulation(level, spawnSector);
            this.checkCarrierPopulation(level, spawnSector);
            this.checkDistributerPopulation(level, spawnSector);
            this.checkDefenseBuilderPopulation(level, spawnSector);
            this.checkChemistPopulation(level, spawnSector);
        }
        else {
            // controller not claimed yet
            this.checkClaimerPopulation(level, spawnSector);
        }
    }

    private checkHarvesterPopulation(level: number, spawnSector: Sector): void {
        const sources = this.sector.getSources();
        const harvesters = this.sector.getAuglsWithJob(Job.HARVESTER);
        let perSourceMax = 10;
        if (level == 2) {
            perSourceMax = 2;
        }
        else if (level >= 3) {
            perSourceMax = 1;
        }
        // create map of sources to number of harvesters required
        const requiredHarvesters: {[sourceID: string]: number} = {};
        for (const s of sources) {
            requiredHarvesters[s.sourceID] = Math.min(s.workspaces.length, perSourceMax);
        }
        const mineral = this.sector.getMineral();
        if (mineral && this.sector.getBuildings()?.getExtractor() && mineral.getTicksToRegeneration() == undefined) {
            const terminal = this.sector.getBuildings()?.getTerminal();
            const resourceType = mineral.getResourceType();
            if (terminal && resourceType && !terminal.hasResource(20000, resourceType)) {
                requiredHarvesters[mineral.ID] = 1;
            }
        }
        // find source that requires a new harvester
        for (const a of harvesters) {
            if (!a.memory.sourceId) continue;             
            requiredHarvesters[a.memory.sourceId]--;
        }
        for (const sourceID in requiredHarvesters) {
            if (requiredHarvesters[sourceID] > 0) {
                spawnSector.addToSpawnQueue(Job.HARVESTER, {sourceId: sourceID}, level, this.sector.roomName);
                return;
            }
        }
    }

    private checkUpgraderPopulation(level: number, spawnSector: Sector): void {        
        const upgrader = this.sector.getAuglsWithJob(Job.UPGRADER);
        let max = 1;
        const buildings = this.sector.getBuildings();
        if (!buildings) return;
        const storage = buildings.getStorage();
        const centreStores = buildings.getCentreStores();
        const totalCapacity = centreStores.reduce((acc, store) => acc + store.getCapacity(), storage?.getCapacity() ?? 0);
        const usedCapacity = centreStores.reduce((acc, store) => acc + store.getUsedCapacity(), storage?.getUsedCapacity() ?? 0);
        if (totalCapacity > 0 && this.sector.getLevel() < 8){
            const percentage = usedCapacity*100/totalCapacity;
            if (percentage > 95 || usedCapacity > 100000) {
                max = Math.max(upgrader.length + 1, 7);
            }
        }
        if (this.sector.getLevel() == 8) {
            level = 2;
        }
        if (upgrader.length < max) {
            spawnSector.addToSpawnQueue(Job.UPGRADER, {}, level, this.sector.roomName);
        }
    }

    private checkBuilderPopulation(level: number, spawnSector: Sector): void {
        const buildings = this.sector.getBuildings();
        if (!buildings) return;
        const builder = this.sector.getAuglsWithJob(Job.BUILDER);
        const constructionSite = buildings.getConstructionSites().find(cs => !cs.isDefenseBuilding());
        const deconstructionTargets = buildings.getDeconstructionTargets();
        if ((constructionSite != undefined || deconstructionTargets.length > 0) && builder.length < 2) {
            spawnSector.addToSpawnQueue(Job.BUILDER, {}, level, this.sector.roomName);
        }
    }

    private checkRepairmanPopulation(level: number, spawnSector: Sector): void {
        const repairman = this.sector.getAuglsWithJob(Job.REPAIRMAN);
        const damaged = this.sector.getBuildings()?.getDamagedBuildings() ?? [];
        if (damaged.length > 0 && repairman.length < 1) {
            spawnSector.addToSpawnQueue(Job.REPAIRMAN, {}, level, this.sector.roomName);
        }
    }

    private checkCarrierPopulation(level: number, spawnSector: Sector): void {
        if (this.sector.getBuildings()?.getSpawns()[0] == undefined) return;
        const carrier = this.sector.getAuglsWithJob(Job.CARRIER);
        const containers = this.sector.getBuildings()?.getPeripheryContainers() ?? [];
        if (containers.find(c => c.getUsedCapacity() > 100) == undefined) {
            // all containers are empty, no carrier required.
            return;
        }
        const fullContainer = containers.find(c => c.isFull());
        const max = fullContainer != undefined ? containers.length * 3 : containers.length;
        if (carrier.length < max) {
            spawnSector.addToSpawnQueue(Job.CARRIER, {}, level, this.sector.roomName);
        }
    }

    private checkDistributerPopulation(level: number, spawnSector: Sector): void {
        if ((this.sector.getBuildings()?.getCentreStores().length ?? 0) == 0) return;
        const distributer = this.sector.getAuglsWithJob(Job.DISTRIBUTER);
        if (distributer.length == 1 && distributer[0].getTicksToLive() < 200) {
            spawnSector.addToSpawnQueue(Job.DISTRIBUTER, {}, level, this.sector.roomName);
        }
        else if (distributer.length < 1) {
            spawnSector.addToSpawnQueue(Job.DISTRIBUTER, {}, level, this.sector.roomName);
        }
        else if (this.sector.getBuildings()?.getTowers().find(t => t.isEmpty()) && distributer.length < 2) {
            spawnSector.addToSpawnQueue(Job.DISTRIBUTER, {}, level, this.sector.roomName);
        }
    }

    private checkDefenseBuilderPopulation(level: number, spawnSector: Sector): void {
        const builder = this.sector.getAuglsWithJob(Job.BUILDER_DEFENSE);
        const damaged = this.sector.getBuildings()?.getDamagedBuildings().find(d => d.isDefenseBuilding());
        const sites = this.sector.getBuildings()?.getConstructionSites().filter(d => d.isDefenseBuilding()) ?? [];
        if (sites.length > 5 && builder.length < 2) {
            spawnSector.addToSpawnQueue(Job.BUILDER_DEFENSE, {}, level, this.sector.roomName);
        }
        else if (damaged && builder.length < 1) {
            spawnSector.addToSpawnQueue(Job.BUILDER_DEFENSE, {}, level, this.sector.roomName);
        }
    }

    private checkClaimerPopulation(level: number, spawnSector: Sector): void {
        const claimers = this.sector.getAuglsWithJob(Job.CLAIMER);
        if (claimers.length < 1) {
            spawnSector.addToSpawnQueue(Job.CLAIMER, {}, level, this.sector.roomName);
        }
    }

    private checkChemistPopulation(level: number, spawnSector: Sector): void {
        const chemists = this.sector.getAuglsWithJob(Job.CHEMIST);
        const complex = this.sector.getBuildings()?.getLabComplexes().find(c => c.needsChemist());
        if (chemists.length < 1 && complex != undefined) {
            spawnSector.addToSpawnQueue(Job.CHEMIST, {}, level, this.sector.roomName);
        }
    }
}