import { AIBase } from "ai/Base";
import { Sector } from "class/world/Sector";
import { PlatoonType } from "util/Constants";

export class AISectorDefend extends AIBase {

    private sector: Sector;

    constructor(sector: Sector) {
        super();
        this.sector = sector;
    }

    public shouldExecute(): boolean {
        const enemies = this.sector.getEnemies();
        return enemies.find(e => e.pos.x > 3 && e.pos.x < 46 && e.pos.y > 3 && e.pos.y < 46) !== undefined;
    }

    public run(): void {
        if (this.sector.empire.numberOfPlatoons(this.sector.roomName) == 0) {
            if (this.sector.getEnemies().length == 1) {
                console.log("Requesting tiny defense platoon!");
                this.sector.empire.requestPlatoon(this.sector, PlatoonType.TINY_DEFENSE);
            }
            else {
                console.log("Requesting small defense platoon!");
                this.sector.empire.requestPlatoon(this.sector, PlatoonType.SMALL_DEFENSE);
            }
        }
    }
}