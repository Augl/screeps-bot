import { AIBase } from "ai/Base";
import { AuglBase } from "class/augl/Base";
import { Healer } from "class/augl/military/Healer";
import { Warrior } from "class/augl/military/Warrior";
import { Spawner } from "class/buildings/Spawn";
import { SquadBase } from "class/military/squads/SquadBase";
import { Sector } from "class/world/Sector";
import { IAuglMemory, ISpawnQueue } from "types";
import { Job, Levels, Result } from "util/Constants";
import { Helper } from "util/Helper";
import { Names } from "util/Names";

/**
 * AI module that handles spawning of augls.
 */
export class AISectorSpawnMilitary extends AIBase {

    private static jobs = [Job.WARRIOR, Job.HEALER];

    private lastSquad: string | undefined;

    private sector: Sector;

    private freeSpawn?: Spawner;

    private toBeSpawned?: ISpawnQueue;

    private requestedJob?: Job;

    constructor(sector: Sector) {
        super();
        this.sector = sector;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is an augl waiting to be spawned.
     */
    public shouldExecute(): boolean {
        // check if there are spawns available
        this.freeSpawn = this.sector.getBuildings()?.getSpawns().find(s => !s.isSpawning());
        if (!this.freeSpawn) {
            return false;
        }
        // check if there are augls of the same squad as before waiting to be spawned
        this.toBeSpawned = undefined;
        const queue = this.sector.getSpawnQueue();
        for (const job of AISectorSpawnMilitary.jobs) {
            if (queue[job]?.home == this.lastSquad) {
                this.toBeSpawned = queue[job];
                this.requestedJob = Number(job);
                break;
            }
        }
        if (!this.toBeSpawned) {
            // check if there are any augls waiting to be spawned
            for (const job of AISectorSpawnMilitary.jobs) {
                if (queue[job]) {
                    this.toBeSpawned = queue[job];
                    this.requestedJob = Number(job);
                    this.lastSquad = queue[job]?.home;
                    break;
                }
            }
        }
        if (!this.toBeSpawned) {
            return false;
        }
        const memory = this.sector.getMemory();
        // check if the energy is full
        if (this.sector.getAvailableEnergy() < Levels[this.toBeSpawned.level]) {
            const cooldown = memory.spawnCooldown || 60;
            memory.spawnCooldown = cooldown - 1;
            this.sector.setMemory(memory);
            if (cooldown > 1) {
                return false;
            }
            else {
                // waited long enough, spawn with highest possible level
                console.log("Military spawn cooldown over!");
                // find highest possible level
                let level = 0;
                for (let i = 1; i < Levels.length; i++) {
                    if (Levels[i] <= this.sector.getAvailableEnergy()) {
                        level = i;
                    }
                }
                if (level == 0) {
                    return false;
                }
                this.toBeSpawned.level = level;
            }
        }
        return true;
    }

    /**
     * Spawns augls.
     */
    public run(): void {
        if (!this.toBeSpawned) return;
        const squad = this.sector.empire.getSquad(this.toBeSpawned.home);
        if (!this.requestedJob || !this.freeSpawn || !squad) return;
        const sectorMemory = this.sector.getMemory();
        delete sectorMemory.spawnCooldown;
        this.sector.setMemory(sectorMemory);
        const memory = this.toBeSpawned?.memory || {};
        memory.home = this.toBeSpawned?.home;
        memory.job = this.requestedJob;
        let result;
        if (this.requestedJob == Job.WARRIOR) {
            result = this.spawnWarrior(this.toBeSpawned.level, memory, squad);
        }
        else if (this.requestedJob == Job.HEALER) {
            result = this.spawnHealer(this.toBeSpawned.level, memory, squad);
        }
        if (result) {
            squad.addAugl(result);
        }
        else {
            console.log("Error spawning augl: ", JSON.stringify(this.toBeSpawned));
        }
        this.sector.removeFromSpawnQueue(this.requestedJob);
        return;
    }
    
    private spawnWarrior(level: number, memory: IAuglMemory, squad: SquadBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level >= 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 4, attack: 4}), name, memory);
        }
        if (result == Result.OK) {
            return new Warrior(name, squad);
        }
        return;
    }
    
    private spawnHealer(level: number, memory: IAuglMemory, squad: SquadBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level >= 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 2, heal: 1, tough: 1}), name, memory);
        }
        if (result == Result.OK) {
            return new Healer(name, squad);
        }
        return;
    }
}