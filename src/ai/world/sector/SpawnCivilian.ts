import { AIBase } from "ai/Base";
import { AuglBase } from "class/augl/Base";
import { Builder } from "class/augl/civilian/Builder";
import { Carrier } from "class/augl/civilian/Carrier";
import { Chemist } from "class/augl/civilian/Chemist";
import { Claimer } from "class/augl/civilian/Claimer";
import { DefenseBuilder } from "class/augl/civilian/DefenseBuilder";
import { Distributer } from "class/augl/civilian/Distributer";
import { Explorer } from "class/augl/civilian/Explorer";
import { Harvester } from "class/augl/civilian/Harvester";
import { InterCarrier } from "class/augl/civilian/InterCarrier";
import { Repairman } from "class/augl/civilian/Repairman";
import { Reserver } from "class/augl/civilian/Reserver";
import { Upgrader } from "class/augl/civilian/Upgrader";
import { Spawner } from "class/buildings/Spawn";
import { WorldBase } from "class/world/Base";
import { Sector } from "class/world/Sector";
import { IAuglMemory, ISpawnQueue } from "types";
import { Job, Levels, Result } from "util/Constants";
import { Helper } from "util/Helper";
import { Names } from "util/Names";

/**
 * AI module that handles spawning of augls.
 */
export class AISectorSpawnCivilian extends AIBase {

    private static jobs = [Job.DISTRIBUTER, Job.CARRIER, Job.HARVESTER, Job.BUILDER, Job.BUILDER_DEFENSE, Job.REPAIRMAN, Job.UPGRADER, 
        Job.INTERCARRIER, Job.CLAIMER, Job.EXPLORER, Job.RESERVER, Job.CHEMIST];

    private sector: Sector;

    private freeSpawn?: Spawner;

    private toBeSpawned?: ISpawnQueue;

    private requestedJob?: Job;

    constructor(sector: Sector) {
        super();
        this.sector = sector;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is an augl waiting to be spawned.
     */
    public shouldExecute(): boolean {
        // check if there are spawns available
        this.freeSpawn = this.sector.getBuildings()?.getSpawns().find(s => !s.isSpawning());
        if (!this.freeSpawn) {
            return false;
        }
        // check if there are augls waiting to be spawned
        this.toBeSpawned = this.requestedJob = undefined;
        const queue = this.sector.getSpawnQueue();
        for (const job of AISectorSpawnCivilian.jobs) {
            if (queue[job]) {
                this.toBeSpawned = queue[job];
                this.requestedJob = Number(job);
                break;
            }
        }
        if (!this.toBeSpawned || !this.requestedJob) {
            return false;
        }
        const memory = this.sector.getMemory();
        // check if the energy is full
        if (this.sector.getAvailableEnergy() < Levels[this.toBeSpawned.level]) {
            const cooldown = memory.spawnCooldown || 60;
            memory.spawnCooldown = cooldown - 1;
            this.sector.setMemory(memory);
            if (cooldown > 1) {
                return false;
            }
            else {
                // waited long enough, spawn with highest possible level
                let level = 0;
                for (let i = 1; i < Levels.length; i++) {
                    if (Levels[i] <= this.sector.getAvailableEnergy()) {
                        level = i;
                    }
                }
                console.log("Civilian spawn cooldown over!", level);
                if (level == 0) {
                    return false;
                }
                this.toBeSpawned.level = level;
            }
        }
        return true;
    }

    /**
     * Spawns augls.
     */
    public run(): void {
        if (!this.toBeSpawned) return;
        const home = this.sector.empire.getWorld(this.toBeSpawned.home);
        if (!this.requestedJob || !this.freeSpawn || !home) return;
        const sectorMemory = this.sector.getMemory();
        delete sectorMemory.spawnCooldown;
        this.sector.setMemory(sectorMemory);
        const memory = this.toBeSpawned?.memory || {};
        memory.home = this.toBeSpawned?.home;
        memory.job = this.requestedJob;
        let result;
        if (this.requestedJob == Job.HARVESTER) {
            result = this.spawnHarvester(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.UPGRADER) {
            result = this.spawnUpgrader(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.BUILDER) {
            result = this.spawnBuilder(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.REPAIRMAN) {
            result = this.spawnRepairman(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.CARRIER) {
            result = this.spawnCarrier(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.DISTRIBUTER) {
            result = this.spawnDistributer(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.BUILDER_DEFENSE) {
            result = this.spawnDefenseBuilder(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.EXPLORER) {
            result = this.spawnExplorer(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.CLAIMER) {
            result = this.spawnClaimer(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.RESERVER) {
            result = this.spawnReserver(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.INTERCARRIER) {
            result = this.spawnIntercarrier(this.toBeSpawned.level, memory, home);
        }
        else if (this.requestedJob == Job.CHEMIST) {
            result = this.spawnChemist(this.toBeSpawned.level, memory, home);
        }
        if (result) {
            home.addAugl(result);
        }
        else {
            console.log("Error spawning augl: ", JSON.stringify(this.toBeSpawned));
        }
        this.sector.removeFromSpawnQueue(this.requestedJob);
        return;
    }

    private spawnHarvester(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level == 1) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 1, carry: 1, work: 2}), name, memory);
        }
        else if (level == 2) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 2, carry: 1, work: 4}), name, memory);
        }
        else if (level == 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 3, carry: 1, work: 6}), name, memory);
        }
        else if (level == 8) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 6, carry: 4, work: 12}), name, memory);
        }
        else if (level >= 4) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 6, carry: 4, work: 8}), name, memory);
        }
        if (result == Result.OK) {
            return new Harvester(name, home);
        }
        return;
    }

    private spawnUpgrader(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        let result;
        const name = Names.getAuglName();
        if (level == 1) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 1, carry: 1, work: 2}), name, memory);
        }
        else if (level == 2) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 3, carry: 2, work: 3}), name, memory);
        }
        else if (level == 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 4, carry:4, work: 4}), name, memory);
        }
        else if (level == 4) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 6, carry:6, work: 6}), name, memory);
        }
        else if (level == 5) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 9, carry:9, work: 9}), name, memory);
        }
        else if (level >= 6) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 12, carry:10, work: 12}), name, memory);
        }
        if (result == Result.OK) {
            return new Upgrader(name, home);
        }
        return;
    }

    private spawnBuilder(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level == 1) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 1, carry: 3, work: 1}), name, memory);
        }
        else if (level == 2) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 2, carry: 3, work: 3}), name, memory);
        }
        else if (level >= 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 4, carry: 4, work: 4}), name, memory);
        }
        if (result == Result.OK) {
            return new Builder(name, home);
        }
        return;
    }

    private spawnRepairman(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level == 1) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 1, carry: 3, work: 1}), name, memory);
        }
        else if (level == 2) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 2, carry: 3, work: 3}), name, memory);
        }
        else if (level >= 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 4, carry: 4, work: 4}), name, memory);
        }
        if (result == Result.OK) {
            return new Repairman(name, home);
        }
        return;
    }
    
    private spawnCarrier(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level == 1) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 2, carry: 4}), name, memory);
        }
        else if (level == 2) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 4, carry: 7}), name, memory);
        }
        else if (level >= 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 5, carry: 10}), name, memory);
        }
        if (result == Result.OK) {
            return new Carrier(name, home);
        }
        return;
    }
    
    private spawnDistributer(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level == 1) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 2, carry: 4}), name, memory);
        }
        else if (level == 2) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 4, carry: 7}), name, memory);
        }
        else if (level == 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 5, carry: 10}), name, memory);
        }
        else if (level >= 4) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 8, carry: 16}), name, memory);
        }
        if (result == Result.OK) {
            return new Distributer(name, home);
        }
        return;
    }

    private spawnDefenseBuilder(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level == 1) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 1, carry: 3, work: 1}), name, memory);
        }
        else if (level == 2) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 2, carry: 3, work: 3}), name, memory);
        }
        else if (level >= 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 4, carry: 4, work: 4}), name, memory);
        }
        if (result == Result.OK) {
            return new DefenseBuilder(name, home);
        }
        return;
    }

    private spawnExplorer(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        const result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 1}), name, memory);
        if (result == Result.OK) {
            return new Explorer(name, home);
        }
        return;
    }

    private spawnClaimer(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        const result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 1, claim: 1}), name, memory);
        if (result == Result.OK) {
            return new Claimer(name, home);
        }
        return;
    }

    private spawnReserver(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level < 4) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 1, claim: 1}), name, memory);
        }
        else {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 2, claim: 2}), name, memory);
        }
        if (result == Result.OK) {
            return new Reserver(name, home);
        }
        return;
    }
    
    private spawnIntercarrier(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level == 1) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 2, carry: 4}), name, memory);
        }
        else if (level == 2) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 4, carry: 7}), name, memory);
        }
        else if (level == 3) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 5, carry: 10}), name, memory);
        }
        else if (level == 4) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 8, carry: 16}), name, memory);
        }
        else if (level >= 5) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 12, carry: 24}), name, memory);
        }
        if (result == Result.OK) {
            return new InterCarrier(name, home);
        }
        return;
    }
    
    private spawnChemist(level: number, memory: IAuglMemory, home: WorldBase): AuglBase | undefined {
        if (!this.freeSpawn) return;
        const name = Names.getAuglName();
        let result;
        if (level >= 4) {
            result = this.freeSpawn?.spawnCreep(Helper.getBody({move: 8, carry: 16}), name, memory);
        }
        if (result == Result.OK) {
            return new Chemist(name, home);
        }
        return;
    }
}