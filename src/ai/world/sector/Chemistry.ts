import { AICooldownBase } from "ai/CooldownBase";
import { LabComplex } from "class/buildings/LabComplex";
import { Sector } from "class/world/Sector";
import { ResourceType } from "util/Constants";

export class AISectorChemistry extends AICooldownBase {

    private sector: Sector;

    constructor(sector: Sector) {
        super(200);
        this.sector = sector;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if the sector is level 8 with a cooldown of 200.
     */
    public shouldExecute(): boolean {
        return this.sector.getLevel() == 8 && super.shouldExecute();
    }

    /**
     * Decides what chemical reactions to run in the room
     */
    public run(): void {
        super.run();
        const terminal = this.sector.getBuildings()?.getTerminal();
        if (!terminal) return;
        // find reaction to run
        const reaction = reactions.find(r => 
            terminal.getUsedCapacity(r.product) < 9000 && terminal.getUsedCapacity(r.reagent1) >= 3000 && 
            terminal.getUsedCapacity(r.reagent2) >= 3000
        );
        if (!reaction) return;
        // find lab complex to run the reaction
        const complexes = this.sector.getBuildings()?.getLabComplexes();
        if (!complexes || complexes.length != 2) return;
        let available: LabComplex | undefined;
        if (complexes[0].isReady()) {
            available = complexes[0];
        }
        else if (!complexes[0].shouldFill() && complexes[1].isReady()){
            available = complexes[1];
        }
        if (!available) return;
        available.setReaction(reaction);
    }
}

export class ChemicalReaction {
    constructor(
        readonly reagent1: ResourceType,
        readonly reagent2: ResourceType,
        readonly product: ResourceType,
    ) {}
}

const reactions = [
    new ChemicalReaction(ResourceType.GHODIUM_ALKALIDE, ResourceType.CATALYST, ResourceType.CATALYZED_GHODIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.GHODIUM_ACID, ResourceType.CATALYST, ResourceType.CATALYZED_GHODIUM_ACID),
    new ChemicalReaction(ResourceType.ZYNTHIUM_ALKALIDE, ResourceType.CATALYST, ResourceType.CATALYZED_ZYNTHIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.ZYNTHIUM_ACID, ResourceType.CATALYST, ResourceType.CATALYZED_ZYNTHIUM_ACID),
    new ChemicalReaction(ResourceType.LEMERGIUM_ALKALIDE, ResourceType.CATALYST, ResourceType.CATALYZED_LEMERGIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.LEMERGIUM_ACID, ResourceType.CATALYST, ResourceType.CATALYZED_LEMERGIUM_ACID),
    new ChemicalReaction(ResourceType.KEANIUM_ALKALIDE, ResourceType.CATALYST, ResourceType.CATALYZED_KEANIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.KEANIUM_ACID, ResourceType.CATALYST, ResourceType.CATALYZED_KEANIUM_ACID),
    new ChemicalReaction(ResourceType.UTRIUM_ALKALIDE, ResourceType.CATALYST, ResourceType.CATALYZED_UTRIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.UTRIUM_ACID, ResourceType.CATALYST, ResourceType.CATALYZED_UTRIUM_ACID),

    new ChemicalReaction(ResourceType.GHODIUM_OXIDE, ResourceType.HYDROXIDE, ResourceType.GHODIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.GHODIUM_HYDRIDE, ResourceType.HYDROXIDE, ResourceType.GHODIUM_ACID),
    new ChemicalReaction(ResourceType.ZYNTHIUM_OXIDE, ResourceType.HYDROXIDE, ResourceType.ZYNTHIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.ZYNTHIUM_HYDRIDE, ResourceType.HYDROXIDE, ResourceType.ZYNTHIUM_ACID),
    new ChemicalReaction(ResourceType.LEMERGIUM_OXIDE, ResourceType.HYDROXIDE, ResourceType.LEMERGIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.LEMERGIUM_HYDRIDE, ResourceType.HYDROXIDE, ResourceType.LEMERGIUM_ACID),
    new ChemicalReaction(ResourceType.KEANIUM_OXIDE, ResourceType.HYDROXIDE, ResourceType.KEANIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.KEANIUM_HYDRIDE, ResourceType.HYDROXIDE, ResourceType.KEANIUM_ACID),
    new ChemicalReaction(ResourceType.UTRIUM_OXIDE, ResourceType.HYDROXIDE, ResourceType.UTRIUM_ALKALIDE),
    new ChemicalReaction(ResourceType.UTRIUM_HYDRIDE, ResourceType.HYDROXIDE, ResourceType.UTRIUM_ACID),

    new ChemicalReaction(ResourceType.GHODIUM, ResourceType.OXYGEN, ResourceType.GHODIUM_OXIDE),
    new ChemicalReaction(ResourceType.GHODIUM, ResourceType.HYDROGEN, ResourceType.GHODIUM_HYDRIDE),
    new ChemicalReaction(ResourceType.ZYNTHIUM, ResourceType.OXYGEN, ResourceType.ZYNTHIUM_OXIDE),
    new ChemicalReaction(ResourceType.ZYNTHIUM, ResourceType.HYDROGEN, ResourceType.ZYNTHIUM_HYDRIDE),
    new ChemicalReaction(ResourceType.LEMERGIUM, ResourceType.OXYGEN, ResourceType.LEMERGIUM_OXIDE),
    new ChemicalReaction(ResourceType.LEMERGIUM, ResourceType.HYDROGEN, ResourceType.LEMERGIUM_HYDRIDE),
    new ChemicalReaction(ResourceType.KEANIUM, ResourceType.OXYGEN, ResourceType.KEANIUM_OXIDE),
    new ChemicalReaction(ResourceType.KEANIUM, ResourceType.HYDROGEN, ResourceType.KEANIUM_HYDRIDE),
    new ChemicalReaction(ResourceType.UTRIUM, ResourceType.OXYGEN, ResourceType.UTRIUM_OXIDE),
    new ChemicalReaction(ResourceType.UTRIUM, ResourceType.HYDROGEN, ResourceType.UTRIUM_HYDRIDE),

    new ChemicalReaction(ResourceType.ZYNTHIUM_KEANITE, ResourceType.UTRIUM_LEMERGITE, ResourceType.GHODIUM),
    new ChemicalReaction(ResourceType.OXYGEN, ResourceType.HYDROGEN, ResourceType.HYDROXIDE),
    new ChemicalReaction(ResourceType.ZYNTHIUM, ResourceType.KEANIUM, ResourceType.ZYNTHIUM_KEANITE),
    new ChemicalReaction(ResourceType.UTRIUM, ResourceType.LEMERGIUM, ResourceType.UTRIUM_LEMERGITE),
];