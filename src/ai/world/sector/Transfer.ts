import { AICooldownBase } from "ai/CooldownBase";
import { Terminal } from "class/buildings/Terminal";
import { Sector } from "class/world/Sector";
import { IScreepsGame, IScreepsOrder } from "screeps";
import { OrderType, ResourceType, Result } from "util/Constants";

declare const Game: IScreepsGame;

export class AISectorTransfer extends AICooldownBase {

    private sector: Sector;

    private prevBuyIndex = 0;

    private prevSellIndex = 0;

    constructor(sector: Sector) {
        super(100);
        this.sector = sector;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a terminal and a cooldown of 100.
     */
    public shouldExecute(): boolean {
        return this.sector.getBuildings()?.getTerminal() != undefined && super.shouldExecute();
    }

    /**
     * Transfers minerals to other sectors or sells them.
     */
    public run(): void {
        super.run();
        const terminal = this.sector.getBuildings()?.getTerminal();
        if (!terminal) return;
        // find resource to send away
        let resource: ResourceType | undefined;
        const terminalResources = terminal.getContainedResources();
        for (let i = 0; i < terminalResources.length; i++) {
            const index = (this.prevSellIndex + i) % terminalResources.length;
            const r = terminalResources[index];
            let maxLimit = 9000;
            if (r == ResourceType.ENERGY) {
                maxLimit = 20000;
            }
            if (terminal.getUsedCapacity(r as ResourceType) >= maxLimit) {
                this.prevSellIndex = index+1;
                resource = r as ResourceType;
                break;
            }
        }
        if (resource) {
            const success = this.sendResource(resource, terminal);
            if (success) {
                return;
            }
        }
        // find resource to buy
        const elements = [ResourceType.OXYGEN, ResourceType.HYDROGEN, ResourceType.UTRIUM, ResourceType.LEMERGIUM, 
            ResourceType.KEANIUM, ResourceType.ZYNTHIUM];
        let wantedResource: ResourceType | undefined = undefined;
        let wantedAmount = 0;
        for (let i = 0; i < elements.length; i++) {
            const index = (this.prevBuyIndex + i) % elements.length;
            const e = elements[index];
            const amount = terminal.getUsedCapacity(e);
            if (amount < 3000) {
                wantedAmount = 3000 - amount;
                wantedResource = e;
                this.prevBuyIndex = index+1;
                break;
            }
        }
        if (wantedResource) {
            this.buyResource(wantedResource, wantedAmount);
        }
    }

    /**
     * Buys the given resource.
     * @param resource The resource to buy.
     * @param wantedAmount The amount wanted.
     * @returns If the ressource was bought sucessfully.
     */
    private buyResource(resource: ResourceType, wantedAmount: number): boolean {
        // find all buy orders
        const orders = Game.market.getAllOrders({
            type: OrderType.SELL,
            resourceType: resource,
        });
        // find cheapest offer
        let minPrice = 1;
        let bestOrder: IScreepsOrder | undefined = undefined;
        for (const o of orders) {
            if (o.price < minPrice) {
                minPrice= o.price;
                bestOrder = o;
            }
        }
        if (!bestOrder) {
            return false;
        }
        // execute order
        const result = Game.market.deal(bestOrder.id, Math.min(bestOrder.amount, wantedAmount), this.sector.roomName);
        if (result != Result.OK) {
            console.log("TransferAI: Error executing buy deal", result);
            return false;
        }
        return true;
    }

    /**
     * Sends the given resource to another room or sells it.
     * @param resource The resource type to send.
     * @param terminal The terminal to use.
     * @returns If the ressource was sent sucessfully.
     */
    private sendResource(resource: ResourceType, terminal: Terminal): boolean {
        // find other sector that needs this resource
        const target = this.sector.empire.getAllSectors().find(s => {
            const resources = s.getBuildings()?.getTerminal()?.getUsedCapacity(resource);
            if (resources == undefined) return false;
            return resources < 6000;
        });
        if (target) {
            // send to target terminal
            const currentAmount = target.getBuildings()?.getTerminal()?.getUsedCapacity(resource);
            if (currentAmount == undefined) return false;
            terminal.sendResources(resource, 6000-currentAmount, target.roomName);
            return true;
        }
        // find all buy orders
        const orders = Game.market.getAllOrders({
            type: OrderType.BUY,
            resourceType: resource,
        });
        // find order offering the most credits
        let maxPrice = 0.2;
        let bestOrder: IScreepsOrder | undefined = undefined;
        for (const o of orders) {
            if (o.price > maxPrice) {
                maxPrice = o.price;
                bestOrder = o;
            }
        }
        if (!bestOrder) {
            return false;
        }
        // execute order
        const currentAmount = terminal.getUsedCapacity(resource);
        const result = Game.market.deal(bestOrder.id, Math.min(bestOrder.amount, currentAmount-6000), this.sector.roomName);
        if (result != Result.OK) {
            console.log("TransferAI: Error executing sell deal", result);
            return false;
        }
        return true;
    }
}