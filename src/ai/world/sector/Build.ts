import { WorldBase } from "class/world/Base";
import { IScreepsRoomPosition, IScreepsStructure } from "screeps";
import { IBuildConfig, IPosition } from "types";
import { LookType, Result, StructureType, TerrainType } from "util/Constants";
import { Helper } from "util/Helper";
import { Names } from "util/Names";
import { AIWorldBuildBase } from "../BuildBase";
declare const RoomPosition: IScreepsRoomPosition;

/**
 * AI module that handles placement of construction sites.
 */
export class AISectorBuild extends AIWorldBuildBase {

    private isDone = true;

    constructor(sector: WorldBase) {
        super(sector, 100);
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if building is in progress or there are no construction sites.
     */
    public shouldExecute(): boolean {
        if (this.isDone && !super.shouldExecute()) return false;
        return (!this.isDone) || ((this.room.getBuildings()?.getConstructionSites().length ?? 0) == 0);
    }

    /**
     * Places construction sites.
     */
    public run(): void {
        super.run();
        const memory = this.room.getMemory();
        const config = memory.bq || { l: 0 };
        if (super.shouldRebuild()) {
            this.isDone = super.rebuild();
        }
        else if (config.l == 0 && this.room.getLevel() >= 1) {
            this.isDone = this.buildFirstSpawn(config);
        }
        else if (config.l == 1) {
            this.isDone = this.buildSourceContainer(config);
        }
        else if (config.l == 2) {
            this.isDone = this.buildSourceRoads(config);
        }
        else if (config.l == 3) {
            this.isDone = this.buildCentreContainers(config);
        }
        else if (config.l == 4) {
            this.isDone = this.buildControllerRoad(config);
        }
        else if (config.l == 5) {
            this.isDone = this.buildRoadBetweenSources(config);
        }
        else if (config.l == 6) {
            this.isDone = this.buildRoadBetweenSourcesAndController(config);
        }
        else if (config.l == 7) {
            this.isDone = this.buildRoadToExits(config);
        }
        else if (config.l == 8) {
            this.isDone = this.buildWalls(config);
        }
        else if (config.l == 9 && this.room.getLevel() > 1) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 5);
        }
        else if (config.l == 10 && this.room.getLevel() > 2) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.TOWER, 1);
        }
        else if (config.l == 11) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 5);
        }
        else if (config.l == 12 && this.room.getLevel() > 3) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 5);
        }
        else if (config.l == 13) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.STORAGE, 1);
        }
        else if (config.l == 14) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 5);
        }
        else if (config.l == 15 && this.room.getLevel() > 4) {
            this.isDone = this.buildCentreLink(config);
        }
        else if (config.l == 16) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.CONTAINER, 1);
        } 
        else if (config.l == 17) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 5);
        }        
        else if (config.l == 18) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.TOWER, 1);
        }
        else if (config.l == 19) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 5);
        }
        else if (config.l == 20) {
            this.isDone = this.buildSourceLink(config, 0);
        }
        else if (config.l == 21) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.CONTAINER, 1);
        }
        else if (config.l == 22 && this.room.getLevel() > 5) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 10);
        }
        else if (config.l == 23) {
            this.isDone = this.buildSourceLink(config, 1);
        }
        else if (config.l == 24) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.CONTAINER, 1);
        }
        else if (config.l == 25 && this.room.getLevel() > 6) {
            this.isDone = this.buildCentreSpawn(config);
        }
        else if (config.l == 26) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 10);
        }
        else if (config.l == 27) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.TOWER, 1);
        }
        else if (config.l == 28) {
            this.isDone = this.buildControllerLink(config);
        }
        else if (config.l == 29) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.TERMINAL, 1);
        }
        else if (config.l == 30) {
            this.isDone = this.buildRoadToMineral(config);
        }
        else if (config.l == 31) {
            this.isDone = this.buildMineralContainer(config);
        }
        else if (config.l == 32) {
            this.isDone = this.buildExtractor(config);
        }
        else if (config.l == 33 && this.room.getLevel() > 7) {
            this.isDone = this.buildCentreSpawn(config);
        }
        else if (config.l == 34) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 3);
        }
        else if (config.l == 35) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.TOWER, 1);
        }
        else if (config.l == 36) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 3);
        }
        else if (config.l == 37) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.TOWER, 1);
        }
        else if (config.l == 38) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 3);
        }
        else if (config.l == 39) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.TOWER, 1);
        }
        else if (config.l == 40) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.EXTENSION, 1);
        }
        else if (config.l == 41) {
            this.isDone = this.buildLabComplex(config);
        }
        else if (config.l == 42) {
            this.isDone = this.buildLabComplex(config);
        }
        else if (config.l == 43) {
            this.isDone = this.buildStructuresInCentre(config, StructureType.NUKER, 1);
        }
        memory.bq = config;
        this.room.setMemory(memory);
    }

    /**
     * Builds the first spawn.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildFirstSpawn(conf: IBuildConfig): boolean {
        // find best position to place the spawn
        const pos = Helper.findLargestArea((x, y) => this.room.getTerrainAt(x, y));
        const result = this.room.createConstructionSite(pos.x, pos.y, StructureType.SPAWN, Names.getNextSpawnName());
        if (result !== Result.OK) {
            console.log("Error: Cannot place spawn! " + result);
        }
        conf.l = 1;
        return true;
    }

    /**
     * Builds containers next to source.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildSourceContainer(conf: IBuildConfig): boolean {
        const step = conf.s || 0;
        const source = this.room.getSources()[step];
        if (source) {
            const space = source.workspaces[0];
            const result = this.room.createConstructionSite(space.x, space.y, StructureType.CONTAINER);
            if (result === Result.OK) {
                conf.s = step + 1;
            }
            else {
                console.log("Error! Can not build container: " + result);
            }
            return false;
        }
        else {
            delete conf.s;
            conf.l += 1;
            return true;
        }
    }

    /**
     * Builds roads from the spawn to the source.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildSourceRoads(conf: IBuildConfig): boolean {
        const step = conf.s || 0;
        const source = this.room.getSources()[step];
        if (source) {
            const sourcePos = source.getPosition();
            const spawnPos = this.room.getBuildings()?.getSpawns()[0].getPosition();
            if (sourcePos && spawnPos) {
                const startPos = new RoomPosition(spawnPos.x, spawnPos.y, this.room.roomName);
                const endPosition = new RoomPosition(sourcePos.x, sourcePos.y, this.room.roomName);
                super.buildRoadFromTo(startPos, endPosition);
            }
            conf.s = step + 1;
            return true;
        }
        else {
            delete conf.s;
            conf.l += 1;
            return true;
        }
    }

    /**
     * Builds the remaining containers in the centre.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildCentreContainers(conf: IBuildConfig): boolean {
        const step = conf.s || 0;
        const goal = 5 - this.room.getSources().length;
        if (step < goal) {
            const result = this.buildCentre(conf, StructureType.CONTAINER);
            if (result) {
                conf.s = step + 1;
            }
        }
        else {
            delete conf.s;
            conf.l += 1;
        }
        return true;
    }

    /**
     * Builds the road from the centre to the controller.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildControllerRoad(conf: IBuildConfig): boolean {
        const controller = this.room.getController();
        if (!controller) return true;
        const controllerPos = controller.getPosition();
        const spawnPos = this.room.getBuildings()?.getSpawns()[0].getPosition();
        if (!controllerPos || !spawnPos) return true;
        const startPos = new RoomPosition(spawnPos.x, spawnPos.y, this.room.roomName);
        const endPosition = new RoomPosition(controllerPos.x, controllerPos.y, this.room.roomName);
        super.buildRoadFromTo(startPos, endPosition);
        conf.l += 1;
        return true;
    }

    /**
     * Builds roads between all pairs of sources.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildRoadBetweenSources(conf: IBuildConfig): boolean {
        const step = conf.s || 0;
        const substep = conf.ss || 1;
        const source1 = this.room.getSources()[step];
        const source2 = this.room.getSources()[substep];
        if (source1 && source2) {
            const sPos1 = source1.getPosition();
            const sPos2 = source2.getPosition();
            if (!sPos1 || !sPos2) return true;
            const startPos = new RoomPosition(sPos1.x, sPos1.y, this.room.roomName);
            const endPosition = new RoomPosition(sPos2.x, sPos2.y, this.room.roomName);            
            super.buildRoadFromTo(startPos, endPosition);
            conf.ss = substep + 1;
        }
        if (!source1) {
            delete conf.s;
            delete conf.ss;
            conf.l += 1;
        }
        if (!source2) {
            conf.s = step + 1;
            conf.ss = step + 2;
        }
        return true;
    }

    /**
     * Builds roads between all source-controller pairs.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildRoadBetweenSourcesAndController(conf: IBuildConfig): boolean {
        const step = conf.s || 0;
        const source = this.room.getSources()[step];
        if (source) {
            const sPos = source.getPosition();
            const cPos = this.room.getController()?.getPosition();
            if (!sPos || !cPos) return true;
            const startPos = new RoomPosition(sPos.x, sPos.y, this.room.roomName);
            const endPosition = new RoomPosition(cPos.x, cPos.y, this.room.roomName);
            super.buildRoadFromTo(startPos, endPosition);
            conf.s = step + 1;
        }
        else {
            delete conf.s;
            conf.l += 1;
        }
        return true;
    }

    /**
     * Builds roads from the spawn to all exits.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildRoadToExits(conf: IBuildConfig): boolean {
        const step = conf.s || 0;
        // get the current exit
        const roomExits = this.room.describeExits();
        let roomName;
        if (step == 0) roomName = roomExits["1"];
        else if (step == 1) roomName = roomExits["3"];
        else if (step == 2) roomName = roomExits["5"];
        else if (step == 3) roomName = roomExits["7"];
        else {
            delete conf.s;
            conf.l += 1;
            return true;
        }
        if (roomName) {
            const spawnPos = this.room.getBuildings()?.getSpawns()[0].getPosition();
            if (!spawnPos) return true;
            const startPos = new RoomPosition(spawnPos.x, spawnPos.y, this.room.roomName);
            // build the road to the exit
            super.buildRoadToExit(startPos, roomName);
        }
        conf.s = step + 1;
        return true;
    }

    /**
     * Builds walls at the exits.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildWalls(conf: IBuildConfig): boolean {
        const step = conf.s || 0;
        const substep = conf.ss || 0;
        const isBuilding = conf.isBuilding || false;
        // builds wall or rampart at the given position depending on if there is a road or not
        const buildWall = (x: number, y: number) => {
            const road = this.room.getBuildings()?.getRoads().find(r => {
                const rPos = r.getPosition();
                return rPos && rPos.x == x && rPos.y == y;
            });
            if (road) {
                this.room.createConstructionSite(x, y, StructureType.RAMPART);                
            }
            else {
                this.room.createConstructionSite(x, y, StructureType.WALL);
            }
        };
        // calculates the position given the progress depending on the current direction
        const calculatePosition = (progress: number): IPosition => {
            if (step == 0) return new RoomPosition(progress, 0, this.room.roomName);
            else if (step == 1) return new RoomPosition(49, progress, this.room.roomName);
            else if (step == 2) return new RoomPosition(progress, 49, this.room.roomName);
            else return new RoomPosition(0, progress, this.room.roomName);
        };
        // builds a 3x3 corner at pos where dx and dy are the x and y directions to the corner point
        const buildCorner = (pos: IPosition, dx: number, dy: number): void => {
            // move to start position
            pos.x = pos.x + dx + dx;
            pos.y = pos.y + dy + dy;
            buildWall(pos.x, pos.y);
            // build x direction
            buildWall(pos.x - dx, pos.y);
            buildWall(pos.x - dx - dx, pos.y);
            // build y direction
            buildWall(pos.x, pos.y - dy);
            buildWall(pos.x, pos.y - dy - dy);

        };
        // builds the start of a wall depending on the current direction
        const buildBeginning = (pos: IPosition): void => {
            let dx = -1; let dy = -1;
            // -1, -1 already correct for steps 1 and 2
            if (step == 0) dy = 1;
            else if (step == 3) dx = 1;
            buildCorner(pos, dx, dy);
        };
        // continues the started wall depending on the current direction
        const continueWall = (pos: IPosition): void => {
            if (step == 0) pos.y = pos.y + 2;
            else if (step == 1) pos.x = pos.x - 2;
            else if (step == 2) pos.y = pos.y - 2;
            else pos.x = pos.x + 2;
            buildWall(pos.x, pos.y);
        };
        // builds the end of a wall depending on the current direction
        const buildEnd = (pos: IPosition): void => {
            let dx = 1; let dy = 1;
            // 1, 1 already correct for steps 0 and 3
            if (step == 1) dx = -1;
            else if (step == 2) dy = -1;
            buildCorner(pos, dx, dy);
        };
        // check the borders for non wall terrain and build wall there
        const pos = calculatePosition(substep);
        const terrain = this.room.getTerrainAt(pos.x, pos.y);
        if (terrain != TerrainType.WALL) {
            if (!isBuilding) {
                // create start of wall
                buildBeginning(pos);
                conf.isBuilding = true;
            }
            else {
                // continue wall
                continueWall(pos);
            }
        }
        else if (isBuilding) {
            // finish current wall
            buildEnd(calculatePosition(substep-1));
            conf.isBuilding = false;
        }
        if (step == 3 && substep == 49) {
            delete conf.s;
            delete conf.ss;
            delete conf.isBuilding;
            conf.l += 1;
            return true;
        }
        if (substep == 49) {
            conf.s = step + 1;
            conf.ss = 0;
            conf.isBuilding = false;
            return true;
        }
        else {
            conf.ss = substep + 1;
            return false;
        }
    }

    /**
     * Dismantles a container and builds a link instead.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildCentreLink(conf: IBuildConfig): boolean {
        const step = conf.s || 0;
        if (step == 0) {
            // deconstruct one centre container
            const container = this.room.getBuildings()?.getCentreStores()[0];
            if (!container) return true;
            conf.pos = container.getPosition();
            this.room.getBuildings()?.addDeconstructionTarget(container);
            conf.s = 1;
            return true;
        }
        else if (step == 1) {
            // check if container was deconstructed
            const container = this.room.getBuildings()?.getCentreStores().find(c => {
                const cPos = c.getPosition();
                return cPos && conf.pos && cPos.x == conf.pos.x && cPos.y == conf.pos.y;
            });
            if (!container) {
                conf.s = 2;
            }
            return true;
        }
        else if (step == 2) {
            if (!conf.pos) return false;
            this.room.createConstructionSite(conf.pos.x, conf.pos.y, StructureType.LINK);
            conf.l += 1;
            delete conf.pos;
            delete conf.s;
        }
        return true;
    }

    /**
     * Replaces the container with a link at the given source.
     * @param conf The build configuration.
     * @param sourceIndex The index of the source to build a link for.
     * @returns If the AI is done.
     */
    private buildSourceLink(conf: IBuildConfig, sourceIndex: number): boolean {
        const step = conf.s || 0;
        const source = this.room.getSources()[sourceIndex];
        if (source && step == 0) {
            // deconstruct container
            const container = this.room.getBuildings()?.getPeripheryContainers().find(c => {
                const sPos = source.getPosition();
                const cPos = c.getPosition();
                if (sPos && cPos) return Helper.distance(sPos, cPos) < 3;
                return false;
            });
            if (container) this.room.getBuildings()?.addDeconstructionTarget(container);
            conf.s = 1;
            return true;
        }
        else if (source && step == 1) {
            // check if container was deconstructed
            const container = this.room.getBuildings()?.getPeripheryContainers().find(c => {
                const sPos = source.getPosition();
                const cPos = c.getPosition();
                if (sPos && cPos) return Helper.distance(sPos, cPos) < 3;
                return false;
            });
            if (!container) {
                conf.s = 2;
            }
            return true;
        }
        else if (source && step == 2) {
            // find optimal workspace
            const sPos = source.getPosition();
            if (!sPos) {console.error("Source to build link for has no position!"); return true;}
            let perfectWorkspace: {x: number, y: number} | undefined;
            let map = this.room.lookForAtArea(LookType.LOOK_STRUCTURES, sPos.y-1, sPos.x-1, sPos.y+1, sPos.x+1);
            for (const result of map as {x: number, y: number, structure: IScreepsStructure}[]) {
                if (result.structure.structureType == StructureType.ROAD) {
                    perfectWorkspace = result;
                    break;
                }
            }
            if (!perfectWorkspace) {console.error("Source has no perfect workspace!"); return true;}
            // find space next to optimal workspace to build link
            const roadMap: {[x: number]: {[y: number]: boolean} | undefined} = {};
            map = this.room.lookForAtArea(LookType.LOOK_STRUCTURES, perfectWorkspace.y-1, perfectWorkspace.x-1, perfectWorkspace.y+1, perfectWorkspace.x+1);
            for (const result of map as {x: number, y: number, structure: IScreepsStructure}[]) {
                if (result.structure.structureType == StructureType.ROAD) {
                    const submap = roadMap[result.x] ?? {};
                    submap[result.y] = true;
                    roadMap[result.x] = submap;
                }
            }
            for (let x = perfectWorkspace.x - 1; x <= perfectWorkspace.x + 1; x++) {
                for (let y = perfectWorkspace.y - 1; y <= perfectWorkspace.y + 1; y++) {
                    if (!roadMap[x]?.[y]) {
                        if (this.room.createConstructionSite(x, y, StructureType.LINK) == Result.OK) {
                            conf.l += 1;
                            delete conf.s;
                            return true;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Dismantles a container and builds a spawn instead.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildCentreSpawn(conf: IBuildConfig): boolean {
        const step = conf.s || 0;
        if (step == 0) {
            // deconstruct one centre container
            const container = this.room.getBuildings()?.getCentreStores()[0];
            if (!container) return true;
            conf.pos = container.getPosition();
            this.room.getBuildings()?.addDeconstructionTarget(container);
            conf.s = 1;
            return true;
        }
        else if (step == 1) {
            // check if container was deconstructed
            const container = this.room.getBuildings()?.getCentreStores().find(c => {
                const cPos = c.getPosition();
                return cPos && conf.pos && cPos.x == conf.pos.x && cPos.y == conf.pos.y;
            });
            if (!container) {
                conf.s = 2;
            }
            return true;
        }
        else if (step == 2) {
            if (!conf.pos) return true;
            this.room.createConstructionSite(conf.pos.x, conf.pos.y, StructureType.SPAWN, Names.getNextSpawnName());
            conf.l += 1;
            delete conf.pos;
            delete conf.s;
        }
        return true;
    }

    /**
     * Builds a link next to the controller.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildControllerLink(conf: IBuildConfig): boolean {
        const cPos = this.room.getController()?.getPosition();
        if (!cPos) return true;
        const map = this.room.lookForAtArea(LookType.LOOK_TERRAIN, cPos.y-1, cPos.x-1, cPos.y+1, cPos.x+1);
        for (const m of map as {x: number, y: number, terrain: TerrainType}[]) {
            if (m.terrain != TerrainType.WALL) {
                this.room.createConstructionSite(m.x, m.y, StructureType.LINK);
                conf.l += 1;
                return true;
            }
        }
        return true;
    }

    /**
     * Builds five labs in an x formation.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildLabComplex(conf: IBuildConfig): boolean {
        // helper function to check a single position
        const checkPosition = (x: number, y: number) => {
            const structures = this.room.lookForAt(LookType.LOOK_STRUCTURES, x, y);
            const terrain = this.room.lookForAt(LookType.LOOK_TERRAIN, x, y);
            return (structures.length == 0 && (terrain[0] as TerrainType) != TerrainType.WALL);
        }
        // helper function to check a x formation, starting from the middle
        const checkComplex = (x: number, y: number) => {
            return checkPosition(x+1, y+1) && checkPosition(x-1, y-1) && checkPosition(x+1, y-1) && checkPosition(x-1, y+1) && checkPosition(x, y);
        }
        // create second index to not interfere with centre building
        const step = conf.s || conf.cs?.i || 0;
        const subStep = conf.ss || 0;
        const pos = Helper.getSpiralPosition(conf.cs?.x ?? 0, conf.cs?.y ?? 0, step);
        console.log("buildLabComplex: Looking at", pos.x, pos.y);
        if (step % 2 == 0) {
            console.log("buildLabComplex: Building road");
            // build road and not the actual building
            if (this.room.getTerrainAt(pos.x, pos.y) != TerrainType.WALL) {
                this.room.createConstructionSite(pos.x, pos.y, StructureType.ROAD);
            }
            conf.s = step + 1;
        }
        else {
            // check one of four possibilities
            console.log("buildLabComplex: Checking one possibility", subStep);
            let x = pos.x;
            let y = pos.y;
            if (subStep < 2) x++;
            else x--;
            if (subStep % 2 == 0) y++;
            else y--;
            if (checkComplex(x, y)) {
                console.log("buildLabComplex: Foud free complex around", x, y);
                this.room.createConstructionSite(x, y, StructureType.LAB);
                this.room.createConstructionSite(x+1, y+1, StructureType.LAB);
                this.room.createConstructionSite(x+1, y-1, StructureType.LAB);
                this.room.createConstructionSite(x-1, y+1, StructureType.LAB);
                this.room.createConstructionSite(x-1, y-1, StructureType.LAB);
                this.room.createConstructionSite(x+1, y, StructureType.ROAD);
                this.room.createConstructionSite(x, y+1, StructureType.ROAD);
                this.room.createConstructionSite(x-1, y, StructureType.ROAD);
                this.room.createConstructionSite(x, y-1, StructureType.ROAD);
                delete conf.s;
                delete conf.ss;
                conf.l += 1;
                return true;
            }
            if (subStep == 3) {
                conf.ss = 0;
                conf.s = step + 1;
            }
            else {
                conf.ss = subStep + 1;
            }
        }
        return false;
    }

    /**
     * Builds a road from the centre to the mineral deposit.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    public buildRoadToMineral(conf: IBuildConfig): boolean {
        const mineral = this.room.getMineral();
        if (!mineral) return true;
        const mineralPos = mineral.getPosition();
        const spawnPos = this.room.getBuildings()?.getSpawns()[0].getPosition();
        if (!mineralPos || !spawnPos) return true;
        const startPos = new RoomPosition(spawnPos.x, spawnPos.y, this.room.roomName);
        const endPosition = new RoomPosition(mineralPos.x, mineralPos.y, this.room.roomName);
        super.buildRoadFromTo(startPos, endPosition);
        conf.l += 1;
        return true;
    }

    /**
     * Builds a container next to the mineral deposit.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildMineralContainer(conf: IBuildConfig): boolean {
        const mPos = this.room.getMineral()?.getPosition();
        if (mPos) {
            // find optimal workspace
            let perfectWorkspace: {x: number, y: number} | undefined;
            const map = this.room.lookForAtArea(LookType.LOOK_STRUCTURES, mPos.y-1, mPos.x-1, mPos.y+1, mPos.x+1);
            for (const result of map as {x: number, y: number, structure: IScreepsStructure}[]) {
                if (result.structure.structureType == StructureType.ROAD) {
                    perfectWorkspace = result;
                    break;
                }
            }
            if (!perfectWorkspace) {console.error("Mineral has no perfect workspace!"); return true;}
            this.room.createConstructionSite(perfectWorkspace.x, perfectWorkspace.y, StructureType.CONTAINER);
        }
        conf.l += 1;
        return true;
    }

    /**
     * Builds the extractor on the mineral deposit.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    public buildExtractor(config: IBuildConfig): boolean {
        const mPos = this.room.getMineral()?.getPosition();
        if (mPos) {
            this.room.createConstructionSite(mPos.x, mPos.y, StructureType.EXTRACTOR);
        }
        config.l += 1;
        return true;
    }

    /**
     * Builds the given amount of the given structures in the centre.
     * @param conf The build configuration.
     * @param structure The structure to build.
     * @param amount The number of structures to build.
     */
    private buildStructuresInCentre(conf: IBuildConfig, structure: StructureType, amount: number): boolean {
        const step = conf.s || 0;
        if (step < amount) {
            const result = this.buildCentre(conf, structure);
            if (result) {
                conf.s = step + 1;
            }
            return false;
        }
        else {
            delete conf.s;
            conf.l += 1;
            return true;
        }
    }

    /**
     * Helper function to build the given structure in the centre. Will automatically add roads.
     * @param conf Build configuration object.
     * @param building Building to create.
     * @param name Optional name of the building, only for spawns.
     * @returns If the building was build yet.
     */
    private buildCentre(conf: IBuildConfig, building: StructureType, name?: string): boolean {
        let state = conf.cs;
        if (!state) {
            const spawnPos = this.room.getBuildings()?.getSpawns()[0].getPosition();
            if (!spawnPos) return false;
            state = { x: spawnPos.x, y: spawnPos.y, i: 1 };
        }
        let didSucceed = true;
        const pos = Helper.getSpiralPosition(state.x, state.y, state.i);
        if (pos.x > 2 && pos.y > 2 && pos.x < 48 && pos.y < 48) {
            if (state.i % 2 == 0) {
                // build road and not the actual building
                if (this.room.getTerrainAt(pos.x, pos.y) != TerrainType.WALL) {
                    this.room.createConstructionSite(pos.x, pos.y, StructureType.ROAD);
                }
                didSucceed = false;
            }
            else {
                const buildPos = new RoomPosition(pos.x, pos.y, this.room.roomName);
                const closeSource = this.room.getSources().find(s => {
                    const sPos = s.getPosition();
                    if (sPos) return Helper.distance(sPos, buildPos) < 3;
                    return false;
                });
                if (closeSource == undefined) {
                    // build actual building
                    const result = this.room.createConstructionSite(pos.x, pos.y, building, name);
                    didSucceed = (result === Result.OK);
                    if (!didSucceed) {
                        console.log("AISectorBuild error: " + result);
                    }
                }
                else {
                    didSucceed = false;
                }
            }
        }        
        // increase spiral index
        state.i += 1;
        conf.cs = state;
        return didSucceed;
    }
}