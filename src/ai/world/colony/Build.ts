import { Colony } from "class/world/Colony";
import { IScreepsRoomPosition } from "screeps";
import { IBuildConfig } from "types";
import { Result, StructureType } from "util/Constants";
import { Helper } from "util/Helper";
import { AIWorldBuildBase } from "../BuildBase";

declare const RoomPosition: IScreepsRoomPosition;

export class AIColonyBuild extends AIWorldBuildBase {
    
    private isDone = true;

    constructor(colony: Colony) {
        super(colony, 100);
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if building is in progress or there are no construction sites.
     */
    public shouldExecute(): boolean {
        if (this.isDone && !super.shouldExecute()) return false;
        if (this.room.getController() == undefined) return false;
        if (this.room.getController()?.getReservation()?.username != "Augl") return false;
        return (!this.isDone) || ((this.room.getBuildings()?.getConstructionSites().length ?? 0) == 0);
    }

    /**
     * Places construction sites.
     */
    public run(): void {
        super.run();
        const memory = this.room.getMemory();
        const config = memory.bq || { l: 0 };
        if (config.l == 0) {
            this.isDone = this.buildSourceContainer(config);
        }
        else if (config.l == 1) {
            this.isDone = this.buildRoadsToExit(config);
        }
        else if (config.l == 2) {            
            this.isDone = super.rebuild();
            if (this.isDone) config.l += 1;
        }
        else if (config.l == 3) {
            this.isDone = this.connectNeighbours(config);
        }
        else {
            config.l = 0;
        }
        memory.bq = config;
        this.room.setMemory(memory);
    }

    /**
     * Builds containers next to source.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildSourceContainer(conf: IBuildConfig): boolean {  
        const step = conf.s || 0;
        const source = this.room.getSources()[step];
        console.log("BuildSourceCointainer 1", this.room.roomName, this.room.getSources(), source);
        if (source) {
            console.log("BuildSourceCointainer 2", this.room.roomName);        
            // check if container already exists
            const sourcePos = source.getPosition();
            const container = this.room.getBuildings()?.getPeripheryContainers().find(c => {
                const cPos = c.getPosition();
                if (cPos && sourcePos) return Helper.distance(cPos, sourcePos) < 3;
                return false;
            });
            if (container) {
                console.log("BuildSourceCointainer 3", this.room.roomName);        
                conf.s = step + 1;
                return true;
            }
            const space = source.workspaces[0];
            const result = this.room.createConstructionSite(space.x, space.y, StructureType.CONTAINER);
            if (result === Result.OK) {
                console.log("BuildSourceCointainer 4", this.room.roomName);        
                conf.s = step + 1;
            }
            else {
                console.log("Error! Can not build container: " + result);
            }
            return false;
        }
        else {
            delete conf.s;
            conf.l += 1;
            console.log("BuildSourceCointainer 5", this.room.roomName);        
            return true;
        }
    }

    /**
     * Builds the roads from the sources to the exit.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private buildRoadsToExit(conf: IBuildConfig): boolean {
        const step = conf.s ?? 0;
        const sector = (this.room as Colony).getStorageRoom();
        const source = this.room.getSources()[step];
        const sourcePos = source?.getPosition();
        if (sector && source && sourcePos) {
            // build the road to the exit            
            const interPath = this.room.empire.getPath(this.room.roomName, sector.roomName);
            const startPos = new RoomPosition(sourcePos.x, sourcePos.y, this.room.roomName);
            super.buildRoadToExit(startPos, interPath[0]);
            conf.s = step + 1;
        }
        else {
            delete conf.s;
            conf.l += 1;
        }
        return true;
    }

    /**
     * Builds roads to connect all neighbours.
     * @param conf The build configuration.
     * @returns If the AI is done.
     */
    private connectNeighbours(conf: IBuildConfig): boolean {
        const step = conf.s ?? 0;
        const substep = conf.ss ?? 1;
        const start = this.room.getNeighbours()[step];
        const end = this.room.getNeighbours()[substep];
        if (!start) {
            delete conf.ss;
            delete conf.s;
            conf.l += 1;
            return false;
        }
        if (!end) {
            conf.s = step + 1;
            conf.ss = step + 2;
            return false;
        }
        const startRoom = this.room.empire.getWorld(start);
        const endRoom = this.room.empire.getWorld(end);
        if (startRoom?.isMine() && endRoom?.isMine()) {
            super.buildRoadConnectingNeighbours(startRoom, endRoom);
        }
        conf.ss = substep + 1;
        return false;
    }
}