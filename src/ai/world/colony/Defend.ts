import { AIBase } from "ai/Base";
import { Colony } from "class/world/Colony";
import { PlatoonType } from "util/Constants";

export class AIColonyDefend extends AIBase {

    private colony: Colony;

    constructor(colony: Colony) {
        super();
        this.colony = colony;
    }

    public shouldExecute(): boolean {
        return this.colony.getEnemies().length > 0 || this.colony.getBuildings()?.getInvaderCore() != undefined;
    }

    public run(): void {
        if (this.colony.empire.numberOfPlatoons(this.colony.roomName) == 0) {
            if (this.colony.getEnemies().length == 1) {
                console.log("Requesting tiny defense platoon!");
                this.colony.empire.requestPlatoon(this.colony, PlatoonType.TINY_DEFENSE);
            }
            else {
                console.log("Requesting small defense platoon!");
                this.colony.empire.requestPlatoon(this.colony, PlatoonType.SMALL_DEFENSE);
            }
        }
    }
}