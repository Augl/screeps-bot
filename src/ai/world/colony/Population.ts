import { AICooldownBase } from "ai/CooldownBase";
import { Colony } from "class/world/Colony";
import { Sector } from "class/world/Sector";
import { IScreepsGame } from "screeps";
import { Job, Levels } from "util/Constants";

declare const Game: IScreepsGame;

export class AIColonyPopulation extends AICooldownBase {

    private colony: Colony;

    private numberOfCarriers = 1;

    private carrierPopulationUpdateTime = 0;

    constructor(colony: Colony) {
        super(100);
        this.colony = colony;
    }
    
    /**
     * Checks if it makes sense to execute this AI.
     * @returns True with a cooldown of 100 ticks.
     */
     public shouldExecute(): boolean {
        return super.shouldExecute();
    }

    /**
     * Checks which augls need to be spawned.
     */
     public run(): void {
        super.run();
        // find sector to spawn the augls
        const spawnSector = this.colony.empire.getClosestSpawnRoom(this.colony.roomName, 4) as Sector | undefined;
        if (!spawnSector) throw new Error("AIColonyPopulation: No spawn sector found!");
        // use maximal possible level
        let level = 0;
        for (let i = 0; i < Levels.length; i++) {
            if (Levels[i] <= spawnSector.getEnergyCapacity()) {
                level = i;
            }
        }
        if (level < 3) throw new Error("AIColonyPopulation: Energy level of " + spawnSector.roomName + " is below 3!");
        if (this.colony.getStorageRoom()) {
            this.checkHarvesterPopulation(level, spawnSector);
            this.checkCarrierPopulation(level, spawnSector);
        }
        this.checkBuilderPopulation(level, spawnSector);
        this.checkReserverPopulation(level, spawnSector);
        this.checkRepairmanPopulation(level, spawnSector);
    }

    private checkHarvesterPopulation(level: number, spawnSector: Sector): void {
        const sources = this.colony.getSources();
        const harvesters = this.colony.getAuglsWithJob(Job.HARVESTER);
        // create map of sources to number of harvesters required
        const requiredHarvesters: {[sourceID: string]: number} = {};
        for (const s of sources) {
            requiredHarvesters[s.sourceID] = 1;
        }
        // find source that requires a new harvester
        for (const a of harvesters) {
            if (!a.memory.sourceId) continue;             
            if (a.getTicksToLive() > 300) requiredHarvesters[a.memory.sourceId]--;
        }
        for (const sourceID in requiredHarvesters) {
            if (requiredHarvesters[sourceID] > 0) {
                spawnSector.addToSpawnQueue(Job.HARVESTER, {sourceId: sourceID}, level, this.colony.roomName);
                return;
            }
        }
    }

    private checkReserverPopulation(level: number, spawnSector: Sector): void {
        const controller = this.colony.getController();
        const reservers = this.colony.getAuglsWithJob(Job.RESERVER);
        if ((controller?.getReservation()?.ticksToEnd ?? 0 < 3000) && reservers.length < 1) {
            spawnSector.addToSpawnQueue(Job.RESERVER, {}, level, this.colony.roomName);
        }
    }

    private checkBuilderPopulation(level: number, spawnSector: Sector): void {
        const builder = this.colony.getAuglsWithJob(Job.BUILDER);
        const constructionSite = this.colony.getBuildings()?.getConstructionSites().find(cs => !cs.isDefenseBuilding());
        const deconstructionTargets = this.colony.getBuildings()?.getDeconstructionTargets() ?? [];
        if ((constructionSite != undefined || deconstructionTargets.length > 0) && builder.length < 2) {
            spawnSector.addToSpawnQueue(Job.BUILDER, {}, level, this.colony.roomName);
        }
    }

    private checkCarrierPopulation(level: number, spawnSector: Sector): void {
        const carriers = this.colony.getAuglsWithJob(Job.INTERCARRIER);
        const containers = this.colony.getBuildings()?.getPeripheryContainers() ?? [];
        if (containers.length == 0) return;
        if (Game.time > this.carrierPopulationUpdateTime && this.numberOfCarriers < 10 && containers.find(c => c.isFull())) {
            this.numberOfCarriers += 1;
            this.carrierPopulationUpdateTime = Game.time + 200;
        }
        if (Game.time > this.carrierPopulationUpdateTime && this.numberOfCarriers > 1 && containers.find(c => c.isEmpty())) {
            this.numberOfCarriers -= 1;
            this.carrierPopulationUpdateTime = Game.time + 200;
        }
        if (carriers.length < this.numberOfCarriers) {
            spawnSector.addToSpawnQueue(Job.INTERCARRIER, {}, level, this.colony.roomName);
        }
    }

    private checkRepairmanPopulation(level: number, spawnSector: Sector): void {
        const repairman = this.colony.getAuglsWithJob(Job.REPAIRMAN);
        const damaged = this.colony.getBuildings()?.getDamagedBuildings() ?? [];
        if (damaged.length > 0 && repairman.length < 1) {
            spawnSector.addToSpawnQueue(Job.REPAIRMAN, {}, level, this.colony.roomName);
        }
    }
}