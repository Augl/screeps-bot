import { AICooldownBase } from "ai/CooldownBase";
import { Confederate } from "class/world/Confederate";
import { Sector } from "class/world/Sector";
import { Job, Levels } from "util/Constants";

export class AIConfederatePopulation extends AICooldownBase {

    private confederate: Confederate;

    constructor(confederate: Confederate) {
        super(100);
        this.confederate = confederate;
    }
    
    /**
     * Checks if it makes sense to execute this AI.
     * @returns True with a cooldown of 100.
     */
     public shouldExecute(): boolean {
        return super.shouldExecute();
    }

    /**
     * Checks which augls need to be spawned.
     */
     public run(): void {
         super.run();
        // find sector to spawn the augls
        const spawnSector = this.confederate.empire.getClosestSpawnRoom(this.confederate.roomName, 1) as Sector;
        if (!spawnSector) throw new Error("AISectorPopulation: No spawn sector found!");
        // use maximal possible level
        let level = 0;
        for (let i = 0; i < Levels.length; i++) {
            if (Levels[i] <= spawnSector.getEnergyCapacity()) {
                level = i;
            }
        }
        this.checkExplorerPopulation(level, spawnSector);
    }

    private checkExplorerPopulation(level: number, spawnSector: Sector): void {
        const explorers = this.confederate.getAuglsWithJob(Job.EXPLORER);
        if (explorers.length < 1) {
            spawnSector.addToSpawnQueue(Job.EXPLORER, {targets: [this.confederate.roomName]}, level, this.confederate.roomName);
        }
    }
}