import { AICooldownBase } from "ai/CooldownBase";
import { WorldBase } from "class/world/Base";
import { Sector } from "class/world/Sector";
import { Job } from "util/Constants";

export class AISectorExplore extends AICooldownBase {

    private sector: WorldBase;

    private targets?: string[];

    constructor(sector: WorldBase) {
        super(100);
        this.sector = sector;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there are rooms to be explored.
     */
    public shouldExecute(): boolean {
        if (!super.shouldExecute()) return false;
        if (this.sector.getAuglsWithJob(Job.EXPLORER).length > 0) return false;
        // check if there are neighbours to explore
        this.targets = [];
        const neighbours = this.sector.getNeighbours();
        for (const name of neighbours) {
            const sector = this.sector.empire.getWorld(name);
            if (!sector) {
                this.targets.push(name);
            }
        }
        return this.targets.length > 0;
    }

    /**
     * Spawns explorer to check out neighbouring rooms.
     */
    public run(): void {
        super.run();
        const spawnSector = this.sector.empire.getClosestSpawnRoom(this.sector.roomName, 1);
        if (!spawnSector) {
            console.log("AISectorExplore: No sector to spawn explorer in!");
            return;
        }
        (spawnSector as Sector).addToSpawnQueue(Job.EXPLORER, {targets: this.targets}, 1, this.sector.roomName);
    }
}