import { AICooldownBase } from "ai/CooldownBase";
import { WorldBase } from "class/world/Base";
import { IScreepsCostMatrix, IScreepsGame, IScreepsPathFinder, IScreepsRoomPosition, IScreepsStructure } from "screeps";
import { IPosition } from "types";
import { FindType, Result, StructureType } from "util/Constants";

declare const PathFinder: IScreepsPathFinder;
declare const RoomPosition: IScreepsRoomPosition;
declare const Game: IScreepsGame;

export abstract class AIWorldBuildBase extends AICooldownBase {

    protected room: WorldBase;

    constructor(room: WorldBase, cooldown: number) {
        super(cooldown);
        this.room = room;
    }

    /**
     * @returns If the rebuild AI should be executed.
     */
    public shouldRebuild(): boolean {
        const memory = this.room.getMemory();
        if (!memory.rq) return false;
        for (const ID in memory.rq) {
            return true;
        }
        return false;
    }

    /**
     * Rebuilds one destroyed building or construction site.
     */
    public rebuild(): boolean {
        const memory = this.room.getMemory();
        if (!memory.rq) return true;
        let target = undefined;
        let targetID = undefined;
        for (const ID in memory.rq) {
            target = memory.rq[ID];
            targetID = ID;
            break;
        }
        if (!target || !targetID) return true;
        const result = this.room.createConstructionSite(target.x, target.y, target.type as StructureType);
        let isDone = true;
        if (result == Result.OK) {
            // construction site created or building already exists at position
            delete memory.rq[targetID];
            console.log("Successfully rebuilt " + target.type + " at", target.x, target.y, "in", this.room.roomName)
        }
        else if (result == Result.ERR_INVALID_TARGET) {
            isDone = false;
            delete memory.rq[targetID];
            console.log("Skipping " + target.type + " at", target.x, target.y, "in", this.room.roomName)
        }
        else {
            console.log("RebuildAI result: ", result, this.room.roomName, target.type, target.x, target.y)
        }
        this.room.setMemory(memory);
        return isDone;
    }

    /**
     * Builds a road from the given start position to the exit to the given room.
     * @param startPos The road start position.
     * @param roomName The exit to choose.
     */
    public buildRoadToExit(startPos: IPosition, roomName: string): void {
        const goalPos = new RoomPosition(25, 25, roomName);
        const path = PathFinder.search(startPos, { pos: goalPos, range: 20 }, {
            plainCost: 2,
            swampCost: 2,
            maxRooms: 2,
            maxOps: 5000,
            roomCallback: (current: string) => {
                if (current != startPos.roomName && current != roomName) {
                    return false;
                }
                return this.getCostMatrix(current);
            }
        });
        for (const point of path.path) {
            if (point.roomName == this.room.roomName) this.room.createConstructionSite(point.x, point.y, StructureType.ROAD);
        }
    }

    /**
     * Builds a road from the given start position to the given end position in the same room.
     * @param startPos The desired start position.
     * @param endPos The desired end position.
     */
    public buildRoadFromTo(startPos: IPosition, endPos: IPosition): void {
        if (startPos.roomName != endPos.roomName) throw new Error("Start and end position must be in the same room!");
        const path = PathFinder.search(startPos, { pos: endPos, range: 1 }, {
            plainCost: 2,
            swampCost: 2,
            maxRooms: 1,
            maxOps: 5000,
            roomCallback: (roomName: string) => this.getCostMatrix(roomName)
        });
        for (const point of path.path) {
            this.room.createConstructionSite(point.x, point.y, StructureType.ROAD);
        }
    }

    /**
     * Builds a road from one neighbour to another.
     * @param startRoom The room to start from.
     * @param endRoom The room to end at.
     */
    public buildRoadConnectingNeighbours(startRoom: WorldBase, endRoom: WorldBase): void {
        const startPos = startRoom.getBuildings()?.getSpawns()[0]?.getPosition() ?? new RoomPosition(25, 25, startRoom.roomName);
        const goalPos = endRoom.getBuildings()?.getSpawns()[0]?.getPosition() ?? new RoomPosition(25, 25, endRoom.roomName);
        if (!startPos || !goalPos) return;
        const path = PathFinder.search(startPos, { pos: goalPos, range: 1 }, {
            plainCost: 2,
            swampCost: 2,
            maxRooms: 3,
            maxOps: 5000,
            roomCallback: (roomName: string) => {
                if (roomName != startPos.roomName && roomName != endRoom.roomName && roomName != this.room.roomName) return false;
                return this.getCostMatrix(roomName);
            }
        });
        for (const point of path.path) {
            if (point.roomName == this.room.roomName) this.room.createConstructionSite(point.x, point.y, StructureType.ROAD);
        }
    }

    /**
     * Calculates the cost matrix for the given room.
     * Does not use caching because it is supposed to be used very rarely.
     * @param roomName The name of the room.
     * @returns The room's cost matrix.
     */
    private getCostMatrix(roomName: string): IScreepsCostMatrix | undefined {
        const room = Game.rooms[roomName];
        if (!room) return;
        const costs = new PathFinder.CostMatrix;
        room.find<IScreepsStructure>(FindType.FIND_STRUCTURES).forEach(function (struct) {
            if (struct.structureType == StructureType.ROAD) {
                // Favor roads over plain tiles
                costs.set(struct.pos.x, struct.pos.y, 1);
            }
            else if (struct.structureType != StructureType.CONTAINER && struct.structureType != StructureType.RAMPART) {
                // Can't walk through non-walkable buildings
                costs.set(struct.pos.x, struct.pos.y, 0xff);
            }
        });
        return costs;
    }
}