import { AIBase } from "ai/Base";
import { PlatoonBase } from "class/military/platoons/PlatoonBase";
import { IScreepsGame } from "screeps";
import { MilitaryAction } from "util/Constants";

declare const Game: IScreepsGame;

/**
 * AI module that handles coordination of squads.
 */
export class AIPlatoonControl extends AIBase {

    private platoon: PlatoonBase;

    constructor(platoon: PlatoonBase) {
        super();
        this.platoon = platoon;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True every three ticks.
     */
    public shouldExecute(): boolean {
        return Game.time % 3 == 0;
    }

    /**
     * Coordinates squad behaviour.
     */
    public run(): void {
        const squads = this.platoon.getSquads();
        const currentAction = this.platoon.getAction();
        if (currentAction == MilitaryAction.SPAWN) {
            console.log("Checking if squads have spawned for platoon ", this.platoon.getName());
            // check if all squads have gathered
            let haveSpawned = true;
            for (const squad of squads) {
                haveSpawned = haveSpawned && squad.hasSpawned();
            }
            if (haveSpawned) {
                console.log("Setting platoon action to gather for platoon ", this.platoon.getName());
                this.platoon.setAction(MilitaryAction.GATHER);
            }
        }
        else if (currentAction == MilitaryAction.GATHER) {
            console.log("Checking if squads have gathered for platoon ", this.platoon.getName());
            // check if all squads have gathered
            let haveGathered = true;
            for (const squad of squads) {
                haveGathered = haveGathered && squad.hasGathered();
            }
            if (haveGathered) {
                console.log("Setting platoon action to fight for platoon ", this.platoon.getName());
                this.platoon.setAction(MilitaryAction.FIGHT);
            }
        }
        if (currentAction == MilitaryAction.FIGHT || currentAction == MilitaryAction.GATHER) {
            // check if the squads have been eliminated
            let eliminated = true;
            for (const squad of squads) {
                eliminated = eliminated && squad.wasEliminated();
            }
            if (eliminated) {
                console.log("Setting platoon action to eliminated for platoon ", this.platoon.getName());
                this.platoon.setAction(MilitaryAction.ELIMINATED);
            }
        }
    }
}