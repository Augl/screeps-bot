import { SquadBase } from "class/military/squads/SquadBase";
import { Sector } from "class/world/Sector";
import { Job } from "util/Constants";
import { AISquadBase } from "./Base";

/**
 * AI module that handles spawning of soldiers.
 */
export class AISquadPopulation extends AISquadBase {

    private queue: {job: Job, level: number}[];

    private sector?: Sector;

    constructor(squad: SquadBase) {
        super(squad);
        this.queue = squad.getDesiredPopulation();
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there are still soldiers to spawn.
     */
    public shouldExecute(): boolean {
        return this.queue.length > 0;
    }

    /**
     * Spawns the soldiers.
     */
    public run(): void {
        if (!this.sector) {
            // find max required level
            let requiredLevel = 0;
            for (const entry of this.queue) {
                if (entry.level > requiredLevel) {
                    requiredLevel = entry.level;
                }
            }
            // find spawn sector
            const empire = this.squad.getPlatoon().getEmpire();
            const target = this.squad.getPlatoon().getTargetRoom();
            if (!target) {
                console.log("AISquadPopulation no target room of platoon!");
                return;
            }
            const spawnSector = empire.getClosestSpawnRoom(target, requiredLevel) as Sector;
            if (!spawnSector) {
                console.log("AISquadPopulation no spawn sector found!");
                return;
            }
            this.sector = spawnSector;
        }
        if (!this.sector) return;
        const toSpawn = this.queue[0];
        const result = this.sector.addToSpawnQueue(toSpawn.job, {}, toSpawn.level, this.squad.getName());
        if (result) {
            this.queue.shift();
        }
    }    
}