import { AIBase } from "ai/Base";
import { SquadBase } from "class/military/squads/SquadBase";

export abstract class AISquadBase extends AIBase {

    protected squad: SquadBase;

    constructor(squad: SquadBase) {
        super();
        this.squad = squad;
    }
}