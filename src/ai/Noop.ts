import { AIBase } from "./Base";

/**
 * AIModule that handles doing nothing.
 */
export class AINoop extends AIBase {

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True.
     */
    public shouldExecute(): boolean {
        return true;
    }

    /**
     * Immediately returns.
     */
    public run(): void {
        return;
    }    
}