import { AIBase } from "ai/Base";
import { AuglBase } from "class/augl/Base";
import { WorldBase } from "class/world/Base";
import { IScreepsRoomPosition } from "screeps";
import { IPosition } from "types";
import { LookType, TerrainType } from "util/Constants";
import { Helper } from "util/Helper";

declare const RoomPosition: IScreepsRoomPosition;

/**
 * AI module that handles waiting somewhere off the road.
 */
 export class AIAuglWait extends AIBase {

    private augl: AuglBase;

    private previousPos?: IPosition;

    private target?: IPosition;

    private index = 0;

    constructor(augl: AuglBase) {
        super();
        this.augl = augl;
    }

    /**
     * Find position with no structures that is walkable
     */
    private findLocation(aPos: IPosition, room: WorldBase): void {
        if (aPos.x != this.previousPos?.x || aPos.y != this.previousPos?.y) {
            this.index = 0;
            this.previousPos = aPos;
        }
        const option = Helper.getSpiralPosition(aPos.x, aPos.y, this.index);
        this.index++;
        if (option.x < 1 || option.y < 1 || option.x > 48 || option.y > 48) return;
        const structures = room.lookForAt(LookType.LOOK_STRUCTURES, option.x, option.y);
        const terrain = room.lookForAt(LookType.LOOK_TERRAIN, option.x, option.y);
        if (structures.length == 0 && (terrain as TerrainType[])[0] != TerrainType.WALL) {
            this.target = new RoomPosition(option.x, option.y, aPos.roomName);
        }
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if the augl is not at the target location yet.
     */
    public shouldExecute(): boolean {
        const aPos = this.augl.getPosition();
        return this.target == undefined || aPos?.x != this.target.x || aPos.y != this.target.y;
    }

    /**
     * Find and move to location off the road.
     */
    public run(): void {
        const aPos = this.augl.getPosition();
        const room = this.augl.getCurrentRoom();
        if (!aPos || !room) return;
        if (this.target && Helper.distance(this.target, aPos) > 8) {
            this.target = undefined;
        }
        if (!this.target) {
            this.findLocation(aPos, room);
        }
        if (this.target) this.augl.moveToTarget(this.target, 0);
    }
}