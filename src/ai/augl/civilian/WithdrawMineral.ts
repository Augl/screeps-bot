import { LabComplex } from "class/buildings/LabComplex";
import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles withdrawal of minerals from the terminal.
 */
export class AIAuglWithdrawMineral extends AIAuglCivilianBase {

    private target?: StoreBase;

    private targetMineral?: ResourceType;

    private targetComplex?: LabComplex;

    /**
     * Finds a mineral that is needed by one of the labs.
     * @returns The mineral type needed.
     */
    private findRequiredMineral(): ResourceType | undefined {
        const terminal = this.sector.getBuildings()?.getTerminal();
        if (!terminal) return;
        if (this.target && this.targetMineral && this.target.getFreeCapacity(this.targetMineral) >= 100
        && terminal.getUsedCapacity(this.targetMineral) > 100) {
            if (this.targetComplex && this.targetComplex.shouldFill()) {
                // target is a lab and complex should be filled
                return this.targetMineral;
            }
            else if (!this.targetComplex) {
                // target is not a lab
                return this.targetMineral;
            }
        }
        // find lab requiring minerals
        const complexes = this.sector.getBuildings()?.getLabComplexes();
        if (complexes) {
            for (const c of complexes) {
                if (!c.shouldFill()) continue;
                const labs = c.getReagentLabs();
                for (const l of labs) {
                    if (l.lab.getFreeCapacity(l.mineral) >= 100 && terminal.getUsedCapacity(l.mineral) > 100) {
                        this.target = l.lab;
                        this.targetComplex = c;
                        return l.mineral;
                    }
                }
            }
        }
        // find nuker
        const nuker = this.sector.getBuildings()?.getNuker();
        if (nuker && nuker.getFreeCapacity(ResourceType.GHODIUM) > 0 && terminal.getUsedCapacity(ResourceType.GHODIUM) > 100) {
            this.target = nuker;
            this.targetComplex = undefined;
            this.targetMineral = ResourceType.GHODIUM;
            return this.targetMineral;
        }
        return;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a lab requiring minerals.
     */
    public shouldExecute(): boolean {
        return this.findRequiredMineral() != undefined;
    }

    /**
     * Withdraws minerals needed by a lab from the terminal.
     */
    public run(): void {
        const terminal = this.sector.getBuildings()?.getTerminal();
        if (!terminal) return;
        const mineral = this.findRequiredMineral();
        if (!mineral) return;
        this.augl.withdraw(terminal, mineral);
    }
}