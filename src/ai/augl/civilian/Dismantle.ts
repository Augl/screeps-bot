import { BuildingBase } from "class/buildings/Base";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles deconstruction of buildings.
 */
 export class AIAuglDismantle extends AIAuglCivilianBase {

    private target?: BuildingBase;

    /**
     * Finds a building that should be deconstructed.
     * @returns The building to deconstruct.
     */
    private getCurrentTarget(): BuildingBase | undefined {
        if (!this.target || this.target.isDestroyed()) {
            this.target = this.sector.getBuildings()?.getDeconstructionTargets().find(b => !b.isDestroyed());
        }
        return this.target;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if a buildings needs to be deconstructed.
     */
    public shouldExecute(): boolean {
        return this.getCurrentTarget() != undefined;
    }

    /**
     * Dismantles the building.
     */
    public run(): void {
        const target = this.getCurrentTarget();
        if (!target || this.augl.isFull()) {
            return;
        }
        this.augl.dismantle(target);
    }    
}