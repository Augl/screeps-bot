import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles deposition of energy into containers in the centre.
 */
export class AIAuglDepositCentre extends AIAuglCivilianBase {

    private target?: StoreBase;

    /**
     * Finds an empty container in the centre.
     * @returns The store to deposit energy to.
     */
    private findCurrentTarget(): StoreBase | undefined {
        if (!this.target || this.target.isFull(ResourceType.ENERGY)) {
            // check labs
            this.target = this.sector.getBuildings()?.getLabs().find(l => !l.isFull(ResourceType.ENERGY));
            if (this.target) {
                return this.target;
            }
            // check nuker
            const nuker = this.sector.getBuildings()?.getNuker();
            if (nuker && nuker?.getFreeCapacity(ResourceType.ENERGY) > 0) {
                this.target = nuker;
                return this.target;
            }
            // check containers and storage
            if (!this.target) {
                const aPos = this.augl.getPosition();
                this.target = Helper.getMinWith(this.sector.getBuildings()?.getCentreStores() ?? [], c => !c.isFull(), c => {
                    const cPos = c.getPosition();
                    if (cPos && aPos) {
                        return Helper.distance(cPos, aPos);
                    }
                    return 500;
                });
            }
            if (!this.target) this.target = this.sector.getBuildings()?.getStorage();
        }
        return this.target;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a container in the centre that can be filled.
     */
    public shouldExecute(): boolean {
        return this.findCurrentTarget() != undefined;
    }

    /**
     * Deposits energy in a container located in the centre.
     */
    public run(): void {
        const target = this.findCurrentTarget();
        if (!target) {
            return;
        }
        if (this.augl.isEmpty()) {
            return;
        }
        this.augl.transfer(target, ResourceType.ENERGY);
    }
    
}