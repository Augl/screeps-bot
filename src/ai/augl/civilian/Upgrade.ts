import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles upgrading of the controller.
 */
export class AIAuglUpgrade extends AIAuglCivilianBase {

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a controller in the room.
     */
    public shouldExecute(): boolean {
        return this.sector.getController()?.isMine() == true;
    }

    /**
     * Upgrades the room controller.
     */
    public run(): void {
        const controller = this.sector.getController();
        if (this.augl.isEmpty() || controller == undefined) {
            return;
        }
        this.augl.upgradeController(controller);
    }
    
}