import { BuildingBase } from "class/buildings/Base";
import { ConstructionSite } from "class/buildings/ConstructionSite";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles construction of buildings that are either ramparts or walls.
 */
export class AIAuglBuildDefense extends AIAuglCivilianBase {

    private target?: BuildingBase;

    private isBasicBuild = false;

    private constructionSite?: ConstructionSite;

    /**
     * Called when the AI is executed after a break. Resets the target.
     */
    public reset(): void {
        this.target = undefined;
        this.constructionSite = undefined;
    }

    /**
     * Finds a newly built wall/rampart, a construction site, or a wall/rampart that needs to be repaired.
     * @returns The construction site or buiding to work on.
     */
    private getCurrentTarget(): { target?: BuildingBase, constructionSitie?: ConstructionSite } | undefined {
        if (this.target && this.isBasicBuild && this.target.getHitPoints() < 7000) {
            // old target is still relevant
            return { target: this.target };
        }
        if (this.target && !this.isBasicBuild && (this.target.getHitPoints() ?? 0) < (this.target.getMaxHitPoints() ?? 0)) {
            // old target is still relevant
            return { target: this.target };
        }
        if (this.constructionSite && !this.constructionSite.isCompleted()) {
            // old construction site is still relevant
            return { constructionSitie: this.constructionSite };
        }
        // check for newly built rampart or wall
        this.target = Helper.getMinWith(this.sector.getBuildings()?.getRamparts() ?? [], w => (w.getHitPoints() || 10000) < 5000, w => w.getHitPoints() ?? 0);
        if (!this.target) {
            this.target = this.sector.getBuildings()?.getWalls().find(r => (r.getHitPoints() || 10000) < 5000);
        }
        if (this.target) {
            this.isBasicBuild = true;
            return { target: this.target };
        }
        // check for construction site
        this.constructionSite = this.sector.getBuildings()?.getConstructionSites().find(c => !c.isCompleted() && c.isDefenseBuilding());
        if (this.constructionSite) return { constructionSitie: this.constructionSite };
        // check for defense building requesting to be repaired
        this.target = Helper.getMinWith(this.sector.getBuildings()?.getDamagedBuildings() ?? [], b => {
                return b.isDefenseBuilding();
            }, b => {
                const hp = b.getHitPoints();
                if (hp) {
                    return hp;
                }
                return 300000000;
            }
        );
        if (this.target) {
            this.isBasicBuild = false;
            return { target: this.target };
        }
        return {};
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a construction site or building to work on.
     */
    public shouldExecute(): boolean {
        const target = this.getCurrentTarget();
        return target?.constructionSitie != undefined || target?.target != undefined;
    }

    /**
     * Builds the construction or repairs the structure.
     */
    public run(): void {
        const target = this.getCurrentTarget();        
        if (this.augl.isEmpty()) {
            return;
        }
        if (target?.constructionSitie) {
            this.augl.build(target.constructionSitie);
        }
        else if (target?.target) {
            this.augl.repair(target.target);
        }
    }
}