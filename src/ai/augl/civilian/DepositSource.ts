import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles deposition of energy in containers and links next to the assigned source.
 */
export class AIAuglDepositSource extends AIAuglCivilianBase {

    private target?: StoreBase;

    /**
     * Finds an empty container or link next to the assigned source.
     * @returns A store to deposit energy to.
     */
    private getCurrentTarget(): StoreBase | undefined {
        if (!this.target || this.target.isFull()) {
            // find assigned source
            const sourceId = this.augl.memory.sourceId;
            if (!sourceId) return;
            const source = this.sector.getSource(sourceId) ?? this.sector.getMineral();
            if (!source) return;
            // find container next to assigned source
            const containers = this.sector.getBuildings()?.getPeripheryContainers().concat(this.sector.getBuildings()?.getPeripheryLinks() ?? []) ?? [];
            this.target = containers.find(c => {
                if (c.isFull() || c.shouldBeDeconstructed()) return false;
                const sP = source.getPosition();
                const cP = c.getPosition();
                if (sP && cP) {
                    return Helper.distance(sP, cP) < 3;
                }
                return false;
            });
        }
        return this.target;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns If there is a store next to the source that is not full yet.
     */
    public shouldExecute(): boolean {
        return this.getCurrentTarget() != undefined;
    }

    /**
     * Deposits the energy in the container or link next to the assigned source
     */
    public run(): void {
        const target = this.getCurrentTarget();
        if (!target) {
            return;
        }
        if (this.augl.isEmpty() || target.isFull()) {
            return;
        }
        // deposit energy into container
        this.augl.transfer(target, this.augl.getCarriedResource() ?? ResourceType.ENERGY);
    }
}