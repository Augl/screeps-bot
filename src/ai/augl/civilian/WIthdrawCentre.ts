import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles withdrawal of energy from containers in the centre.
 */
 export class AIAuglWithdrawCentre extends AIAuglCivilianBase {

    private source?: StoreBase;

    /**
     * Finds a full container in the centre.
     * @returns A container to withdraw energy from.
     */
    private findSourceContainer(): StoreBase | undefined {
        if (!this.source || this.source.isEmpty()) {
            const aPos = this.augl.getPosition();
            this.source = Helper.getMinWith(this.sector.getBuildings()?.getCentreStores() ?? [], (s) => !s.isEmpty(), (s) => {
                const sPos = s.getPosition();
                if (aPos && sPos) {
                    return Helper.distance(aPos, sPos);
                }
                return 500;
            });
            if (!this.source) this.source = this.sector.getBuildings()?.getStorage();
        }
        return this.source;
    }

    /**
     * Deletes the previously used source.
     */
    public reset(): void {
        this.source = undefined;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is energy available in one of the source stores.
     */
    public shouldExecute(): boolean {
        return this.findSourceContainer() != undefined;
    }

    /**
     * Withdraws energy from a container in the centre.
     */
    public run(): void {
        const source = this.findSourceContainer();
        if (!source || this.augl.isFull()) {
            return;
        }
        this.augl.withdraw(source, ResourceType.ENERGY);
    }    
}