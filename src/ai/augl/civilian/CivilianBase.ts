import { AIBase } from "ai/Base";
import { AuglBase } from "class/augl/Base";
import { WorldBase } from "class/world/Base";

export abstract class AIAuglCivilianBase extends AIBase {

    protected augl: AuglBase;

    protected sector: WorldBase;

    constructor(augl: AuglBase, sector: WorldBase) {
        super();
        this.augl = augl;
        this.sector = sector;
    }
}