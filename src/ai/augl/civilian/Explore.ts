import { AIAuglCivilianBase } from "./CivilianBase";

export class AIAuglExplore extends AIAuglCivilianBase {

    private target?: string;

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if the augl has rooms to explore.
     */
    public shouldExecute(): boolean {
        return (this.augl.memory.targets?.length ?? 0) > 0;
    }

    /**
     * Moves the augl to the rooms that need to be explored.
     */
    public run(): void {
        if (this.augl.getCurrentRoom().roomName == this.target) {
            this.sector.empire.getWorld(this.target); // force room init
            this.augl.memory.targets?.shift();
            this.target = undefined;
        }
        if (!this.target) {
            // set current exploration target
            this.target = this.augl.memory.targets?.[0];
        }
        if (this.target) {
            this.augl.moveToRoom(this.target);
        }
    }    
}