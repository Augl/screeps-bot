import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles deposition of energy into towers.
 */
export class AIAuglDepositTower extends AIAuglCivilianBase {

    private target?: StoreBase;

    /**
     * Finds a tower to fill.
     * @returns The tower to deposit to.
     */
    private findCurrentTarget(): StoreBase | undefined {
        if (!this.target || this.target.isFull(ResourceType.ENERGY)) {
            this.target = this.sector.getBuildings()?.getTowers().find(t => !t.isFull(ResourceType.ENERGY));
        }
        return this.target;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a tower that can be filled.
     */
    public shouldExecute(): boolean {
        return this.findCurrentTarget() != undefined;
    }

    /**
     * Deposits the resources in a tower.
     */
    public run(): void {
        const target = this.findCurrentTarget();
        if (!target) {
            return;
        }
        if (this.augl.isEmpty()) {
            return;
        }
        this.augl.transfer(target, ResourceType.ENERGY);
    }
    
}