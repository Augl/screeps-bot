import { LabComplex } from "class/buildings/LabComplex";
import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AIModule that handles deposition of minerals in labs and the nuker.
 */
export class AIAuglDepositLab extends AIAuglCivilianBase {

    private target?: StoreBase;

    private targetMineral?: ResourceType;

    private targetComplex?: LabComplex;

    /**
     * Finds a lab or nuker that needs the mineral carried.
     * @returns The target lab.
     */
    private findTarget(): StoreBase | undefined {
        const mineral = this.augl.getCarriedResource();
        if (this.target && this.targetComplex && this.targetComplex.shouldFill() && mineral == this.targetMineral && this.target.getFreeCapacity(mineral) > 0) {
            return this.target;
        }
        else if (this.target && !this.targetComplex && this.target.getFreeCapacity(mineral) > 0) {
            return this.target;
        }
        if (!mineral) return;
        // check labs
        const complexes = this.sector.getBuildings()?.getLabComplexes();
        if (complexes) {
            for (const c of complexes) {
                if (!c.shouldFill()) continue;
                const labs = c.getReagentLabs();
                for (const l of labs) {
                    if (l.mineral == mineral && l.lab.getFreeCapacity(mineral) > 0) {
                        this.target = l.lab;
                        this.targetMineral = l.mineral;
                        this.targetComplex = c;
                        return this.target;
                    }
                }
            }
        }
        // check nuker
        const nuker = this.sector.getBuildings()?.getNuker();
        if (nuker && nuker.getFreeCapacity(mineral) > 0) {
            this.targetComplex = undefined;
            this.target = nuker;
            this.targetMineral = undefined;
            return this.target;
        }
        return;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a lab to fill.
     */
    public shouldExecute(): boolean {
        return this.findTarget() != undefined;
    }

    /**
     * Deposits carried minerals to a lab.
     */
    public run(): void {
        const mineral = this.augl.getCarriedResource();
        const target = this.findTarget();
        if (!target || this.augl.isEmpty() || ! mineral) {
            return;
        }
        this.augl.transfer(target, mineral);
    }
}