import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles withdrawal of resources from containers close to the source.
 */
export class AIAuglWithdrawSource extends AIAuglCivilianBase {

    private source?: StoreBase;

    /**
     * Finds a full container or link next to a source.
     * @returns A container to withdraw energy from.
     */
    private findSourceContainer(): StoreBase | undefined {
        if (!this.source || this.source.getUsedCapacity() < 100) {
            const aPos = this.augl.getPosition();
            this.source = Helper.getMinWith(this.sector.getBuildings()?.getPeripheryContainers() ?? [], (s) => s.getUsedCapacity() >= 100, (s) => {
                const sPos = s.getPosition();
                if (aPos && sPos) {
                    return Helper.distance(aPos, sPos);
                }
                return 500;
            });
        }
        return this.source;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is resources available in one of the source stores.
     */
    public shouldExecute(): boolean {
        return this.findSourceContainer() != undefined;
    }

    /**
     * Withdraws resources from a container close to a source.
     */
    public run(): void {
        const source = this.findSourceContainer();
        if (!source || this.augl.isFull()) {
            return;
        }
        this.augl.withdraw(source, source.getContainedResources()[0] ?? ResourceType.ENERGY);
    }    
}