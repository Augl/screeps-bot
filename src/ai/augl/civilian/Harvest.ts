import { MineralSource } from "class/resources/Mineral";
import { EnergySource } from "class/resources/source";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles harvesting of energy or minerals at a source.
 */
export class AIAuglHarvest extends AIAuglCivilianBase {

    private source?: EnergySource | MineralSource;

    /**
     * Finds a source to harvest from.
     * @returns The source to harvest from.
     */
    private getCurrentSource(): EnergySource | MineralSource | undefined {
        if (!this.source) {
            let sourceId = this.augl.memory.sourceId;
            if (!sourceId) {
                // randomly choose source to use for the rest of the life
                const sources = this.sector.getSources();
                if (sources.length > 0) {
                    sourceId = sources[Math.floor(Math.random()*sources.length)].sourceID;
                    this.augl.memory.sourceId = sourceId;
                }            
            }
            if (!sourceId) {
                return undefined;
            }
            this.source = this.sector.getSource(sourceId) ?? this.sector.getMineral();
        }
        return this.source;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if the augl is not full yet.
     */
    public shouldExecute(): boolean {
        return this.getCurrentSource() != undefined;
    }

    /**
     * Harvests energy from a source.
     */
    public run(): void {
        const source = this.getCurrentSource();
        if (!source || this.augl.isFull()) {
            return;
        }
        this.augl.harvest(source);
    }
}