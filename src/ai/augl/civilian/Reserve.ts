import { Names } from "util/Names";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles reservation of controllers.
 */
 export class AIAuglReserve extends AIAuglCivilianBase {
    
    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if the controller needs to be reserved.
     */
    public shouldExecute(): boolean {
        if (this.sector.getController()?.getReservation()?.ticksToEnd ?? 0 < 5000) {
            return true;
        }
        return false;
    }

    /**
     * Reserves and signs the controller.
     */
    public run(): void {
        const controller = this.sector.getController();
        if (!controller) {
            this.augl.moveToRoom(this.sector.roomName);
        }
        else if (controller.getSign()?.username != "Augl" && controller.getSign()?.username != "Screeps") {
            this.augl.signController(controller, Names.getSignatureText());
        }
        else if (controller.getOwner() != "" && controller.getOwner() != "Augl") {
            this.augl.attackController(controller);
        }
        else if ((controller.getReservation()?.username ?? "Augl") != "Augl") {
            this.augl.attackController(controller);
        }
        else {
            this.augl.reserveController(controller);
        }
    }    
}