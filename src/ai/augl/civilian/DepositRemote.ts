import { AuglBase } from "class/augl/Base";
import { WorldBase } from "class/world/Base";
import { Colony } from "class/world/Colony";
import { ResourceType } from "util/Constants";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles depositing energy in another room.
 */
export class AIAuglDepositRemote extends AIAuglCivilianBase {

    private colony: Colony;

    constructor(augl: AuglBase, sector: WorldBase) {
        super(augl, sector);
        if ((sector as Colony).getStorageRoom) {
            this.colony = sector as Colony;
        }
        else {
            throw new Error("AIAuglDepositRemote can only be initialized in a colony.");
        }
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if the other room has capacity for more energy.
     */
    public shouldExecute(): boolean {
        return this.colony.getStorageRoom()?.getBuildings()?.getStorage()?.isFull() == false;
    }

    /**
     * Moves to the other room and deposits the energy.
     */
    public run(): void {
        const sector = this.colony.getStorageRoom();
        const storage = sector?.getBuildings()?.getStorage();
        if (!sector || !storage) return;
        const current = this.augl.getCurrentRoom();
        if (!current) return;        
        this.augl.transfer(storage, ResourceType.ENERGY);
    }    
}