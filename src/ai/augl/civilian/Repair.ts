import { BuildingBase } from "class/buildings/Base";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles repair of damaged buildings.
 */
 export class AIAuglRepair extends AIAuglCivilianBase {

    private target?: BuildingBase;

    /**
     * Finds a building that needs repair.
     * @returns The building to work on.
     */
    private getCurrentTarget(): BuildingBase | undefined {
        if (!this.target || (this.target.getHitPoints() ?? 0) == (this.target.getMaxHitPoints() ?? 1)) {
            const aPos = this.augl.getPosition();
            if (!aPos) return;
            this.target = Helper.getMinWith(this.sector.getBuildings()?.getDamagedBuildings() ?? [], 
                b => !b.isDestroyed() && !b.isDefenseBuilding() && !b.shouldBeDeconstructed(), 
                (target) => {
                    const tPos = target.getPosition();
                    if (tPos) {
                        return Helper.distance(tPos, aPos);
                    }
                    return 500;
                }
            )
        }
        return this.target;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if a building needs to be repaired
     */
    public shouldExecute(): boolean {
        return this.getCurrentTarget() != undefined;
    }

    /**
     * Repairs the building.
     */
    public run(): void {
        const target = this.getCurrentTarget();
        if (!target || this.augl.isEmpty()) {
            return;
        }
        this.augl.repair(target);
    }    
}