import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles withdrawal of energy from any container.
 */
 export class AIAuglWithdrawAny extends AIAuglCivilianBase {

    private source?: StoreBase;

    /**
     * Finds a full container in the room.
     * @returns A container to withdraw energy from.
     */
    private findSourceContainer(): StoreBase | undefined {
        if (!this.source || this.source.isEmpty()) {
            const aPos = this.augl.getPosition();
            const options = this.sector.getBuildings()?.getCentreStores().concat(this.sector.getBuildings()?.getPeripheryContainers() ?? []) ?? [];
            const storage = this.sector.getBuildings()?.getStorage();
            if (storage) options.push(storage);
            const link = this.sector.getBuildings()?.getControllerLink();
            if (link) options.push(link);
            this.source = Helper.getMinWith(options, (s) => s.hasResource(this.augl.getCarryCapacity(), ResourceType.ENERGY), (s) => {
                const sPos = s.getPosition();
                if (aPos && sPos) {
                    return Helper.distance(aPos, sPos);
                }
                return 500;
            });
        }
        return this.source;
    }

    /**
     * Deletes the previously used source.
     */
    public reset(): void {
        this.source = undefined;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is energy available in one of the stores.
     */
    public shouldExecute(): boolean {
        return this.findSourceContainer() != undefined;
    }

    /**
     * Withdraws energy from a container.
     */
    public run(): void {
        const source = this.findSourceContainer();
        if (!source || this.augl.isFull()) {
            return;
        }
        this.augl.withdraw(source, ResourceType.ENERGY);
    }    
}