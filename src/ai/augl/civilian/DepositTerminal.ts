import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles deposition of energy or chemicals into the terminal.
 */
export class AIAuglDepositTerminal extends AIAuglCivilianBase {

    private target?: StoreBase;

    /**
     * Finds the terminal.
     * @returns The terminal to deposit to.
     */
    private findCurrentTarget(): StoreBase | undefined {
        if (!this.target || this.target.isFull()) {
            this.target = undefined;
            const terminal = this.sector.getBuildings()?.getTerminal();
            if (!terminal?.isFull(this.augl.getCarriedResource() ?? ResourceType.ENERGY)) {
                this.target = terminal;
            }
        }
        return this.target;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a terminal that can be filled.
     */
    public shouldExecute(): boolean {
        return this.findCurrentTarget() != undefined;
    }

    /**
     * Deposits the resources in a terminal.
     */
    public run(): void {
        const target = this.findCurrentTarget();
        if (!target) {
            return;
        }
        if (this.augl.isEmpty()) {
            return;
        }
        this.augl.transfer(target, this.augl.getCarriedResource() ?? ResourceType.ENERGY);
    }
    
}