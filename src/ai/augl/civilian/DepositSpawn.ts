import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles deposition of energy into spawns and extensions.
 */
export class AIAuglDepositSpawn extends AIAuglCivilianBase {

    private target?: StoreBase;

    /**
     * Finds an empty spawn or extension.
     * @returns The spawn or extension to deposit energy to.
     */
    private findCurrentTarget(): StoreBase | undefined {
        if (!this.target || this.target.isFull(ResourceType.ENERGY)) {
            this.target = undefined;
            if (this.sector.getEnergyCapacity() > this.sector.getAvailableEnergy()) {
                const options = (this.sector.getBuildings()?.getSpawns() ?? [] as StoreBase[]).concat(this.sector.getBuildings()?.getExtensions() ?? []);
                const aPos = this.augl.getPosition();
                this.target = Helper.getMinWith(options, c => !c.isFull(ResourceType.ENERGY), c => {
                    const cPos = c.getPosition();
                    if (cPos && aPos) {
                        return Helper.distance(cPos, aPos);
                    }
                    return 500;
                });
            }
        }
        return this.target;
    }

    /**
     * Called when the AI is executed after a break. Resets the target.
     */
    public reset(): void {
        this.target = undefined;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a spawn or extension that can be filled.
     */
    public shouldExecute(): boolean {
        return this.findCurrentTarget() != undefined;
    }

    /**
     * Deposits energy in a spawn or extension.
     */
    public run(): void {
        const target = this.findCurrentTarget();
        if (!target || this.augl.isEmpty()) {
            return;
        }
        this.augl.transfer(target, ResourceType.ENERGY);
    }
    
}