import { Lab } from "class/buildings/Lab";
import { LabComplex } from "class/buildings/LabComplex";
import { ResourceType } from "util/Constants";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles withdrawal of minerals from the lab.
 */
export class AIAuglWithdrawLab extends AIAuglCivilianBase {

    private target?: Lab;

    private targetComplex?: LabComplex;

    /**
     * Finds a lab to withdraw from.
     * @returns The lab.
     */
    private findLab(): Lab | undefined {
        const mineral = this.target?.getContainedResources().find(r => r != ResourceType.ENERGY);
        if (this.target && mineral && this.target.getUsedCapacity(mineral) > 0 && this.targetComplex?.shouldEmpty()) {
            return this.target;
        }
        const complexes = this.sector.getBuildings()?.getLabComplexes();
        if (!complexes) return undefined;
        for (const c of complexes) {
            if (!c.shouldEmpty()) continue;
            const labs = c.getAllLabs();
            for (const l of labs) {
                const mineral = l.getContainedResources().find(r => r != ResourceType.ENERGY);
                if (mineral && l.getUsedCapacity(mineral) > 0) {
                    this.target = l;
                    this.targetComplex = c;
                    return l;
                }
            }
        }
        return;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a lab to withdraw from.
     */
    public shouldExecute(): boolean {
        return this.findLab() != undefined;
    }

    /**
     * Withdraws minerals from the lab.
     */
    public run(): void {
        const lab = this.findLab();
        if (!lab) return;
        const mineral = lab.getContainedResources().find(r => r != ResourceType.ENERGY);
        if (!mineral) return;
        this.augl.withdraw(lab, mineral);
    }
}