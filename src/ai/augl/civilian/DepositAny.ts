import { StoreBase } from "class/buildings/StoreBase";
import { ResourceType } from "util/Constants";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles deposition of energy into containers.
 */
export class AIAuglDepositAny extends AIAuglCivilianBase {

    private target?: StoreBase;

    /**
     * Finds an empty container.
     * @returns The store to deposit energy to.
     */
    private findCurrentTarget(): StoreBase | undefined {
        if (!this.target || this.target.isFull()) {
            const aPos = this.augl.getPosition();
            const buildings = this.sector.getBuildings();
            if (!buildings) return;
            const options = buildings.getCentreStores().concat(buildings.getPeripheryContainers());
            this.target = Helper.getMinWith(options, c => !c.isFull(), c => {
                const cPos = c.getPosition();
                if (cPos && aPos) {
                    return Helper.distance(cPos, aPos);
                }
                return 500;
            });
            if (!this.target) this.target = this.sector.getBuildings()?.getStorage();
        }
        return this.target;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a container that can be filled.
     */
    public shouldExecute(): boolean {
        return this.findCurrentTarget() != undefined;
    }

    /**
     * Deposits energy in a container.
     */
    public run(): void {
        const target = this.findCurrentTarget();
        if (!target) {
            return;
        }
        if (this.augl.isEmpty()) {
            return;
        }
        this.augl.transfer(target, ResourceType.ENERGY);
    }
    
}