import { ResourceType } from "util/Constants";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles withdrawal of energy from the link in the centre.
 */
 export class AIAuglWithdrawCentreLink extends AIAuglCivilianBase {

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is energy available in the centre link.
     */
    public shouldExecute(): boolean {
        return this.sector.getBuildings()?.getCentreLink()?.isEmpty(ResourceType.ENERGY) == false;
    }

    /**
     * Withdraws energy from a container in the centre.
     */
    public run(): void {
        const source = this.sector.getBuildings()?.getCentreLink();
        if (!source || this.augl.isFull()) {
            return;
        }
        this.augl.withdraw(source, ResourceType.ENERGY);
    }    
}