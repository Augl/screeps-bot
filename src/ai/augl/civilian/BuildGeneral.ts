import { ConstructionSite } from "class/buildings/ConstructionSite";
import { Helper } from "util/Helper";
import { AIAuglCivilianBase } from "./CivilianBase";

/**
 * AI module that handles construction of buildings that are neither ramparts nor walls.
 */
export class AIAuglBuildGeneral extends AIAuglCivilianBase {

    private target?: ConstructionSite;

    /**
     * Finds a construction site of a building that is neither a wall nor a rampart.
     * @returns The construction site to work on.
     */
    private getCurrentTarget(): ConstructionSite | undefined {
        if (!this.target || this.target.isCompleted()) {
            const aPos = this.augl.getPosition();
            if (!aPos) return;
            this.target = Helper.getMinWith(this.sector.getBuildings()?.getConstructionSites() ?? [], c => !c.isCompleted() && !c.isDefenseBuilding(), c => {
                const cPos = c.getPosition();
                if (cPos) return Helper.distance(aPos, cPos);
                return 500;
            });
        }
        return this.target;
    }

    /**
     * Resets the current target.
     */
    public reset(): void {
        this.target = undefined;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if a construction site needs to be worked on.
     */
    public shouldExecute(): boolean {
        return this.getCurrentTarget() != undefined;
    }

    /**
     * Builds the construction site until it is done or the augl has no more energy.
     */
    public run(): void {
        const target = this.getCurrentTarget();
        if (!target) {
            return;
        }
        if (this.augl.isEmpty()) {
            return;
        }
        this.augl.build(target);
    }    
}