import { AIAuglCivilianBase } from "ai/augl/civilian/CivilianBase";
import { Names } from "util/Names";

/**
 * AI module that handles claiming of controllers.
 */
export class AIAuglClaim extends AIAuglCivilianBase {
    
    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if the controller was not yet claimed.
     */
    public shouldExecute(): boolean {
        if (!this.sector.getController()?.isMine()) {
            return true;
        }
        return false;
    }

    /**
     * Claims and signs the controller.
     */
    public run(): void {
        const controller = this.sector.getController();
        if (!controller) {
            this.augl.moveToRoom(this.sector.roomName);
        }
        else if (controller.getSign()?.username != "Augl") {
            this.augl.signController(controller, Names.getSignatureText());
        }
        else {
            this.augl.claimController(controller);
        }
    }    
}