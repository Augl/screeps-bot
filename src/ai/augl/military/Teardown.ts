import { BuildingBase } from "class/buildings/Base";
import { Helper } from "util/Helper";
import { AIAuglMilitaryBase } from "./Base";

/**
 * AI module that handles attack of enemy buildings.
 */
export class AIAuglTeardown extends AIAuglMilitaryBase {

    private target?: BuildingBase;

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is a building to attack.
     */
    public shouldExecute(): boolean {
        return this.squad.getPlatoon().getHostileBuildings().length > 0;
    }

    /**
     * Attacks enemy creeps.
     */
    public run(): void {
        if (!this.target || this.target.isDestroyed()) {
            const aPos = this.augl.getPosition();
            if (!aPos) return;
            this.target = Helper.getMinWith(this.squad.getPlatoon().getHostileBuildings(), e => !e.isDestroyed(), e => {
                const pos = e.getPosition();
                if (!pos) return 500;
                return Helper.distance(aPos, pos);
            });
        }
        if (!this.target) return;
        this.augl.teardown(this.target);
    }    
}