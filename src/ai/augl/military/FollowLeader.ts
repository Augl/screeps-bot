import { Helper } from "util/Helper";
import { AIAuglMilitaryBase } from "./Base";

/**
 * AI module that handles following the squad leader.
 */
 export class AIAuglFollow extends AIAuglMilitaryBase {

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if the leader is far away.
     */
    public shouldExecute(): boolean {
        const aPos = this.augl.getPosition();
        const leader = this.squad.getLeader()?.getPosition();
        return aPos != undefined && leader != undefined && Helper.distance(aPos, leader) > 3;
    }

    /**
     * Follows the leader.
     */
    public run(): void {
        const aPos = this.augl.getPosition();
        const leader = this.squad.getLeader()?.getPosition();
        if (!aPos || !leader) return;
        this.augl.moveToTarget(leader, 2);
    }    
}