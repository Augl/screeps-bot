import { AuglBase } from "class/augl/Base";
import { Helper } from "util/Helper";
import { AIAuglMilitaryBase } from "./Base";

/**
 * AI module that handles healing of squad mates.
 */
 export class AIAuglHealSquad extends AIAuglMilitaryBase {

    private target?: AuglBase;

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is an augl to heal.
     */
    public shouldExecute(): boolean {
        return this.squad.getAugls().find(a => a.isInjured()) != undefined;
    }

    /**
     * Attacks enemy creeps.
     */
    public run(): void {
        if (!this.target || !this.target.isInjured()) {
            this.target = Helper.getMinWith(this.squad.getAugls(), a => a.isInjured(), a => {
                return a.getHits();
            });
        }
        if (!this.target) return;
        this.augl.heal(this.target);
    }    
}