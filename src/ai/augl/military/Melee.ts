import { IScreepsCreep } from "screeps";
import { BodyPart } from "util/Constants";
import { Helper } from "util/Helper";
import { AIAuglMilitaryBase } from "./Base";

/**
 * AI module that handles melee attacks.
 */
export class AIAuglAttackMelee extends AIAuglMilitaryBase {

    private target?: IScreepsCreep;

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there is an enemy to attack.
     */
    public shouldExecute(): boolean {
        return this.squad.getPlatoon().getEnemies().length > 0;
    }

    /**
     * Attacks enemy creeps.
     */
    public run(): void {
        const aPos = this.augl.getPosition();
        if (!aPos) return;
        this.target = Helper.getMinWith(this.squad.getPlatoon().getEnemies(), e => {
            return e.getActiveBodyparts(BodyPart.ATTACK) > 0 || 
                e.getActiveBodyparts(BodyPart.HEAL) > 0 || 
                e.getActiveBodyparts(BodyPart.RANGED_ATTACK) > 0;
        }, e => {
            return Helper.distance(aPos, e.pos);
        });
        if (!this.target) {
            // take any
            this.target = this.squad.getPlatoon().getEnemies()[0];
        }
        if (!this.target) return;
        this.augl.attack(this.target);
    }    
}