import { AIBase } from "ai/Base";
import { AuglBase } from "class/augl/Base";
import { SquadBase } from "class/military/squads/SquadBase";

export abstract class AIAuglMilitaryBase extends AIBase {

    protected augl: AuglBase;

    protected squad: SquadBase;

    constructor(augl: AuglBase, squad: SquadBase) {
        super();
        this.augl = augl;
        this.squad = squad;
    }
}