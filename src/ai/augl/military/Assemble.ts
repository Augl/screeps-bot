import { IScreepsRoomPosition } from "screeps";
import { AIAuglMilitaryBase } from "./Base";

declare const RoomPosition: IScreepsRoomPosition;

/**
 * AI module that handles assembling of soldiers of a squad.
 */
 export class AIAuglAssemble extends AIAuglMilitaryBase {

    private assembleRoom?: string;
    
    /**
     * Checks if it makes sense to execute this AI.
     * @returns True.
     */
    public shouldExecute(): boolean {
        return true
    }

    /**
     * Gathers with the squad right next to the target room.
     */
    public run(): void {
        const targetRoomName = this.squad.getPlatoon().getTargetRoom();
        if (!targetRoomName) {
            console.log("AIAuglAssemble: No target room!");
            return;
        }
        if (!this.assembleRoom) {
            // get name of the room right next to the target room
            const path = this.squad.getPlatoon().getEmpire().getPath(this.augl.getCurrentRoom().roomName, targetRoomName);
            this.assembleRoom = path[path.length - 2] ?? this.augl.getCurrentRoom().roomName;
        }
        if (!this.assembleRoom) {
            console.log("AIAuglAssemble: No assemble room!");
            return;
        }
        if (this.augl.getCurrentRoom().roomName != this.assembleRoom) {
            // move to room right next to target room
            this.augl.moveToRoom(this.assembleRoom);
            console.log(this.augl.name, "moving to assemble room", this.assembleRoom, "to meet", this.squad.getName());
        }
        else {
            const leader = this.squad.getLeader();
            if (leader == this.augl) {     
                // move leader close to the exit           
                const targetRoomName = this.squad.getPlatoon().getTargetRoom();
                if (!targetRoomName) {
                    console.log("AIAuglAssemble: No target room!");
                    return;
                }
                if (!this.assembleRoom) return;
                this.augl.moveToTarget(new RoomPosition(25, 25, this.assembleRoom), 15);
                console.log(this.augl.name, "is leader and is moving closer to target room", targetRoomName);
            }
            else {
                // move to leader
                if (!leader) return;
                const leaderPos = leader.getPosition();
                if (!leaderPos) return;
                console.log(this.augl.name, "is not leader and is moving to leader", leader.name);
                this.augl.moveToTarget(leaderPos, 1);
            }
        }
    }    
}