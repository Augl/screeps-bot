import { IScreepsRoomPosition } from "screeps";
import { Helper } from "util/Helper";
import { AIAuglMilitaryBase } from "./Base";

declare const RoomPosition: IScreepsRoomPosition;

/**
 * AI module that handles marching to the target room.
 */
 export class AIAuglMarch extends AIAuglMilitaryBase {

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if the augl is not in the target room yet.
     */
    public shouldExecute(): boolean {
        const pos = this.augl.getPosition();
        if (!pos) return false;
        const closeToCentre = Helper.distance(pos, new RoomPosition(25, 25, pos.roomName)) < 10;
        return this.squad.getPlatoon().getTargetRoom() != this.augl.getCurrentRoom().roomName || !closeToCentre;
    }

    /**
     * Marches to the target room.
     */
    public run(): void {
        if (this.augl == this.squad.getLeader()) {
            const target = this.squad.getPlatoon().getTargetRoom();
            if (!target) return;
            this.augl.moveToTarget(new RoomPosition(25, 25, target), 3);
        }
        else {
            const leader = this.squad.getLeader()?.getPosition();
            if (!leader) return;
            this.augl.moveToTarget(leader, 3);
        }
    }    
}