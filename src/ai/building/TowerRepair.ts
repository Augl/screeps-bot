import { AIBase } from "ai/Base";
import { BuildingBase } from "class/buildings/Base";
import { Tower } from "class/buildings/Tower";
import { WorldBase } from "class/world/Base";
import { ResourceType } from "util/Constants";
import { Helper } from "util/Helper";

/**
 * AI module that handles repair of damaged buildings.
 */
export class AITowerRepair extends AIBase {

    private tower: Tower;

    private sector: WorldBase;

    private target?: BuildingBase;

    constructor(tower: Tower, sector: WorldBase) {
        super();
        this.tower = tower;
        this.sector = sector;
    }

    /**
     * Finds a building that needs repair.
     * @returns The building to work on.
     */
    private getCurrentTarget(): BuildingBase | undefined {
        if (!this.target || (this.target.getHitPoints() ?? 0) == (this.target.getMaxHitPoints() ?? 1)) {
            this.target = Helper.getMinWith(this.sector.getBuildings()?.getDamagedBuildings() ?? [], b => !b.isDefenseBuilding() && !b.shouldBeDeconstructed(), target => {
                return target.getHitPoints();
            })
        }
        return this.target;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there are buildings that need to be repaired and the tower has plenty of energy.
     */
    public shouldExecute(): boolean {
        if (this.tower.getUsedCapacity(ResourceType.ENERGY) < this.tower.getCapacity(ResourceType.ENERGY)/2) return false;
        return this.getCurrentTarget() != undefined;
    }

    /**
     * Repairs a damaged building.
     */
    public run(): void {
        const target = this.getCurrentTarget();
        if (!target) {
            console.log("AITowerRepair: No building found!");
            return;
        }
        this.tower.repair(target);
    }
}