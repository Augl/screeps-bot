import { AIBase } from "ai/Base";
import { Link } from "class/buildings/Link";
import { WorldBase } from "class/world/Base";
import { ResourceType } from "util/Constants";

/**
 * AI module that handles transfer or energy between links.
 */
export class AILinkTransfer extends AIBase {

    private link: Link;

    private sector: WorldBase;

    private isSender: boolean;

    constructor(link: Link, sector: WorldBase, isSender: boolean) {
        super();
        this.link = link;
        this.sector = sector;
        this.isSender = isSender;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if energy can be transfered to another link.
     */
    public shouldExecute(): boolean {
        if (!this.isSender) return false;
        const isFull = this.link.isFull(ResourceType.ENERGY);
        if (!isFull) return false;
        // check controller link
        if (this.sector.getBuildings()?.getControllerLink()?.isEmpty(ResourceType.ENERGY)) {
            return true;
        }
        // find centre link
        const target = this.sector.getBuildings()?.getCentreLink();
        return target?.isEmpty(ResourceType.ENERGY) == true;
    }

    /**
     * Transfers energy to a centre link.
     */
    public run(): void {
        let target = this.sector.getBuildings()?.getControllerLink();
        if (!target || target.hasResource(600, ResourceType.ENERGY) || this.sector.getBuildings()?.getStorage()?.hasResource(1000, ResourceType.ENERGY) == false) {
            target = this.sector.getBuildings()?.getCentreLink();
        }
        if (!target) return;
        this.link.transferEnergy(target);
    }
}