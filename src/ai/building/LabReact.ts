import { AIBase } from "ai/Base";
import { LabComplex } from "class/buildings/LabComplex";

/**
 * AI module that handles running chemical reactions.
 */
 export class AILabReact extends AIBase {

    private labComplex: LabComplex;

    constructor(labComplex: LabComplex) {
        super();
        this.labComplex = labComplex;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if reactions can be executed.
     */
    public shouldExecute(): boolean {
        return this.labComplex.getReagentLabs().find(l => l.lab.isEmpty()) == undefined &&
            this.labComplex.getProductLabs().find(l => l.isFull() && l.getCooldown() > 0) == undefined;
    }

    /**
     * Executes the chemical reaction.
     */
    public run(): void {
        this.labComplex.runReaction();
    }
}