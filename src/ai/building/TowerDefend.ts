import { AIBase } from "ai/Base";
import { Tower } from "class/buildings/Tower";
import { WorldBase } from "class/world/Base";
import { IScreepsCreep } from "screeps";
import { BodyPart, Result } from "util/Constants";

/**
 * AI module that handles attack of enemy creeps in the room.
 */
export class AITowerDefend extends AIBase {

    private tower: Tower;

    private sector: WorldBase;

    private static target?: IScreepsCreep;

    constructor(tower: Tower, sector: WorldBase) {
        super();
        this.tower = tower;
        this.sector = sector;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there are enemy creeps in the room.
     */
    public shouldExecute(): boolean {
        if (AITowerDefend.target) return true;
        const hostiles = this.sector.getEnemies();
        if (hostiles.length == 0) return false;
        AITowerDefend.target = hostiles.find(h => {
            const body = h.body.find(p => p.type == BodyPart.HEAL);
            return body != null
        });
        if (!AITowerDefend.target) {
            AITowerDefend.target = hostiles.find(h => {
                const body = h.body.find(p => p.type == BodyPart.ATTACK);
                return body != null
            });
        }
        if (!AITowerDefend.target) {
            AITowerDefend.target = hostiles[0];
        }
        return AITowerDefend.target != undefined;
    }

    /**
     * Attacks the designated target.
     */
    public run(): void {
        if (!AITowerDefend.target) return;
        if (AITowerDefend.target.hits == 0) return;
        const result = this.tower.attack(AITowerDefend.target);
        if (result == Result.ERR_INVALID_TARGET) {
            AITowerDefend.target = undefined;
        }
    }
}