import { AIBase } from "ai/Base";
import { BuildingBase } from "class/buildings/Base";
import { Tower } from "class/buildings/Tower";
import { WorldBase } from "class/world/Base";
import { ResourceType } from "util/Constants";
import { Helper } from "util/Helper";

/**
 * AI module that handles repair of damaged walls or ramparts.
 */
export class AITowerRepairDefense extends AIBase {

    private tower: Tower;

    private sector: WorldBase;

    private target?: BuildingBase;

    private cooldown = 20;

    constructor(tower: Tower, sector: WorldBase) {
        super();
        this.tower = tower;
        this.sector = sector;
    }

    /**
     * Finds a building that needs repair.
     * @returns The building to work on.
     */
    private getCurrentTarget(): BuildingBase | undefined {
        if (!this.target || (this.target.getHitPoints() ?? 0) == (this.target.getMaxHitPoints() ?? 1)) {
            this.cooldown = 20;
            this.target = Helper.getMinWith(this.sector.getBuildings()?.getDamagedBuildings() ?? [], b => b.isDefenseBuilding(), target => {
                return target.getHitPoints();
            })
        }
        return this.target;
    }

    /**
     * Checks if it makes sense to execute this AI.
     * @returns True if there are buildings that need to be repaired and the tower and sector has plenty of energy.
     */
    public shouldExecute(): boolean {
        if ((this.sector.getBuildings()?.getStorage()?.getUsedCapacity(ResourceType.ENERGY) ?? 0) < 10000) return false;
        if (this.tower.getUsedCapacity(ResourceType.ENERGY) < this.tower.getCapacity(ResourceType.ENERGY)/2) return false;
        return this.getCurrentTarget() != undefined;
    }

    /**
     * Repairs a damaged building.
     */
    public run(): void {
        const target = this.getCurrentTarget();
        if (!target) {
            console.log("AITowerRepair: No building found!");
            return;
        }
        this.tower.repair(target);
        this.cooldown -= 1;
        if (this.cooldown <= 0) {
            this.target = undefined;
        }
    }
}