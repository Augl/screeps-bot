export abstract class AIBase {

    /**
     * Called when the AI is considered for execution after not being used for some time.
     */
    public reset(): void {
        return;
    }

    public abstract shouldExecute(): boolean;

    public abstract run(): void;
}