let AIBase = require("./ai.base");
let Constants = require("./util.constants");

class AISectorExpand extends AIBase {

    /**
     * Creates a new AISectorBuild module.
     * @param {import('./class.world.sector.js')} sector The sector to work in.
     */
    constructor(sector) {
        super();
        this._sector = sector;
    }

    shouldExecute() {
        let cooldown = this._sector.recall("ecCooldown");
        if (cooldown > 0) {
            //console.log("ecCooldown" + cooldown);
            this._sector.remember("ecCooldown", cooldown-1);
            return false;
        }
        return (this._sector.getLevel() > 4 && Game.time % 10 == 7);
    }

    run() {
        let expandConfig = this._sector.recall("ec") || {level: 1};
        if (expandConfig.level == 1) {
            this.runLevel1(expandConfig);
        }
        else if (expandConfig.level == 2) {
            this.runLevel2(expandConfig);
        }
        this._sector.remember("ec", expandConfig);
    }

    /**
     * Colonize all neighbours with 1 source.
     * @param {*} config The build config object.
     */
    runLevel2(config) {
        let step = config.step || 0;
        let neighbours = this._sector.recall("neighbours");
        if (!neighbours[step]) {
            delete config.step;
            config.level = 3;
            console.log("Expand step 3 ");
            return;
        }
        let sector = this._sector.getEmpire().getSector(neighbours[step]);
        if (sector && sector.getNumberOfSources() == 1) {
            console.log(sector.getName());
            // activate colony and set a cooldown
            sector.setActive();
            this._sector.remember("ecCooldown", 1000);
            
        }
        config.step = step + 1;
    }

    /**
     * Explores all neighbour rooms.
     * @param {*} config The build config object.
     */
    runLevel1(config) {
        // check if all neighbours were explored
        let exits = this._sector.describeExits();
        let allExplored = true;
        for (let key in exits) {
            if (!this._sector.getEmpire().roomWasExplored(exits[key])) {
                allExplored = false;
                break;
            }
        }
        if (allExplored) {
            config.level = 2;
        }
        else {
            if (this._sector.getAuglsWithJob(Constants.Jobs.EXPLORER) == 0) {
                this._sector.addToSpawnQueue(Constants.Jobs.EXPLORER, {}, 1, this._sector.getName());
            }
        }
    }

}

module.exports = AISectorExpand;