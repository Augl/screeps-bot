/**
 * 
 * Base class for all Artificial Intelligence modules.
 * 
 * 
*/

let BaseClass = require("./class.base");

class AIBase extends BaseClass {

    constructor() {
        super(null);
    }

    shouldExecute() {
        console.warn("AI.shouldExecute should be overwritten!");
        return false;
    }

    run() {}
}

module.exports = AIBase;