let AuglBase = require("./class.augl.base");
let AIHarvest = require("./ai.augl.harvest");
let AIUpgrade = require("./ai.augl.upgrade");
let AIBuild = require("./ai.augl.build");
let AIRepairDefense = require("./ai.augl.repair.defense");
let AIRepair = require("./ai.augl.repair");
let AIFlee = require("./ai.augl.flee");

class RepairmanDefense extends AuglBase {

    constructor(creep, sector) {
        super(creep, sector);
        this._augl = creep;
        this.addAI(new AIFlee(this, sector));
        this.addAI(new AIRepairDefense(this, sector));
        this.addAI(new AIRepair(this, sector));
        this.addAI(new AIBuild(this, sector));
        this.addAI(new AIHarvest(this, sector));
        this.addAI(new AIUpgrade(this, sector));
    }
}

module.exports = RepairmanDefense;