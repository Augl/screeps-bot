let AIBase = require("./ai.base");
let Helper = require("./util.helper");

/**
 * AIAuglFlee makes the augl run away from enemy creeps.
 */
class AIAuglFlee extends AIBase {

    /**
     * Creates a new AIAuglFlee module.
     * @param {import('./class.augl.base')} augl The augl to control.
     * @param {import('./class.world.sector.js')} sector The home sector.
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        if (this._sector.getEnemies().length == 0) {
            return false;
        }
        return this._sector.getEnemies().filter(c => Helper.distance(c.pos, this._augl.getPosition()) <= 10).length > 0;
    }

    run() {
        let enemies = this._sector.getEnemies();
        enemies.sort((a, b) => {
            let dA = Helper.distance(a.pos, this._augl.getPosition());
            let dB = Helper.distance(b.pos, this._augl.getPosition());
            if (dA < dB) { return -1; }
            else if (dA > dB) { return 1; }
            else { return 0; }
        });
        let closest = enemies[0];
        if (Helper.distance(this._augl.getPosition(), closest.pos) > 10) {
            // more than 10 squares away, not dangerous
            return;
        }
        // go to a neighbour room, preferrably one that belongs to me
        let neighbours = this._sector.getNeighbours();
        for (let roomName of neighbours){
            let target = this._sector.getEmpire().getSector(roomName)
            if (target) {
                this._augl.moveToTarget(new RoomPosition(25, 25, roomName));
                return;
            }
        }
        // no neighbour belongs to me, pick the first one
        this._augl.moveToTarget(new RoomPosition(25, 25, neighbours[0]));
    }
}

module.exports = AIAuglFlee;