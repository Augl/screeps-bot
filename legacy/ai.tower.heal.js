let AIBase = require("./ai.base");

class AITowerHeal extends AIBase {

    /**
     * Creates a new AITowerHeal.
     * @param {import('./class.building.tower.js')} tower The tower to run.
     * @param {import('./class.world.sector.js')} sector The sector to run in.
     */
    constructor(tower, sector) {
        super();
        this._tower = tower;
        this._sector = sector;
    }

    shouldExecute() {
        this._injured = this._sector.find(FIND_MY_CREEPS, {}, 5).filter(c => c.hits < c.hitsMax);
        if (this._injured.length > 0) {
            return true;
        }
        return false;
    }

    run() {
        this._tower.heal(this._injured[0]);
    }
}

module.exports = AITowerHeal;