let AIBase = require("./ai.base");
let Cache = require("./util.cache");
let Constants = require("./util.constants");

class AIColonyBuild extends AIBase {

    /**
     * Creates a new AIColonyBuild module.
     * @param {import('./class.world.colony')} sector The sector to work in.
     */
    constructor(sector) {
        super();
        this._sector = sector;
    }

    shouldExecute() {
        if (!this._sector.isVisible()) return false;
        let sites = this._sector.getConstructionSites();
        if (sites.length === 0) {
            return true;
        }
        return false;
    }

    run() {
        let buildConfig = this._sector.recall("bc") || {level: 1};
        if (buildConfig.level == 1) {
            this.buildLevel1(buildConfig);
        }
        else if (buildConfig.level == 2) {
            this.buildLevel2(buildConfig);
        }
        this._sector.remember("bc", buildConfig);
    }

    /**
     * Builds a road to the parent sector.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel1(config) {
        // get parent sector
        let parent = this._sector.getNeighbourSector();
        if (!parent) {
            console.log("Error - AIColonyBuild: No parent sector found!");
            return;
        }
        let sources = this._sector.getSources();
        if (sources.length == 0) {
            return;
        }
        let source = Cache.getGameObject(sources[0].id);
        // build road to other room
        let goalPosition = new RoomPosition(25, 25, parent.getName());
        var path = PathFinder.search(source.pos, {pos: goalPosition, range: 5}, {
            plainCost: 2, 
            swampCost: 2,
            maxOps: 5000,
            roomCallback: function(roomName){
                if (roomName == this._sector.getName()) {
                    var costs = new PathFinder.CostMatrix;
                    this._sector.find(FIND_STRUCTURES).forEach(function(struct) {
                    if (struct.structureType === STRUCTURE_ROAD) {
                            // Favor roads over plain tiles
                            costs.set(struct.pos.x, struct.pos.y, 1);
                        }
                    });
                    return costs;
                }
                if (roomName == parent.getName()) {
                    var costs = new PathFinder.CostMatrix;
                    parent.find(FIND_STRUCTURES).forEach(function(struct) {
                    if (struct.structureType === STRUCTURE_WALL) {
                            costs.set(struct.pos.x, struct.pos.y, 255);
                        }
                    });
                    return costs;
                }
            }.bind(this)
        });
        for (let point of path.path){
            if (point.roomName == this._sector.getName()) {
                this._sector.createConstructionSite(point.x, point.y, STRUCTURE_ROAD);
            }
        }
        config.level = 2;
    }

    /**
     * Builds containers next to the sources.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel2(config) {
        let step = config.step || 0;
        let source = this._sector.getSources()[step];
        if (source) {
            let space = source.workspaces[0];
            let result = this._sector.createConstructionSite(space.x, space.y, STRUCTURE_CONTAINER);
            if (result === OK) {
                config.step = step+1;
            }
            else {
                console.log("Error! Can not build container: " + result);
            }
        }
        else {
            delete config.step;
            config.level = 3;
        }
    }

    
}

module.exports = AIColonyBuild;