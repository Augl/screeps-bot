
module.exports = {
    Jobs: {
        HARVESTER: 1,
        UPGRADER: 2,
        BUILDER: 3,
        CARRIER: 4,
        DISTRIBUTER: 5,
        REPAIRMAN: 6,        
        REPAIRMAN_DEFENSE: 7,
        EXPLORER: 8,
        RESERVER: 9,
        INTERCARRIER: 10,
        CLAIMER: 11,
        WARRIOR: 12
    },
    Tasks: {
        HARVEST: 1,
        DEPOSIT: 2,
        UPGRADE: 3,
        BUILD: 4,
        REPAIR: 5,
        DISMANTLE: 6
    },
    Levels: {
        1: 300,
        2: 550,
        3: 800,
        4: 1300,
        5: 1800
    },
    RoomTypes: {
        AREA: 1,
        COLONY: 2,
        WARZONE: 3,
        SECTOR: 4,
        ENEMY: 5
    }
};