var _cache = {};

/**
 * Cache keeping often used values or function results in memory to reduce CPU cost.
 */
class Cache {

    constructor() {}

    getGameObject(id, maxAge) {
        maxAge = maxAge || 5;
        var result = _cache[id];
        if (!result || result.timestamp + maxAge < Game.time) {
            // no valid cache entry
            result = {
                value: Game.getObjectById(id),
                timestamp: Game.time
            };
            _cache[id] = result;
        }
        return result.value;
    }

    storeFunction(key, callback, thisObj, args, maxAge) {
        args = args || [];
        maxAge = maxAge || 10;
        var result = _cache[key];        
        if (!result || result.timestamp + maxAge < Game.time) {
            // no cache result or expired result, invoke callback function and store result
            result = {
                value: callback.apply(thisObj, args),
                timestamp: Game.time
            };
            _cache[key] = result;
        }
        return result.value;
    }

}

module.exports = new Cache();