let AIBase = require("./ai.base");
let Helper = require("./util.helper");

/**
 * AIAuglFight makes the augl fight enemy creeps.
 */
class AIAuglFight extends AIBase {

    /**
     * Creates a new AIAuglFight module.
     * @param {import('./class.augl.base')} augl The augl to control.
     * @param {import('./class.world.sector.js')} sector The home sector.
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        if (this._sector.getEnemies().length == 0) {
            return false;
        }
        return true;
    }

    run() {
        // get closest enemy
        let enemies = this._sector.getEnemies();
        enemies.sort((a, b) => {
            let dA = Helper.distance(a.pos, this._augl.getPosition());
            let dB = Helper.distance(b.pos, this._augl.getPosition());
            if (dA < dB) { return -1; }
            else if (dA > dB) { return 1; }
            else { return 0; }
        });
        let closest = enemies[0];
        this._augl.attack(closest);
    }
}

module.exports = AIAuglFight;