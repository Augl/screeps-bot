let AuglBase = require("./class.augl.base");
let AIHarvest = require("./ai.augl.harvest");
let AIUpgrade = require("./ai.augl.upgrade");
let AIFlee = require("./ai.augl.flee");

class Upgrader extends AuglBase {

    constructor(creep, sector) {
        super(creep, sector);
        this._augl = creep;
        this.addAI(new AIFlee(this, sector));
        this.addAI(new AIUpgrade(this, sector));
        this.addAI(new AIHarvest(this, sector));
    }
}

module.exports = Upgrader;