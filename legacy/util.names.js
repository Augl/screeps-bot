let BaseClass = require("./class.base");

class Names extends BaseClass{

    constructor() {
        super("names");
    }

    getNextName() {
        let name = this.recall("n") || "1";
        this.remember("n", Number(name) + 1);
        return name;
    }

    getNextSpawnName() {
        let names = ["Winterfell", "Kingslanding", "Dragonstone", "Dorne", "Braavos", "Volantis", "Qarth", "Mereen", "Astapor", "Asshai", "Lannisport"];
        let index = this.recall("s") || 0;
        this.remember("s", index+1);
        let name = names[index];
        if (!name) {
            console.log("Error: Out of spawn names!");
            return index;
        }
        return name;
    }
}

module.exports = new Names();