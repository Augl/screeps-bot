
class Helper {

    /**
     * Returns the distance between two points.
     * @param {{x: number, y: number}} a 
     * @param {{x: number, y: number}} b 
     */
    distance(a, b) {
        return Math.max(Math.abs(a.x - b.x), Math.abs(a.y - b.y));
    }

    /**
     * Callback to get the weight of an array element.
     *
     * @callback minCallback
     * @param {*} element The array element.
     * @returns {number} The weight value.
     */
    /**
     * Callback to get if the element should be considered or not.
     *
     * @callback predicate
     * @param {*} element The array element.
     * @returns {boolean} If the element should be considered.
     */
    /**
     * Gets the element of the array for which the minCallback is minimal and predicate is true.
     * @param {*[]} array The array to work on.
     * @param {minCallback} minCallback Callback to use for determining the weight value.
     * @param {predicate} predicate 
     * @returns {?*} The element with a minimal weight and a holding predicate.
     */
    getMinWith(array, minCallback, predicate) {
        let currentMin;
        let currentMinWeight = Number.POSITIVE_INFINITY;
        for (let i = 0; i < array.length; i++) {
            if (predicate(array[i])) {
                let weight = minCallback(array[i]);
                if (weight < currentMinWeight) {
                    currentMinWeight = weight
                    currentMin = array[i];
                }
            }
        }
        return currentMin;
    }

    /**
     * Callback to get the terrrain at a given position.
     *
     * @callback terrainCallback
     * @param {number} x The x coordniate.
     * @param {number} y The y coordinate.
     * @returns {number} The terrain type.
     */
    /**
     * Finds the center of the largest free area in the room.
     * @param {terrainCallback} terrainCallback The callback function to get the terrain at a position.
     * @returns {{x: number, y: number}} The center of the largest area.
     */
    findLargestArea(terrainCallback) {
        // find the largest area using a dynamic programming approach.
        let maxX, maxY;
        let maxSize = 0;
        let dp = [];
        for (let y = 3; y < 47; y++) {
            for (let x = 3; x < 47; x++) {
                if (!dp[x-1]) dp[x-1] = [];
                if (!dp[x]) dp[x] = [];
                let result = Math.min((dp[x][y-1] || 0), Math.min((dp[x-1][y] || 0), (dp[x-1][y-1] || 0)));
                if (terrainCallback(x, y) != 1) {
                    result++;
                }
                else {
                    result = 0;
                }
                dp[x][y] = result;
                if (result > maxSize) {
                    maxX = x;
                    maxY = y;
                    maxSize = result;
                }
            }
        }
        let finalX = maxX - (Math.floor(maxSize/2));
        let finalY = maxY - (Math.floor(maxSize/2));
        return {x: finalX, y: finalY};
    }
}

module.exports = new Helper();