let BaseClass = require("./class.base");
let Sector = require("./class.world.sector");
let Colony = require("./class.world.colony");
let WorldRoom = require("./class.world.room");
let Constants = require("./util.constants");

/**
 * The empire is the top most object in the hierarchy.
 */
class Empire extends BaseClass {

    constructor() {
        super("empire");
        this.initialiseRooms();
    }

    /**
     * Initialises all rooms by iterating over the global Rooms object and the rooms in the memory.
    */
    initialiseRooms() {
        let buildingMap = this.initialiseBuildings();
        let constructionSitesMap = this.sortConstructionSites();
        let creeps = this.sortCreeps();
        this.sectors = [];
        this.colonies = [];
        this.sectorMap = {}; // contains all sectors, colonies etc.
        for (let roomName in Game.rooms) {
            let room = Game.rooms[roomName];
            let type;
            let sector;
            if (Memory.rooms[roomName]) {
                type = Memory.rooms[roomName].type;
            }
            if (type == Constants.RoomTypes.SECTOR) {
                sector = new Sector(roomName, Game.rooms[roomName], creeps[roomName], buildingMap[roomName], constructionSitesMap[roomName], this);
                this.sectors.push(sector);
            }
            else if (type == Constants.RoomTypes.COLONY) {
                sector = new Colony(roomName, room, creeps[roomName], buildingMap[roomName], constructionSitesMap[roomName], this);
                this.colonies.push(sector);
            }
            else {
                // classify room
                new WorldRoom(room);
            }
            this.sectorMap[roomName] = sector;
        }
        for (let roomName in Memory.rooms) {
            if (Game.rooms[roomName]) continue;
            let type = Memory.rooms[roomName].type;
            let sector;
            if (type == Constants.RoomTypes.SECTOR) {
                sector = new Sector(roomName, null, creeps[roomName], buildingMap[roomName], constructionSitesMap[roomName], this);
                this.sectors.push(sector);
            }
            else if (type == Constants.RoomTypes.COLONY) {
                sector = new Colony(roomName, null, creeps[roomName], buildingMap[roomName], constructionSitesMap[roomName], this);
                this.colonies.push(sector);
            }
            this.sectorMap[roomName] = sector;
        }
    }

    /**
     * Sorts the creeps by room.
     * @returns {Object.<string, Creep[]>} A map of room names to arrays of creeps in the room's control.
     */
    sortCreeps() {
        let map = {};        
        for (let creepName in Game.creeps) {
            let creep = Game.creeps[creepName];
            let home = creep.memory.home;
            if (!home) {
                console.log("Error: Creep has no home!");
            }
            if (!map[home]) {
                map[home] = [];                
            }
            map[home].push(creep);
        }
        if (Game.time % 10 == 9) {
            for (let creepName in Memory.creeps) {
                if (!Game.creeps[creepName]) {
                    delete Memory.creeps[creepName];
                }
            }
        }
        return map;
    }

    /**
     * Maps the construction sites to their room.
     * @returns {Object.<string, ConstructionSite[]>} A map of roomNames and their construction sites.
     */
    sortConstructionSites() {
        let map = {};
        for (let id in Game.constructionSites) {
            let cs = Game.constructionSites[id];
            if (!cs.room) continue;
            if (!map[cs.room.name]) {
                map[cs.room.name] = [];
            }
            map[cs.room.name].push(cs);
        }
        return map;
    }

    /**
     * Initialises all buildings by iterating over the global structures map.
     * @returns {Object.<string, Object.<string, *[]>>} An object mapping all room names to objects mapping the structure type to an array of all the structures.
     */
    initialiseBuildings() {
        let map = {};
        for (let id in Game.structures) {
            let struc = Game.structures[id];
            if (!map[struc.room.name]) {
                map[struc.room.name] = {};
            }
            if (!map[struc.room.name][struc.structureType]) {
                map[struc.room.name][struc.structureType] = [];
            }
            map[struc.room.name][struc.structureType].push(struc);
        }
        return map;
    }

    run() {
        for (let i = 0; i < this.sectors.length; i++) {
            this.sectors[i].run();
        }
        for (let i = 0; i < this.colonies.length; i++) {
            this.colonies[i].run();
        }
    }

    /**
     * @param {string} roomName The name of the room
     * @returns {boolean} If the room has been explored.
     */
    roomWasExplored(roomName) {
        return Memory.rooms[roomName]!=undefined;
    }

    /**
     * Returns the sector/colony etc. with the given name.
     * @param {string} roomName The room name
     * @returns {import('./class.world.base')} The sector/colony/... object.
     */
    getSector(roomName) {
        return this.sectorMap[roomName];
    }

    /**
     * Returns a room of type sector closest to the given room.
     * @param {string} roomName The name of the room searching for a sector.
     * @returns {?import('./class.world.sector')} The closest sector.
     */
    getClosestSector(roomName) {
        let queue = [roomName];
        while (queue.length > 0) {
            let current = this.getSector(queue.shift());
            if (current && current.getType() == Constants.RoomTypes.SECTOR && current.getController() && current.getController().my && current.getSpawns().length > 0) {
                return current;
            }
            if (current) {
                queue = queue.concat(current.getNeighbours());
            }            
        }
    }

}

module.exports = Empire