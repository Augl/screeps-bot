let AIBase = require("./ai.base");
let Constants = require("./util.constants");
let Cache = require("./util.cache");
let Helper = require("./util.helper");

/**
 * AIAuglInterCarry handles energy delivery from a colony to the parent sector.
 */
class AIAuglInterCarry extends AIBase {

    /**
     * Creates a new AIAuglInterCarry module.
     * @param {import('./class.augl.base')} augl The augl to control.
     * @param {import('./class.world.colony.js')} colony The colony to run in.
     */
    constructor(augl, colony) {
        super();
        this._augl = augl;
        this._colony = colony;
    }

    shouldExecute() {
        // find source
        let sourceId = this._augl.recall("carrySrc");
        if (!sourceId) {
            this._source = Helper.getMinWith(this._colony.getContainersAtSources(), c => _.sum(c.store), c => _.sum(c.store) > this._augl.getCarryCapacity())
        }
        else {
            this._source = Cache.getGameObject(sourceId, 10);
        }
        if (!this._source) {
            return false;
        }
        // find destination
        let parent = this._colony.getNeighbourSector();
        if (!parent) {
            return false;
        }
        this._target = parent.getStorage();
        if (!this._target) {
            return false;
        }
        this._augl.remember("carrySrc", this._source.id);
        return true;
    }

    run() {
        let isDone = true;
        let task = this._augl.recall("task");
        if (task == Constants.Tasks.HARVEST) {
            this._augl.withdrawEnergy(this._source);
            if (this._augl.isFull() || this._source.store[RESOURCE_ENERGY] == 0) {
                this._augl.remember("task", Constants.Tasks.DEPOSIT);
            }
        }
        else if (task == Constants.Tasks.DEPOSIT) {
            this._augl.transferEnergy(this._target);
            if (this._augl.isEmpty()) {
                this._augl.forget("carrySrc");
                this._augl.remember("task", Constants.Tasks.HARVEST);
            }
            isDone = false;
        }
        else {
            this._augl.remember("task", Constants.Tasks.HARVEST);
        }
        return isDone;
    }
}

module.exports = AIAuglInterCarry;