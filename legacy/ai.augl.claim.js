let AIBase = require("./ai.base");

class AIAuglClaim extends AIBase {

    /**
     * Creates a new AIAuglClaim module.
     * @param {import('./class.augl.base')} augl The augl to control.
     * @param {import('./class.world.sector.js')} sector The sector to run in.
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        if (!this._sector.getController() || !this._sector.getController().my) {
            return true;
        }
        return false;
    }

    run() {
        let controller = this._sector.getController();
        if (!controller) {
            // not in the correct room yet
            this._augl.moveToTarget(new RoomPosition(25, 25, this._sector.getName()));
        }
        else {
            this._augl.claimController(controller);
        }
    }
}

module.exports = AIAuglClaim;