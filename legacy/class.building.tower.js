let BaseClass = require("./class.base");
let AIDefend = require("./ai.tower.defend");
let AIHeal = require("./ai.tower.heal");

class Tower extends BaseClass {

    /**
     * Creates a new Tower.
     * @param {*} tower The underlying tower object.
     * @param {import('./class.world.sector.js')} sector The sector the tower is in.
     */
    constructor(tower, sector) {
        super(); // possible memory root "towers."+sector.getName()+""+tower.pos.x+""+tower.pos.y
        this._tower = tower;
        this._sector = sector;
    }

    run() {
        let ais = [new AIDefend(this, this._sector), new AIHeal(this, this._sector)];
        for (let ai of ais) {
            if (ai.shouldExecute()) {
                ai.run();
                break;
            }
        }
    }

    /**
     * Remotely attacks any creep in the room.
     * @param {*} target The target to attack.
     */
    attack(target) {
        return this._tower.attack(target);
    }

    /**
     * Remotely heals any creep in the room.
     * @param {*} target The target to heal.
     */
    heal(target) {
        return this._tower.heal(target);
    }

    /**
     * @returns {boolean} If the tower is full.
     */
    isFull() {
        return this._tower.energy == this._tower.energyCapacity;
    }
}

module.exports = Tower;