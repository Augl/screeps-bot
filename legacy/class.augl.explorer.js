let AuglBase = require("./class.augl.base");
let AIExplore = require("./ai.augl.explore");
let AIGoHome = require("./ai.augl.gohome");

class Explorer extends AuglBase {

    constructor(creep, sector) {
        super(creep, sector);
        this._augl = creep;
        this.addAI(new AIGoHome(this, sector));
        this.addAI(new AIExplore(this, sector));
    }

    run() {
        super.run();
        let exits = this._sector.describeExits();
        let allExplored = true;
        for (let key in exits) {
            if (!this._sector.getEmpire().roomWasExplored(exits[key])) {
                allExplored = false;
                break;
            }
        }
        if (allExplored) {
            this.suicide();
        }
    }
}

module.exports = Explorer;