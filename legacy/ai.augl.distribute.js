let AIBase = require("./ai.base");
let Constants = require("./util.constants");
let Helper = require("./util.helper");

class AIAuglDistribute extends AIBase {

    /**
     * Creates a new AIAuglDistribute module.
     * @param {import('./class.augl.base')} augl 
     * @param {import('./class.world.sector.js')} sector 
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        // check if distribution is needed
        let augl = this._augl;
        let options = this._sector.getSpawns();
        options = options.concat(this._sector.getExtensions());
        this._target = Helper.getMinWith(options, o => Helper.distance(o.pos, augl.getPosition()), s => s.energy < s.energyCapacity);
        if (!this._target) {
            let tower = this._sector.getTowers().find(t => !t.isFull());
            if (tower) {
                this._target = tower._tower;
            }
        }        
        // empty link has first priority 
        let link = this._sector.getLinksInCentre()[0];
        if (link && !link.isEmpty()) {
            this._source = link._link;
            if (!this._target) {
                let containers = this._sector.getContainersInCentre();
                containers.push(this._sector.getStorage());
                this._target = containers.find(s => s && _.sum(s.store) < s.storeCapacity);
            }            
            return true;
        }
        // find source
        options = this._sector.getLinksInCentre();
        options = options.concat(this._sector.getContainersInCentre());
        options.push(this._sector.getStorage());
        this._source = options.find(c => c && c.store && c.store[RESOURCE_ENERGY] > 0);
        return (this._source != null && this._target != null);
    }

    run() {
        let isDone = true;
        let task = this._augl.recall("task");
        if (task == Constants.Tasks.HARVEST) {
            this._augl.withdrawEnergy(this._source);
            if (this._augl.isFull() || (this._source.store && this._source.store[RESOURCE_ENERGY] == 0) || (this._source.energy && this._source.energy == 0)) {
                this._augl.remember("task", Constants.Tasks.DEPOSIT);
            }
        }
        else if (task == Constants.Tasks.DEPOSIT) {
            this._augl.transferEnergy(this._target);
            if (this._augl.isEmpty()) {
                this._augl.remember("task", Constants.Tasks.HARVEST);
            }
            isDone = false;
        }
        else {
            this._augl.remember("task", Constants.Tasks.HARVEST);
        }
        return isDone;
    }
}

module.exports = AIAuglDistribute;