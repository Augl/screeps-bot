let AIBase = require("./ai.base");
let Cache = require("./util.cache");
let Helper = require("./util.helper");
let Names = require("./util.names");

class AISectorBuild extends AIBase {

    /**
     * Creates a new AISectorBuild module.
     * @param {import('./class.world.sector.js')} sector The sector to work in.
     */
    constructor(sector) {
        super();
        this._sector = sector;
    }

    shouldExecute() {
        let sites = this._sector.getConstructionSites();
        if (sites.length === 0) {
            return true;
        }
        return false;
    }

    run() {
        let buildConfig = this._sector.recall("bc") || {level: 0};
        if (buildConfig.level == 0 && this._sector.getController() && this._sector.getController().my) {
            this.buildLevel0(buildConfig);
        }
        else if (buildConfig.level == 1) {
            this.buildLevel1(buildConfig);
        }
        else if (buildConfig.level == 2) {
            this.buildLevel2(buildConfig);
        }
        else if (buildConfig.level == 3) {
            this.buildLevel3(buildConfig);
        }
        else if (buildConfig.level == 4 && this._sector.getLevel() > 1) {
            this.buildStructures(buildConfig, STRUCTURE_EXTENSION, 5);
        }
        else if (buildConfig.level == 5) {
            this.buildLevel5(buildConfig);
        }
        else if (buildConfig.level == 6) {
            this.buildLevel6(buildConfig);
        }
        else if (buildConfig.level == 7) {
            this.buildLevel7(buildConfig);
        }
        else if (buildConfig.level == 8 && this._sector.getLevel() > 2) {
            this.buildStructures(buildConfig, STRUCTURE_EXTENSION, 5);
        }
        else if (buildConfig.level == 9) {
            this.buildStructures(buildConfig, STRUCTURE_TOWER, 1);
        }
        else if (buildConfig.level == 10) {
            this.buildLevel10(buildConfig);
        }
        else if (buildConfig.level == 11 && this._sector.getLevel() > 3) {
            this.buildStructures(buildConfig, STRUCTURE_STORAGE, 1);
        }
        else if (buildConfig.level == 12) {
            this.buildStructures(buildConfig, STRUCTURE_EXTENSION, 10);
        }
        else if (buildConfig.level == 13 && this._sector.getLevel() > 4) {
            this.buildStructures(buildConfig, STRUCTURE_EXTENSION, 10);
        }
        else if (buildConfig.level == 14) {
            this.buildStructures(buildConfig, STRUCTURE_TOWER, 1);
        }
        else if (buildConfig.level == 15) {
            this.buildLevel15(buildConfig);
        }
        else if (buildConfig.level == 16 && this._sector.getLevel() > 5) {
            this.buildLevel16(buildConfig);
        }
        else if (buildConfig.level == 17) {
            this.buildStructures(buildConfig, STRUCTURE_EXTENSION, 10);
        }
        else if (buildConfig.level == 18 && this._sector.getLevel() > 6) {
            this.buildStructures(buildConfig, STRUCTURE_SPAWN, 1, Names.getNextSpawnName());
        }
        else if (buildConfig.level == 19) {
            this.buildStructures(buildConfig, STRUCTURE_EXTENSION, 10);
        }
        else if (buildConfig.level == 20) {
            this.buildStructures(buildConfig, STRUCTURE_TOWER, 1);
        }
        else if (buildConfig.level == 21) {
            this.buildLevel21(buildConfig);
        }
        this._sector.remember("bc", buildConfig);
    }

    buildLevel0(config) {
        // find best position to place the spawn
        let pos = Helper.findLargestArea((x, y) => this._sector.getTerrainAt(x, y));
        let result = this._sector.createConstructionSite(pos.x, pos.y, STRUCTURE_SPAWN, Names.getNextSpawnName());
        if (result !== OK) {
            console.log("Error: Cannot place spawn! " + result);
        }
        config.level = 1;
    }

    /**
     * Builds containers next to the sources.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel1(config) {
        let step = config.step || 0;
        let source = this._sector.getSources()[step];
        if (source) {
            let space = source.workspaces[0];
            let result = this._sector.createConstructionSite(space.x, space.y, STRUCTURE_CONTAINER);
            if (result === OK) {
                config.step = step+1;
            }
            else {
                console.log("Error! Can not build container: " + result);
            }
        }
        else {
            delete config.step;
            config.level = 2;
        }
    }

    /**
     * Builds the roads to the sources.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel2(config) {
        let step = config.step || 0;
        let sourceId = this._sector.getSources()[step];        
        if (sourceId) {
            let source = Cache.getGameObject(sourceId.id);
            var path = PathFinder.search(source.pos, {pos: this._sector.getSpawns()[0].pos, range: 1}, {
                plainCost: 2, 
                swampCost: 2,
                maxRooms: 1,
                maxOps: 500,
                roomCallback: function(){
                    var costs = new PathFinder.CostMatrix;
                    this._sector.find(FIND_STRUCTURES).forEach(function(struct) {
                      if (struct.structureType === STRUCTURE_ROAD) {
                        // Favor roads over plain tiles
                        costs.set(struct.pos.x, struct.pos.y, 1);
                      }
                    });
                    return costs;
                }.bind(this)
            });
            for (let point of path.path){
                this._sector.createConstructionSite(point.x, point.y, STRUCTURE_ROAD);
            }
            config.step = step + 1;
        }
        else {
            delete config.step;
            config.level = 3;
        }
    }

    /**
     * Builds the remaining containers in the centre.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel3(config) {
        let step = config.step || 0;
        let goal = 5 - this._sector.getSources().length;
        if (step < goal) {
            let result = this._build(config, STRUCTURE_CONTAINER);
            if (result) {
                config.step = step + 1;
            }
        }
        else {
            delete config.step;
            config.level = 4;
        }
    }

    /**
     * Builds the road to the controller.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel5(config) {
        let controller = this._sector.getController();
        var path = PathFinder.search(controller.pos, {pos: this._sector.getSpawns()[0].pos, range: 1}, {
            plainCost: 2, 
            swampCost: 2,
            maxRooms: 1,
            maxOps: 500,
            roomCallback: function(){
                var costs = new PathFinder.CostMatrix;
                this._sector.find(FIND_STRUCTURES).forEach(function(struct) {
                    if (struct.structureType === STRUCTURE_ROAD) {
                    // Favor roads over plain tiles
                    costs.set(struct.pos.x, struct.pos.y, 1);
                    }
                });
                return costs;
            }.bind(this)
        });
        for (let point of path.path){
            this._sector.createConstructionSite(point.x, point.y, STRUCTURE_ROAD);
        }
        config.level = 6;
    }

    /**
     * Builds the roads between sources.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel6(config) {
        let step = config.step || 0;
        let substep = config.substep || 0;
        if (substep == step) {
            substep++;
        }
        let sid1 = this._sector.getSources()[step];
        let sid2 = this._sector.getSources()[substep];
        if (sid1 != null && sid2 != null) {
            let source1 = Cache.getGameObject(sid1.id, 100);
            let source2 = Cache.getGameObject(sid2.id, 100);
            var path = PathFinder.search(source1.pos, {pos: source2.pos, range: 1}, {
                plainCost: 2, 
                swampCost: 5,
                maxRooms: 1,
                maxOps: 500,
                roomCallback: function(){
                    var costs = new PathFinder.CostMatrix;
                    this._sector.find(FIND_STRUCTURES).forEach(function(struct) {
                        if (struct.structureType === STRUCTURE_ROAD) {
                            // Favor roads over plain tiles
                            costs.set(struct.pos.x, struct.pos.y, 1);
                        }
                    });
                    return costs;
                }.bind(this)
            });
            for (let point of path.path){
                this._sector.createConstructionSite(point.x, point.y, STRUCTURE_ROAD);
            }
            config.substep = substep + 1;
        }
        if (!sid1) {
            delete config.step;
            delete config.substep;
            config.level = 7;
        }
        if (!sid2) {
            config.step = step + 1;
            config.substep = 0;
        }
    }

    /**
     * Builds the roads between sources and the controller.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel7(config) {
        let step = config.step || 0;
        let sourceId = this._sector.getSources()[step];        
        if (sourceId) {
            let source = Cache.getGameObject(sourceId.id);
            var path = PathFinder.search(source.pos, {pos: this._sector.getController().pos, range: 1}, {
                plainCost: 2, 
                swampCost: 2,
                maxRooms: 1,
                maxOps: 500,
                roomCallback: function(){
                    var costs = new PathFinder.CostMatrix;
                    this._sector.find(FIND_STRUCTURES).forEach(function(struct) {
                      if (struct.structureType === STRUCTURE_ROAD) {
                        // Favor roads over plain tiles
                        costs.set(struct.pos.x, struct.pos.y, 1);
                      }
                    });
                    return costs;
                }.bind(this)
            });
            for (let point of path.path){
                this._sector.createConstructionSite(point.x, point.y, STRUCTURE_ROAD);
            }
            config.step = step + 1;
        }
        else {
            delete config.step;
            config.level = 8;
        }
    }

    /**
     * Builds walls and ramparts at the room exits.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel10(config) {
        let step = config.step || 0;
        if (step < 4) {
            let substep = config.substep || 0;
            if (substep == 0) {
                // get the current exit
                let roomExits = this._sector.describeExits();
                let roomName;
                if (step == 0) { roomName = roomExits["1"]; }
                else if (step == 1) { roomName = roomExits["3"]; }
                else if (step == 2) { roomName = roomExits["5"]; }
                else if (step == 3) { roomName = roomExits["7"]; }
                if (roomName) {
                    // build the road to the exit
                    let goalPosition = new RoomPosition(25, 25, roomName);
                    var path = PathFinder.search(this._sector.getSpawns()[0].pos, {pos: goalPosition, range: 10}, {
                        plainCost: 2, 
                        swampCost: 2,
                        maxRooms: 1,
                        maxOps: 500,
                        roomCallback: function(){
                            var costs = new PathFinder.CostMatrix;
                            this._sector.find(FIND_STRUCTURES).forEach(function(struct) {
                              if (struct.structureType === STRUCTURE_ROAD) {
                                // Favor roads over plain tiles
                                costs.set(struct.pos.x, struct.pos.y, 1);
                              }
                            });
                            return costs;
                        }.bind(this)
                    });
                    for (let point of path.path){
                        this._sector.createConstructionSite(point.x, point.y, STRUCTURE_ROAD);
                    }
                }
                config.substep = substep + 1;
            }
            else if (substep == 1) {
                let roadMap = this._sector.getRoadMap(50);
                let buildWall = function(x, y) {
                    if (roadMap[x] && roadMap[x][y]) {
                        console.log("Rampart: " + x + ", " + y + ": " + this._sector.createConstructionSite(x, y, STRUCTURE_RAMPART));
                    }
                    else {
                        console.log("Wall: " + x + ", " + y + ": " + this._sector.createConstructionSite(x, y, STRUCTURE_WALL));
                    }
                }.bind(this);
                // build walls
                let isBuilding = false;
                for (let i = 0; i < 50; i++) {
                    let x, y;
                    if (step == 0) { x = i; y = 0; }
                    else if (step == 1) { x = 49; y = i; }
                    else if (step == 2) { x = i; y = 49; }
                    else if (step == 3) { x = 0; y = i; }
                    let terrain = this._sector.getTerrainAt(x, y);
                    if (!isBuilding && terrain != 1) {
                        isBuilding = true;
                        // build start of the wall
                        let sx = x;
                        if (step == 0 || step == 2) sx -= 2;
                        let sy = y;
                        if (step == 1 || step == 3) sy -= 2;
                        for (let j = 0; j < 2; j++) {                            
                            if (step == 0) sy++;
                            else if (step == 1) sx--;
                            else if (step == 2) sy--;
                            else if (step == 3) sx++;
                            buildWall(sx, sy);
                        }
                        for (let j = 0; j < 2; j++) {
                            if (step == 0 || step == 2) sx++;
                            else if (step == 1 || step == 3) sy++;
                            buildWall(sx, sy);
                        }
                    }
                    if (isBuilding && terrain == 1) {
                        isBuilding = false;
                        // build end of the wall
                        let sx = x;
                        if (step == 0 || step == 2) sx += 1;
                        let sy = y;
                        if (step == 1 || step == 3) sy += 1;
                        for (let j = 0; j < 2; j++) {                            
                            if (step == 0) sy++;
                            else if (step == 1) sx--;
                            else if (step == 2) sy--;
                            else if (step == 3) sx++;
                            buildWall(sx, sy);
                        }
                        for (let j = 0; j < 2; j++) {
                            if (step == 0 || step == 2) sx--;
                            else if (step == 1 || step == 3) sy--;
                            buildWall(sx, sy);
                        }
                    }
                    if (isBuilding) {                        
                        let cx = x;
                        if (step == 1) cx -= 2;
                        else if (step == 3) cx += 2;
                        let cy = y;
                        if (step == 0) cy += 2;
                        else if (step == 2) cy -= 2;
                        buildWall(cx, cy);
                    }
                }
                delete config.substep;
                config.step = step + 1;
            }
        }
        else {
            delete config.step;
            delete config.gate;
            config.level = 11;
        }
    }

    /**
     * Deconstructs container at first source and builds a link.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel15(config) {
        let step = config.step || 0;
        if (step == 0) {
            // build link in the centre
            if (this._build(config, STRUCTURE_LINK)) {
                config.step = step + 1;
            }
        }
        else if (step == 1) {
            // deconstruct first source container
            let source = this._sector.getSources()[0];
            let container = this._sector.getContainerForSource(source.id);
            this._sector.deconstructBuilding(container);
            config.step = step + 1;
        }
        else if (step == 2) {
            let deconstruct = this._sector.getDeconstructionTarget();
            if (!deconstruct) {
                // find optimal mining position
                let id = this._sector.getSources()[0].id;
                let s = Cache.getGameObject(id);
                let map = this._sector.lookAtArea(s.pos.y-1, s.pos.x-1, s.pos.y+1, s.pos.x+1, true, 100);
                let mX, mY;
                for (let m of map) {
                    if (m.type == "structure" && m.structure.structureType == STRUCTURE_ROAD) {
                        mX = m.x;
                        mY = m.y;
                    }
                }
                console.log("Best workplace: " + mX + " " + mY);
                map = this._sector.lookAtArea(mY-1, mX-1, mY+1, mX+1, false, 100);
                let lX, lY;
                for (let y in map) {
                    for (let x in map[y]) {
                        let hasRoad = false;
                        console.log(map[y][x])
                        for (let m of map[y][x]) {
                            if (m.type == "structure" && m.structure.structureType == STRUCTURE_ROAD) {
                                hasRoad = true;
                                break;
                            }
                            if (m.type == "terrain" && m.terrain == "wall") {
                                hasRoad = true;
                                break;
                            }
                        }
                        if (!hasRoad) {
                            lX = x;
                            lY = y;
                            break;
                        }
                    }
                    if (lX) break;
                }
                console.log("build source link: " + lX + " " + lY);
                console.log(this._sector.createConstructionSite(Number(lX), Number(lY), STRUCTURE_LINK));
                config.step = step + 1;
            }
        }
        else if (step == 3) {
            this._build(config, STRUCTURE_CONTAINER);
            delete config.step;
            config.level = 16;
        }
    }

    /**
     * Deconstructs container at second source and builds a link.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel16(config) {
        let step = config.step || 0;
        if (step == 0) {
            // deconstruct first source container
            let source = this._sector.getSources()[1];
            let container = this._sector.getContainerForSource(source.id);
            this._sector.deconstructBuilding(container);
            config.step = step + 1;
        }
        else if (step == 1) {
            let deconstruct = this._sector.getDeconstructionTarget();
            if (!deconstruct) {
                // find optimal mining position
                let id = this._sector.getSources()[1].id;
                let s = Cache.getGameObject(id);
                let map = this._sector.lookAtArea(s.pos.y-1, s.pos.x-1, s.pos.y+1, s.pos.x+1, true, 100);
                let mX, mY;
                for (let m of map) {
                    if (m.type == "structure" && m.structure.structureType == STRUCTURE_ROAD) {
                        mX = m.x;
                        mY = m.y;
                    }
                }
                console.log("Best workplace: " + mX + " " + mY);
                map = this._sector.lookAtArea(mY-1, mX-1, mY+1, mX+1, false, 100);
                let lX, lY;
                for (let y in map) {
                    for (let x in map[y]) {
                        let hasRoad = false;
                        console.log(map[y][x])
                        for (let m of map[y][x]) {
                            if (m.type == "structure" && m.structure.structureType == STRUCTURE_ROAD) {
                                hasRoad = true;
                                break;
                            }
                            if (m.type == "terrain" && m.terrain == "wall") {
                                hasRoad = true;
                                break;
                            }
                        }
                        if (!hasRoad) {
                            lX = x;
                            lY = y;
                            break;
                        }
                    }
                    if (lX) break;
                }
                console.log("build source link: " + lX + " " + lY);
                console.log(this._sector.createConstructionSite(Number(lX), Number(lY), STRUCTURE_LINK));
                config.step = step + 1;
            }
        }
        else if (step == 2) {
            this._build(config, STRUCTURE_CONTAINER);
            delete config.step;
            config.level = 17;
        }
    }

    /**
     * Builds link next to the controller.
     * @param {{level: number, step: ?number}} config The build configuration object.
     */
    buildLevel21(config) {
        // find optimal upgrading position
        let s = this._sector.getController();
        let map = this._sector.lookAtArea(s.pos.y-1, s.pos.x-1, s.pos.y+1, s.pos.x+1, true, 100);
        let mX, mY;
        for (let m of map) {
            if (m.type == "structure" && m.structure.structureType == STRUCTURE_ROAD) {
                mX = m.x;
                mY = m.y;
            }
        }
        console.log("Best workplace: " + mX + " " + mY);
        map = this._sector.lookAtArea(mY-1, mX-1, mY+1, mX+1, false, 100);
        let lX, lY;
        for (let y in map) {
            for (let x in map[y]) {
                let hasRoad = false;
                for (let m of map[y][x]) {
                    if (m.type == "structure" && m.structure.structureType == STRUCTURE_ROAD) {
                        hasRoad = true;
                        break;
                    }
                    if (m.type == "terrain" && m.terrain == "wall") {
                        hasRoad = true;
                        break;
                    }
                }
                if (!hasRoad) {
                    lX = x;
                    lY = y;
                    break;
                }
            }
            if (lX) break;
        }
        console.log("build controller link: " + lX + " " + lY);
        console.log(this._sector.createConstructionSite(Number(lX), Number(lY), STRUCTURE_LINK));
        config.level = 22;
    }

    /**
     * Builds the given number of structures.
     * @param {{level: number, step: ?number}} config The build configuration object.
     * @param {*} structure The building to add.
     * @param {string} name The name of the structure, only works for spawns.
     * @param {number} amount The amount of buildings to construct.
     */
    buildStructures(config, structure, amount, name) {
        let step = config.step || 0;
        if (step < amount) {
            let result = this._build(config, structure, name);
            if (result) {
                config.step++;
            }
        }
        else {
            delete config.step;
            config.level++;
        }
    }

    /**
     * Tries to build the given building.
     * @param {{bs: {s: number, d: number, p: number, x: number, y: number}}} config The current build configuration. Has to be saved afterwards.
     * @param {string} building One of the STRUCTURE_* constants.
     * @param {string} name The name of the structure, only works for spawns.
     * @returns {boolean} If the building was placed.
     */
    _build(config, building, name) {
        /* s = size of the current "ring" - 1 (e.g. 2, 4, 6 ...)
         * d = current direction - 0: right, 1: down, 2: left, 3: up
         * p = progress in current direction, therefore between 0 and s, excluding s
         * x, y = coordinates of current point
         */
        let state = config.bs || {s: 2, d: 0, p: 0, x: this._sector.getSpawns()[0].pos.x-1, y: this._sector.getSpawns()[0].pos.y-1};
        let didSucceed = true;
        if (state.p % 2 == 1) {
            // build road and not the actual building
            if (this._sector.getTerrainAt(state.x, state.y) != 1) {
                this._sector.createConstructionSite(state.x, state.y, STRUCTURE_ROAD);
            }            
            didSucceed = false;
        }
        else {
            // build actual building
            let result = this._sector.createConstructionSite(state.x, state.y, building, name);
            didSucceed = (result === OK) || (result === ERR_INVALID_TARGET);
            if (result === ERR_RCL_NOT_ENOUGH) {
                return true;
            }
            if (!didSucceed) {
                console.log("Build error: " + result);
                return false;
            }
        }
        // increase coordinates in the current direction
        if (state.d == 0) { state.x++; }
        else if (state.d == 1) { state.y++; }
        else if (state.d == 2) { state.x--; }
        else if (state.d == 3) { state.y--; }
        else { console.log("Error! Building direction corrupt!"); }
        state.p++;
        if (state.p == state.s) {
            // change direction
            state.d = (state.d+1) % 4;
            state.p = 0;
            if (state.d == 0) {
                // go to the next "ring"
                state.x--;
                state.y--;
                state.s += 2;
            }
        }
        config.bs = state;
        return didSucceed;
    }
}

module.exports = AISectorBuild;