let BaseClass = require("./class.base");
let Constants = require("./util.constants");
let Cache = require("./util.cache");
let Helper = require("./util.helper");
let AITalk = require("./ai.augl.talk");

class AuglBase extends BaseClass {

    /**
     * Creates a new AuglBase.
     * @param {*} creep The underlying creep. 
     * @param {import('./class.world.sector.js')} sector The sector the augl works in.
     */
    constructor(creep, sector, aiPatience=100) {
        super("creeps."+creep.name);
        this._passiveAi = [new AITalk(this)];
        this._ai = [];
        this._creep = creep;
        this._sector = sector;
        this._aiPatience = aiPatience;
    }

    /**
     * Adds the given AI to be run by the augl.
     * @param {AIBase} ai An AIAugl module.
     */
    addAI(ai) {
        this._ai.push(ai);
    }

    /**
     * Returns the name of the augl.
     * The AI modules will be run in the order they were added.
     */
    getName() {
        return this._creep.name;
    }

    run() {
        this.remember("isMoving", false);
        for (let ai of this._passiveAi) {
            if (ai.shouldExecute()) {
                ai.run();
                break;
            }
        }
        // check if there still is a task that needs completion for at most 100 ticks
        let didExecute = false;
        let previousTaskIndex = this.recall("aiTask");
        let previousTaskTime = this.recall("aiTaskTime");
        this.remember("aiTaskTime", previousTaskTime-1);
        if (previousTaskIndex !== undefined && previousTaskTime > 0) { // if aiTask == 0 it should also be true!
            let ai = this._ai[previousTaskIndex];
            if (ai.shouldExecute()) {
                didExecute = true;
                let isDone = ai.run();
                if (isDone) {
                    // task is now done, forget it.
                    this.forget("aiTask");
                    this.forget("aiTaskTime");
                }
            }
            else {
                // the ai did not run, forget the task
                this.forget("aiTask");
                this.forget("aiTaskTime");
            }
        }
        if (!didExecute) {
            // if no task had to be completed run the next best ai.
            for (let i = 0; i < this._ai.length; i++) {
                let ai = this._ai[i];
                if (ai.shouldExecute()) {
                    let isDone = ai.run();
                    if (isDone === false) { // undefined should NOT trigger the if.
                        this.remember("aiTask", i);
                        this.remember("aiTaskTime", this._aiPatience);
                    }
                    break;
                }
            }
        }        
        if (global.DEBUG_VISUALS) {
            this.drawDebugVisuals();
        }
        if (!this.recall("isMoving")) {
            if (this._creep.pos.y < 2){
                this._creep.move(BOTTOM);
            }
            else if (this._creep.pos.x < 2){
                this._creep.move(RIGHT);
            }
            else if (this._creep.pos.y > 47){
                this._creep.move(TOP);
            }
            else if (this._creep.pos.x > 47){
                this._creep.move(LEFT);
            }
        }        
    }

    drawDebugVisuals() {
        let job = this.recall("job");
        let text = "";
        if (job == Constants.Jobs.HARVESTER){
            text = "🪓";
        }
        else if (job == Constants.Jobs.UPGRADER){
            text = "🔼";
        }
        else if (job == Constants.Jobs.BUILDER){
            text = "🔨";
        }
        else if (job == Constants.Jobs.CARRIER){
            text = "🚚";
        }
        else if (job == Constants.Jobs.REPAIRMAN){
            text = "🔧";
        }
        else if (job == Constants.Jobs.REPAIRMAN_DEFENSE) {
            text = "🧰";
        }
        else if (job == Constants.Jobs.DISTRIBUTER) {
            text = "🚛";
        }
        else if (job == Constants.Jobs.EXPLORER) {
            text = "🔭";
        }
        else if (job == Constants.Jobs.RESERVER) {
            text = "📘";
        }
        else if (job == Constants.Jobs.INTERCARRIER) {
            text = "✈️";
        }
        else if (job == Constants.Jobs.CLAIMER) {
            text = "📕";
        }
        else if (job == Constants.Jobs.WARRIOR) {
            text = "⚔️";
        }
        this._creep.room.visual.text(text, this._creep.pos.x+1, this._creep.pos.y);
    }

    /**
     * @returns {{x: number, y: number, roomName: string}} The augl's position.
     */
    getPosition() {
        return this._creep.pos;
    }

    /**
     * Publicly displays the given message.
     * @param {string} message 
     */
    say(message) {
        return this._creep.say(message, true);
    }

    /**
     * Moves the augl to the source and harvests energy or minerals from it.
     * @param {string} sourceId The object id of the source.
     */
    harvest(sourceId) {
        let source = Cache.getGameObject(sourceId, 2);
        let result = this._creep.harvest(source);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(source);
        }
        return result;
    }

    /**
     * Moves the augl to the closest container or source to get energy.
     * @param {boolean} [useContainers=false] If the augl is allowed to use containers to fill up on energy.
     */
    getEnergy(useContainers = false) {
        if (useContainers) {
            // check for closest filled container
            let containers = Array.from(this._sector.getContainers());
            if (this._sector.getStorage()) {
                containers.push(this._sector.getStorage());
            }
            if (this._sector.getLinkForController()) {
                containers.push(this._sector.getLinkForController());
            }
            let container = Helper.getMinWith(containers, c => {
                if (c.pos) {
                    return Helper.distance(c.pos, this.getPosition());
                }
                else {
                    return Helper.distance(c.getPosition(), this.getPosition());
                }                
            }, c => {
                if (c.store) {
                    return c.store[RESOURCE_ENERGY] >= this._creep.carryCapacity;
                }
                else {
                    return !c.isEmpty();
                }
            });
            if (container) {
                if (container._link) container = container._link;
                this.withdrawEnergy(container);
                return;
            }
        }
        let sourceId = this.recall("sourceId");
        if (!sourceId) {
            let sources = this._sector.getSources();
            if (sources.length > 0) {
                sourceId = sources[Math.floor(Math.random()*sources.length)].id;
                this.remember("sourceId", sourceId);
            }            
        }        
        this.harvest(sourceId);
    }

    /**
     * Transfers the energy from the augl to the target.
     * @param {object} target The target object.
     */
    transferEnergy(target) {
        let result = this._creep.transfer(target, RESOURCE_ENERGY);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(target);
        }
        return result;
    }

    /**
     * Moves the augl to the target to withdraw energy.
     * @param {*} target The target object.
     */
    withdrawEnergy(target) {
        let result = this._creep.withdraw(target, RESOURCE_ENERGY);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(target);
        }
        return result;
    }

    /**
     * Updates the given controller.
     * @param {object} controller The controller object.
     */
    upgradeController(controller) {
        let result = this._creep.upgradeController(controller);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(controller);
        }
        return result;
    }

    /**
     * Moves the augl to the target object.
     * @param {object} target An object that contains a RoomPosition or is itself a RoomPosition.
     */
    moveToTarget(target) {
        /*var perf = Memory.performance || {d: 0, n: 0};
        var startUsed = Game.cpu.getUsed();
        var res = function() {  */
            let reusePath = 50;
            let ignoreCreeps = true;
            this.remember("isMoving", true);
            let conf = this.recall("mc");
            if (conf && conf.x == this.getPosition().x && conf.y == this.getPosition().y) {
                // augl has not moved, increase counter
                conf.c++;            
            }
            else {
                // augl did move, reset move config
                conf = {x: this.getPosition().x, y: this.getPosition().y, c: 0};
                
            }
            this.remember("mc", conf);
            if (conf.c > 3) {
                // augl seems to be blocked, recalculate path
                reusePath = 0;
            }
            if (conf.c > 6) {
                // augl is really stuck, stop ignoring augls
                ignoreCreeps = false;
            }
            let result = this._creep.moveTo(target, {
                ignoreCreeps: ignoreCreeps,
                reusePath: reusePath,
                visualizePathStyle: {},
                range: 1,
                maxOps: 2000,
                costCallback: function(roomName, matrix) {
                    let key = "cost-matrix-"+roomName;
                    matrix = Cache.storeFunction(key, (r, m) => {
                        let sector = this._sector.getEmpire().getSector(r);
                        if (!sector) return m;
                        for (let augl of sector.getAllAugls()) {
                            if (!augl.isMoving()) {
                                m.set(augl.getPosition().x, augl.getPosition().y, 255);
                                if (sector._room) sector._room.visual.text("❌", augl.getPosition().x, augl.getPosition().y);
                            }
                        }
                        return m;
                    }, this, [roomName, matrix], 1);
                }.bind(this)
            });
            return result;
        /*}.bind(this)();
        perf.d += Game.cpu.getUsed() - startUsed;
        perf.n ++;
        Memory.performance = perf;
        return res;*/
    }

    /**
     * @returns {boolean} True if the augl wants to move.
     */
    isMoving() {
        return this.recall("isMoving") == true;
    }

    /**
     * @returns {boolean} True if the augl is full, false otherwise.
     */
    isFull() {
        return _.sum(this._creep.carry) === this._creep.carryCapacity;
    }

    /**
     * @returns {boolean} True if the augl is not carrying anything.
     */
    isEmpty() {
        return _.sum(this._creep.carry) === 0;
    }

    /**
     * @returns {number} How much the augl can carry.
     */
    getCarryCapacity() {
        return this._creep.carryCapacity;
    }

    /**
     * Moves the augl to the construction site and build the building.
     * @param {*} target The construction site to go to.
     */
    build(target) {
        let result = this._creep.build(target);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(target);
        }
        return result;
    }

    /**
     * Moves the augl to the building and repairs the building.
     * @param {*} target The building to repair.
     */
    repair(target) {
        let result = this._creep.repair(target);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(target);
        }
        return result;
    }

    /**
     * Reserves the given controller.
     * @param {*} target The controller to reserve.
     */
    reserveController(target) {
        let result = this._creep.reserveController(target);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(target);
        }
        return result;
    }

    /**
     * Claims the given controller.
     * @param {*} target The controller to claim.
     */
    claimController(target) {
        let result = this._creep.claimController(target);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(target);
        }
        return result;
    }

    /**
     * Commits suicide.
     */
    suicide() {
        this._creep.suicide();
    }

    /**
     * Moves the augl to the building and dismantles the building.
     * @param {*} target The building to dismantle.
     */
    dismantle(target) {
        let result = this._creep.dismantle(target);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(target);
        }
        return result;
    }

    /**
     * Moves the augl to the creep and attacks it.
     * @param {*} target The creep to attack.
     */
    attack(target) {
        let result = this._creep.attack(target);
        if (result === ERR_NOT_IN_RANGE) {
            return this.moveToTarget(target);
        }
        return result;
    }

    /**
     * @returns {number} The number of ticks until the augl dies.
     */
    getTicksToLive() {
        return this._creep.ticksToLive;
    }
}

module.exports = AuglBase;