let AIBase = require("./ai.base");
let Constants = require("./util.constants");

/** 
 * AI module responsible for calculating the number and types of Augls required in a colony.
 */
class AIColonyPopulation extends AIBase {


    /**
     * Creates a new AIColonyPopulation module.
     * @param {import('./class.world.colony.js')} sector The colony the AI should run on.
     */
    constructor(sector) {
        super();
        this._sector = sector;
    }

    shouldExecute() {
        return (Game.time % 20 == 3);
    }

    run() {
        // get parent to spawn the augls, since a colony does not have a spawn.
        let parent = this._sector.getNeighbourSector();
        // we need two reservers
        let reservers = 2;
        let controller = this._sector.getController();
        if (controller && controller.reservation && controller.reservation.ticksToEnd > 4000) {
            reservers = 1;
        }
        if (this._sector.getAuglsWithJob(Constants.Jobs.RESERVER).length < reservers) {
            parent.addToSpawnQueue(Constants.Jobs.RESERVER, {}, 3, this._sector.getName());
        }
        // we need as many harvesters and intercarriers as there are sources, as soon as everything was built.
        if (this._sector.getConstructionSites().length == 0) {
            if (this._sector.getAuglsWithJob(Constants.Jobs.HARVESTER).length < this._sector.getSources().length) {
                let source = this._sector.getSources()[this._sector.getAuglsWithJob(Constants.Jobs.HARVESTER).length];
                parent.addToSpawnQueue(Constants.Jobs.HARVESTER, {sourceId: source.id}, 4, this._sector.getName());
            }
            if (this._sector.getAuglsWithJob(Constants.Jobs.INTERCARRIER).length < this._sector.getNumberOfSources()*2) {
                parent.addToSpawnQueue(Constants.Jobs.INTERCARRIER, {}, 4, this._sector.getName());
            }
            if (this._sector.getAuglsWithJob(Constants.Jobs.REPAIRMAN).length < 1) {
                parent.addToSpawnQueue(Constants.Jobs.REPAIRMAN, {}, 3, this._sector.getName());
            }
        }
        else {
            // we need builders to build the initial infrastructure.
            if (this._sector.getAuglsWithJob(Constants.Jobs.BUILDER).length < 2) {
                parent.addToSpawnQueue(Constants.Jobs.BUILDER, {}, 3, this._sector.getName());
            }
        }
    }
}

module.exports = AIColonyPopulation;