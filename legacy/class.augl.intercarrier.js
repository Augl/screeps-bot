let AuglBase = require("./class.augl.base");
let AIHarvest = require("./ai.augl.harvest");
let AIBuild = require("./ai.augl.build");
let AIInterCarry = require("./ai.augl.intercarry");
let AIRepair = require("./ai.augl.repair");
let AIUpgrade = require("./ai.augl.upgrade");
let AIFlee = require("./ai.augl.flee");

class InterCarrier extends AuglBase {

    constructor(creep, sector) {
        super(creep, sector);
        this._augl = creep;
        this.addAI(new AIFlee(this, sector));
        this.addAI(new AIInterCarry(this, sector));        
        this.addAI(new AIRepair(this, sector));
        this.addAI(new AIBuild(this, sector));
        this.addAI(new AIHarvest(this, sector));
        this.addAI(new AIUpgrade(this, sector));
    }
}

module.exports = InterCarrier;