let AuglBase = require("./class.augl.base");
let AIFight = require("./ai.augl.fight");
let AIGoHome = require("./ai.augl.gohome");
let AIExplore = require("./ai.augl.explore");

class Warrior extends AuglBase {

    constructor(creep, sector) {
        super(creep, sector);
        this._augl = creep;
        this.addAI(new AIFight(this, sector));
        this.addAI(new AIGoHome(this, sector));
        this.addAI(new AIExplore(this, sector));
    }
}

module.exports = Warrior;