let AuglBase = require("./class.augl.base");
let AIHarvest = require("./ai.augl.harvest");
let AIUpgrade = require("./ai.augl.upgrade");
let AIBuild = require("./ai.augl.build");
let AICarry = require("./ai.augl.carry");
let AIGoHome = require("./ai.augl.gohome");
let AIFlee = require("./ai.augl.flee");

class Harvester extends AuglBase {

    constructor(creep, sector) {
        super(creep, sector);
        this._augl = creep;
        this.addAI(new AIFlee(this, sector));
        this.addAI(new AIHarvest(this, sector));
        this.addAI(new AICarry(this, sector));
        this.addAI(new AIBuild(this, sector));
        this.addAI(new AIUpgrade(this, sector));
        this.addAI(new AIGoHome(this, sector));
    }
}

module.exports = Harvester;