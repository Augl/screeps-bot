let AIBase = require("./ai.base");
let Constants = require("./util.constants");
let Cache = require("./util.cache");
let Helper = require("./util.helper");

class AIAuglRepair extends AIBase {

    /**
     * Creates a new AIAuglRepair module.
     * @param {import('./class.augl.base.js')} augl The augl to work with.
     * @param {import('./class.world.sector.js')} sector The sector to work in.
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        // check if the augl already has a repair target
        let targetId = this._augl.recall("repair_target");
        if (targetId) {
            this._target = Cache.getGameObject(targetId);
            return this._target != undefined;
        }
        // find building to repair
        let buildings = this._sector.getContainers();
        buildings = buildings.concat(this._sector.getRoads());
        buildings = buildings.concat(this._sector.getAllBuildings());
        this._target = Helper.getMinWith(buildings, b => b.hits, b => b.hits < b.hitsMax*75/100);
        // make sure the target is not supposed to be deconstructed
        let deconstruction = this._sector.getDeconstructionTarget();
        if (deconstruction && this._target.id == deconstruction.id) {
            return false;
        }
        return this._target != undefined;
    }

    run() {
        let isDone = true;
        let task = this._augl.recall("task");
        if (task === Constants.Tasks.HARVEST) {
            this._augl.getEnergy(true);
            if (this._augl.isFull()) {
                this._augl.remember("task", Constants.Tasks.REPAIR);
            }
        }
        else if (task === Constants.Tasks.REPAIR) {
            this._augl.repair(this._target);
            this._augl.remember("repair_target", this._target.id);
            if (this._augl.isEmpty() || this._target.hits == this._target.hitsMax) {
                this._augl.forget("repair_target");
                this._augl.remember("task", Constants.Tasks.HARVEST);
            }
            isDone = false;
        }
        else {
            this._augl.remember("task", Constants.Tasks.HARVEST);
        }
        return isDone;
    }
}

module.exports = AIAuglRepair;