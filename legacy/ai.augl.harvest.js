let AIBase = require("./ai.base");
let Constants = require("./util.constants");
let Helper = require("./util.helper");

class AIAuglHarvest extends AIBase {

    /**
     * Creates a new AIAuglHarvest module.
     * @param {import('./class.augl.base')} augl 
     * @param {import('./class.world.sector.js')} sector 
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {       
        // check if more energy can be stored in this sector
        if (this._sector.getAvailableEnergy() < this._sector.getEnergyCapacity()
                && this._sector.getAuglsWithJob(Constants.Jobs.CARRIER) == 0 && this._sector.getAuglsWithJob(Constants.Jobs.DISTRIBUTER) == 0) {
            return true;
        }
        let sourceId = this._augl.recall("sourceId");
        if (sourceId) {
            let container = this._sector.getContainerForSource(sourceId);
            if (container && _.sum(container.store) < container.storeCapacity) {
                this._container = container;
                return true;
            }
            let link = this._sector.getLinkForSource(sourceId);
            if (link && !link.isFull()) {
                this._container = link._link;
                return true;
            }
        }        
        return false;
    }

    run() {
        let isDone = true;
        let task = this._augl.recall("task");
        if (task == Constants.Tasks.HARVEST) {
            if (this._augl.isFull()) {
                this._augl.remember("task", Constants.Tasks.DEPOSIT);
            }
            else {
                this._augl.getEnergy();
            }            
        }
        else if (task == Constants.Tasks.DEPOSIT) {
            isDone = false;
            // find target to deliver to
            let target = this._container;
            if (!target) {
                // check if a spawn needs energy
                target = this._sector.getSpawns().find(s => s.energy < s.energyCapacity);
            }
            if (!target) {
                // check extensions
                target = this._sector.getExtensions().find(s => s.energy < s.energyCapacity);
            }
            if (target) {
                this._augl.transferEnergy(target);
            }
            else {
                console.log("No other targets to deliver energy to!");
                isDone = true;
            }
            if (this._augl.isEmpty()) {
                this._augl.remember("task", Constants.Tasks.HARVEST);
            }            
        }
        else {
            this._augl.remember("task", Constants.Tasks.HARVEST);
        }
        return isDone;
    }
}

module.exports = AIAuglHarvest;