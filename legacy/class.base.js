/**
 *
 * The base class all other classes inherit from.
 *
*/

class BaseClass {

    constructor(memoryPath) {
        this.initialiseMemoryRoot(memoryPath);
    }

    /**
     * Creates the memory object specified by the given path and sets it for remember and forget functions.
     * @param {string} memoryPath Dot seperated path of the memory location, e.g. "rooms.sim".
     */
    initialiseMemoryRoot(memoryPath) {
        if (!memoryPath) {
            this._memory = null;
            return;
        }
        let splitted = memoryPath.split(".");
        var memoryObject = Memory;
        for (let i = 0; i < splitted.length; i++) {
            if (!memoryObject[splitted[i]]) {
                memoryObject[splitted[i]] = {};
            }
            memoryObject = memoryObject[splitted[i]];
        }
        this._memory = memoryObject;
    }

    /**
     * Remembers the given value by the given key. Remembered values will be stored to memory and are available in future ticks.
     * @param {string} key A string identifiyng the value.
     * @param {string | number} value The value to be remembered.
     */
    remember(key, value) {
        if (!this._memory) {
            // No memory path was given with the constructor, unable to remember anything.
            console.error("Unable to remember without memory object");
            return;
        }
        this._memory[key] = value;
    }

    /**
     * Recalls the value stored using the remember function.
     * @param {string} key The key used to store the value.
     * @returns {string | number} The value that was stored.
     */
    recall(key) {
        if (!this._memory) {
            // No memory path was given with the constructor, unable to recall anything.
            console.error("Unable to recall without memory object");
            return;
        }
        return this._memory[key];
    }

    /**
     * Forgets the value associated with the given key.
     * @param {string} key The key used in the remember function that should now be forgotten.
     */
    forget(key) {
        if (!this._memory) {
            // No memory path was given with the constructor, unable to forget anything.
            console.error("Unable to forget without memory object");
            return;
        }
        delete this._memory[key];
    }
}

module.exports = BaseClass;