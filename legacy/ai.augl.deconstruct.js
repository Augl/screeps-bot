let AIBase = require("./ai.base");
let Constants = require("./util.constants");

class AIAuglDeconstruct extends AIBase {

    /**
     * Creates a new AIAuglDeconstruct module.
     * @param {import('./class.augl.base.js')} augl The augl to work with.
     * @param {import('./class.world.sector.js')} sector The sector to work in.
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        this._target = this._sector.getDeconstructionTarget();
        return this._target != null;
    }

    run() {
        let isDone = true;
        let task = this._augl.recall("task");
        if (task === Constants.Tasks.DEPOSIT) {
            this._augl.transferEnergy(this._sector.getStorage());
            if (this._augl.isEmpty()) {
                this._augl.remember("task", Constants.Tasks.DISMANTLE);
            }
        }
        else if (task === Constants.Tasks.DISMANTLE) {
            this._augl.dismantle(this._target);
            if (this._augl.isFull()) {
                this._augl.remember("task", Constants.Tasks.DEPOSIT);
            }
            isDone = false;
        }
        else {
            this._augl.remember("task", Constants.Tasks.DEPOSIT);
        }
        return isDone;
    }
}

module.exports = AIAuglDeconstruct;