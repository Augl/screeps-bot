let WorldBase = require("./class.world.base");
let Constants = require("./util.constants");

/**
 * Room is an unclassified space in the world.
 * The Room will classify itself as one of the other possible categories.
 */
class WorldRoom extends WorldBase {
    
    constructor(room) {
        super(room.name);
        this._room = room;
        this.classifyRoom();
    }

    classifyRoom() {
        // get neighbours
        let neighbours = [];
        let exits = this.describeExits();
        for (let key in exits) {
            neighbours.push(exits[key]);
        }
        this.remember("neighbours", neighbours);
        // check if the room has a controller
        let controller = this.getController();
        if (controller) {
            // check who owns the room
            let owner = controller.owner;
            if (!owner) {
                // room is free => colony
                // store number of sources
                let sources = this.getSources();
                this.remember("sources", sources.length);
                if (sources.length == 1) {
                    this.remember("type", Constants.RoomTypes.COLONY);
                }
                else {
                    this.remember("type", Constants.RoomTypes.SECTOR);
                }
            }
            else {
                // either enemy or my own
                if (controller.my) {
                    this.remember("type", Constants.RoomTypes.SECTOR);
                }
                else {
                    this.remember("type", Constants.RoomTypes.ENEMY);
                }
            }
        }
        else {
            // no controller available
            // check sources
            let sources = this.getSources();
            this.remember("sources", sources.length);
            this.remember("type", Constants.RoomTypes.WARZONE);
        }
        this.remember("inactive", true);
    }
}

module.exports = WorldRoom;