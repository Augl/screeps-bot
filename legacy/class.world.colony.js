let WorldBase = require("./class.world.base");

let AIBuild = require("./ai.colony.build");
let AIPopulation = require("./ai.colony.population");
let AIDefend = require("./ai.colony.defend");
let AIExpand = require("./ai.colony.expand");

let Constants = require("./util.constants");

class Colony extends WorldBase {

    
    constructor(roomName, room, creeps, buildings, constructionSites, empire) {
        super(roomName, room, creeps, buildings, constructionSites, empire);
    }

    run() {
        if (!this.isActive()) return;
        super.run();
        let ais = [new AIDefend(this), new AIPopulation(this), new AIExpand(this), new AIBuild(this)];
        for (let ai of ais) {
            if (ai.shouldExecute()) {
                ai.run();
                break;
            }
        }
    }

    /**
     * Returns a neighbour of type SECTOR.
     * @returns {import('./class.world.sector')} The neighbour sector.
     */
    getNeighbourSector() {
        let neighbours = this.getNeighbours();
        let parent;
        for (let name of neighbours) {
            let sector = this.getEmpire().getSector(name);
            if (sector && sector.getType() == Constants.RoomTypes.SECTOR && sector.isActive()) {
                parent = sector;
                break;
            }
        }
        return parent
    }
}

module.exports = Colony;