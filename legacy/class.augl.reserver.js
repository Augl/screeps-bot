let AuglBase = require("./class.augl.base");
let AIReserve = require("./ai.augl.reserve");
let AIFlee = require("./ai.augl.flee");

class Reserver extends AuglBase {

    constructor(creep, sector) {
        super(creep, sector);
        this._augl = creep;
        this.addAI(new AIFlee(this, sector));
        this.addAI(new AIReserve(this, sector));
    }
}

module.exports = Reserver;