let AIBase = require("./ai.base");

class AIAuglReserve extends AIBase {

    /**
     * Creates a new AIAuglReserve module.
     * @param {import('./class.augl.base')} augl 
     * @param {import('./class.world.sector.js')} sector 
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        return true;
    }

    run() {
        let controller = this._sector.getController();
        if (!controller) {
            // not in the correct room yet
            this._augl.moveToTarget(new RoomPosition(25, 25, this._sector.getName()));
        }
        else {
            this._augl.reserveController(controller);
        }
    }
}

module.exports = AIAuglReserve;