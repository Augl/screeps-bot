let BaseClass = require("./class.base");
let AITransfer = require("./ai.link.transfer");

class Link extends BaseClass {

    /**
     * Creates a new Link.
     * @param {*} link The underlying Link object.
     * @param {import('./class.world.sector.js')} sector The sector the Link is in.
     */
    constructor(link, sector) {
        super(); // possible memory root "Links."+sector.getName()+""+Link.pos.x+""+Link.pos.y
        this._link = link;
        this._sector = sector;
    }

    run() {
        let ais = [new AITransfer(this, this._sector)];
        for (let ai of ais) {
            if (ai.shouldExecute()) {
                ai.run();
                break;
            }
        }
    }
    
    /**
     * @returns {{x: number, y: number, roomName: string}} The position in the room.
     */
    getPosition() {
        return this._link.pos;
    }

    /**
     * Transfers energy to the target link.
     * @param {Link} target The target link object.
     */
    transferEnergy(target) {
        return this._link.transferEnergy(target._link);
    }

    /**
     * @returns {boolean} If the link is empty.
     */
    isEmpty() {
        return this._link.energy == 0;
    }

    /**
     * @returns {boolean} If the link is full.
     */
    isFull() {
        return this._link.energy == this._link.energyCapacity;
    }

    /**
     * @returns {number} The link fill percentage.
     */
    getFill() {
        return this._link.energy*100/this._link.energyCapacity
    }

    /**
     * @returns {string} The link id.
     */
    getId() {
        return this._link.id;
    }
}

module.exports = Link;