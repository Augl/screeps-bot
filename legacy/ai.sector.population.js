let AIBase = require("./ai.base");
let Constants = require("./util.constants");

/** 
 * AI module responsible for calculating the number and types of Augls required in a sector.
 */
class AISectorPopulation extends AIBase {


    /**
     * Creates a new AISectorPopulation module.
     * @param {import('./class.world.sector.js')} sector The sector the AI should run on.
     */
    constructor(sector) {
        super();
        this._sector = sector;
    }

    shouldExecute() {
        return (Game.time % 20 == 3);
    }

    run() {
        // check if the sector has been claimed
        if (!this._sector.getController() || !this._sector.getController().my) {
            // we need one claimer to claim the controller
            let target = this._sector.getEmpire().getClosestSector(this._sector.getName());
            if (this._sector.getAuglsWithJob(Constants.Jobs.CLAIMER) == 0) {
                target.addToSpawnQueue(Constants.Jobs.CLAIMER, {}, 3, this._sector.getName());
            }
            return;
        }
        if (!this._sector.getSpawns() || this._sector.getSpawns().length == 0) {
            // we need as many harvesters as there are sources.
            let target = this._sector.getEmpire().getClosestSector(this._sector.getName());
            if (this._sector.getAuglsWithJob(Constants.Jobs.BUILDER).length < this._sector.getNumberOfSources()*2) {
                target.addToSpawnQueue(Constants.Jobs.BUILDER, {}, 3, this._sector.getName());
            }
            return;
        }
        // if the sector is claimed calculate the required population.
        let level = 0;
        for (let i in Constants.Levels) {
            if (Constants.Levels[i] <= this._sector.getEnergyCapacity()) {
                level = i;
            }
        }        
        this.checkHarvesterPopulation(level);
        this.checkCarrierPopulation(level);
        this.checkDistributerPopulation(level);
        this.checkUpgraderPopulation(level);
        this.checkBuilderPopulation(level);
        let standard = 2;
        if (level > 2) {
            standard = 1;
        }
        if (this._sector.getAuglsWithJob(Constants.Jobs.REPAIRMAN).length < 1) {
            this._sector.addToSpawnQueue(Constants.Jobs.REPAIRMAN, {}, this._getEnergyDependentLevel(level), this._sector.getName());
        }
        if (this._sector.getAuglsWithJob(Constants.Jobs.REPAIRMAN_DEFENSE).length < 1) {
            this._sector.addToSpawnQueue(Constants.Jobs.REPAIRMAN_DEFENSE, {}, this._getEnergyDependentLevel(level), this._sector.getName());
        }
    }

    checkBuilderPopulation(level) {
        // TODO: Maybe move to sector object
        let site = this._sector.getConstructionSites().find(c => c.structureType != STRUCTURE_WALL && c.structureType != STRUCTURE_RAMPART);
        if (site) {
            let max = level > 2 ? 1 : 2;
            if (this._sector.getAuglsWithJob(Constants.Jobs.BUILDER).length < max) {
                this._sector.addToSpawnQueue(Constants.Jobs.BUILDER, {}, this._getEnergyDependentLevel(level), this._sector.getName());
            }
        }
    }

    checkUpgraderPopulation(possibleLevel) {
        let max = 1;
        let centreStores = this._sector.getContainersInCentre();
        if (this._sector.getStorage()) {
            centreStores.push(this._sector.getStorage());
        }
        let totalCapacity = centreStores.reduce((acc, store) => acc + store.store.getCapacity(), 0);
        let usedCapacity = centreStores.reduce((acc, store) => acc + store.store.getUsedCapacity("energy"), 0);
        if (totalCapacity > 0){
            let percentage = usedCapacity*100/totalCapacity;
            if (percentage > 99) {
                max = this._sector.getAuglsWithJob(Constants.Jobs.UPGRADER).length + 1;
            }
            else if (percentage > 95) {
                max = 3;
            }
            else if (percentage > 90) {
                max = 2;
            }
        }
        if (this._sector.getAuglsWithJob(Constants.Jobs.UPGRADER).length < max) {
            // choose a upgrader level depending on the available energy in the storage        
            this._sector.addToSpawnQueue(Constants.Jobs.UPGRADER, {}, this._getEnergyDependentLevel(possibleLevel), this._sector.getName());
        }        
    }

    checkCarrierPopulation(possibleLevel) {
        let centre = this._sector.getContainersInCentre().length;
        if (centre == 0) {
            return;
        }
        let factor = 2;
        if (possibleLevel > 2) {
            factor = 1;
        }
        let sources = this._sector.getContainersAtSources().length;
        if (this._sector.getAuglsWithJob(Constants.Jobs.CARRIER).length < sources*factor) {
            this._sector.addToSpawnQueue(Constants.Jobs.CARRIER, {}, possibleLevel, this._sector.getName());
        }
    }

    checkDistributerPopulation(possibleLevel) {
        let centre = this._sector.getContainersInCentre().length;
        if (centre == 0) {
            return;
        }
        if (this._sector.getAuglsWithJob(Constants.Jobs.DISTRIBUTER).length < 1) {
            this._sector.addToSpawnQueue(Constants.Jobs.DISTRIBUTER, {}, possibleLevel, this._sector.getName());
        }
        if (this._sector.getAuglsWithJob(Constants.Jobs.DISTRIBUTER).length == 1) {
            let distributer = this._sector.getAuglsWithJob(Constants.Jobs.DISTRIBUTER)[0];
            // if that guy is about to die soon we already spawn a new one
            if (distributer.getTicksToLive() < 150) {
                this._sector.addToSpawnQueue(Constants.Jobs.DISTRIBUTER, {}, possibleLevel, this._sector.getName());
            }
        }
    }

    checkHarvesterPopulation(possibleLevel) {
        let sources = this._sector.getSources();
        let map = {};
        // map harvesters to sources
        let harvesters = this._sector.getAuglsWithJob(Constants.Jobs.HARVESTER);
        for (let source of sources) {
            if (possibleLevel > 3) {
                // set one augl per source
                map[source.id] = 1;
            }
            else if (possibleLevel == 3) {
                // set at most 2 augls
                map[source.id] = Math.min(2, source.workspaces.length);
            }
            else {
                // set max amount of augls
                map[source.id] = source.workspaces.length;
            }
            
        }
        for (let augl of harvesters) {
            let sourceId = augl.recall("sourceId");
            map[sourceId]--;
        }
        for (let id in map) {
            if (map[id] > 0) {
                this._sector.addToSpawnQueue(Constants.Jobs.HARVESTER, {sourceId: id}, possibleLevel, this._sector.getName());
                return;
            }
        }
    }

    _getEnergyDependentLevel(possibleLevel) {
        let storage = this._sector.getStorage();
        if (!storage) {
            return Math.min(3, possibleLevel);
        }
        let energy = storage.store[RESOURCE_ENERGY];
        if (energy < 400000) {
            return Math.min(3, possibleLevel);
        }
        else if (energy < 650000) {
            return Math.min(4, possibleLevel);
        }
        else if (energy < 800000) {
            return Math.min(5, possibleLevel);
        }
        else if (energy < 900000) {
            return Math.min(6, possibleLevel);
        }
        else if (energy < 950000) {
            return Math.min(7, possibleLevel);
        }
        else {
            return Math.min(8, possibleLevel);
        }
    }

}

module.exports = AISectorPopulation;