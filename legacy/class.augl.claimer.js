let AuglBase = require("./class.augl.base");
let AIClaim = require("./ai.augl.claim");
let AIFlee = require("./ai.augl.flee");

class Claimer extends AuglBase {

    constructor(creep, sector) {
        super(creep, sector);
        this._augl = creep;
        this.addAI(new AIFlee(this, sector));
        this.addAI(new AIClaim(this, sector));
    }
}

module.exports = Claimer;