let AIBase = require("./ai.base");

/**
 * AIAuglGoHome makes the augl return to it's home sector.
 */
class AIAuglGoHome extends AIBase {

    /**
     * Creates a new AIAuglGoHome module.
     * @param {import('./class.augl.base')} augl The augl that needs to go home.
     * @param {import('./class.world.sector.js')} sector The home sector.
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        return this._augl.getPosition().roomName != this._sector.getName();
    }

    run() {
        this._augl.moveToTarget(new RoomPosition(25, 25, this._sector.getName()));
    }
}

module.exports = AIAuglGoHome;