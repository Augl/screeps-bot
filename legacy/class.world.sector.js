let WorldBase = require("./class.world.base");
let Cache = require("./util.cache");
let Constants = require("./util.constants");

let AISectorPopulation = require("./ai.sector.population");
let AISectorSpawn = require("./ai.sector.spawn");
let AISectorBuild = require("./ai.sector.build");
let AISectorExpand = require("./ai.sector.expand");
let AuglBase = require("./class.augl.base");

/**
 * A Sector is a main room with a spawn and a claimed controller.
 */
class Sector extends WorldBase {

    /**
     * Creates a new sector.
     * @param {*} room The room object.
     * @param {*[]} creeps An array of creep objects.
     * @param {Object.<string, *[]>} buildings A map containing array of buildings.
     * @param {*[]} constructionSites An array of construction sites.
     */
    constructor(roomName, room, creeps, buildings, constructionSites, empire) {
        super(roomName, room, creeps, buildings, constructionSites, empire);
        this._room = room;
        this._roomName = roomName;
    }

    run() {
        if (!this.isActive()) return;
        super.run();
        // run sector AI
        let ais = [new AISectorPopulation(this), new AISectorSpawn(this), new AISectorExpand(this), new AISectorBuild(this)];
        for (let ai of ais) {
            if (ai.shouldExecute()) {
                ai.run();
                break;
            }
        }
        // run towers
        for (let tower of this.getTowers()) {
            tower.run();
        }
        // run links
        for (let link of this.getLinks()) {
            link.run();
        }
    }

    /**
     * Adds the given augl to the spawn queue.
     * @param {number} job One of the Constants.Jobs.* values
     * @param {object} initialMemory Object holding the memory of the augl.
     * @param {number} level The energy level of the augl.
     * @param {string} home Name of the sector in control.
     * @returns {boolean} True if the augl was successfully added to the queue, false otherwise.
     */
    addToSpawnQueue(job, initialMemory, level, home) {
        let queue = this.recall("sq") || {};
        if (!queue[job]) {
            queue[job] = {memory: initialMemory, level: level, home: home};
            this.remember("sq", queue);
            return true
        }
        return false
    }

    /**
     * Removes the spawn queue for the given job.
     * @param {number} job One of the Constants.Jobs.* values.
     */
    removeFromSpawnQueue(job) {
        let queue = this.recall("sq");
        delete queue[job];
        this.remember("sq", queue);
    }

    /**
     * Returns the object holding the next augl to be spawned per job.
     */
    getSpawnQueue() {
        return this.recall("sq") || {};
    }
}

module.exports = Sector;