let AIBase = require("./ai.base");
let Constants = require("./util.constants");

class AIColonyDefend extends AIBase {

    /**
     * Creates a new AIColonyDefend.
     * @param {import('./class.world.colony')} colony The colony to run in.
     */
    constructor(colony) {
        super();
        this._colony = colony;
    }

    shouldExecute() {
        let hostiles = this._colony.getEnemies();
        return hostiles.length > 0;
    }

    run() {
        // spawn a warrior
        if (this._colony.getAuglsWithJob(Constants.Jobs.WARRIOR).length < 1) {
            this._colony.getNeighbourSector().addToSpawnQueue(Constants.Jobs.WARRIOR, {}, 3, this._colony.getName());
        }        
    }
}

module.exports = AIColonyDefend;