let AIBase = require("./ai.base");
let Constants = require("./util.constants");
let Cache = require("./util.cache");

class AIAuglCarry extends AIBase {

    /**
     * Creates a new AIAuglCarry module.
     * @param {import('./class.augl.base')} augl 
     * @param {import('./class.world.sector.js')} sector 
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        // find source
        let sourceId = this._augl.recall("carrySrc");
        if (!sourceId) {
            let containers = this._sector.getContainersAtSources().filter(c => _.sum(c.store) > this._augl._creep.carryCapacity);
            containers = containers.sort((a, b) => {
                if (_.sum(a.store) > _.sum(b.store)) {
                    return -1;
                }
                else if (_.sum(a.store) < _.sum(b.store)) {
                    return 1;
                }
                return 0;
            });
            this._source = containers[0];
        }
        else {
            this._source = Cache.getGameObject(sourceId, 10);
        }
        if (!this._source) {
            return false;
        }
        // find destination
        this._target = this._sector.getSpawns().find(s => s.energy < s.energyCapacity);
        if (!this._target) {
            this._target = this._sector.getExtensions().find(s => s.energy < s.energyCapacity);
        }
        if (!this._target) {
            this._target = this._sector.getContainersInCentre().find(c => c && _.sum(c.store) < c.storeCapacity);
        }
        if (!this._target) {
            this._target = this._sector.getStorage();
        }
        if (!this._target) {
            return false;
        }
        this._augl.remember("carrySrc", this._source.id);
        return true;
    }

    run() {
        let isDone = true;
        let task = this._augl.recall("task");
        if (task == Constants.Tasks.HARVEST) {
            this._augl.withdrawEnergy(this._source);
            if (this._augl.isFull() || this._source.store[RESOURCE_ENERGY] == 0) {
                this._augl.remember("task", Constants.Tasks.DEPOSIT);
            }
        }
        else if (task == Constants.Tasks.DEPOSIT) {
            this._augl.transferEnergy(this._target);
            if (this._augl.isEmpty()) {
                this._augl.forget("carrySrc");
                this._augl.remember("task", Constants.Tasks.HARVEST);
            }
            isDone = false;
        }
        else {
            this._augl.remember("task", Constants.Tasks.HARVEST);
        }
        return isDone;
    }
}

module.exports = AIAuglCarry;