let AIBase = require("./ai.base");
let Constants = require("./util.constants");
let Names = require("./util.names");

/**
 * AISectorSpawn handles spawning augls in the sectors.
 */
class AISectorSpawn extends AIBase {

    /**
     * Creates a new AISectorPopulation module.
     * @param {import('./class.world.sector.js')} sector The sector the AI should run on.
     */
    constructor(sector) {
        super();
        this._sector = sector;
    }

    shouldExecute() {
        // check if there are spawns available
        this.freeSpawn = this._sector.getSpawns().find(s => !s.spawning);
        if (!this.freeSpawn) {
            return false;
        }
        // check if there are augls waiting to be spawned
        let queue = this._sector.getSpawnQueue();
        for (let job in queue) {
            if (queue[job]) {
                this.toBeSpawned = queue[job];
                this.job = Number(job);
                break;
            }
        }
        if (!this.toBeSpawned) {
            return false;
        }
        // check if the energy is full
        if (this._sector.getAvailableEnergy() < Constants.Levels[this.toBeSpawned.level-1]) {
            let cooldown = this._sector.recall("spawn-cooldown") || 60;
            this._sector.remember("spawn-cooldown", cooldown-1);
            if (cooldown > 1) {
                return false;
            }
            else {
                // waited long enough, spawn with highest possible level
                this._sector.forget("spawn-cooldown");
                console.log("Spawn cooldown over!!");
                // find highest possible level
                let level = 0;
                for (let i in Constants.Levels) {
                    if (Constants.Levels[i] <= this._sector.getAvailableEnergy()) {
                        level = i;
                    }
                }
                if (level == 0) {
                    return false;
                }
                this.toBeSpawned.level = level;
            }
        }
        return true;
    }

    run() {
        this._sector.forget("spawn-cooldown");
        let memory = this.toBeSpawned.memory || {};
        memory.home = this.toBeSpawned.home;
        memory.job = this.job;
        let result;
        if (this.job == Constants.Jobs.HARVESTER) {
            result = this.spawnHarvester(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.UPGRADER) {
            result = this.spawnUpgrader(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.BUILDER) {
            result = this.spawnBuilder(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.CARRIER) {
            result = this.spawnCarrier(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.INTERCARRIER) {
            result = this.spawnInterCarrier(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.REPAIRMAN) {
            result = this.spawnRepairman(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.REPAIRMAN_DEFENSE) {
            result = this.spawnRepairmanDefense(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.DISTRIBUTER) {
            result = this.spawnDistributer(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.EXPLORER) {
            result = this.spawnExplorer(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.RESERVER) {
            result = this.spawnReserver(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.CLAIMER) {
            result = this.spawnClaimer(this.toBeSpawned.level, memory);
        }
        else if (this.job == Constants.Jobs.WARRIOR) {
            result = this.spawnWarrior(this.toBeSpawned.level, memory);
        }
        if (result === OK) {
            this._sector.removeFromSpawnQueue(this.job);
        }
    }

    /**
     * Spawns a carrier with an appropriate body for the given level.
     * @param {number} level The level of the carrier.
     * @param {*} memory The initial memory.
     */
    spawnCarrier(level, memory) {
        if (level == 1) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, CARRY, CARRY, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 2) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level >= 3) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, WORK], Names.getNextName(), {memory: memory});
        }
    }

    /**
     * Spawns an intercarrier with an appropriate body for the given level.
     * @param {number} level The level of the intercarrier.
     * @param {*} memory The initial memory.
     */
    spawnInterCarrier(level, memory) {
        if (level >= 4) {
            return this.freeSpawn.spawnCreep(
                [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, WORK], 
                Names.getNextName(), {memory: memory});
        }
    }

    /**
     * Spawns a harvester with an appropriate body for the given level.
     * @param {number} level The level of the harvester.
     * @param {*} memory The initial memory.
     */
    spawnHarvester(level, memory) {
        if (level == 1) {
            return this.freeSpawn.spawnCreep([MOVE, CARRY, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 2) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, CARRY, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 3) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, CARRY, WORK, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level >= 4) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, WORK, WORK, WORK, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
    }

    /**
     * Spawns an upgrader with an appropriate body for the given level.
     * @param {number} level The level of the upgrader.
     * @param {*} memory The initial memory.
     */
    spawnUpgrader(level, memory) {
        if (level == 1) {
            return this.freeSpawn.spawnCreep([MOVE, CARRY, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 2) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 3) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 4) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level >= 5) {
            return this.freeSpawn.spawnCreep([
                MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
                CARRY, CARRY, CARRY, CARRY, CARRY,
                WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK
            ], Names.getNextName(), {memory: memory});
        }
    }

    /**
     * Spawns a builder with an appropriate body for the given level.
     * @param {number} level The level of the builder.
     * @param {*} memory The initial memory.
     */
    spawnBuilder(level, memory) {
        if (level == 1) {
            return this.freeSpawn.spawnCreep([MOVE, CARRY, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 2) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 3) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 4) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level >= 5) {
            return this.freeSpawn.spawnCreep([
                MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
                CARRY, CARRY, CARRY, CARRY, CARRY,
                WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK
            ], Names.getNextName(), {memory: memory});
        }
    }

    /**
     * Spawns a repairman with an appropriate body for the given level.
     * @param {number} level The level of the repairman.
     * @param {*} memory The initial memory.
     */
    spawnRepairman(level, memory) {
        if (level == 1) {
            return this.freeSpawn.spawnCreep([MOVE, CARRY, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 2) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 3) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 4) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level >= 5) {
            return this.freeSpawn.spawnCreep([
                MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
                CARRY, CARRY, CARRY, CARRY, CARRY,
                WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK
            ], Names.getNextName(), {memory: memory});
        }
    }

    /**
     * Spawns a repairman for defense structures with an appropriate body for the given level.
     * @param {number} level The level of the repairman.
     * @param {*} memory The initial memory.
     */
    spawnRepairmanDefense(level, memory) {
        if (level == 1) {
            return this.freeSpawn.spawnCreep([MOVE, CARRY, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 2) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 3) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 4) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level >= 5) {
            return this.freeSpawn.spawnCreep([
                MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
                CARRY, CARRY, CARRY, CARRY, CARRY,
                WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK
            ], Names.getNextName(), {memory: memory});
        }
    }

    /**
     * Spawns a distributer with an appropriate body for the given level.
     * @param {number} level The level of the distributer.
     * @param {*} memory The initial memory.
     */
    spawnDistributer(level, memory) {
        if (level == 1) {
            return this.freeSpawn.spawnCreep([MOVE, CARRY, CARRY, CARRY, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 2) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level == 3) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, WORK], Names.getNextName(), {memory: memory});
        }
        else if (level >= 5) {
            return this.freeSpawn.spawnCreep([
                MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
                CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY
            ], Names.getNextName(), {memory: memory});
        }
        
        else if (level >= 5) {
            return this.freeSpawn.spawnCreep([
                MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
                CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY
            ], Names.getNextName(), {memory: memory});
        }
    }

    /**
     * Spawns an explorer with an appropriate body for the given level.
     * @param {number} level The level of the explorer.
     * @param {*} memory The initial memory.
     */
    spawnExplorer(level, memory) {
        if (level >= 1) {
            return this.freeSpawn.spawnCreep([MOVE], Names.getNextName(), {memory: memory});
        }
    }
    

    /**
     * Spawns a reserver with an appropriate body for the given level.
     * @param {number} level The level of the reserver.
     * @param {*} memory The initial memory.
     */
    spawnReserver(level, memory) {
        if (level >= 3) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, CLAIM], Names.getNextName(), {memory: memory});
        }
    }
    

    /**
     * Spawns a claimer with an appropriate body for the given level.
     * @param {number} level The level of the claimer.
     * @param {*} memory The initial memory.
     */
    spawnClaimer(level, memory) {
        if (level >= 3) {
            return this.freeSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, CLAIM], Names.getNextName(), {memory: memory});
        }
    }
    

    /**
     * Spawns a warrior with an appropriate body for the given level.
     * @param {number} level The level of the warrior.
     * @param {*} memory The initial memory.
     */
    spawnWarrior(level, memory) {
        if (level >= 3) {
            return this.freeSpawn.spawnCreep([TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH,
                ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK,
                MOVE, MOVE, MOVE, MOVE
            ], Names.getNextName(), {memory: memory});
        }
    }
}

module.exports = AISectorSpawn;