let AIBase = require("./ai.base");

class AITowerDefend extends AIBase {

    /**
     * Creates a new AITowerDefend.
     * @param {import('./class.building.tower.js')} tower The tower to run.
     * @param {import('./class.world.sector.js')} sector The sector to run in.
     */
    constructor(tower, sector) {
        super();
        this._tower = tower;
        this._sector = sector;
    }

    shouldExecute() {
        let hostiles = this._sector.getEnemies();
        this._hostile = hostiles.find(h => {
            let body = h.body.find(p => p.type == HEAL);
            return body != null
        });
        if (!this._hostile) {
            this._hostile = hostiles[0];
        }
        if (this._hostile) {
            return true;
        }
        return false;
    }

    run() {
        this._tower.attack(this._hostile);
    }
}

module.exports = AITowerDefend;