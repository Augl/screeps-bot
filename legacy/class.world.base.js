let BaseClass = require("./class.base");

let Cache = require("./util.cache");
let Constants = require("./util.constants");
let Helper = require("./util.helper");

let Harvester = require("./class.augl.harvester");
let Upgrader = require("./class.augl.upgrader");
let Builder = require("./class.augl.builder");
let Carrier = require("./class.augl.carrier");
let InterCarrier = require("./class.augl.intercarrier");
let RepairMan = require("./class.augl.repairman");
let RepairManDefense = require("./class.augl.repairman.defense");
let Distributer = require("./class.augl.distributer");
let Explorer = require("./class.augl.explorer");
let Reserver = require("./class.augl.reserver");
let Claimer = require("./class.augl.claimer");
let Warrior = require("./class.augl.warrior");

let Tower = require("./class.building.tower");
let Link = require("./class.building.link");

/**
 * The base class for all sectors, colonies, areas etc.
 */
class WorldBase extends BaseClass {

    constructor(roomName, room, creeps, buildings, constructionSites, empire) {
        super("rooms."+roomName);
        this._room = room;
        this._roomName = roomName;
        this._augls = this.initialiseAugls(creeps);
        this._empire = empire;
        this._buildings = this.initialiseBuildings(buildings) || {};
        this._constructionSites = constructionSites;
    }

    run() {        
        // run augl's
        for (let job in this._augls) {
            for (let augl of this._augls[job]) {
                augl.run();
            }
        }
    }

    /**
     * Initialises all buildings that can be initialised.
     * @param {Object.<string, *[]>} buildings A map containing array of buildings.
     * @returns {Object.<string, *[]>} A map containing array of buildings.
     */
    initialiseBuildings(buildings) {
        for (let key in buildings) {
            if (key === STRUCTURE_TOWER) {
                // initialise towers
                for (let i = 0; i < buildings[key].length; i++) {
                    buildings[key][i] = new Tower(buildings[key][i], this);
                }
            }
            else if (key === STRUCTURE_LINK) {
                // initialise links
                for (let i = 0; i < buildings[key].length; i++) {
                    buildings[key][i] = new Link(buildings[key][i], this);
                }
            }
        }
        return buildings;
    }

    /**
     * Initialises all augls by iterating over the creeps array.
     * @returns {Object.<number, AuglBase[]>} An object mapping all jobs to an array of augls.
     */
    initialiseAugls(creeps) {
        let map = {};
        creeps = creeps || [];
        for (let c of creeps) {
            let job = c.memory.job;
            if (!job) {
                console.log("Error: Creep has no job!");
                continue;
            }
            if (!map[job]) {
                map[job] = [];
            }
            switch (job) {
                case Constants.Jobs.HARVESTER:
                    map[job].push(new Harvester(c, this));
                    break;
                case Constants.Jobs.UPGRADER:
                    map[job].push(new Upgrader(c, this));
                    break;
                case Constants.Jobs.BUILDER:
                    map[job].push(new Builder(c, this));
                    break;
                case Constants.Jobs.CARRIER:
                    map[job].push(new Carrier(c, this));
                    break;
                case Constants.Jobs.REPAIRMAN:
                    map[job].push(new RepairMan(c, this));
                    break;
                case Constants.Jobs.REPAIRMAN_DEFENSE:
                    map[job].push(new RepairManDefense(c, this));
                    break;
                case Constants.Jobs.DISTRIBUTER:
                    map[job].push(new Distributer(c, this));
                    break;
                case Constants.Jobs.EXPLORER:
                    map[job].push(new Explorer(c, this));
                    break;
                case Constants.Jobs.RESERVER:
                    map[job].push(new Reserver(c, this));
                    break;
                case Constants.Jobs.INTERCARRIER:
                    map[job].push(new InterCarrier(c, this));
                    break;
                case Constants.Jobs.CLAIMER:
                    map[job].push(new Claimer(c, this));
                    break;
                case Constants.Jobs.WARRIOR:
                    map[job].push(new Warrior(c, this));
                    break;
            }
        }
        return map;
    }    

    /**
     * @returns {boolean} True if the colony is active, false otherwise.
     */
    isActive() {
        return this.recall("inactive") !== true;
    }

    /**
     * Activates the colony.
     */
    setActive() {
        this.remember("inactive", false);
    }

    /**
     * @returns {import('./class.empire.js')} the empire.
     */
    getEmpire() {
        return this._empire;
    }

    /**
     * Gets the room type. See Constants.RoomTypes.*
     * @returns {number} The room type.
     */
    getType() {
        return this.recall("type");
    }

    /**
     * Returns the names of the neighbour rooms.
     * @returns {string[]} The names of the neighbours.
     */
    getNeighbours() {
        return this.recall("neighbours");
    }

    /**
     * @returns {boolean} If the room is visible.
     */
    isVisible() {
        return this._room != null;
    }

    /**
     * Returns all augls.
     * @returns {import('./class.augl.base')[]} Array containing all augls.
     */
    getAllAugls() {
        let result = [];
        for (let key in this._augls) {
            result = result.concat(this._augls[key]);
        }
        return result;
    }

    /**
     * Returns all the augls in the sector with the given job.
     * @param {number} job One of the Jobs constants.
     * @returns {import('./class.augl.base')[]} Array of augls with the job.
     */
    getAuglsWithJob(job) {
        return this._augls[job] || [];
    }

    /**
     * @returns {{1: string, 3: string, 5: string, 7: string}} Rooms the exits go to, 1 = top, 3 = right, 5 = bottom, 7 = left
     */
    describeExits() {
        return Game.map.describeExits(this._roomName);
    }

    /**
     * @returns {*} The room controller.
     */
    getController() {
        if (!this._room) return null;
        return this._room.controller;
    }

    /**
     * @returns {number} The current controller level.
     */
    getLevel() {
        if (!this._room) return -1;
        return this._room.controller.level;
    }

    /**
     * @returns {string} The room name.
     */
    getName() {
        return this._roomName;
    }

    /**
     * Returns an array of maps of source id's to the available workspaces in the sector.
     * @see getNumberOfSources for results even if the room is inactive.
     * @returns {{id: string, workspaces: {x: number, y:number}[]}[]} Array of maps of source id's to their workspaces, empty array if the room is not visible.
     */
    getSources() {
        let key = "room-get-sources-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let sources = this.find(FIND_SOURCES, null, 100);
            let result = [];
            for (var s of sources) {
                // look at area around the source
                let map = this.lookAtArea(s.pos.y-1, s.pos.x-1, s.pos.y+1, s.pos.x+1, true, 100);
                let workspaces = [];
                for (var m of map) {
                    if (m.type === "terrain" && m.terrain !== "wall") {
                        workspaces.push({x: m.x, y: m.y});
                    }
                }
                result.push({id: s.id, workspaces: workspaces});
            }
            return result;
        }, this, [], 100);
        
    }

    /**
     * @returns {?number} The number of sources.
     */
    getNumberOfSources() {
        return this.recall("sources");
    }

    /**
     * Finds all objects of the specified type in the room.
     * @param {number} type One of the FIND_* constants.
     * @param {object} options An object with an additional filter property.
     * @param {number} maxAge Max age of the cache entry to be a hit.
     * @returns {*[]} Array of found objects, empty if the room is not visible.
     */
    find(type, options, maxAge) {
        if (!this._room) return [];
        let key = "room-find-"+type+"-"+this._roomName;
        return Cache.storeFunction(key, this._room.find, this._room, [type, options], maxAge);
    }

    /**
     * Returns the list of objects at the specified room area.
     * @param {number} top The top Y boundary of the area.
     * @param {number} left The left X boundary of the area.
     * @param {number} bottom The bottom Y boundary of the area.
     * @param {number} right The right X boundary of the area.
     * @param {boolean} asArray Set to true if you want to get the result as a plain array.
     * @param {number} maxAge Max age of the cache entry to be a hit.
     */
    lookAtArea(top, left, bottom, right, asArray, maxAge) {
        if (!this._room) return [];
        asArray = asArray;
        let key = "room-look-at-area-"+top+"-"+left+"-"+bottom+"-"+right+"-"+asArray+"-"+this._roomName;
        return Cache.storeFunction(key, this._room.lookAtArea, this._room, [top, left, bottom, right, asArray], maxAge);
    }

    /**
     * Returns the amount of energy in the spawn and all extensions.
     * @returns {number} Amount of available energy.
     */
    getAvailableEnergy() {
        if (!this._room) return 0;
        return this._room.energyAvailable;
    }

    /**
     * Returns the available energy capacity.
     * @returns {number} Available energy capacity.
     */
    getEnergyCapacity() {
        if (!this._room) return 0;
        return this._room.energyCapacityAvailable;
    }

    /**
     * @param {number} maxAge The max age for the cache.
     * @returns {*[]} Array of container structures.
     */
    getContainers(maxAge) {
        let key = "get-containers-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let structures = this.find(FIND_STRUCTURES, {}, 5).filter(s => s.structureType === STRUCTURE_CONTAINER);
            return structures;
        }, this, {}, maxAge || 5);
    }

    /**
     * Returns the container next to the given source.
     * @param {string} sourceId The id of the source.
     * @returns {any} The container object or undefined.
     */
    getContainerForSource(sourceId) {
        let key = "container-for-source-"+sourceId;
        return Cache.storeFunction(key, () => {
            let source = Cache.getGameObject(sourceId, 100);
            let container = this.getContainers().find(c => Helper.distance(source.pos, c.pos) < 3);
            return container;
        }, this, [], 5);        
    }

    /**
     * Returns the containers not in the centre.
     * @returns {*[]} Containers belonging to sources.
     */
    getContainersAtSources() {
        let key = "get-contaienrs-at-sources-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let sources = this.find(FIND_SOURCES, null, 100);
            let containers = this.getContainers(20);
            let result = [];
            for (let c of containers) {
                for (let s of sources) {
                    if (Helper.distance(s.pos, c.pos) < 3) {
                        result.push(c);
                        break;
                    }
                }
            }
            return result;}, this, {}, 20);
    }

    /**
     * Returns the containers in the centre.
     * @returns {*[]} Containers in the centre.
     */
    getContainersInCentre() {
        let key = "get-contaienrs-in-centre-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let sources = this.find(FIND_SOURCES, null, 100);
            let containers = this.getContainers(20);
            let result = [];
            for (let c of containers) {
                let inCentre = true;
                for (let s of sources) {
                    if (Helper.distance(s.pos, c.pos) < 3) {
                        inCentre = false;
                        break;
                    }
                }
                if (inCentre) {
                    result.push(c);
                }
            }
            return result;
        }, this, {}, 20);
    }

    /**
     * @returns {import('./class.building.link')[]} Array of Links.
     */
    getLinks() {
        return this._buildings[STRUCTURE_LINK] || [];
    }

    /**
     * Returns the link next to the given source.
     * @param {string} sourceId The id of the source.
     * @returns {import('./class.building.link')} The link object or undefined.
     */
    getLinkForSource(sourceId) {
        let key = "link-for-source-"+sourceId;
        return Cache.storeFunction(key, () => {
            let source = Cache.getGameObject(sourceId, 100);
            let link = this.getLinks().find(l => Helper.distance(source.pos, l.getPosition()) < 3);
            return link;
        }, this, [], 5);
    }

    /**
     * Returns the link next to the controller.
     * @returns {import('./class.building.link')} The link object or undefined.
     */
    getLinkForController() {
        let key = "link-for-controller-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let link = this.getLinks().find(l => Helper.distance(this.getController().pos, l.getPosition()) < 3);
            return link;
        }, this, [], 5);
    }

    /**
     * Returns the links not in the centre.
     * @returns {import('./class.building.link')[]} Links belonging to sources.
     */
    getLinksAtSources() {
        let key = "get-contaienrs-at-sources-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let sources = this.find(FIND_SOURCES, null, 100);
            let links = this.getLinks();
            let result = [];
            for (let l of links) {
                for (let s of sources) {
                    if (Helper.distance(s.pos, l.getPosition()) < 3) {
                        result.push(l);
                        break;
                    }
                }
            }
            return result;
        }, this, {}, 20);
    }

    /**
     * Returns the links in the centre.
     * @returns {import('./class.building.link')[]} Links in the centre.
     */
    getLinksInCentre() {
        let key = "get-links-in-centre-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let sources = this.find(FIND_SOURCES, null, 100);
            let links = this.getLinks();
            let result = [];
            for (let l of links) {
                let inCentre = true;
                for (let s of sources) {
                    if (Helper.distance(s.pos, l.getPosition()) < 3) {
                        inCentre = false;
                        break;
                    }
                }
                if (inCentre) {
                    result.push(l);
                }
            }
            return result;
        }, this, {}, 2);
    }

    /**
     * @returns {*[]} The construction sites in the sector.
     */
    getConstructionSites() {
        return this._constructionSites || [];
    }

    /**
     * Returns all spawns in the sector.
     * @returns {*[]} Array of spawn objects.
     */
    getSpawns() {
        return this._buildings[STRUCTURE_SPAWN] || [];
    }

    /**
     * Returns all extensions in the sector.
     * @returns {*[]} Array of extensions.
     */
    getExtensions() {
        return this._buildings[STRUCTURE_EXTENSION] || [];
    }

    /**
     * @returns {import('./class.building.tower.js')[]} Array of towers.
     */
    getTowers() {
        return this._buildings[STRUCTURE_TOWER] || [];
    }

    /**
     * @returns {*} The storage in the sector.
     */
    getStorage() {
        if (!this._room) return null;
        return this._room.storage;
    }

    /**
     * @returns {*[]} Array of rampart structures.
     */
    getRamparts() {
        return this._buildings[STRUCTURE_RAMPART] || [];
    }

    /**
     * Returns all owned buildings. Containers and Roads are NOT included!
     * @returns {*[]} Array containing all buildings.
     */
    getAllBuildings() {
        let result = [];
        for (let key in this._buildings) {
            result = result.concat(this._buildings[key]);
        }
        return result;
    }

    /**
     * Gets all roads
     * @param {number} [maxAge=5] The max age for the cache.
     */
    getRoads(maxAge = 5) {
        let key = "get-roads-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let structures = this.find(FIND_STRUCTURES, {}, 5).filter(s => s.structureType === STRUCTURE_ROAD);
            return structures;
        }, this, {}, maxAge);
    }

    /**
     * Returns a map of all roads in the room.
     * @param {number} [maxAge=5] The max age for the cache.
     * @returns {Object.<number, Object.<number, *>>} The map with road structures.
     */
    getRoadMap(maxAge = 5) {
        let key = "get-raods-map-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let map = {};
            let structures = this.find(FIND_STRUCTURES, {}, 5);
            for (let s of structures) {
                if (s.structureType === STRUCTURE_ROAD) {
                    if (!map[s.pos.x]) {
                        map[s.pos.x] = {};
                    }
                    map[s.pos.x][s.pos.y] = s;
                }
            }
            return map;
        }, this, {}, maxAge);
    }

    /**
     * Gets all walls
     * @param {number} maxAge The max age for the cache.
     */
    getWalls(maxAge) {
        let key = "get-walls-"+this._roomName;
        return Cache.storeFunction(key, () => {
            let structures = this.find(FIND_STRUCTURES, {}, 5).filter(s => s.structureType === STRUCTURE_WALL);
            return structures;
        }, this, {}, maxAge || 5);
    }

    /**
     * Creates a new construction site
     * @param {number} x The x coordinate.
     * @param {number} y The y coordinate.
     * @param {string} structureType One of the STRUCTURE_* constants.
     * @param {string} name The name of the structure, only works for spawns.
     */
    createConstructionSite(x, y, structureType, name="") {
        if (!this._room) return -99;
        return this._room.createConstructionSite(x, y, structureType, name);
    }

    /**
     * Adds the given building as the deconstruction target.
     * @param {{id: string}} target The target building.
     * @returns {boolean} Returns true if the building was added, false if there already is another one in the queue.
     */
    deconstructBuilding(target) {
        if (!this.recall("deconstruction")) {
            this.remember("deconstruction", target.id);
            return true;
        }
        return false;
    }

    /**
     * Returns the current deconstruction target.
     * @returns {*} The building or null.
     */
    getDeconstructionTarget() {
        let id = this.recall("deconstruction");
        if (id) {
            let building = Cache.getGameObject(id);
            if (!building) {
                this.forget("deconstruction");
            }
            return building;
        }
        return null;
    }

    /**
     * Returns the terrain at the given position.
     * @param {number} x The x coordinate.
     * @param {number} y The y coordinate.
     * @returns {number} 0 for plain, 1 for wall, 2 for swamp.
     */
    getTerrainAt(x, y) {
        if (!this._room) return -1;
        return this._room.getTerrain().get(x, y);
    }

    /**
     * Returns an arry of enemy creeps with at least on WORK, ATTACK, RANGED_ATTACK or HEAL body part.
     * @returns {*[]} The array of enemy creeps.
     */
    getEnemies() {
        let key = "get-enemies-" + this.getName();
        return Cache.storeFunction(key, () => {
            let hostiles = this.find(FIND_HOSTILE_CREEPS, {}, 2);
            hostiles = hostiles.filter(h => {
                let body = h.body.find(p => p.type == WORK || p.type == ATTACK || p.type == RANGED_ATTACK || p.type == HEAL);
                return body != undefined;
            });
            return hostiles;
        }, this, [], 1);
    }
}

module.exports = WorldBase;