let AIBase = require("./ai.base");

class AIAuglExplore extends AIBase {

    /**
     * Creates a new AIAuglExplore module.
     * @param {import('./class.augl.base')} augl 
     * @param {import('./class.world.sector.js')} sector 
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        let exits = this._sector.describeExits();
        for (let key in exits) {
            if (!this._sector.getEmpire().roomWasExplored(exits[key])) {
                this._target = exits[key];
                break;
            }
        }
        return this._target != null;
    }

    run() {
        this._augl.moveToTarget(new RoomPosition(25, 25, this._target));
    }
}

module.exports = AIAuglExplore;