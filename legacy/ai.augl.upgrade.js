let AIBase = require("./ai.base");
let Constants = require("./util.constants");

class AIAuglUpgrade extends AIBase {

    /**
     * Creates a new AIAuglUpgrade.
     * @param {import('./class.augl.base.js')} augl The augl to work with.
     * @param {import('./class.world.sector.js')} sector The sector to work on.
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        let controller = this._sector.getController();
        if (controller && controller.level < 8) {
            return true;
        }
        return false;
    }

    run() {
        let isDone = true;
        let task = this._augl.recall("task");
        if (task === Constants.Tasks.HARVEST) {
            this._augl.getEnergy(true);
            if (this._augl.isFull()) {
                this._augl.remember("task", Constants.Tasks.UPGRADE);
            }
        }
        else if (task === Constants.Tasks.UPGRADE) {
            this._augl.upgradeController(this._sector.getController());
            if (this._augl.isEmpty()) {
                this._augl.remember("task", Constants.Tasks.HARVEST);
            }
            isDone = false;
        }
        else {
            this._augl.remember("task", Constants.Tasks.HARVEST);
        }
        return isDone;
    }
}

module.exports = AIAuglUpgrade;