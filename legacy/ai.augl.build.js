let AIBase = require("./ai.base");
let Constants = require("./util.constants");

class AIAuglBuild extends AIBase {

    /**
     * Creates a new AIAuglBuild module.
     * @param {import('./class.augl.base.js')} augl The augl to work with.
     * @param {import('./class.world.sector.js')} sector The sector to work in.
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        let site = this._sector.getConstructionSites().find(c => c.structureType != STRUCTURE_WALL && c.structureType != STRUCTURE_RAMPART);
        if (site) {
            this._target = site;
            return true;
        }
        return false;
    }

    run() {
        let isDone = true;
        let task = this._augl.recall("task");
        if (task === Constants.Tasks.HARVEST) {
            this._augl.getEnergy(true);
            if (this._augl.isFull()) {
                this._augl.remember("task", Constants.Tasks.BUILD);
            }
        }
        else if (task === Constants.Tasks.BUILD) {
            this._augl.build(this._target);
            if (this._augl.isEmpty()) {
                this._augl.remember("task", Constants.Tasks.HARVEST);
            }
            isDone = false;
        }
        else {
            this._augl.remember("task", Constants.Tasks.HARVEST);
        }
        return isDone;
    }
}

module.exports = AIAuglBuild;