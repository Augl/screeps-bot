let AIBase = require("./ai.base");
let Constants = require("./util.constants");

class AIColonyExpand extends AIBase {

    /**
     * Creates a new AIColonyExpand module.
     * @param {import('./class.world.colony.js')} colony The colony to work in.
     */
    constructor(colony) {
        super();
        this._colony = colony;
    }

    shouldExecute() {
        // check if all neighbours were explored
        let exits = this._colony.describeExits();
        let allExplored = true;
        for (let key in exits) {
            if (!this._colony.getEmpire().roomWasExplored(exits[key])) {
                allExplored = false;
                break;
            }
        }
        return !allExplored;
    }

    run() {
        if (this._colony.getAuglsWithJob(Constants.Jobs.EXPLORER) == 0) {
            let parent = this._colony.getNeighbourSector();
            parent.addToSpawnQueue(Constants.Jobs.EXPLORER, {}, 1, this._colony.getName());
        }
    }
}

module.exports = AIColonyExpand;