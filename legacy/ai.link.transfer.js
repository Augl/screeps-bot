let AIBase = require("./ai.base");

class AILinkTransfer extends AIBase {

    /**
     * Creates a new AILinkTransfer.
     * @param {import('./class.building.link.js')} link The link to run.
     * @param {import('./class.world.sector.js')} sector The sector to run in.
     */
    constructor(link, sector) {
        super();
        this._link = link;
        this._sector = sector;
    }

    shouldExecute() {
        var centreId = this._sector.getLinksInCentre()[0].getId();
        var controllerId = null;
        if (this._sector.getLinkForController()) {
            controllerId = this._sector.getLinkForController().getId();
        }
        
        if (this._link.getId() == centreId || this._link.getId() == controllerId) {
            return false;
        }
        if (this._link.getFill() < 80) {
            return false;
        }
        this._target = this._sector.getLinkForController();
        if (this._target && this._target.getFill() < 80) {
            return true;
        }
        this._target = this._sector.getLinksInCentre()[0];
        if (!this._target) {
            return false;
        }
        return !this._target.isFull();
    }

    run() {
        this._link.transferEnergy(this._target);
    }
}

module.exports = AILinkTransfer;