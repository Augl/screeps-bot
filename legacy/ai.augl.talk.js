var AIBase = require("./ai.base");

class AIAuglTalk extends AIBase {

    constructor(augl) {
        super();
        this._augl = augl;
    }

    shouldExecute() {
        if (Math.floor(Math.random() * 100) < 3) {
            return true;
        }
    }

    run() {
        let messages = ["I ❤ Augl", "I need 🍰", "🧬🧠", "😎", "☀️🥰", "🎵🎶🎵", "👨‍💻"];
        this._augl.say(messages[Math.floor(Math.random()*messages.length)]);
    }
}

module.exports = AIAuglTalk;