/**
 * 
 * screeps bot by Augl (Luca Halm).
 * All rights reserved.
 * 
*/

'use strict'

//import Empire from "class.empire";
let Empire = require("class.empire");

global.DEBUG_VISUALS = true;

module.exports.loop = function() {
    //return;    
    var empire = new Empire();
    empire.run();

    /*var perf = Memory.performance;
    if (perf) {
        console.log("Average CPU: " + (perf.d / (perf.n)) + ", ticks: " + (perf.n));
    }*/
}