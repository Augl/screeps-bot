let AIBase = require("./ai.base");
let Constants = require("./util.constants");
let Cache = require("./util.cache");
let Helper = require("./util.helper");

class AIAuglRepairDefense extends AIBase {

    /**
     * Creates a new AIAuglRepairDefense module.
     * @param {import('./class.augl.base.js')} augl The augl to work with.
     * @param {import('./class.world.sector.js')} sector The sector to work in.
     */
    constructor(augl, sector) {
        super();
        this._augl = augl;
        this._sector = sector;
    }

    shouldExecute() {
        // check for new rampart or wall
        this._target = this._sector.getWalls().find(w => w.hits == 1);
        if (!this._target) {
            this._target = this._sector.getRamparts().find(r => r.hits == 1);
        }
        if (this._target) {
            return true;
        }
        // check if the augl already has a repair target
        let targetId = this._augl.recall("repair_target");
        if (targetId) {
            this._target = Cache.getGameObject(targetId);
            return this._target != undefined;
        }
        // find rampart or wall construction site
        this._constructionSite = this._sector.getConstructionSites().find(c => c.structureType == STRUCTURE_WALL || c.structureType == STRUCTURE_RAMPART);
        if (this._constructionSite) {
            return true;
        }
        // repair walls and ramparts
        let buildings = this._sector.getWalls();
        buildings = buildings.concat(this._sector.getRamparts());
        this._target = Helper.getMinWith(buildings, b => b.hits, b => b.hits < b.hitsMax*75/100);
        return this._target != undefined;
    }

    run() {        
        let isDone = true;
        let task = this._augl.recall("task");
        if (task === Constants.Tasks.HARVEST) {
            this._augl.getEnergy(true);
            if (this._augl.isFull()) {
                this._augl.remember("task", Constants.Tasks.REPAIR);
            }
        }
        else if (task === Constants.Tasks.REPAIR) {
            if (this._constructionSite) {
                this._augl.build(this._constructionSite);
            }
            if (this._target) {
                this._augl.repair(this._target);
                this._augl.remember("repair_target", this._target.id);
                if (this._augl.isEmpty() || this._target.hits == this._target.hitsMax) {
                    this._augl.forget("repair_target");
                    this._augl.remember("task", Constants.Tasks.HARVEST);
                }
            }            
            isDone = false;
        }
        else {
            this._augl.remember("task", Constants.Tasks.HARVEST);
        }
        return isDone;
    }
}

module.exports = AIAuglRepairDefense;