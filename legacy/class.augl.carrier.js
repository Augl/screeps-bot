let AuglBase = require("./class.augl.base");
let AIHarvest = require("./ai.augl.harvest");
let AIBuild = require("./ai.augl.build");
let AICarry = require("./ai.augl.carry");
let AIRepair = require("./ai.augl.repair");
let AIUpgrade = require("./ai.augl.upgrade");
let AIDistribute = require("./ai.augl.distribute");
let AIFlee = require("./ai.augl.flee");

class Builder extends AuglBase {

    constructor(creep, sector) {
        super(creep, sector, 30);
        this._augl = creep;
        this.addAI(new AIFlee(this, sector));
        this.addAI(new AICarry(this, sector));
        this.addAI(new AIDistribute(this, sector));
        this.addAI(new AIHarvest(this, sector));
        this.addAI(new AIRepair(this, sector));
        this.addAI(new AIBuild(this, sector));
        this.addAI(new AIUpgrade(this, sector));
    }
}

module.exports = Builder;